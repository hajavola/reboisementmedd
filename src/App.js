import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import './App.css';
// import { Button } from '@material-ui/core';
import Routes from "./components/Route/Routes";


function App() {
  return (
    <div className="App">
      <BrowserRouter >
         <Routes/> 
      </BrowserRouter>
      {/* <h1>Hello, Bienvenue dans le site de reboisement</h1> */}
      {/* <h2>La protection de l'environnement d&eacute;pend de nous tous.</h2> */}
      {/* <Button>Commencer</Button> */}
      {/* <a className="App-link" href="https://reactjs.org" targer="_blank" rel="noopener noreferrer">Learn React</a> */}
    </div>
  );
}

export default App;
