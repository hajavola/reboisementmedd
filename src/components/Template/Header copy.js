import React, { Component } from 'react';
import "./Header.css";
// import Dropdown from 'react-bootstrap/Dropdown';
import Logout from './../Authentification/Logout';

export default class Header extends Component {
    constructor(props){
        super(props);  
        //const userAdmin=JSON.parse(localStorage.getItem("userAdmin"));
        this.state = {  
           nom:'',
           prenoms:'',
           identifiant:''
         
        }  
        // this.logout=this.logout.bind(this);
       
    }
    componentDidMount(){
        if(localStorage.getItem("user")!=null){
            const user=JSON.parse(localStorage.getItem("user"));
            this.setState({identifiant:user.identifiant});
        }
    }
    // logout(){     
    //     localStorage.clear();
    //     window.location="/";       
    // }

    render() {
        return (
            <nav className="navbar navbar-default navbar-static-top m-b-0">
            <div className="navbar-header">
                <div className="top-left-part logo-bar">
                    {/* Logo */}
                    <a className="logo" >
                        {/* <!-- Logo icon image, you can use font-icon also --> */}
                        <b>
                            {/* <!--This is dark logo icon--> */}
                            <img src="assets/images/logos/MEDDLogo.jpg" alt="home" className="dark-logo logo-img" />
                            {/* <!--This is light logo icon--> */}
                            <img src="assets/images/logos/MEDDLogo.jpg" alt="home" className="light-logo logo-img" />
                        </b>
                        {/* <!-- Logo text image you can use text also --> */}
                        <span className="hidden-xs">
                            {/* <!--This is dark logo text--> */}
                            <img src="assets/images/logos/logoTitre.png" alt="home" className="dark-logo title-img" />
                            {/* <!--This is light logo text--> */}
                            <img src="assets/images/logos/logoTitre.png" alt="home" className="light-logo title-img" />
                        </span> 
                    </a>
                </div>
                {/* <!-- /Logo --> */}
                <ul className="nav navbar-top-links navbar-right pull-right">
                    <li>
                        {/* <a className="nav-toggler open-close waves-effect waves-light hidden-md hidden-lg" href="javascript:void(0)"><i className="fa fa-bars"></i></a> */}
                        <a className="nav-toggler open-close waves-effect waves-light " href="javascript:void(0)"><i className="fa fa-bars"></i></a>
                    </li>
                    {/* <li>
                        <form role="search" className="app-search hidden-sm hidden-xs m-r-10">
                            <input type="text" placeholder="Search..." className="form-control"/> 
                            <a href="">
                                <i className="fa fa-search"></i>
                            </a> 
                        </form>
                    </li> */}
                     <li><h2 className="grand-title hidden-xs">Minist&egrave;re de l'Environnement et du D&eacute;veloppement Durable</h2></li>
                    <li>
                    <Logout/>
                    </li>
                    {/* <li>
                    <Dropdown>
                    <Dropdown.Toggle variant="success" style={{backgroundColor:'#2f323e',border: '2px solid #2f323e '}} id="dropdown-basic">
                    <a className="profile-pic" href="#"> <img src="assets/plugins/images/users/varun.jpg" alt="user-img" width="36" className="img-circle"/><b className="hidden-xs">{this.state.identifiant}</b></a> 
                    </Dropdown.Toggle>
                    <Dropdown.Menu style={{borderRadius:'5px'}}>
                        <ul>

                            <li>
                                <Dropdown.Divider />
                                <Dropdown.Item  onClick={this.logout}>
                                    <div className="dropdown-item" onClick={this.logout}>
                                    <i className="fa fa-power-off m-r-5 m-l-5"></i> Se d&eacute;connecter</div>
                                </Dropdown.Item>
                                <Dropdown.Divider />
                            </li>
                            
                        </ul> 
                    </Dropdown.Menu>
                    </Dropdown>
                    </li> */}
                   
                </ul>
            </div>
            {/* <!-- /.navbar-header --> */}
            {/* <!-- /.navbar-top-links --> */}
            {/* <!-- /.navbar-static-side --> */}
        </nav>
        )
    }
}
