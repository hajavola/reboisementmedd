import React, { Component } from 'react';
import Signature from "./Signature";

export default class Footer extends Component {
    render() {
        return (
            <footer className="footer text-center"> <Signature/></footer>
        )
    }
}
