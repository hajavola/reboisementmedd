import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import "./LeftSideBar.css";
import Grid from '@material-ui/core/Grid';
import * as Config from './../Config/Config';

export default class LeftSideBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loggedUser:{},
            beginningURL: Config.beginningURL
        };
        this.showUtilisateurMenu=this.showUtilisateurMenu.bind(this);
    }

    componentDidMount(){
        if(localStorage.getItem("user")!=null){
            this.setState({
              loggedUser:JSON.parse(localStorage.getItem("user"))
            });
        }
    }

    showUtilisateurMenu(){
        if(parseInt(this.state.loggedUser.estAdmin)===1){
            return(
                <li>
                        <Link to='/utilisateurlist' className="waves-effect"><i className="fa fa-users fa-fw"
                                aria-hidden="true"></i>Gestion des utilisateurs </Link>
                        
                    </li>
            );
        }
        else{
            return
        }

    }

    render() {
        return (
            <div className="navbar-default sidebar sidebarproperty" role="navigation">
            <div className="sidebar-nav slimscrollsidebar">
                <div className="sidebar-head">
                    <h3><span className="fa-fw open-close"><i className="ti-close ti-menu"></i></span> <span
                            className="hide-menu">Navigation</span></h3>
                </div>
                <ul className="nav " id="side-menu">
                    <li className="liDashboard ">
                        <Link to='/dashboard' className="waves-effect"><i className="fa  fa-th-large fa-fw"
                                aria-hidden="true"></i>Tableau de bord</Link>
                    </li>
                    <li>
                        <a href="index.html" className="waves-effect">
                            <Grid container spacing={2}>
                                <Grid item xs={11} ><i className="fa  fa-home fa-fw" aria-hidden="true"></i>P&eacute;pini&egrave;res</Grid>
                                <Grid item xs={1}><i  className="fa fa-angle-down fa-fw"></i></Grid>
                            </Grid>
                        </a>
                         <ul className="nav side-menu-child" id="side-menu">
                                <li className="side-menu-child-li"><Link to='/pepinierelist'><i  className="fa fa-list fa-fw"></i><span className="hide-menu"> Liste des p&eacute;pini&egrave;res </span></Link></li>
                                <li className="side-menu-child-li"><Link to='/entreestock'><i  className="fa fa-shopping-basket fa-fw"></i><span className="hide-menu"> Gestion de stock </span></Link></li>
                                {/* <li className="side-menu-child-li"><Link to='/commandereboisement'><i  className="fa fa-list-alt fa-fw"></i><span className="hide-menu"> Commande de plants</span></Link></li> */}
                        </ul>
                    </li>
                    <li>
                        <a href="index.html" className="waves-effect">
                            <Grid container spacing={2}>
                                <Grid item xs={11} ><i className="fa  fa-tree fa-fw" aria-hidden="true"></i>Reboisement</Grid>
                                <Grid item xs={1}><i  className="fa fa-angle-down fa-fw"></i></Grid>
                            </Grid>
                        </a>
                        <ul className="nav side-menu-child" id="side-menu">
                                <li className="side-menu-child-li"><Link to='/newreboisement'><i  className="fa fa-plus-square fa-fw"></i><span className="hide-menu"> Nouveau </span></Link></li>
                                <li className="side-menu-child-li"><Link to='/reboisementhistoric'><i  className="fa fa-list fa-fw"></i><span className="hide-menu"> Historiques </span></Link></li>
                                <li className="side-menu-child-li"><Link to='/reboisementtovalidate'><i  className="fa  fa-check-square-o fa-fw"></i><span className="hide-menu"> Validation</span></Link></li>
                                <li className="side-menu-child-li"><Link to='/campagnelist'><i  className="fa  fa-calendar fa-fw"></i><span className="hide-menu"> Campagnes</span></Link></li>
                        
                                
                        </ul>
                    </li>
                    <li>
                        <Link to='/reboisementmap' className="waves-effect"><i className="fa fa-globe fa-fw"
                                aria-hidden="true"></i>Carte</Link>
                    </li>
                    <li>
                        <a href="index.html" className="waves-effect">
                            <Grid container spacing={2}>
                                <Grid item xs={11} ><i className="fa  fa-user fa-fw" aria-hidden="true"></i>Gestion des clients</Grid>
                                <Grid item xs={1}><i  className="fa fa-angle-down fa-fw"></i></Grid>
                            </Grid>
                        </a>
                        <ul className="nav side-menu-child" id="side-menu">
                                <li className="side-menu-child-li"><Link to='/newclient'><i  className="fa fa-plus-square fa-fw"></i><span className="hide-menu"> Nouveau </span></Link></li>
                                <li className="side-menu-child-li"><Link to='/clientlist'><i  className="fa fa-list fa-fw"></i><span className="hide-menu"> Liste des clients </span></Link></li>
                        </ul>
                    </li>
                    <li>
                        <a href="index.html" className="waves-effect">
                            <Grid container spacing={2}>
                                <Grid item xs={11} ><i className="fa  fa-bar-chart-o fa-fw" aria-hidden="true"></i>Statistiques</Grid>
                                <Grid item xs={1}><i  className="fa fa-angle-down fa-fw"></i></Grid>
                            </Grid>
                        </a>
                        <ul className="nav side-menu-child" id="side-menu">
                                <li className="side-menu-child-li"><Link to='/statistictable'><i  className="fa  fa-table fa-fw"></i><span className="hide-menu"> Tableaux de statistiques </span></Link></li>
                                <li className="side-menu-child-li"><Link to='/statisticchart'><i  className="fa  fa-bar-chart-o fa-fw"></i><span className="hide-menu"> Graphes</span></Link></li>
                        </ul>
                    </li>
                   {this.showUtilisateurMenu()}
                </ul>
              
            </div>
        </div>
        )
    }
}
