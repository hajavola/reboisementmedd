import React, { Component } from 'react'
import {  Switch, Route,Redirect } from 'react-router-dom'

import Header from "./Header";
import LeftSideBar from "./LeftSideBar";
import Footer from "./Footer";
import Dashboard from "./../Dashboard/Dashboard";
import PepiniereList from '../Pepiniere/PepiniereList';
import EmployeeCreate from '../Employee/EmployeeCreate';
import EmployeeEdit from '../Employee/EmployeeEdit';
import EmployeeList from '../Employee/EmployeeList';
import UtilisateurCreate from '../Utilisateur/UtilisateurCreate';
import UtilisateurEdit from '../Utilisateur/UtilisateurEdit';
import UtilisateurList from '../Utilisateur/UtilisateurList';
import NewPepiniere from '../Pepiniere/NewPepiniere';
import PepiniereDetails from '../Pepiniere/PepiniereDetails';
import CommandeReboisement from '../Pepiniere/CommandeReboisement';
import CommandeDetails from '../Pepiniere/CommandeDetails';
import PepiniereEdit from '../Pepiniere/PepiniereEdit';
import NewReboisement from '../Reboisement/NewReboisement';
import ReboisementHistoric from '../Reboisement/ReboisementHistoric';
import ReboisementDetails from '../Reboisement/ReboisementDetails';
import ReboisementValidation from '../Reboisement/ReboisementValidation';
import ReboisementToValidate from '../Reboisement/ReboisementToValidate';
import ReboisementEdit from '../Reboisement/ReboisementEdit';
import CampagneList from '../Campagne/CampagneList';
import NewCampagne from '../Campagne/NewCampagne';
import CampagneEdit from '../Campagne/CampagneEdit';
import ClientList from '../Client/ClientList';
import NewClient from '../Client/NewClient';
import ClientDetails from '../Client/ClientDetails';
import ReboisementHistoricClient from '../Client/ReboisementHistoricClient';
import ClientEdit from '../Client/ClientEdit';
import EntreeStock from '../Stock/EntreeStock';
import EntreeHistoric from '../Stock/EntreeHistoric';
import EtatDeStock from '../Stock/EtatDeStock';
import SortieHistoric from '../Stock/SortieHistoric';
import StatisticChart from '../Statistics/StatisticChart';
import StatisticTable from '../Statistics/StatisticTable';
import ReboisementMap from '../Map/ReboisementMap';
import AddToCampagne from '../Pepiniere/AddToCampagne';




export default class Template extends Component {
    constructor(props){
        super(props);
        
        const isLoggedIn= localStorage.getItem("loggedIn");

        let loggedIn = true;
        if(isLoggedIn==null){
            loggedIn=false ;
        }
        this.state={
            loggedIn
        }
    }
  
    render() {
        //const {match} = this.props;
        if(this.state.loggedIn === false){
            
            return  <Redirect to="/"/>
        }
        return (
        <div id="wrapper">
            {/* <!-- ============================================================== --> */}
            {/* <!-- Topbar header - style you can find in pages.scss --> */}
            {/* <!-- ============================================================== --> */}
            <Header/>  
            {/* <!-- End Top Navigation --> */}
            {/* <!-- ============================================================== --> */}
            {/* <!-- Left Sidebar - style you can find in sidebar.scss  --> */}
            {/* <!-- ============================================================== --> */}
            <LeftSideBar/> 
            {/* <!-- ============================================================== --> */}
            {/* <!-- End Left Sidebar --> */}
            {/* <!-- ============================================================== --> */}
            {/* <!-- ============================================================== --> */}
            {/* <!-- Page Content --> */}
            {/* <!-- ============================================================== --> */}
            <div id="page-wrapper">
                {/* Content of the page */}
                <Switch>
                    <Route path='/' component={Dashboard} exact/>
                    <Route path='/dashboard' component={Dashboard} exact/>
                    {/* Pépiniere */}
                    <Route path='/pepinierelist' component={PepiniereList} exact/>
                    <Route path="/pepinieredetails/:id" component={PepiniereDetails} exact/>
                    <Route path="/pepiniereedit/:id" component={PepiniereEdit} exact/>
                    <Route path='/newpepiniere' component={NewPepiniere} exact/>
                    <Route path='/addtocampagne/:id' component={AddToCampagne} exact/>
                    <Route path='/commandereboisement' component={CommandeReboisement} exact/>
                    <Route path="/commandedetails/:id" component={CommandeDetails} exact/>
                    {/* Gestion de stock */}
                    <Route path='/entreestock' component={EntreeStock} exact/>
                    <Route path='/entreehistoric' component={EntreeHistoric} exact/>
                    <Route path='/etatdestock' component={EtatDeStock} exact/>
                    <Route path='/sortiehistoric' component={SortieHistoric} exact/>
                    {/* Reboisement */}
                    <Route path='/newreboisement' component={NewReboisement} exact/>
                    <Route path='/reboisementhistoric' component={ReboisementHistoric} exact/>
                    <Route path='/reboisementtovalidate' component={ReboisementToValidate} exact/>
                    <Route path="/reboisementdetails/:id" component={ReboisementDetails} exact/>
                    <Route path="/reboisementvalidation/:id" component={ReboisementValidation} exact/>
                    <Route path="/reboisementedit/:id" component={ReboisementEdit} exact/>
                    {/* Campagne */}
                    <Route path="/campagnelist" component={CampagneList} exact/>
                    <Route path="/newcampagne" component={NewCampagne} exact/>
                    <Route path="/campagneedit/:id" component={CampagneEdit} exact/>
                    {/* Map */}
                    <Route path="/reboisementmap" component={ReboisementMap} exact/>
                    {/* Statistics */}
                    <Route path='/statistictable' component={StatisticTable} exact/>
                    <Route path='/statisticchart' component={StatisticChart} exact/>
                    {/* Client */}
                    <Route path="/clientlist" component={ClientList} exact/>
                    <Route path="/newClient" component={NewClient} exact/>
                    <Route path="/clientedit/:id" component={ClientEdit} exact/>
                    <Route path="/clientdetails/:id" component={ClientDetails} exact/>
                    <Route path="/reboisementhistoricclient/:id" component={ReboisementHistoricClient} exact/>
                    {/* Employee */}
                    <Route path="/employeelist" component={EmployeeList} exact/>
                    <Route path="/employeeedit/:id" component={EmployeeEdit} exact/>
                    <Route path="/employeecreate" component={EmployeeCreate} exact/> 
                    {/* Utilisateur */}
                    <Route path="/utilisateurlist" component={UtilisateurList} exact/>
                    <Route path="/utilisateuredit/:id" component={UtilisateurEdit} exact/>
                    <Route path="/utilisateurcreate" component={UtilisateurCreate} exact/> 
                    

                </Switch>

                {/* <!-- /.content of the page--> */}
                <Footer/> 
            </div>
            {/* <!-- ============================================================== --> */}
            {/* <!-- End Page Content --> */}
            {/* <!-- ============================================================== --> */}
        </div>

        
        )
    }
}
