import React, { Component } from 'react'


export default class Signature extends Component {
    render() {
        return(
 
            <div >
                © Minist&egrave;re de l'Environnement et du D&eacute;veloppement Durable<br/>
                MEDD - Madagascar - 2020
            </div>

        );
    }
}