import React, { Component } from 'react';
import "./ReboisementDetails.css";
import * as Config from './../Config/Config';
import { Link } from 'react-router-dom';
import axios from 'axios';

//Search
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
//grid for the search
import Grid from '@material-ui/core/Grid';

//Table
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
//Table
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
// import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';





const  EspeceMiseEnTerre= props =>(
    <TableRow hover role="checkbox" tabIndex={-1} key={props.numero}>
        <TableCell style={tableCellStyle}>{props.numero}</TableCell>
        <TableCell style={tableCellStyle}>{props.espece.nomEspece}</TableCell>
        <TableCell style={tableCellStyle}>{props.espece.nombre}</TableCell>
    </TableRow>
)

//Table
const useStyles = makeStyles({
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 440,
    },

  });



// form 
const useStylesForm = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));

// end form


//design
let appBarStyle= {borderRadius:'4px',textAlign:'right',backgroundColor:'#000e2f'};
let buttonBackStyle= {backgroundColor:'#0bca4b', color:'white', marginTop:'5px'};
let fontStyle={fontSize:'14px'};
//container
let containerStyle={backgroundColor:'white', marginTop:'-20px'}
//table
let tableCellTitleStyle={fontSize:'14px',fontFamily:'Segoe UI'};
let tableRowTitleStyle={backgroundColor:'rgb(245, 245, 245)'};
//let tableCellTitleActionColumnStyle={fontSize:'14px',fontFamily:'Segoe UI', textAlign:'center'};
let tableCellStyle={fontSize:'13px', color:'rgb(90, 88, 88)', fontFamily:'Segoe UI' };
//let tableCellActionColumnStyle={fontSize:'13px', color:'rgb(90, 88, 88)', fontFamily:'Segoe UI', textAlign:'center' };


export default class ReboisementDetails extends Component {
    constructor(props) {
        super(props);
        //data
        /*let lEspeceMiseEnTerre=[
            {id:1,idReboisement:1,idEspece:1, nomEspece:'PIN',nombre:3000},
            {id:2,idReboisement:1,idEspece:2, nomEspece:'FRUIT',nombre:1000},
            {id:3,idReboisement:1,idEspece:3, nomEspece:'GRAND ARBRE',nombre:1000},
        ];*/
      
        this.state = {
            userFrom:'',
            userRegion:{},
            loggedUser:{},
            idReboisement:this.props.match.params.id,
            reboisement:{},
            especesMiseEnTerre:[],
            dateDebut:'',
            dateFin:'',
           
            page:0,
            rowsPerPage:10,
         
            beginningURL: Config.beginningURL
        };
        
        this.especeList=this.especeList.bind(this);
        this.showButtonValider=this.showButtonValider.bind(this);

    }
    componentDidMount(){
          //Init title for the page
          if(localStorage.getItem("userRegion")!=null){
            this.setState({
              userRegion:JSON.parse(localStorage.getItem("userRegion"))
            });
        }
        if(localStorage.getItem("title")!=null){
            this.setState({
              userFrom:localStorage.getItem("title")
            });
        }
        if(localStorage.getItem("user")!=null){
            this.setState({
              loggedUser:JSON.parse(localStorage.getItem("user"))
            });
        }

       
        axios.get(this.state.beginningURL+'reboisement/single_read.php?id='+this.props.match.params.id)
        .then(response=>{
            this.setState({
                reboisement:response.data,
                dateDebut:response.data.dateDebut.substring(0,10),
                dateFin:response.data.dateFin.substring(0,10)

               
            })
        })
        .catch(function(error){
            console.log(error);
        });
        axios.get(this.state.beginningURL+'especemiseenterre/readbyidreboisement.php?id='+this.props.match.params.id)
        .then(response=>{
            this.setState({
                especesMiseEnTerre:response.data.body
               
            })
        })
        .catch(function(error){
            console.log(error);
        });
       
        
       
    }
    showButtonValider(){
        if(parseInt(this.state.reboisement.estAcheve)===1){
            return;
        }
        else{
            return(
                <Link to={"/reboisementvalidation/"+this.state.idReboisement}>
                    <Button variant="contained" fullWidth color=""  style={fontStyle}>
                        <img style={{width:'25px'}}  alt="Valider" src="/assets/images/logos/logovalidation.png"/>&nbsp;Valider 
                    </Button>  
                </Link>   
                
            );
        }
    }
    especeList(){
        let i=0;
      
        return this.state.especesMiseEnTerre.slice(this.state.page * this.state.rowsPerPage, this.state.page * this.state.rowsPerPage + this.state.rowsPerPage).map((currentEspece) => {
            i++;
            return <EspeceMiseEnTerre numero={i}  espece={currentEspece}  key={i}/>;
        });
     
    }

      
    
      


    render() {
        //Table
        const classes = useStyles;
        
        const handleChangePage = (event, newPage) => {
            this.setState({page:newPage})
            //setPage(newPage);
        };
        const handleChangeRowsPerPage = (event) => {
            //setRowsPerPage(+event.target.value);
            //setPage(0);
            this.setState({rowsPerPage:+event.target.value});
            this.setState({page:0});
        };
        //end table

        //form 
        const classesForm = useStylesForm;

        

        return (
            <div className="container-fluid">
                <div className="row bg-title">
                    <div className="left-title col-lg-4 col-md-4 col-sm-4 col-xs-12" >
                        <h4 className="text-left-title page-title">REBOISEMENTS</h4>
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <h3 className="text-middle-title page-title">{this.state.userFrom}</h3>
                    </div>

                    <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                        {/* <a href="https://wrappixel.com/templates/ampleadmin/" target="_blank"
                            className="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Upgrade
                            to Pro

                        </a> */}
                        <ol className="breadcrumb">
                            
                            <li className="active"><a>Reboisements</a>&nbsp;&nbsp;<i className="linking fa fa-angle-right fa-fw"></i>D&eacute;tails</li>
                        </ol>
                    </div>
                    {/* <!-- /.col-lg-12 --> */}
                </div>
            
                {/* <!-- content of the page--> */}
                {/* Head */}
                <div className="divAppBar" >
                    <AppBar position="static" style={appBarStyle}>
                        <Toolbar>
                            {/* grid */}
                            
                                <Grid container spacing={3}>
                                    <Grid item xs={3} style={{textAlign:'left'}}>
                                    <Link to='/reboisementhistoric'>
                                        <Button variant="contained" style={buttonBackStyle}>
                                            Liste Des Reboisements
                                        </Button>
                                    </Link>
                                    </Grid>
                                    <Grid item xs={6} style={{textAlign:'center'}}>
                                        <h4 className="smaller-title" >Reboisement N°{this.state.idReboisement}</h4>
                                    </Grid>
                                    <Grid item xs={3}>                                    
                                    </Grid>
                                </Grid>
                            
                            {/* end grid */}
                        
                        </Toolbar>
                    </AppBar>
                </div>
                {/* end head */}
                {/* Table */}
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box formStyle1" >
                            <Container component="main" maxWidth="md" style={containerStyle}>
                                <div className={classesForm.paper} >                      
                                    <div className="contentReboisementDetails" >
                                        <div className=" row">
                                            <h4 className="labelContent">Campagne: </h4>
                                        </div>  
                                        <div className=" row">
                                                {this.state.reboisement.nomCampagne}<br/>
                                        </div> 
                                        <div className=" row">
                                            <h4 className="labelContent">Institution: </h4>
                                        </div>  
                                        <div className=" row">
                                                {this.state.reboisement.nomInstitution}<br/>
                                        </div> 
                                        <div className=" row">
                                            <h4 className="labelContent">DREDD/CIREDD: </h4>
                                        </div>  
                                        <div className=" row">
                                                {this.state.reboisement.typeInstitution}<br/>
                                        </div> 
                                        <div className=" row">
                                            <h4 className="labelContent">Situation juridique: </h4>
                                        </div>  
                                        <div className=" row">
                                                {this.state.reboisement.nomSituation}<br/>
                                        </div> 
                                        <div className=" row">
                                            <h4 className="labelContent">Coordonn&eacute;es: </h4>
                                        </div>  
                                        <div className=" row">
                                                (x:{this.state.reboisement.coordonneeX} ;&nbsp;y:{this.state.reboisement.coordonneeY}) <br/>
                                        </div> 
                                        <div className=" row">
                                            <h4 className="labelContent">Responsable: </h4>
                                        </div>  
                                        <div className=" row">
                                                {this.state.reboisement.nomClient}<br/>
                                        </div> 
                                        <div className=" row">
                                            <h4 className="labelContent">Objectif: </h4>
                                        </div>  
                                        <div className=" row">
                                                {this.state.reboisement.objectif}<br/>
                                        </div> 
                                        <div className=" row">
                                            <h4 className="labelContent">Approche: </h4>
                                        </div>  
                                        <div className=" row">
                                                {this.state.reboisement.approche}<br/>
                                        </div> 
                                        <div className=" row">
                                            <h4 className="labelContent">Surface pr&eacute;vue (Ha): </h4>
                                        </div>  
                                        <div className=" row">
                                                {this.state.reboisement.surfacePrevue}<br/>
                                        </div> 
                                        <div className=" row">
                                            <h4 className="labelContent">Shapefile avant reboisement: </h4>
                                        </div>  
                                        <div className=" row">
                                                {this.state.reboisement.shapeFilePlan}<br/>
                                        </div> 
                                        <div className=" row">
                                            <h4 className="labelContent">Date de d&eacute;but: </h4>
                                        </div>  
                                        <div className=" row">
                                                {this.state.dateDebut}<br/>
                                        </div> 
                                        <div className=" row">
                                            <h4 className="labelContent">Date de fin: </h4>
                                        </div>  
                                        <div className=" row">
                                                {this.state.dateFin}<br/>
                                        </div> 
                                        <div className=" row">
                                            <h4 className="labelContent">P&eacute;pini&egrave;re: </h4>
                                        </div>  
                                        <div className=" row">
                                                {this.state.reboisement.nomPepiniere}<br/>
                                        </div> 
                                        <div className=" row">
                                            <h4 className="labelContent">Superficie (Ha): </h4>
                                        </div>  
                                        <div className=" row">
                                                {this.state.reboisement.superficie}<br/>
                                        </div> 
                                        <div className=" row">
                                            <h4 className="labelContent">Shapefile: </h4>
                                        </div>  
                                        <div className=" row">
                                                {this.state.reboisement.shapeFile}<br/>
                                        </div> 
                                        <div className=" row">
                                            <h4 className="labelContent">R&eacute;gion: </h4>
                                        </div>  
                                        <div className=" row">
                                                {this.state.reboisement.nomRegion}<br/>
                                        </div> 
                                        <div className=" row">
                                            <h4 className="labelContent">District: </h4>
                                        </div>  
                                        <div className=" row">
                                                {this.state.reboisement.nomDistrict}<br/>
                                        </div> 
                                        <div className=" row">
                                            <h4 className="labelContent">Commune: </h4>
                                        </div>  
                                        <div className=" row">
                                                {this.state.reboisement.nomCommune}<br/>
                                        </div> 
                                        <div className=" row">
                                            <h4 className="labelContent">Fokontany: </h4>
                                        </div>  
                                        <div className=" row">
                                                {this.state.reboisement.nomFokontany}<br/>
                                        </div> 
                                        <div className=" row">
                                            <h4 className="labelContent">Site: </h4>
                                        </div>  
                                        <div className=" row">
                                                {this.state.reboisement.nomSite}<br/>
                                        </div> 
                                        <div className=" row">
                                            <h4 className="labelContent">Nombre de plants: </h4>
                                        </div>  
                                        <div className=" row">
                                                {this.state.reboisement.nbPlants}<br/>
                                        </div> 
                                        {/* ------------------------ */}
                                        

                                        <Grid container spacing={1} style={{marginTop:'40px'}} >
                                            <Grid item xs={4} > </Grid>
                                            <Grid item xs={4}> 
                                               {this.showButtonValider()}        
                                            </Grid>
                                            <Grid item xs={4}>
                                            <Link to={"/reboisementedit/"+this.state.idReboisement}>
                                                <Button
                                                    type="button"
                                                    fullWidth
                                                    variant="contained"
                                                    color="primary"
                                                    className={classesForm.submit}
                                                    style={fontStyle}
                                                >
                                                Modifier
                                                </Button>
                                            </Link>
                                            </Grid>
                                        </Grid> 
                                    </div>
                                    <br/>
                                    <div className="" >
                                        <div className=" ">
                                                <h3 className="labelDetailsPlants">D&eacute;tails plants: </h3>
                                        </div>
                                        <Paper className={classes.root}>
                                
                                            <TableContainer className={classes.container}>
                                                <Table  stickyHeader aria-label="sticky table">
                                                <TableBody>
                                                    <TableRow style={tableRowTitleStyle}>
                                                        <TableCell style={tableCellTitleStyle}>Num&eacute;ro</TableCell>
                                                        <TableCell style={tableCellTitleStyle}>Esp&egrave;ce</TableCell>
                                                        <TableCell style={tableCellTitleStyle}>Nombre</TableCell>
                                                       
                                                    </TableRow>
                                                    
                                                </TableBody>
                                                <TableBody>
                                                    {this.especeList()}       
                                                </TableBody>
                                               

                                                </Table>
                                            </TableContainer>
                                            <TablePagination
                                                style={tableCellStyle}
                                                rowsPerPageOptions={[10, 25, 100]}
                                                component="div"
                                                count={this.state.especesMiseEnTerre.length}
                                                rowsPerPage={this.state.rowsPerPage}
                                                page={this.state.page}
                                                onChangePage={handleChangePage}
                                                onChangeRowsPerPage={handleChangeRowsPerPage}
                                            />
                                        </Paper>  

                                    </div>
                                    
                                </div>
                            </Container>
                        </div>
                    </div>
                </div>
                {/* end table */}
              
                {/* <!-- /.content of the page --> */}

            </div>
        )
    }
}
