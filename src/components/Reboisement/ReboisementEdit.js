import React, { Component } from 'react';
import "./ReboisementEdit.css";
import * as Config from './../Config/Config';
import * as Design from './../Config/Design';
import { Link } from 'react-router-dom';
import axios from 'axios';
import Typography from '@material-ui/core/Typography';

//Search
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
//grid for the search
import Grid from '@material-ui/core/Grid';

//Table
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';


//select
import Select from 'react-select';

import { MDBInput } from 'mdbreact';


//autocomplete
import TextField from '@material-ui/core/TextField';

//Dialog
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';





const Plant = props =>(
    <TableRow hover role="checkbox" tabIndex={-1} key={props.plant.id}>
        <TableCell style={Design.tableCellStyle}>{props.plant.nomEspece}</TableCell>
        <TableCell style={Design.tableCellStyle}>{props.plant.nombre}</TableCell>
        <TableCell style={Design.tableCellActionColumnStyle}><img className="imageSupprimer" alt="supprimer" src="assets/images/logos/LogoDelete.png" onClick={()=> {props.deletePlant(props.plant.id)}}/> </TableCell>
    </TableRow>
)
  



//Table
const useStyles = makeStyles({
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 600,
    },

  });



// form 
const useStylesForm = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));

// end form


//design
let appBarStyle= {borderRadius:'4px',textAlign:'right',backgroundColor:'#000e2f'};
let buttonBackStyle= {backgroundColor:'#0bca4b', color:'white', marginTop:'5px'};
let fontStyle={fontSize:'14px'};
let styleContainer={backgroundColor:'white',borderRadius:'3px'};

export default class NewReboisement extends Component {
    constructor(props) {
        super(props);
 
         //Data
        /*let lCampagnes=[
            {id:1,nomCampagne:'Campagne 2020'},
            {id:2,nomCampagne:'Campagne 2021'},
            {id:3,nomCampagne:'Campagne 2022'},
        ];
        let lInstutions=[
            {id:1,nomInstitution:'MEDD'},
            {id:2,nomInstitution:'MNP'},
            {id:3,nomInstitution:'FANAMBY'},
        ];
        let lTypeInstitutions=[
            {id:1,typeInstitution:'DREDD'},
            {id:2,typeInstitution:'CIREDD'},
            {id:3,typeInstitution:'CEF'}
        ];
        let lSituationJuridique=[
            {id:1,nomSituation:'NAP'},
            {id:2,nomSituation:'Prive'},
            {id:3,nomSituation:'Domanial'},
            {id:4,nomSituation:'terrain gendarme'},
        ];
        let lClients=[
            {id:1,nom:'MBG ORONJIA',Description:'MBG ORONJIA'},
            {id:2,nom:'SAGE',Description:'SAGE'},
            {id:3,nom:'JIRAMA',Description:'JIRAMA'}, 
        ];
        let lObjectifs=[
            {id:1,objectif:'Restauration'},
            {id:2,objectif:'Reboisement'},
            {id:3,objectif:'Agroforesterie'},
            {id:4,objectif:'Protection de BV Embroussaillement Restauration de foret degradée'},
        ];

        let lApproches=[
            {id:1,approche:'Communautaire'},
            {id:2,approche:'Privé'},
            {id:3,approche:'AP'}
        ];
        let lPepinieres=[
            {id:1,nomPepiniere:'Pepiniere 1'},
            {id:2,nomPepiniere:'Pepiniere 2'},
            {id:3,nomPepiniere:'Pepiniere 3'}
        ];
        let lDistricts=[
            {id:1,nomDistrict: 'Ambanja'},
            {id:2,nomDistrict: 'Nosy Be'},
            {id:3,nomDistrict: 'SAVA'}, 
            {id:4,nomDistrict: 'Ambilobe'}
        ];
        let lCommunes=[
            {id:1,idDistrict:1,nomCommune:'Ramena'},
            {id:2,idDistrict:1,nomCommune:'Antanamitarana'},
            {id:3,idDistrict:1,nomCommune:'Mangaoka'},
            {id:4,idDistrict:1,nomCommune:'Sakaramy'},
        ];
        let lFokontany=[
            {id:1,idCommune:1,nomFokontany:'Taga maty'},
            {id:2,idCommune:1,nomFokontany:'Andavakoera'},
            {id:3,idCommune:1,nomFokontany:'Ambodimagnary'},
        ];
        let lSites=[
            {id:1,idFokontany:1,nomSite:'Antaolagnaomby'},
            {id:2,idFokontany:1,nomSite:'MAHATSINJO'},
            {id:3,idFokontany:1,nomSite:'AMBARIOBE'},
            {id:4,idFokontany:1,nomSite:'anciens ZUC/ZOC dans l\'AP Oronjia'},
        ];*/

        this.state = {
            userFrom:'',
            userRegion:{},
            loggedUser:{},
            errorMessage:'',
            idReboisement:this.props.match.params.id,
            reboisement:{},
            especesMiseEnTerre:[],
            openSupprimer:false,
            openClient:false,
            newClientNom:'',
            newClientDescription:'',
            openSite:false,
            newSite:'',
         
            plants:[],
            especePlants:[],
            nbEspecePlant:0,
            campagnes:[],
            idCampagne:0,
            institutions: [],
            idInstitution:0,
            typeInstitutions: [],
            idTypeInstitution:0,
            situationsJuridiques:[],
            idSituationJuridique:0,
            clients:[],
            idClient:0,
            objectifs:[],
            idObjectif:0,
            approches:[],
            idApproche:0,
            surfacePrevue:0,
            shapefilePlan:'',
            nbPlants:0,
            coordonneeX:'',
            coordonneeY:'',
            pepinieres:[],
            idPepiniere:0,
            dateDebut:'',
            dateFin:'',
            districts:[],
            idDistrict:0,
            communes:[],
            idCommune:0,
            fokontany:[],
            idFokontany:0,
            sites:[],
            idSite:0,
            superficie:0,
            shapeFile:'Néant',
            estAcheve:0,
            optionsEspecePlants: [],
            optionsCampagnes: [],
            optionsInstitutions: [],
            optionsTypeInstitutions:[],
            optionsSituationJuridiques:[],
            optionsClients:[],
            optionsObjectifs:[],
            optionsApproches:[],
            optionsDistricts:[],
            optionsPepinieres:[],
            optionsCommunes:[],
            optionsFokontany:[],
            optionsSites:[],
            errorMessageClient:'',
            errorMessageSite:'',  
            beginningURL: Config.beginningURL
        };
      
      
        this.onChangeSurfacePrevue=this.onChangeSurfacePrevue.bind(this);
        this.onChangeShapefilePlan=this.onChangeShapefilePlan.bind(this);
        this.onChangeNbPlants=this.onChangeNbPlants.bind(this);
        this.onChangeDateDebut=this.onChangeDateDebut.bind(this);
        this.onChangeDateFin=this.onChangeDateFin.bind(this);
        this.onChangeNbEspecePlant=this.onChangeNbEspecePlant.bind(this);
        this.onChangeCoordonneeX=this.onChangeCoordonneeX.bind(this);
        this.onChangeCoordonneeY=this.onChangeCoordonneeY.bind(this);
        this.onChangeSuperficie=this.onChangeSuperficie.bind(this);
        this.onChangeShapeFile=this.onChangeShapeFile.bind(this);
       
        this.onSubmit=this.onSubmit.bind(this);
        //Modals
        this.showLocalisationModal=this.showLocalisationModal.bind(this);
        this.plantList=this.plantList.bind(this);
        this.removePlant=this.removePlant.bind(this);
        this.annuler=this.annuler.bind(this);
        this.addToListEspece=this.addToListEspece.bind(this);
        this.showErrorMessage=this.showErrorMessage.bind(this);
        //Dialog
        this.onChangeNewClientNom=this.onChangeNewClientNom.bind(this);
        this.onChangeNewClientDescription=this.onChangeNewClientDescription.bind(this);
        this.onChangeNewSite=this.onChangeNewSite.bind(this);
        
        this.onSubmitClient=this.onSubmitClient.bind(this);
        this.onSubmitSite=this.onSubmitSite.bind(this);
        //delete
        this.showButtonDelete=this.showButtonDelete.bind(this);
        this.deleteReboisement=this.deleteReboisement.bind(this);
       
        

    }
    state = {
        selectedOptionEspecePlant: null,
        selectedOptionCampagne:null,
        selectedOptionInstitution:null,
        selectedOptionTypeInstitution:null,
        selectedOptionSituationJuridique:null,
        selectedOptionClient:null,
        selectedOptionObjectif:null,
        selectedOptionApproche:null,
        selectedOptionPepiniere:null,
        selectedOptionDistrict:null,
        selectedOptionCommune:null,
        selectedOptionFokontany:null,
        selectedOptionSite:null
      };

    componentDidMount(){
        //Init title for the page

        if(localStorage.getItem("userRegion")!=null){
            this.setState({
              userRegion:JSON.parse(localStorage.getItem("userRegion"))
            });
        }
        if(localStorage.getItem("title")!=null){
            this.setState({
              userFrom:localStorage.getItem("title")
            });
        }
        if(localStorage.getItem("user")!=null){
            this.setState({
              loggedUser:JSON.parse(localStorage.getItem("user"))
            });
        }
        

            //--------------------------------------
             //get the current Reboisement
        axios.get(this.state.beginningURL+'reboisement/single_read.php?id='+this.props.match.params.id)
        .then(response=>{
            this.setState({
                reboisement:response.data,

                idRegion:response.data.idRegion,
                plants:response.data.plants,
                idCampagne : response.data.idCampagne,
                idInstitution : response.data.idInstitution,
                idTypeInstitution: response.data.idTypeInstitution,
                idSituationJuridique: response.data.idSituationJuridique,
                idClient:response.data.idClient,
                idObjectif:response.data.idObjectifReboisement,
                idApproche:response.data.idApproche,
                surfacePrevue:response.data.surfacePrevue,
                coordonneeX:response.data.coordonneeX,
                coordonneeY:response.data.coordonneeY,
                shapefilePlan:response.data.shapeFilePlan,
                nbPlants:response.data.nbPlants,
                idPepiniere:response.data.idPepiniere,
                dateDebut:response.data.dateDebut.substring(0,10),
                dateFin:response.data.dateFin.substring(0,10),
                idDistrict:response.data.idDistrict,
                idCommune:response.data.idCommune,
                idFokontany:response.data.idFokontany,
                idSite:response.data.idSite,

                superficie:response.data.superficie,
                shapeFile:response.data.shapeFile,
                estAcheve:response.data.estAcheve
       
               
            });


            //here, we take one by one the correspondant
             //init especePlants
            axios.get(this.state.beginningURL+'especeplants/read.php')
            .then(resEp=>{
                this.setState({
                    especePlants:resEp.data.body
                
                })
                let lEs=[{value:0,label:""}];
                let currentEspeces=resEp.data.body;
                for(let i=0;i<currentEspeces.length;i++){
                    lEs.push({value:currentEspeces[i].id, label:currentEspeces[i].nomEspece});
                }
                this.setState({optionsEspecePlants:lEs});
                

                //-----------------------------
                 //select campagnes 
                axios.get(this.state.beginningURL+'campagne/read.php')
                .then(resCam=>{
                    this.setState({
                        campagnes:resCam.data.body
                    
                    })
                    let lCamp=[{value:0,label:""}];
                    let currentCampagnes=resCam.data.body;
                    for(let i=0;i<currentCampagnes.length;i++){
                        lCamp.push({value:currentCampagnes[i].id, label:currentCampagnes[i].nomCampagne});
                        if(currentCampagnes[i].id===response.data.idCampagne){
                            this.setState({ selectedOptionCampagne:{value:currentCampagnes[i].id, label:currentCampagnes[i].nomCampagne} }); 
                        }
                    }
                    this.setState({optionsCampagnes:lCamp});
                    //-----------------------------
                     //select institutions 
                    axios.get(this.state.beginningURL+'institution/read.php')
                    .then(resInst=>{
                        this.setState({
                            institutions:resInst.data.body
                        
                        })
                        let lInst=[{value:0,label:""}];
                        let currentInstitutions=resInst.data.body;
                        for(let i=0;i<currentInstitutions.length;i++){
                            lInst.push({value:currentInstitutions[i].id, label:currentInstitutions[i].nomInstitution});
                            //init the combobox
                            if(currentInstitutions[i].id===response.data.idInstitution){
                                this.setState({ selectedOptionInstitution:{value:currentInstitutions[i].id, label:currentInstitutions[i].nomInstitution} }); 
                            }
                        }
                        this.setState({optionsInstitutions:lInst});

                        //---------------------------
                        //select type institutions 
                        axios.get(this.state.beginningURL+'typeinstitution/read.php')
                        .then(resTIns=>{
                            this.setState({
                                typeInstitutions:resTIns.data.body
                            
                            })
                            let lTypeInst=[{value:0,label:""}];
                            let currentTypeInstitutions=resTIns.data.body;
                            for(let i=0;i<currentTypeInstitutions.length;i++){
                                lTypeInst.push({value:currentTypeInstitutions[i].id, label:currentTypeInstitutions[i].typeInstitution});
                                //init the combobox
                                if(currentTypeInstitutions[i].id===response.data.idTypeInstitution){
                                    this.setState({ selectedOptionTypeInstitution:{value:currentTypeInstitutions[i].id, label:currentTypeInstitutions[i].typeInstitution} }); 
                                }
                            }
                            this.setState({optionsTypeInstitutions:lTypeInst});
                        
                            //--------------------------------------
                            //select situation juridique 
                            axios.get(this.state.beginningURL+'situationjuridique/read.php')
                            .then(resSJ=>{
                                this.setState({
                                    situationsJuridiques:resSJ.data.body
                                
                                })
                                // let lSit=[{value:0,label:""}];
                                let lSit=[];
                                let currentSituations=resSJ.data.body;
                                for(let i=0;i<currentSituations.length;i++){
                                    lSit.push({value:currentSituations[i].id, label:currentSituations[i].nomSituation});
                                    //init the combobox
                                    if(currentSituations[i].id===response.data.idSituationJuridique){
                                        this.setState({ selectedOptionSituationJuridique:{value:currentSituations[i].id, label:currentSituations[i].nomSituation} }); 
                                    }
                                }
                                this.setState({optionsSituationJuridiques:lSit});
                            
                                //----------------------------------
                                //select Client
                                axios.get(this.state.beginningURL+'client/read.php')
                                .then(resCl=>{
                                    this.setState({
                                        clients:resCl.data.body
                                        
                                    })
                                    let lCli=[{value:0,label:""}];
                                    let currentClients=resCl.data.body;
                                    for(let i=0;i<currentClients.length;i++){
                                        lCli.push({value:currentClients[i].id, label:currentClients[i].nomClient});
                                        //init the combobox
                                        if(currentClients[i].id===response.data.idClient){
                                            this.setState({ selectedOptionClient:{value:currentClients[i].id, label:currentClients[i].nomClient} }); 
                                        }
                                    }
                                    this.setState({optionsClients:lCli});

                                    //-------------------------------
                                    //select Objectifs
                                    axios.get(this.state.beginningURL+'objectifreboisement/read.php')
                                    .then(resObj=>{
                                        this.setState({
                                            objectifs:resObj.data.body
                                        
                                        })
                                        let lObj=[{value:0,label:""}];
                                        let currentObjectifs=resObj.data.body;
                                        for(let i=0;i<currentObjectifs.length;i++){
                                            lObj.push({value:currentObjectifs[i].id, label:currentObjectifs[i].objectif});
                                            //init the combobox
                                            if(currentObjectifs[i].id===response.data.idObjectifReboisement){
                                                this.setState({ selectedOptionObjectif:{value:currentObjectifs[i].id, label:currentObjectifs[i].objectif} }); 
                                            }
                                        }
                                        this.setState({optionsObjectifs:lObj});
                                        
                                        //--------------------
                                        //select approches
                                        axios.get(this.state.beginningURL+'approche/read.php')
                                        .then(resApp=>{
                                            this.setState({
                                                approches:resApp.data.body
                                            
                                            })
                                            let lAppr=[{value:0,label:""}];
                                            let currentApproches=resApp.data.body;
                                            for(let i=0;i<currentApproches.length;i++){
                                                lAppr.push({value:currentApproches[i].id, label:currentApproches[i].approche});
                                                //init the combobox
                                                if(currentApproches[i].id===response.data.idApproche){
                                                    this.setState({ selectedOptionApproche:{value:currentApproches[i].id, label:currentApproches[i].approche} }); 
                                                }
                                            }
                                            this.setState({optionsApproches:lAppr});

                                            //---------------------------
                                            //pepiniere
                                            const dataPepiniere ={
                                                wordToSearch:"",
                                                idRegion:response.data.idRegion,
                                                idCampagne:response.data.idCampagne
                                    
                                            };
                                            // axios.post(this.state.beginningURL+'pepiniere/readperregionandcampagneid.php', dataPepiniere)
                                            axios.get(this.state.beginningURL+'pepiniere/readperregionandcampagneidget.php?wordToSearch='+dataPepiniere.wordToSearch+'&idRegion='+dataPepiniere.idRegion+'&idCampagne='+dataPepiniere.idCampagne)
                                            .then(res =>{
                                                    this.setState({
                                                        pepinieres:res.data.body

                                                    });
                                                let lPep=[{value:0,label:""}];
                                                let currentPepinieres=res.data.body;
                                                for(let i=0;i<currentPepinieres.length;i++){
                                                    lPep.push({value:currentPepinieres[i].id, label:currentPepinieres[i].nomPepiniere});
                                                    //init the combobox
                                                    if(currentPepinieres[i].id===response.data.idPepiniere){
                                                        this.setState({ selectedOptionPepiniere:{value:currentPepinieres[i].id, label:currentPepinieres[i].nomPepiniere} }); 
                                                    }
                                                }
                                                this.setState({optionsPepinieres:lPep});
                                            })
                                            .catch(err=>{
                                                console.log(err);
                                            })
                                        
                                        
                                        })
                                        .catch(function(error){
                                            console.log(error);
                                        });

                                    })
                                    .catch(function(err){
                                        console.log(err);
                                    });

                                })
                                .catch(function(err){
                                    console.log(err);
                                });
                            })
                            .catch(function(err){
                                console.log(err);
                            });

                        })
                        .catch(function(err){
                            console.log(err);
                        });
                    })
                    .catch(function(err){
                        console.log(err);
                    });

                })
                .catch(function(err){
                    console.log(err);
                });

            })
            .catch(function(err){
                console.log(err);
            });

  
            
            
            

             //select Districts
            
            axios.get(this.state.beginningURL+'district/readbyidregion.php/?id='+response.data.idRegion)
            .then(res=>{
                this.setState({
                    districts:res.data.body
                });
                // let lDis=[{value:0,label:""}];
                let lDis=[];
                let currentDistricts=res.data.body;
                for(let i=0;i<currentDistricts.length;i++){
                    lDis.push({value:currentDistricts[i].id, label:currentDistricts[i].nomDistrict});
                    //init the combobox
                    if(currentDistricts[i].id===response.data.idDistrict){
                        this.setState({ selectedOptionDistrict:{value:currentDistricts[i].id, label:currentDistricts[i].nomDistrict} }); 
                    }
                }
                this.setState({optionsDistricts:lDis});

            })
            .catch(function(error){
                console.log(error);
            });

            //select commune
            axios.get(this.state.beginningURL+'commune/readbyiddistrict.php/?id='+response.data.idDistrict)
            .then(res=>{
                this.setState({
                    communes:res.data.body
                });
                //let lCom=[{value:0,label:""}];
                let lCom=[];
                let currentCommunes=res.data.body;
                for(let i=0;i<currentCommunes.length;i++){
                    lCom.push({value:currentCommunes[i].id, label:currentCommunes[i].nomCommune});
                     //init the combobox
                    if(currentCommunes[i].id===response.data.idCommune){
                        this.setState({ selectedOptionCommune:{value:currentCommunes[i].id, label:currentCommunes[i].nomCommune} }); 
                    }
                }
                this.setState({optionsCommunes:lCom});
                
                
    
            })
            .catch(function(err){
                console.log(err);
            });

            //select Fokontany
            axios.get(this.state.beginningURL+'fokontany/readbyidcommune.php/?id='+response.data.idCommune)
            .then(res=>{
                this.setState({
                    fokontany:res.data.body
                });
                let lFok=[];
                let currentFokontany=res.data.body;
                for(let i=0;i<currentFokontany.length;i++){
                    lFok.push({value:currentFokontany[i].id, label:currentFokontany[i].nomFokontany});
                     //init the combobox
                    if(currentFokontany[i].id===response.data.idFokontany){
                        this.setState({ selectedOptionFokontany:{value:currentFokontany[i].id, label:currentFokontany[i].nomFokontany} }); 
                    }
                }
                this.setState({optionsFokontany:lFok});
                
                

            })
            .catch(function(err){
                console.log(err);
            });

            //site
            axios.get(this.state.beginningURL+'site/readbyidfokontany.php/?id='+response.data.idFokontany)
            .then(res=>{
                //console.log(res);
                this.setState({
                    site:res.data.body
                });
                let lSite=[];
                let currentSites=res.data.body;
                for(let i=0;i<currentSites.length;i++){
                   lSite.push({value:currentSites[i].id, label:currentSites[i].nomSite});
                    //init the combobox
                    if(currentSites[i].id===response.data.idSite){
                        this.setState({ selectedOptionSite:{value:currentSites[i].id, label:currentSites[i].nomSite} }); 
                    }
                }
                this.setState({optionsSites:lSite});

                //-------------------------------
                //get the especes plants
                axios.get(this.state.beginningURL+'especemiseenterre/readbyidreboisement.php?id='+this.props.match.params.id)
                .then(res1=>{
                    //console.log(response.data.body);
                    this.setState({
                        especesMiseEnTerre:res1.data.body
                    
                    });
                    let tablePlants=[]
                    for(let i=0;i<res1.data.body.length;i++){
                    tablePlants.push({id:res1.data.body[i].idEspecePlants, nomEspece:res1.data.body[i].nomEspece,nombre:res1.data.body[i].nombre});
                    }  
                    //console.log(tablePlants);
                    this.setState({plants:tablePlants});


                })
                .catch(function(err1){
                    console.log(err1);
                });
                
                
    
            })
            .catch(function(err){
                console.log(err);
            });

        })
        .catch(function(error){
            console.log(error);
        });

        
       

  
      }
      //Dialog
    handleClickOpenClient  = () => {
        this.setState({openClient:true});
      };
  
    handleCloseClient  = () => {
        this.setState({openClient:false,errorMessageClient:''});
      };

    handleClickOpenSite  = () => {
        this.setState({openSite:true});
      };
  
    handleCloseSite  = () => {
        this.setState({openSite:false,errorMessageSite:''});
      };
    
    //Dialog forms 
    onChangeNewClientNom(e){
        this.setState({
            newClientNom:e.target.value
        });
      }
    onChangeNewClientDescription(e){
        this.setState({
            newClientDescription:e.target.value
        });
      }
    onChangeNewSite(e){
        this.setState({
            newSite:e.target.value
        });
      }
    //OnSubmit
    onSubmitSite(e){
        e.preventDefault();
        console.log("here");
        if(this.state.newSite==='' ){
            this.setState({errorMessageSite:'Champ vide, veuillez remplir le champ'});
            
        }
        else if(this.state.idFokontany===0){
            this.setState({errorMessageSite:'Veuillez préciser le nom les information concernant le site dans le formulaire précédent(district,commune, fokontany)'});

        }
        else{
                
                    const site ={
                        idFokontany : this.state.idFokontany,
                        nomSite : this.state.newSite
                       
                    };
                    //console.log(site);
            
                    axios.post(this.state.beginningURL+'site/create.php',site)
                     .then(res => {
                        console.log(res.data);
                        
                        if(res.data==="ok"){
                           
                            axios.get(this.state.beginningURL+'site/readbyidfokontany.php/?id='+this.state.idFokontany)
                            .then(response=>{
                                this.setState({
                                    site:response.data.body
                                });
                                let lSite=[];
                                let currentSites=response.data.body;
                                for(let i=0;i<currentSites.length;i++){
                                lSite.push({value:currentSites[i].id, label:currentSites[i].nomSite});
                                }
                                this.setState({optionsSites:lSite});
                                
                                

                            })
                            .catch(function(error){
                                console.log(error);
                            });
                                alert('Insertion de la nouvelle site achevée avec succès');
                                this.setState({errorMessageSite:'',newSite:'', openSite:false});
       
                        
                        }
                        else{
                            this.setState({errorMessageSite:'Erreur lors de l\'insertion de la nouvelle site.Veuillez réessayer.'});
                            
                        }
                    });
                }        
                   
    }
    onSubmitClient(e){
        e.preventDefault();
      
        if(this.state.newClientNom==='' ){
            this.setState({errorMessageClient:'Champ vide, veuillez remplir le nom du client.'});
           
        }
        else if(this.state.newClientDescription===''){
            this.setState({errorMessageClient:'Champ vide, veuillez remplir la description du client.'});
         

        }
        else{
                
                    const client ={
                        nomClient : this.state.newClientNom,
                        description : this.state.newClientDescription                      
                    };
                    //console.log(pepinieriste);
            
                    axios.post(this.state.beginningURL+'client/create.php',client)
                     .then(res => {
                        console.log(res.data);
                        
                        if(res.data==="Client created successfully."){
                           
                             //select Client
                                axios.get(this.state.beginningURL+'client/read.php')
                                .then(response=>{
                                    this.setState({
                                        clients:response.data.body
                                        
                                    })
                                    let lCli=[{value:0,label:""}];
                                    let currentClients=response.data.body;
                                    for(let i=0;i<currentClients.length;i++){
                                        lCli.push({value:currentClients[i].id, label:currentClients[i].nomClient});
                                    }
                                    this.setState({optionsClients:lCli});
                        
                                })
                                .catch(function(error){
                                    console.log(error);
                                });
                            alert('Insertion du nouveau client achevée avec succès');
                            this.setState({errorMessageClient:'',newClientNom:'',newClientDescription:'', openClient:false});
         
       
                        
                        }
                        else{
                            this.setState({errorMessageClient:'Erreur lors de l\'insertion du nouveau client.Veuillez réessayer.'});
         
                        }
                    });
                }        
                   
    }
    

      //Modals
      state = {
        showLocalisation: false
      };
      showLocalisationModal = e => {
        this.setState({
            showLocalisation: !this.state.showLocalisation
        });
      };


    onChangeSurfacePrevue(e){
        this.setState({
            surfacePrevue:e.target.value
        });
    }
    onChangeShapefilePlan(e){
        this.setState({
            shapefilePlan:e.target.value
        });
    }
    onChangeNbPlants(e){
        this.setState({
            nbPlants:e.target.value
        });
    }
    onChangeDateDebut(e){
        this.setState({
            dateDebut:e.target.value
        });
    }
    onChangeDateFin(e){
        this.setState({
            dateFin:e.target.value
        });
    }
    onChangeNbEspecePlant(e){
        this.setState({
            nbEspecePlant:e.target.value
        });
    }
    onChangeCoordonneeX(e){
        this.setState({
            coordonneeX:e.target.value
        });
    }
    onChangeCoordonneeY(e){
        this.setState({
            coordonneeY:e.target.value
        });
    }
    onChangeSuperficie(e){
        this.setState({
            superficie:e.target.value
        });
    }
    onChangeShapeFile(e){
        this.setState({
            shapeFile:e.target.value
        });
    }
    
    
    
    //select (combobox) 
   
    handleChangeEspecePlant = selectedOptionEspecePlant => {
        this.setState({ selectedOptionEspecePlant});
    };
    handleChangeCampagne = selectedOptionCampagne => {
        this.setState({ selectedOptionCampagne , idCampagne:selectedOptionCampagne.value});
        let idCurrentRegion=0;
      
            
            idCurrentRegion=this.state.loggedUser.idRegion;
            //select for the region
            

                    //get Pepiniere depending on Region and Campagne
                    // get all the pepinieres
                    const data ={
                        wordToSearch:"",
                        idRegion:idCurrentRegion,
                        idCampagne:selectedOptionCampagne.value
              
                    };
                    
                    //console.log(data);
                    // axios.post(this.state.beginningURL+'pepiniere/readperregionandcampagneid.php', data)
                    axios.get(this.state.beginningURL+'pepiniere/readperregionandcampagneid.php?wordToSearch='+data.wordToSearch+'&idRegion='+data.idRegion+'&idCampagne='+data.idCampagne)
                    .then(response =>{
                            this.setState({
                                pepinieres:response.data.body

                            });
                            let lPep=[{value:0,label:""}];
                        let currentPepinieres=response.data.body;
                        for(let i=0;i<currentPepinieres.length;i++){
                        lPep.push({value:currentPepinieres[i].id, label:currentPepinieres[i].nomPepiniere});
                        }
                        this.setState({optionsPepinieres:lPep});
                    })
                    .catch(error=>{
                        console.log(error);
                    })
        
    };
    handleChangeInstitution = selectedOptionInstitution => {
        this.setState({ selectedOptionInstitution , idInstitution:selectedOptionInstitution.value});
    };
    handleChangeTypeInstitution = selectedOptionTypeInstitution => {
        this.setState({ selectedOptionTypeInstitution , idTypeInstitution:selectedOptionTypeInstitution.value});
    };
    handleChangeSituationJuridique = selectedOptionSituationJuridique=> {
        this.setState({ selectedOptionSituationJuridique , idSituationJuridique:selectedOptionSituationJuridique.value});
    };
    handleChangeClient = selectedOptionClient => {
        this.setState({ selectedOptionClient , idClient:selectedOptionClient.value});
    };
    handleChangeObjectif = selectedOptionObjectif=> {
        this.setState({ selectedOptionObjectif , idObjectif:selectedOptionObjectif.value});
    };
    handleChangeApproche = selectedOptionApproche=> {
        this.setState({ selectedOptionApproche , idApproche:selectedOptionApproche.value});
    };
    handleChangePepiniere = selectedOptionPepiniere=> {
        this.setState({ selectedOptionPepiniere , idPepiniere:selectedOptionPepiniere.value});
    };
    handleChangeDistrict = selectedOptionDistrict=> {
        this.setState({ selectedOptionDistrict , idDistrict:selectedOptionDistrict.value});
       
        axios.get(this.state.beginningURL+'commune/readbyiddistrict.php/?id='+selectedOptionDistrict.value)
        .then(response=>{
            this.setState({
                communes:response.data.body
            });
            //let lCom=[{value:0,label:""}];
            let lCom=[];
            let currentCommunes=response.data.body;
            for(let i=0;i<currentCommunes.length;i++){
            lCom.push({value:currentCommunes[i].id, label:currentCommunes[i].nomCommune});
            }
            this.setState({optionsCommunes:lCom});
            
            

        })
        .catch(function(error){
            console.log(error);
        });
        
    };
    handleChangeCommune = selectedOptionCommune=> {
        this.setState({ selectedOptionCommune , idCommune:selectedOptionCommune.value});
        axios.get(this.state.beginningURL+'fokontany/readbyidcommune.php/?id='+selectedOptionCommune.value)
        .then(response=>{
            this.setState({
                fokontany:response.data.body
            });
            let lFok=[];
            let currentFokontany=response.data.body;
            for(let i=0;i<currentFokontany.length;i++){
               lFok.push({value:currentFokontany[i].id, label:currentFokontany[i].nomFokontany});
            }
            this.setState({optionsFokontany:lFok});
            
            

        })
        .catch(function(error){
            console.log(error);
        });
    };
    handleChangeFokontany = selectedOptionFokontany=> {
        this.setState({ selectedOptionFokontany , idFokontany:selectedOptionFokontany.value});
        axios.get(this.state.beginningURL+'site/readbyidfokontany.php/?id='+selectedOptionFokontany.value)
        .then(response=>{
            this.setState({
                site:response.data.body
            });
            let lSite=[];
            let currentSites=response.data.body;
            for(let i=0;i<currentSites.length;i++){
               lSite.push({value:currentSites[i].id, label:currentSites[i].nomSite});
            }
            this.setState({optionsSites:lSite});
            
            

        })
        .catch(function(error){
            console.log(error);
        });
        
    };
    handleChangeSite = selectedOptionSite=> {
        this.setState({ selectedOptionSite , idSite:selectedOptionSite.value});
    };
    
    showErrorMessage(){
        if(this.state.errorMessage!==''){
            return(
                <Typography component="h5"  style={Design.errorMessageStyle}>
                            {this.state.errorMessage} 
                    </Typography>
            );
        }
        else{
            return
        }
        
    }


    
    
      onSubmit(e){
        e.preventDefault();
      
        if(this.state.idCampagne===0 || 
         
            this.state.idInstitution===0 ||
         
            this.state.idTypeInstitution===0 ||
          
            this.state.idTypeInstitution===0 ||
         
            this.state.idSituationJuridique===0 ||
         
            this.state.idClient===0 ||
      
            this.state.idObjectif===0 ||
          
            this.state.idApproche===0 ||
       
            this.state.idPepiniere===0 ||
            this.state.dateDebut==='' ||
            this.state.dateFin==='' ||
           
            this.state.idDistrict===0 ||
          
            this.state.idCommune===0 ||
         
            this.state.idFokontany===0 ||
          
            this.state.idSite===0 ||
            this.state.nbPlants===0 ||
            this.state.nbPlants==='0'||
            this.state.coordonneeX===''||
            this.state.coordonneeY===''
            




            
            ){
            //console.log(this.state.selectedOptionCampagne);
            this.setState({errorMessage:'Veullez bien remplir tous les champs.'});
        }
        else{
                
                    const reboisement ={
                        id:this.state.idReboisement,
                        userModule:this.state.loggedUser.module,
                        userType:this.state.loggedUser.type,
                        userProfil:this.state.loggedUser.profil,
                        idRegion:this.state.userRegion.id,
                        plants:this.state.plants,
                        idCampagne : this.state.idCampagne,
                        idInstitution : this.state.idInstitution,
                        idTypeInstitution: this.state.idTypeInstitution,
                        idSituationJuridique: this.state.idSituationJuridique,
                        idClient:this.state.idClient,
                        idObjectif:this.state.idObjectif,
                        idApproche:this.state.idApproche,
                        surfacePrevue:this.state.surfacePrevue,
                        coordonneeX:this.state.coordonneeX,
                        coordonneeY:this.state.coordonneeY,
                        shapefilePlan:this.state.shapefilePlan,
                        nbPlants:this.state.nbPlants,
                        idPepiniere:this.state.idPepiniere,
                        dateDebut:this.state.dateDebut,
                        dateFin:this.state.dateFin,
                        idDistrict:this.state.idDistrict,
                        idCommune:this.state.idCommune,
                        idFokontany:this.state.idFokontany,
                        idSite:this.state.idSite,
                        superficie:this.state.surfacePrevue,
                        shapeFile:this.state.shapefilePlan,
                        estAcheve:this.state.estAcheve


                       
                    };
                    //console.log(reboisement);
            
                    axios.post(this.state.beginningURL+'reboisement/update.php',reboisement)
                     .then(res => {
                        console.log(res.data);
                        
                        if(res.data==="ok"){
                            alert('Mise à jour du reboisement achevée avec succès');
                            window.location="/reboisementhistoric";
                        }
                        else{
                            alert('Erreur lors de la mise à jour du reboisement, veuillez réessayer');
                        }
                    })
                    .catch(error=>{
                        console.log(error);
                    })
                    ;
                }     
    }

    plantList(){
        //console.log(this.state.plants);
        if(this.state.plants!==undefined){
            return this.state.plants.map((currentPlant) => {
                return <Plant  plant={currentPlant} deletePlant={this.removePlant}  key={currentPlant.id}/>;
            });
        }
        else{
            return
        }
     
    }
    removePlant(id){
        let plantToRemove={};
        for(let i=0;i<this.state.plants.length;i++){
            if(this.state.plants[i].id===id){
                plantToRemove=this.state.plants[i];
                break;
            }
        }
        let currentNbPlants= parseInt(this.state.nbPlants)-parseInt(plantToRemove.nombre);
        //console.log(plantToRemove);
        this.setState({
            plants:this.state.plants.filter(el=>el.id !==id),
            nbPlants:currentNbPlants
        });    
        
    }
    annuler(){
        //on annule tout
        //init the values 
        this.setState({
            userFrom:'',
            userRegion:{},
            loggedUser:{},
            errorMessage:'',
            plants:[],
            especePlants:[],
            nbEspecePlant:0,
            campagnes:[],
            idCampagne:0,
            institutions: [],
            idInstitution:0,
            typeInstitutions: [],
            idTypeInstitution:0,
            situationsJuridiques:[],
            idSituationJuridique:0,
            clients:[],
            idClient:0,
            objectifs:[],
            idObjectif:0,
            approches:[],
            idApproche:0,
            surfacePrevue:0,
            shapefilePlan:'',
            nbPlants:0,
            coordonneeX:'',
            coordonneeY:'',
            pepinieres:[],
            idPepiniere:0,
            dateDebut:'',
            dateFin:'',
            districts:[],
            idDistrict:0,
            communes:[],
            idCommune:0,
            fokontany:[],
            idFokontany:0,
            sites:[],
            idSite:0,
            optionsEspecePlants: [],
            optionsCampagnes: [],
            optionsInstitutions: [],
            optionsTypeInstitutions:[],
            optionsSituationJuridiques:[],
            optionsClients:[],
            optionsObjectifs:[],
            optionsApproches:[],
            optionsDistricts:[],
            optionsPepinieres:[],
            optionsCommunes:[],
            optionsFokontany:[],
            optionsSites:[],
            selectedOptionEspecePlant: null,
            selectedOptionCampagne:null,
            selectedOptionInstitution:null,
            selectedOptionTypeInstitution:null,
            selectedOptionSituationJuridique:null,
            selectedOptionClient:null,
            selectedOptionObjectif:null,
            selectedOptionApproche:null,
            selectedOptionPepiniere:null,
            selectedOptionDistrict:null,
            selectedOptionCommune:null,
            selectedOptionFokontany:null,
            selectedOptionSite:null
        });
        this.componentDidMount();
    }
    addToListEspece(){
        
      
        let currentEspece=this.state.selectedOptionEspecePlant;
        let currentNombreEspece=this.state.nbEspecePlant;
        if(currentEspece!==null && currentEspece!==undefined && currentNombreEspece!=="0" && currentNombreEspece!=="" ){
            if( currentEspece.label!=="" && currentEspece.label!==undefined &&  parseInt(currentNombreEspece)>0){
                let tablePlants=this.state.plants;
                //console.log(currentEspece);
            
                //voir si une même espève n'a pas encore été inséré dans le tableau
                let previousInserted=null;
                for(let i=0;i<tablePlants.length;i++){
                    if(tablePlants[i].id===currentEspece.value){
                        previousInserted=this.state.plants[i];
                        
                        break;
                    }
                }
                let currentNbPlants=parseInt(this.state.nbPlants)+parseInt(currentNombreEspece);
                //console.log(currentNbPlants);

                if(previousInserted!==null){
                    //currentNbPlants=currentNbPlants-parseInt(previousInserted.nombre);
                    
                    currentNombreEspece=parseInt(currentNombreEspece)+parseInt(previousInserted.nombre);   
                    tablePlants=tablePlants.filter(el=>el.id !==currentEspece.value);
                    
                }
            
                
                
                tablePlants.push({id:currentEspece.value, nomEspece:currentEspece.label,nombre:currentNombreEspece});
                this.setState({
                    plants:tablePlants,
                    nbPlants:currentNbPlants
                })
            }
        }
    }

    deleteReboisement(){
        axios.delete(this.state.beginningURL+'reboisement/delete.php',{data:{id:this.props.match.params.id}})
        .then(res => {
             console.log(res.data);
             this.setState({openSupprimer:false});
             alert("Le reboisement a été supprimé avec succès.");
             window.location="/reboisementhistoric";
         });
        this.setState({openSupprimer:false});
    
    }

    showButtonDelete(){
        return(
            <Button variant="contained" onClick={this.handleClickOpenSupprimer}  style={Design.buttonDeleteStyle}>
                 <img className="imageSupprimer" alt="supprimer" src="/assets/images/logos/LogoDelete.png"/>&nbsp;
                Supprimer
            </Button>
        )
    }

    //Dialog
    handleClickOpenSupprimer  = () => {
        this.setState({openSupprimer:true});
      };
  
      handleCloseSupprimer  = () => {
        this.setState({openSupprimer:false});
      };



    render() {
        //select (combobox)
        const { 
            selectedOptionEspecePlant,
            selectedOptionCampagne,
            selectedOptionInstitution, 
            selectedOptionTypeInstitution,
            selectedOptionSituationJuridique, 
            selectedOptionClient,
            selectedOptionObjectif,
            selectedOptionApproche,
            selectedOptionPepiniere,
            selectedOptionDistrict,
            selectedOptionCommune,
            selectedOptionFokontany,
            selectedOptionSite
        } = this.state; 
        //Table
        const classes = useStyles;

        //form 
        const classesForm = useStylesForm;

        

        return (
            <div className="container-fluid">
                <div className="row bg-title">
                    <div className="left-title col-lg-4 col-md-4 col-sm-4 col-xs-12" >
                        <h4 className="text-left-title page-title">REBOISEMENTS</h4>
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <h3 className="text-middle-title page-title">{this.state.userFrom}</h3>
                    </div>

                    <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                        {/* <a href="https://wrappixel.com/templates/ampleadmin/" target="_blank"
                            className="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Upgrade
                            to Pro

                        </a> */}
                        <ol className="breadcrumb">
                            
                            <li className="active"><a>Reboisement</a>&nbsp;&nbsp;<i className="linking fa fa-angle-right fa-fw"></i>Modification</li>
                        </ol>
                    </div>
                    {/* <!-- /.col-lg-12 --> */}
                </div>
            
                {/* <!-- content of the page--> */}
                {/* Head */}
                <div className="divAppBar" >
                    <AppBar position="static" style={appBarStyle}>
                        <Toolbar>
                            {/* grid */}
                            
                                <Grid container spacing={3}>
                                    <Grid item xs={3} style={{textAlign:'left'}}>
                                    <Link to='/reboisementhistoric'>
                                        <Button variant="contained" style={buttonBackStyle}>
                                            Voir l'historique
                                        </Button>
                                    </Link>
                                    </Grid>
                                    <Grid item xs={6} style={{textAlign:'center'}}>
                                        <h4 className="smaller-title" >Modification reboisement n°{this.state.idReboisement}</h4>
                                    </Grid>
                                    <Grid item xs={3}>  
                                        {this.showButtonDelete()}                                  
                                    </Grid>
                                </Grid>
                            
                            {/* end grid */}
                        
                        </Toolbar>
                    </AppBar>
                </div>
                {/* end head */}
                  {/* Dialog supprimer  */}
                  <Dialog
                    open={this.state.openSupprimer}
                    onClose={this.handleCloseSupprimer}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                    >
                    <DialogTitle  style={Design.fontStyleDialog} id="alert-dialog-title">{"Suppression de reboisement"}</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description"  style={Design.fontStyleDialog}>
                        Souhaitez-vous vraiment supprimer le reboisement ID={this.state.idReboisement}
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleCloseSupprimer}  style={Design.fontStyleCancelDialog} color="primary">
                        Annuler
                        </Button>
                        <Button onClick={()=> {this.deleteReboisement()}}  style={Design.fontStyleDeleteDialog} color="primary" autoFocus>
                        Supprimer
                        </Button>
                    </DialogActions>
                </Dialog>
                {/* End Dialog supprimer  */}
                
                {/* Table */}
                <div className="row">
                    <div className="col-sm-12">
                        <div className="white-box formStyleReboisement" >
                        <Container component="main" maxWidth="xs" style={styleContainer}>
                            <Paper className={classes.root}>
                            {this.showErrorMessage()}
                            <form className={classesForm.form} onSubmit={this.onSubmit}  noValidate>
                            
                            <label className="label-style">Campagne</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                    <Select
                                        autofocus
                                        placeholder="Campagne"
                                        value={selectedOptionCampagne}
                                        onChange={this.handleChangeCampagne}
                                        options={this.state.optionsCampagnes}
                                    />
                                </Grid>
                                <Grid item xs={1}></Grid>
                            </Grid>

                            

                            <label className="label-style">Institution</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                    <Select
                                        placeholder="Institution"
                                        value={selectedOptionInstitution}
                                        onChange={this.handleChangeInstitution}
                                        options={this.state.optionsInstitutions}
                                    />
                                </Grid>
                                <Grid item xs={1}></Grid>
                            </Grid>

                            <label className="label-style">DREDD/CIREDD</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                    <Select
                                        placeholder="DREDD/CIREDD"
                                        value={selectedOptionTypeInstitution}
                                        onChange={this.handleChangeTypeInstitution}
                                        options={this.state.optionsTypeInstitutions}
                                    />
                                </Grid>
                                <Grid item xs={1}></Grid>
                            </Grid>

                            <label className="label-style">Situation Juridique</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                    <Select
                                        placeholder="Situation Juridique"
                                        value={selectedOptionSituationJuridique}
                                        onChange={this.handleChangeSituationJuridique}
                                        options={this.state.optionsSituationJuridiques}
                                    />
                                </Grid>
                                <Grid item xs={1}></Grid>
                            </Grid>

                            <label className="label-style">Responsable</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                    <Select
                                        placeholder="Responsable"
                                        value={selectedOptionClient}
                                        onChange={this.handleChangeClient}
                                        options={this.state.optionsClients}
                                    />
                                </Grid>
                                <Grid item xs={1}>
                                    <i onClick={this.handleClickOpenClient} className="new-icon-style fa  fa-plus-circle fa-fw"></i>
                                </Grid>
                            </Grid>

                            <label className="label-style">Objectif</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                    <Select
                                        placeholder="Objectif"
                                        value={selectedOptionObjectif}
                                        onChange={this.handleChangeObjectif}
                                        options={this.state.optionsObjectifs}
                                    />
                                </Grid>
                                <Grid item xs={1}></Grid>
                            </Grid>

                            <label className="label-style">Approche</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                    <Select
                                        placeholder="Approche"
                                        value={selectedOptionApproche}
                                        onChange={this.handleChangeApproche}
                                        options={this.state.optionsApproches}
                                    />
                                </Grid>
                                <Grid item xs={1}></Grid>
                            </Grid>

                            <label className="label-style">Surface pr&eacute;vue (Ha)</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    id="surfacePrevue"
                                    label=""
                                    name="surfacePrevue"
                                    autoComplete="surfacePrevue"
                                    type="number"
                                    inputProps={{style: fontStyle}} 
                                    InputLabelProps={{style: fontStyle}} 
                                    value={this.state.surfacePrevue} onChange={this.onChangeSurfacePrevue}
                                />
                                </Grid>
                                <Grid item xs={1}></Grid>
                            </Grid> 

                            <label className="label-style">Shapefile</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    id="shapefileplan"
                                    label=""
                                    placeholder="Shapefile"
                                    name="shapefileplan"
                                    autoComplete="shapefileplan"
                                    inputProps={{style: fontStyle}} 
                                    InputLabelProps={{style: fontStyle}} 
                                    value={this.state.shapefilePlan} onChange={this.onChangeShapefilePlan}
                                />
                                </Grid>
                                <Grid item xs={1}></Grid>
                            </Grid>  
                            <label className="label-style">Latitude</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    id="coordonneeX"
                                    label=""
                                    placeholder="Latitude"
                                    name="coordonneeX"
                                    autoComplete="coordonneeX"
                                    type="number"
                                    inputProps={{style: fontStyle}} 
                                    InputLabelProps={{style: fontStyle}} 
                                    value={this.state.coordonneeX} onChange={this.onChangeCoordonneeX}
                                />
                                </Grid>
                                <Grid item xs={1}></Grid>
                            </Grid>  
                            <label className="label-style">Longitude</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    id="coordonneeY"
                                    label=""
                                    placeholder="Longitude"
                                    name="coordonneeY"
                                    autoComplete="coordonneeY"
                                    type="number"
                                    inputProps={{style: fontStyle}} 
                                    InputLabelProps={{style: fontStyle}} 
                                    value={this.state.coordonneeY} onChange={this.onChangeCoordonneeY}
                                />
                                </Grid>
                                <Grid item xs={1}></Grid>
                            </Grid>  

                          

                            <label className="label-style">P&eacute;pini&egrave;re</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                    <Select
                                        placeholder="Pépinière"
                                        value={selectedOptionPepiniere}
                                        onChange={this.handleChangePepiniere}
                                        options={this.state.optionsPepinieres}
                                    />
                                </Grid>
                                <Grid item xs={1}></Grid>
                            </Grid>

                            <label className="label-style">Date de d&eacute;but</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    type="date"
                                    id="dateDebut"
                                    name="dateDebut"
                                    autoComplete="dateDebut"
                                    inputProps={{style: fontStyle}} 
                                    InputLabelProps={{style: fontStyle}} 
                                    value={this.state.dateDebut} onChange={this.onChangeDateDebut}
                                />
                                </Grid>
                                <Grid item xs={1}></Grid>
                            </Grid> 
                            <label className="label-style">Date fin</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    type="date"
                                    id="dateFin"
                                    name="dateFin"
                                    autoComplete="dateFin"
                                    inputProps={{style: fontStyle}} 
                                    InputLabelProps={{style: fontStyle}} 
                                    value={this.state.dateFin} onChange={this.onChangeDateFin}
                                />
                                </Grid>
                                <Grid item xs={1}></Grid>
                            </Grid> 

                            <label className="label-style">District</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                    <Select
                                        placeholder="District"
                                        value={selectedOptionDistrict}
                                        onChange={this.handleChangeDistrict}
                                        options={this.state.optionsDistricts}
                                    />
                                </Grid>
                                <Grid item xs={1}></Grid>
                            </Grid>
                            <label className="label-style">Commune</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                    <Select
                                        placeholder="Commune"
                                        value={selectedOptionCommune}
                                        onChange={this.handleChangeCommune}
                                        options={this.state.optionsCommunes}
                                    />
                                </Grid>
                                <Grid item xs={1}></Grid>
                            </Grid>

                            <label className="label-style">Fokontany</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                    <Select
                                        placeholder="Fokontany"
                                        value={selectedOptionFokontany}
                                        onChange={this.handleChangeFokontany}
                                        options={this.state.optionsFokontany}
                                    />
                                </Grid>
                                <Grid item xs={1}></Grid>
                            </Grid>
                            <label className="label-style">Site</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                    <Select
                                        placeholder="Site"
                                        value={selectedOptionSite}
                                        onChange={this.handleChangeSite}
                                        options={this.state.optionsSites}
                                    />
                                </Grid>
                                <Grid item xs={1}>
                                    <i onClick={this.handleClickOpenSite} className="new-icon-style fa  fa-plus-circle fa-fw"></i>
                                </Grid>
                            </Grid>
                            <label className="label-style">Esp&egrave;ce de plants</label>
                            <Grid container spacing={0} style={Design.styleAddEspece} >
                           
                               <Grid item xs={2}> </Grid>
                               
                                <Grid item xs={8} >
                                    <label className="label-style">Esp&egrave;ce</label>
                                    <Select
                                        placeholder="Espèce de plant"
                                        value={selectedOptionEspecePlant}
                                        onChange={this.handleChangeEspecePlant}
                                        options={this.state.optionsEspecePlants}
                                    />
                                    <label className="label-style">Nombre</label>
                                    <TextField
                                        variant="outlined"
                                        margin="normal"
                                        fullWidth
                                        id="nbCurrentPlants"
                                        name="nbCurrentPlants"
                                        autoComplete="nbCurrentPlants"
                                        type="number"
                                        inputProps={{style: fontStyle}} 
                                        InputLabelProps={{style: fontStyle}} 
                                        value={this.state.nbEspecePlant} onChange={this.onChangeNbEspecePlant}
                                    />
                        
                                    <Button
                                        type="button"
                                        variant="contained"
                                        color="primary"
                                        className={classesForm.submit}
                                        onClick={()=> {this.addToListEspece()}}  
                                        
                                    >
                                        Ajouter à la liste
                                    </Button>  
                                </Grid>
                               
                                <Grid item xs={2}>
                                                                                       
                                </Grid>
                            </Grid>
                            <br/>
                             

                            <Paper className={classes.root}>
                                
                                <TableContainer className={classes.container}>
                                    <Table  stickyHeader aria-label="sticky table">
                                    <TableBody>
                                        <TableRow style={Design.tableRowTitleStyle}>
                                            <TableCell style={Design.tableCellTitleStyle}>Esp&egrave;ce de plant</TableCell>
                                            <TableCell style={Design.tableCellTitleStyle}>Nombre </TableCell>
                                            <TableCell style={Design.tableCellTitleActionColumnStyle}></TableCell>                                           
                                           
                                        </TableRow>
                                        
                                    </TableBody>
                                    <TableBody>
                                        {this.plantList()}       
                                    </TableBody>
                                    </Table>
                                </TableContainer>
                           
                            </Paper>  
                            <label className="label-style">Nombre de plants</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={12} >
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    id="nbPlants"
                                    name="nbPlants"
                                    autoComplete="nbPlants"
                                    type="number"
                                    inputProps={{style: fontStyle}} 
                                    InputLabelProps={{style: fontStyle}} 
                                    value={this.state.nbPlants}  onChange={this.onChangeNbPlants}
                                />
                                </Grid>
                                <Grid item xs={0}></Grid>
                            </Grid> 
                    
                            <Grid container spacing={1} style={{marginTop:'40px'}} >
                            <Grid item xs={6} >
                              <Button
                                  type="button"
                                  fullWidth
                                  variant="contained" 
                                  style={fontStyle}   
                                  onClick={()=> {this.annuler()}}                               
                              >
                                  Annuler
                              </Button>
                            </Grid>
                            <Grid item xs={6}>
                              <Button
                                  type="submit"
                                  fullWidth
                                  variant="contained"
                                  color="primary"
                                  className={classesForm.submit}
                                  style={fontStyle}
                                  
                              >
                                  Enregistrer
                              </Button>
                            </Grid>
                        </Grid>
                            </form>
                            <br/>
                                
                            </Paper>  
                            {/* end content of the table */}
                            </Container>
                        </div>
                    </div>
                </div>
                {/* end table */}
              
                {/* <!-- /.content of the page --> */}
                {/* Dialogs */}
                {/* Client */}
                <Dialog
                    open={this.state.openClient}
                    onClose={this.handleCloseClient}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                    >
                    <DialogTitle  style={Design.fontStyleDialog} id="alert-dialog-title">{"Enregistrer un nouveau client"}</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description"  style={Design.fontStyleDialog}>
                          
                            <Typography component="h5"  style={Design.errorMessageStyle}>
                                    {this.state.errorMessageClient} 
                            </Typography>
                        
                            <label className="label-style"><strong>Nom du client</strong></label>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    id="newclient"
                                    placeholder="Nom du client"
                                    name="newclient"
                                    autoComplete="newclient"
                
                                    inputProps={{style: fontStyle}} 
                                    InputLabelProps={{style: fontStyle}} 
                                    value={this.state.newClientNom} onChange={this.onChangeNewClientNom}
                                /> 
                            <label className="label-style"><strong>Description du client</strong></label>
                          
                            <MDBInput type="textarea"  style={Design.fontStyle2}  placeholder="Description" rows="5" value={this.state.newClientDescription} onChange={this.onChangeNewClientDescription} />
 
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                    <form className={classesForm.form} onSubmit={this.onSubmitClient}  noValidate>
                        <Button onClick={this.handleCloseClient}  style={Design.fontStyleCancelDialog} color="primary">
                            Annuler
                        </Button>
                        <Button
                                  type="submit"
                                
                                  variant="contained"
                                  color="primary"
                                  className={classesForm.submit}
                                  style={fontStyle}
                                  autoFocus
                              >
                                  Enregistrer
                        </Button>
                        </form> 
                    </DialogActions>
                   
                </Dialog>
                {/* Site */}
                <Dialog
                    open={this.state.openSite}
                    onClose={this.handleCloseSite}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                    >
                    <DialogTitle  style={Design.fontStyleDialog} id="alert-dialog-title">{"Enregistrer une nouvelle site"}</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description"  style={Design.fontStyleDialog}>
                          
                            <Typography component="h5"  style={Design.errorMessageStyle}>
                                    {this.state.errorMessageSite} 
                            </Typography>
                            <label className="label-style"><strong>Nouvelle site</strong></label>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    id="newsite"
                                    label="Site"
                                    name="newsite"
                                    autoComplete="newsite"
                
                                    inputProps={{style: fontStyle}} 
                                    InputLabelProps={{style: fontStyle}} 
                                    value={this.state.newSite} onChange={this.onChangeNewSite}
                                />  
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                    <form className={classesForm.form} onSubmit={this.onSubmitSite}  noValidate>
                        <Button onClick={this.handleCloseSite}  style={Design.fontStyleCancelDialog} color="primary">
                            Annuler
                        </Button>
                        <Button
                                  type="submit"
                                
                                  variant="contained"
                                  color="primary"
                                  className={classesForm.submit}
                                  style={fontStyle}
                                  autoFocus
                              >
                                  Enregistrer
                        </Button>
                        </form> 
                    </DialogActions>
                   
                </Dialog>
                 
                

            </div>
        )
    }
}
