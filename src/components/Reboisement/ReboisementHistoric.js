import React, { Component } from 'react';
import "./ReboisementHistoric.css";
import * as Config from './../Config/Config';
import { Link } from 'react-router-dom';
import axios from 'axios';

//Search
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
//grid for the search
import Grid from '@material-ui/core/Grid';

//Table
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
// import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';


const  Reboisement= props =>(
        <TableRow hover role="checkbox" tabIndex={-1} key={props.reboisement.id}>
        <TableCell style={tableCellStyle}>{props.reboisement.id}</TableCell>
        <TableCell style={tableCellStyle}>{props.reboisement.nomCampagne} </TableCell>
        <TableCell style={tableCellStyle}>{props.reboisement.nomInstitution} / {props.reboisement.typeInstitution}</TableCell>
        <TableCell style={tableCellStyle}>{props.reboisement.dateDebut.substring(0,10)}</TableCell>
        <TableCell style={tableCellStyle}>{props.reboisement.nomClient}</TableCell>
        <TableCell style={tableCellStyle}>{props.reboisement.superficie}</TableCell>
        <TableCell style={tableCellActionColumnStyle}>{props.showButtonValider(props.reboisement.id,props.reboisement.estAcheve)}</TableCell>
        <TableCell style={tableCellActionColumnStyle}><Link to={"/reboisementdetails/"+props.reboisement.id}><img className="imageModifier" alt="details" src="assets/images/logos/logoAdd.png"/></Link></TableCell>
    </TableRow>
)

//Table
const useStyles = makeStyles({
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 440,
    },

  });
//design
let appBarStyle= {borderRadius:'4px',textAlign:'right',backgroundColor:'#000e2f'};
//grid
//let gridSelectStyle={marginBottom:'15px'};
//table
let tableCellTitleStyle={fontSize:'14px',fontFamily:'Segoe UI'};
let tableRowTitleStyle={backgroundColor:'rgb(245, 245, 245)'};
let tableCellTitleActionColumnStyle={fontSize:'14px',fontFamily:'Segoe UI', textAlign:'center'};
let tableCellStyle={fontSize:'13px', color:'rgb(90, 88, 88)', fontFamily:'Segoe UI' };
let tableCellActionColumnStyle={fontSize:'13px', color:'rgb(90, 88, 88)', fontFamily:'Segoe UI', textAlign:'center' };

export default class ReboisementHistoric extends Component {
    constructor(props) {
        super(props);
        //data
        this.state = {
            userFrom:'',
            userRegion:{},
            loggedUser:{},
            reboisements:[],
            wordToSearch:'',
            nbSearchResult:0,
            isSubmit:false,
            page:0,
            rowsPerPage:10,
            listPage:1,
            nblistToShow:50,
            
            beginningURL: Config.beginningURL
        };
        this.reboisementList=this.reboisementList.bind(this);
        this.onChangeSearch= this.onChangeSearch.bind(this);
        this.onSubmitSearch=this.onSubmitSearch.bind(this);
        this.displaySearchResult=this.displaySearchResult.bind(this);
        this.showButtonValider=this.showButtonValider.bind(this);
      

    }
    componentDidMount(){
         //Init title for the page
         if(localStorage.getItem("userRegion")!=null){
            this.setState({
              userRegion:JSON.parse(localStorage.getItem("userRegion"))
            });
        }
        if(localStorage.getItem("title")!=null){
            this.setState({
              userFrom:localStorage.getItem("title")
            });
        }
        if(localStorage.getItem("user")!=null){
            this.setState({
              loggedUser:JSON.parse(localStorage.getItem("user"))
            });
        }
       let loggedUser=JSON.parse(localStorage.getItem("user"));
       /* const data ={
            wordToSearch:this.state.wordToSearch,
            idRegion:loggedUser.idRegion,
            listPage:this.state.listPage,
            nblistToShow:this.state.nblistToShow,
            userModule:loggedUser.module,
            userType:loggedUser.type,
            userProfil:loggedUser.profil
          
        };
        console.log(data);*/
        axios.get(this.state.beginningURL+'reboisement/historicreboisement.php?wordToSearch='+this.state.wordToSearch+'&idRegion='+loggedUser.idRegion+'&listPage='+this.state.listPage+'&nblistToShow='+this.state.nblistToShow+'&userModule='+loggedUser.module+'&userType='+loggedUser.type+'&userProfil='+loggedUser.profil)
        .then(response =>{
                this.setState({
                    reboisements:response.data.body

                });
        })
        .catch(error=>{
            console.log(error);
        })

        
      }

   

    // Search
    onChangeSearch(e){
        this.setState({
            wordToSearch:e.target.value
        });
        //if we erase the word written
        if(e.target.value===""){
            this.setState({
                isSubmit:false,   
            });
            //change to another function depending on the  criteria in select options
            this.componentDidMount();
        }
    }
    onSubmitSearch(e){
        e.preventDefault();
       
        if(this.state.wordToSearch===""){
            this.setState({
                isSubmit:false,   
            });
            this.componentDidMount();
        }
        else{
            this.setState({
                isSubmit:true,   
            });
            /*const data ={
                wordToSearch:this.state.wordToSearch,
                idRegion:this.state.loggedUser.idRegion,
                listPage:this.state.listPage,
                nblistToShow:this.state.nblistToShow,
                userModule:this.state.loggedUser.module,
                userType:this.state.loggedUser.type,
                userProfil:this.state.loggedUser.profil
              
            };*/
            //console.log(data);
            //axios.post(this.state.beginningURL+'reboisement/historic.php', data)
            axios.get(this.state.beginningURL+'reboisement/historicreboisement.php?wordToSearch='+this.state.wordToSearch+'&idRegion='+this.state.loggedUser.idRegion+'&listPage='+this.state.listPage+'&nblistToShow='+this.state.nblistToShow+'&userModule='+this.state.loggedUser.module+'&userType='+this.state.loggedUser.type+'&userProfil='+this.state.loggedUser.profil)
            .then(response =>{
                    this.setState({
                        reboisements:response.data.body,
                        nbSearchResult:response.data.itemCount,
                        isSubmit:true
    
                    });
            })
            .catch(error=>{
                console.log(error);
            })
          
        }    
    }
    displaySearchResult(){
        if(this.state.isSubmit===true){
        return <div className="searchResultDiv"><b className="searchResultStyle">{this.state.nbSearchResult} R&eacute;sultat(s) trouv&eacute;(s)</b></div> 
        } 
        else{
            return 
        }  
    }

    showButtonValider(id,estAcheve){
        if(parseInt(estAcheve)===1){
            return;
        }
        else{
            return(
                <Link to={"/reboisementvalidation/"+id}><i className="fa fa-check-circle fa-fw iconValidation"></i></Link>
            );
        }
    }
    reboisementList(){

        return this.state.reboisements.slice(this.state.page * this.state.rowsPerPage, this.state.page * this.state.rowsPerPage + this.state.rowsPerPage).map((currentReboisement) => {
            return <Reboisement showButtonValider={this.showButtonValider}  reboisement={currentReboisement}  key={currentReboisement.id}/>;
        });
     
    }

    render() {
        
        //Table
        const classes = useStyles;
        
        const handleChangePage = (event, newPage) => {
            this.setState({page:newPage})
            //setPage(newPage);
        };
        const handleChangeRowsPerPage = (event) => {
            //setRowsPerPage(+event.target.value);
            //setPage(0);
            this.setState({rowsPerPage:+event.target.value});
            this.setState({page:0});
        };
        //end table

        

        return (
            <div className="container-fluid">
                <div className="row bg-title">
                    <div className="left-title col-lg-4 col-md-4 col-sm-4 col-xs-12" >
                        <h4 className="text-left-title page-title">Reboisements</h4>
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <h3 className="text-middle-title page-title">{this.state.userFrom}</h3>
                    </div>

                    <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                        {/* <a href="https://wrappixel.com/templates/ampleadmin/" target="_blank"
                            className="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Upgrade
                            to Pro

                        </a> */}
                        <ol className="breadcrumb">
                            
                            <li className="active"><a>Reboisements</a>&nbsp;&nbsp;<i className="linking fa fa-angle-right fa-fw"></i>Historique</li>
                        </ol>
                    </div>
                    {/* <!-- /.col-lg-12 --> */}
                </div>
            
                {/* <!-- content of the page--> */}
                {/* Head */}
                <div className="divAppBar" >
                    <AppBar position="static" style={appBarStyle}>
                        <Toolbar>
                            {/* grid */}
                            
                                <Grid container spacing={3}>
                                    <Grid item xs={3} style={{textAlign:'left'}}>
                                   
                                    </Grid>
                                    <Grid item xs={6} style={{textAlign:'center'}}>
                                        <h4 className="smaller-title" >Historique des reboisements</h4>
                                    </Grid>
                                    <Grid item xs={3}>
                                   
                                        <div className="div-search" >
                                        <form role="search" id="search" className="app-search hidden-sm hidden-xs m-r-10" onSubmit={this.onSubmitSearch} noValidate>
                                            <input type="text" placeholder="Rechercher..." className="form-control"
                                                value={this.state.wordToSearch}
                                                onChange={this.onChangeSearch}/> 
                                             <a  onClick={this.onSubmitSearch}>
                                                <i className="fa fa-search"></i>
                                            </a> 
                                        </form>

                                      
                                        </div>
                                    
                                    </Grid>
                                </Grid>
                            
                            {/* end grid */}
                        
                        </Toolbar>
                    </AppBar>
                </div>
                {/* end head */}
                {/* Table */}
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                         
                            {/* <h3 class="box-title">Basic Table</h3> */}
                            {/* <p class="text-muted">Add class <code>.table</code></p> */}
                             {/* Content of the table */}
                             {this.displaySearchResult()}
                            <Paper className={classes.root}>
                                
                                <TableContainer className={classes.container}>
                                    <Table  stickyHeader aria-label="sticky table">
                                    <TableBody>
                                        <TableRow style={tableRowTitleStyle}>
                                            <TableCell style={tableCellTitleStyle}>Id</TableCell>
                                            <TableCell style={tableCellTitleStyle}>Campagne </TableCell>
                                            <TableCell style={tableCellTitleStyle}>Institution </TableCell>
                                            <TableCell style={tableCellTitleStyle}>Date pr&eacute;vue</TableCell>
                                            <TableCell style={tableCellTitleStyle}>Responsable</TableCell>
                                            <TableCell style={tableCellTitleStyle}>Surface (Ha)</TableCell>
                                            <TableCell style={tableCellTitleActionColumnStyle}>Valider</TableCell>                                           
                                            <TableCell style={tableCellTitleActionColumnStyle}>D&eacute;tails</TableCell>
                                        </TableRow>
                                        
                                    </TableBody>
                                    <TableBody>
                                        {this.reboisementList()}       
                                    </TableBody>
                                    </Table>
                                </TableContainer>
                                <TablePagination
                                    style={tableCellStyle}
                                    rowsPerPageOptions={[10, 25, 100]}
                                    component="div"
                                    count={this.state.reboisements.length}
                                    rowsPerPage={this.state.rowsPerPage}
                                    page={this.state.page}
                                    onChangePage={handleChangePage}
                                    onChangeRowsPerPage={handleChangeRowsPerPage}
                                />
                            </Paper>  
                    

                            {/* end content of the table */}
                        </div>
                    </div>
                </div>
                {/* end table */}
              
                {/* <!-- /.content of the page --> */}

            </div>
        )
    }
}
