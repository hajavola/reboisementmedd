import React, { Component } from 'react';
import "./ReboisementEdit.css";
import * as Config from './../Config/Config';
import * as Design from './../Config/Design';
import { Link } from 'react-router-dom';
//import axios from 'axios';

//Search
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
//grid for the search
import Grid from '@material-ui/core/Grid';

//Table
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';


//select
import Select from 'react-select';

//autocomplete
import TextField from '@material-ui/core/TextField';

//Dialog
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';


//Table
const useStyles = makeStyles({
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 600,
    },

  });



// form 
const useStylesForm = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));

// end form


//design
let appBarStyle= {borderRadius:'4px',textAlign:'right',backgroundColor:'#000e2f'};
let buttonBackStyle= {backgroundColor:'#0bca4b', color:'white', marginTop:'5px'};
let fontStyle={fontSize:'14px'};
let styleContainer={backgroundColor:'white',borderRadius:'3px'};
let buttonDeleteStyle= {backgroundColor:'#e20f00', color:'white', marginTop:'5px'};

export default class ReboisementEdit extends Component {
    constructor(props) {
        super(props);
 
         //Data
        let lCampagnes=[
            {id:1,nomCampagne:'Campagne 2020'},
            {id:2,nomCampagne:'Campagne 2021'},
            {id:3,nomCampagne:'Campagne 2022'},
        ];
        let lInstutions=[
            {id:1,nomInstitution:'MEDD'},
            {id:2,nomInstitution:'MNP'},
            {id:3,nomInstitution:'FANAMBY'},
        ];
        let lTypeInstitutions=[
            {id:1,typeInstitution:'DREDD'},
            {id:2,typeInstitution:'CIREDD'},
            {id:3,typeInstitution:'CEF'}
        ];
        let lSituationJuridique=[
            {id:1,nomSituation:'NAP'},
            {id:2,nomSituation:'Prive'},
            {id:3,nomSituation:'Domanial'},
            {id:4,nomSituation:'terrain gendarme'},
        ];
        let lClients=[
            {id:1,nom:'MBG ORONJIA',Description:'MBG ORONJIA'},
            {id:2,nom:'SAGE',Description:'SAGE'},
            {id:3,nom:'JIRAMA',Description:'JIRAMA'}, 
        ];
        let lObjectifs=[
            {id:1,objectif:'Restauration'},
            {id:2,objectif:'Reboisement'},
            {id:3,objectif:'Agroforesterie'},
            {id:4,objectif:'Protection de BV Embroussaillement Restauration de foret degradée'},
        ];

        let lApproches=[
            {id:1,approche:'Communautaire'},
            {id:2,approche:'Privé'},
            {id:3,approche:'AP'}
        ];
        let lPepinieres=[
            {id:1,nomPepiniere:'Pepiniere 1'},
            {id:2,nomPepiniere:'Pepiniere 2'},
            {id:3,nomPepiniere:'Pepiniere 3'}
        ];
        let lDistricts=[
            {id:1,nomDistrict: 'Ambanja'},
            {id:2,nomDistrict: 'Nosy Be'},
            {id:3,nomDistrict: 'SAVA'}, 
            {id:4,nomDistrict: 'Ambilobe'}
        ];
        let lCommunes=[
            {id:1,idDistrict:1,nomCommune:'Ramena'},
            {id:2,idDistrict:1,nomCommune:'Antanamitarana'},
            {id:3,idDistrict:1,nomCommune:'Mangaoka'},
            {id:4,idDistrict:1,nomCommune:'Sakaramy'},
        ];
        let lFokontany=[
            {id:1,idCommune:1,nomFokontany:'Taga maty'},
            {id:2,idCommune:1,nomFokontany:'Andavakoera'},
            {id:3,idCommune:1,nomFokontany:'Ambodimagnary'},
        ];
        let lSites=[
            {id:1,idFokontany:1,nomSite:'Antaolagnaomby'},
            {id:2,idFokontany:1,nomSite:'MAHATSINJO'},
            {id:3,idFokontany:1,nomSite:'AMBARIOBE'},
            {id:4,idFokontany:1,nomSite:'anciens ZUC/ZOC dans l\'AP Oronjia'},
        ];
        let currentReboisement={
            id:1,
            idInstitution:1,
            idTypeInstitution:1,
            idSituationJuridique:1,
            coordonneeX:12.256,
            coordonneeY:12.2589,
            idClient:1,
            idObjectif:1,
            idApproche:1,
            surfacePrevue:2000.2,
            shapefilePlan:'Néant',
            nbPlants:5000,
            dateDebut:'2020-05-20',
            dateFin:'2020-05-21',
            idPepiniere:1,
            superficie:2000,
            shapefile:'néant',
            idCampagne:1,
            idRegion:1,
            idDistrict:1,
            idCommune:1,
            idFokontany:1,
            idSite:1

        };

        this.state = {
            userFrom:'',
            userRegion:{},
            idReboisement:this.props.match.params.id,
            openSupprimer:false,
            reboisement:currentReboisement,
            campagnes:lCampagnes,
            idCampagne:0,
            institutions: lInstutions,
            idInstitution:0,
            typeInstitutions: lTypeInstitutions,
            idTypeInstitution:0,
            situationsJuridiques:lSituationJuridique,
            idSituationJuridique:0,
            clients:lClients,
            idClient:0,
            objectifs:lObjectifs,
            idObjectif:0,
            approches:lApproches,
            idApproche:0,
            surfacePrevue:0,
            shapefilePlan:'',
            nbPlants:0,
            pepinieres:lPepinieres,
            idPepiniere:0,
            dateDebut:'',
            dateFin:'',
            districts:lDistricts,
            idDistrict:0,
            communes:lCommunes,
            idCommune:0,
            fokontany:lFokontany,
            idFokontany:0,
            sites:lSites,
            idSite:0,
            optionsCampagnes: [],
            optionsInstitutions: [],
            optionsTypeInstitutions:[],
            optionsSituationJuridiques:[],
            optionsClients:[],
            optionsObjectifs:[],
            optionsApproches:[],
            optionsDistricts:[],
            optionsPepinieres:[],
            optionsCommunes:[],
            optionsFokontany:[],
            optionsSites:[],
            beginningURL: Config.beginningURL
        };
    
      
        this.onChangeSurfacePrevue=this.onChangeSurfacePrevue.bind(this);
        this.onChangeShapefilePlan=this.onChangeShapefilePlan.bind(this);
        this.onChangeNbPlants=this.onChangeNbPlants.bind(this);
        this.onChangeDateDebut=this.onChangeDateDebut.bind(this);
        this.onChangeDateFin=this.onChangeDateFin.bind(this);
        this.onSubmit=this.onSubmit.bind(this);
        this.showButtonDelete=this.showButtonDelete.bind(this);
        this.deleteReboisement=this.deleteReboisement.bind(this);
        //Modals
        this.showLocalisationModal=this.showLocalisationModal.bind(this);
      
        
      

    }
    //select (combobox) 
    state = {
        selectedOptionCampagne:null,
        selectedOptionInstitution:null,
        selectedOptionTypeInstitution:null,
        selectedOptionSituationJuridique:null,
        selectedOptionClient:null,
        selectedOptionObjectif:null,
        selectedOptionApproche:null,
        selectedOptionPepiniere:null,
        selectedOptionDistrict:null,
        selectedOptionCommune:null,
        selectedOptionFokontany:null,
        selectedOptionSite:null
      };
    componentDidMount(){
        //Init title for the page
          if(localStorage.getItem("userRegion")!=null){
            this.setState({
              userRegion:JSON.parse(localStorage.getItem("userRegion"))
            });
        }
        if(localStorage.getItem("title")!=null){
            this.setState({
              userFrom:localStorage.getItem("title")
            });
        }

        this.setState({
            surfacePrevue:this.state.reboisement.surfacePrevue,
            shapefilePlan:this.state.reboisement.shapefilePlan,
            nbPlants:this.state.reboisement.nbPlants,
            dateDebut:this.state.reboisement.dateDebut,
            dateFin:this.state.reboisement.dateFin
        });
        let i=0;
        //select campagnes 
        let lCamp=[{value:0,label:""}];
        let currentCampagnes=this.state.campagnes;
        for(i=0;i<currentCampagnes.length;i++){
            lCamp.push({value:currentCampagnes[i].id, label:currentCampagnes[i].nomCampagne});
            //init the combobox
            if(currentCampagnes[i].id===this.state.reboisement.idCampagne){
                this.setState({ selectedOptionCampagne:{value:currentCampagnes[i].id, label:currentCampagnes[i].nomCampagne} }); 
            }
        }
        this.setState({optionsCampagnes:lCamp});

        //select institutions 
        let lInst=[{value:0,label:""}];
        let currentInstitutions=this.state.institutions;
        for(i=0;i<currentInstitutions.length;i++){
            lInst.push({value:currentInstitutions[i].id, label:currentInstitutions[i].nomInstitution});
            //init the combobox
            if(currentInstitutions[i].id===this.state.reboisement.idInstitution){
                this.setState({ selectedOptionInstitution:{value:currentInstitutions[i].id, label:currentInstitutions[i].nomInstitution} }); 
            }
        }
        this.setState({optionsInstitutions:lInst});

        //select type institutions 
        let lTypeInst=[{value:0,label:""}];
        let currentTypeInstitutions=this.state.typeInstitutions;
        for(i=0;i<currentTypeInstitutions.length;i++){
            lTypeInst.push({value:currentTypeInstitutions[i].id, label:currentTypeInstitutions[i].typeInstitution});
            //init the combobox
            if(currentTypeInstitutions[i].id===this.state.reboisement.idTypeInstitution){
                this.setState({ selectedOptionTypeInstitution:{value:currentTypeInstitutions[i].id, label:currentTypeInstitutions[i].typeInstitution} }); 
            }
        }
        this.setState({optionsTypeInstitutions:lTypeInst});

        //select situation juridique 
        let lSit=[{value:0,label:""}];
        let currentSituations=this.state.situationsJuridiques;
        for(i=0;i<currentSituations.length;i++){
            lSit.push({value:currentSituations[i].id, label:currentSituations[i].nomSituation});
            //init the combobox
            if(currentSituations[i].id===this.state.reboisement.idSituationJuridique){
                this.setState({ selectedOptionSituationJuridique:{value:currentSituations[i].id, label:currentSituations[i].nomSituation} }); 
            }
        }
        this.setState({optionsSituationJuridiques:lSit});

         //select Client
         let lCli=[{value:0,label:""}];
         let currentClients=this.state.clients;
         for(i=0;i<currentClients.length;i++){
            lCli.push({value:currentClients[i].id, label:currentClients[i].nom});
            //init the combobox
            if(currentClients[i].id===this.state.reboisement.idClient){
                this.setState({ selectedOptionClient:{value:currentClients[i].id, label:currentClients[i].nom} }); 
            }
         }
         this.setState({optionsClients:lCli});

        //select Objectifs
        let lObj=[{value:0,label:""}];
        let currentObjectifs=this.state.objectifs;
        for(i=0;i<currentObjectifs.length;i++){
           lObj.push({value:currentObjectifs[i].id, label:currentObjectifs[i].objectif});
            //init the combobox
            if(currentObjectifs[i].id===this.state.reboisement.idObjectif){
                this.setState({ selectedOptionObjectif:{value:currentObjectifs[i].id, label:currentObjectifs[i].objectif} }); 
            }
        }
        this.setState({optionsObjectifs:lObj});

        //select approches
        let lAppr=[{value:0,label:""}];
        let currentApproches=this.state.approches;
        for(i=0;i<currentApproches.length;i++){
           lAppr.push({value:currentApproches[i].id, label:currentApproches[i].approche});
            //init the combobox
            if(currentApproches[i].id===this.state.reboisement.idApproche){
                this.setState({ selectedOptionApproche:{value:currentApproches[i].id, label:currentApproches[i].approche} }); 
            }
        }
        this.setState({optionsApproches:lAppr});

        //select pepinières
        let lPep=[{value:0,label:""}];
        let currentPepinieres=this.state.pepinieres;
        for(i=0;i<currentPepinieres.length;i++){
           lPep.push({value:currentPepinieres[i].id, label:currentPepinieres[i].nomPepiniere});
            //init the combobox
            if(currentPepinieres[i].id===this.state.reboisement.idPepiniere){
                this.setState({ selectedOptionPepiniere:{value:currentPepinieres[i].id, label:currentPepinieres[i].nomPepiniere} }); 
            }
        }
        this.setState({optionsPepinieres:lPep});

        //select Districts
        let lDis=[{value:0,label:""}];
        let currentDistricts=this.state.districts;
        for(i=0;i<currentDistricts.length;i++){
           lDis.push({value:currentDistricts[i].id, label:currentDistricts[i].nomDistrict});
            //init the combobox
            if(currentDistricts[i].id===this.state.reboisement.idDistrict){
                this.setState({ selectedOptionDistrict:{value:currentDistricts[i].id, label:currentDistricts[i].nomDistrict} }); 
            }
        }
        this.setState({optionsDistricts:lDis});

        //select Commune
        let lCom=[{value:0,label:""}];
        let currentCommunes=this.state.communes;
        for(i=0;i<currentCommunes.length;i++){
           lCom.push({value:currentCommunes[i].id, label:currentCommunes[i].nomCommune});
            //init the combobox
            if(currentCommunes[i].id===this.state.reboisement.idCommune){
                this.setState({ selectedOptionCommune:{value:currentCommunes[i].id, label:currentCommunes[i].nomCommune} }); 
            }
        }
        this.setState({optionsCommunes:lCom});

        //select Fokontany
        let lFok=[{value:0,label:""}];
        let currentFokontany=this.state.fokontany;
        for(i=0;i<currentFokontany.length;i++){
           lFok.push({value:currentFokontany[i].id, label:currentFokontany[i].nomFokontany});
            //init the combobox
            if(currentFokontany[i].id===this.state.reboisement.idFokontany){
                this.setState({ selectedOptionFokontany:{value:currentFokontany[i].id, label:currentFokontany[i].nomFokontany} }); 
            }
        }
        this.setState({optionsFokontany:lFok});

        //select Sites
        let lSite=[{value:0,label:""}];
        let currentSites=this.state.sites;
        for(i=0;i<currentSites.length;i++){
           lSite.push({value:currentSites[i].id, label:currentSites[i].nomSite});
           //init the combobox
           if(currentSites[i].id===this.state.reboisement.idSite){
            this.setState({ selectedOptionSite:{value:currentSites[i].id, label:currentSites[i].nomSite} }); 
        }
        }
        this.setState({optionsSites:lSite});
  
      }

      //Modals
      state = {
        showLocalisation: false
      };
      showLocalisationModal = e => {
        this.setState({
            showLocalisation: !this.state.showLocalisation
        });
      };


    onChangeSurfacePrevue(e){
        this.setState({
            surfacePrevue:e.target.value
        });
    }
    onChangeShapefilePlan(e){
        this.setState({
            shapefilePlan:e.target.value
        });
    }
    onChangeNbPlants(e){
        this.setState({
            nbPlants:e.target.value
        });
    }
    onChangeDateDebut(e){
        this.setState({
            dateDebut:e.target.value
        });
    }
    onChangeDateFin(e){
        this.setState({
            dateFin:e.target.value
        });
    }
    

    


    handleChangeCampagne = selectedOptionCampagne => {
        this.setState({ selectedOptionCampagne , idCampagne:selectedOptionCampagne.value});
    };
    handleChangeInstitution = selectedOptionInstitution => {
        this.setState({ selectedOptionInstitution , idInstitution:selectedOptionInstitution.value});
    };
    handleChangeTypeInstitution = selectedOptionTypeInstitution => {
        this.setState({ selectedOptionTypeInstitution , idTypeInstitution:selectedOptionTypeInstitution.value});
    };
    handleChangeSituationJuridique = selectedOptionSituationJuridique=> {
        this.setState({ selectedOptionSituationJuridique , idSituationJuridique:selectedOptionSituationJuridique.value});
    };
    handleChangeClient = selectedOptionClient => {
        this.setState({ selectedOptionClient , idClient:selectedOptionClient.value});
    };
    handleChangeObjectif = selectedOptionObjectif=> {
        this.setState({ selectedOptionObjectif , idObjectif:selectedOptionObjectif.value});
    };
    handleChangeApproche = selectedOptionApproche=> {
        this.setState({ selectedOptionApproche , idApproche:selectedOptionApproche.value});
    };
    handleChangePepiniere = selectedOptionPepiniere=> {
        this.setState({ selectedOptionPepiniere , idPepiniere:selectedOptionPepiniere.value});
    };
    handleChangeDistrict = selectedOptionDistrict=> {
        this.setState({ selectedOptionDistrict , idDistrict:selectedOptionDistrict.value});
    };
    handleChangeCommune = selectedOptionCommune=> {
        this.setState({ selectedOptionCommune , idCommune:selectedOptionCommune.value});
    };
    handleChangeFokontany = selectedOptionFokontany=> {
        this.setState({ selectedOptionFokontany , idFokontany:selectedOptionFokontany.value});
    };
    handleChangeSite = selectedOptionSite=> {
        this.setState({ selectedOptionSite , idSite:selectedOptionSite.value});
    };
    


    
    
      onSubmit(e){
        e.preventDefault();
     
        //verify if all the required field are not empty
        // if((this.state.diseaseName==='' )||(this.state.description==='')||(this.state.idDoctorSpeciality==='')||(this.state.symptomsChecked.length===0)){
        //     this.setState({errorMessage:'Tous les champs doivent être remplis'});
        // }
        // else{
     
        //         const disease ={
        //             name:this.state.diseaseName,
        //             description:this.state.description,
        //             idDoctorSpeciality:this.state.idDoctorSpeciality,
        //             symptoms:this.state.symptomsChecked
                
        //         };
        //         console.log(disease);
        
        //         axios.post(this.state.beginningURL+'disease/addwithsymptoms',disease)
        //          .then(res => console.log(res.data));
        
        //         window.location="/diagnosticlist";
           
  
        //   }   
    }
    deleteReboisement(){
        alert("La pépinière a été supprimée avec succès.");
    //     axios.delete(this.state.beginningURL+'leSaviezVous/'+this.props.match.params.id)
    //   .then(res => {
    //     console.log(res.data);
    //     window.location="/lesaviezvouslist";
    //   });
    this.setState({openSupprimer:false});
    
    }

    showButtonDelete(){
        return(
            <Button variant="contained" onClick={this.handleClickOpenSupprimer}  style={buttonDeleteStyle}>
                 <img className="imageSupprimer" alt="supprimer" src="/assets/images/logos/LogoDelete.png"/>&nbsp;
                Supprimer
            </Button>
        )
    }

    //Dialog
    handleClickOpenSupprimer  = () => {
        this.setState({openSupprimer:true});
      };
  
      handleCloseSupprimer  = () => {
        this.setState({openSupprimer:false});
      };


    render() {
        //select (combobox)
        const { 
            selectedOptionCampagne,
            selectedOptionInstitution, 
            selectedOptionTypeInstitution,
            selectedOptionSituationJuridique, 
            selectedOptionClient,
            selectedOptionObjectif,
            selectedOptionApproche,
            selectedOptionPepiniere,
            selectedOptionDistrict,
            selectedOptionCommune,
            selectedOptionFokontany,
            selectedOptionSite
        } = this.state; 
        //Table
        const classes = useStyles;

        //form 
        const classesForm = useStylesForm;

        

        return (
            <div className="container-fluid">
                <div className="row bg-title">
                    <div className="left-title col-lg-4 col-md-4 col-sm-4 col-xs-12" >
                        <h4 className="text-left-title page-title">REBOISEMENTS</h4>
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <h3 className="text-middle-title page-title">{this.state.userFrom}</h3>
                    </div>

                    <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                        {/* <a href="https://wrappixel.com/templates/ampleadmin/" target="_blank"
                            className="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Upgrade
                            to Pro

                        </a> */}
                        <ol className="breadcrumb">
                            
                            <li className="active"><a>Reboisement</a>&nbsp;&nbsp;<i className="linking fa fa-angle-right fa-fw"></i>Modification</li>
                        </ol>
                    </div>
                    {/* <!-- /.col-lg-12 --> */}
                </div>
            
                {/* <!-- content of the page--> */}
                {/* Head */}
                <div className="divAppBar" >
                    <AppBar position="static" style={appBarStyle}>
                        <Toolbar>
                            {/* grid */}
                            
                                <Grid container spacing={3}>
                                    <Grid item xs={3} style={{textAlign:'left'}}>
                                    <Link to='/reboisementhistoric'>
                                        <Button variant="contained" style={buttonBackStyle}>
                                            Voir l'historique
                                        </Button>
                                    </Link>
                                    </Grid>
                                    <Grid item xs={6} style={{textAlign:'center'}}>
                                        <h4 className="smaller-title" >Modification reboisement n°{this.state.idReboisement}</h4>
                                    </Grid>
                                    <Grid item xs={3}>  
                                        {this.showButtonDelete()}                                  
                                    </Grid>
                                </Grid>
                            
                            {/* end grid */}
                        
                        </Toolbar>
                    </AppBar>
                </div>
                {/* end head */}
                {/* Dialog supprimer  */}
                <Dialog
                    open={this.state.openSupprimer}
                    onClose={this.handleCloseSupprimer}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                    >
                    <DialogTitle  style={Design.fontStyleDialog} id="alert-dialog-title">{"Suppression de reboisement"}</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description"  style={Design.fontStyleDialog}>
                        Souhaitez-vous vraiment supprimer le reboisement ID={this.state.idReboisement}
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleCloseSupprimer}  style={Design.fontStyleCancelDialog} color="primary">
                        Annuler
                        </Button>
                        <Button onClick={()=> {this.deleteReboisement()}}  style={Design.fontStyleDeleteDialog} color="primary" autoFocus>
                        Supprimer
                        </Button>
                    </DialogActions>
                </Dialog>
                {/* End Dialog supprimer  */}
                {/* Table */}
                <div className="row">
                    <div className="col-sm-12">
                        <div className="white-box formStyleReboisement" >
                        <Container component="main" maxWidth="xs" style={styleContainer}>
                            <Paper className={classes.root}>
                            <form className={classesForm.form} onSubmit={this.onSubmit}  noValidate>
                            
                            <label className="label-style">Campagne</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                    <Select
                                        autofocus
                                        placeholder="Campagne"
                                        value={selectedOptionCampagne}
                                        onChange={this.handleChangeCampagne}
                                        options={this.state.optionsCampagnes}
                                    />
                                </Grid>
                                <Grid item xs={1}></Grid>
                            </Grid>

                            

                            <label className="label-style">Institution</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                    <Select
                                        placeholder="Institution"
                                        value={selectedOptionInstitution}
                                        onChange={this.handleChangeInstitution}
                                        options={this.state.optionsInstitutions}
                                    />
                                </Grid>
                                <Grid item xs={1}></Grid>
                            </Grid>

                            <label className="label-style">DREDD/CIREDD</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                    <Select
                                        placeholder="DREDD/CIREDD"
                                        value={selectedOptionTypeInstitution}
                                        onChange={this.handleChangeTypeInstitution}
                                        options={this.state.optionsTypeInstitutions}
                                    />
                                </Grid>
                                <Grid item xs={1}></Grid>
                            </Grid>

                            <label className="label-style">Situation Juridique</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                    <Select
                                        placeholder="Situation Juridique"
                                        value={selectedOptionSituationJuridique}
                                        onChange={this.handleChangeSituationJuridique}
                                        options={this.state.optionsSituationJuridiques}
                                    />
                                </Grid>
                                <Grid item xs={1}></Grid>
                            </Grid>

                            <label className="label-style">Responsable</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                    <Select
                                        placeholder="Responsable"
                                        value={selectedOptionClient}
                                        onChange={this.handleChangeClient}
                                        options={this.state.optionsClients}
                                    />
                                </Grid>
                                <Grid item xs={1}>
                                    <i className="new-icon-style fa  fa-plus-circle fa-fw"></i>
                                </Grid>
                            </Grid>

                            <label className="label-style">Objectif</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                    <Select
                                        placeholder="Objectif"
                                        value={selectedOptionObjectif}
                                        onChange={this.handleChangeObjectif}
                                        options={this.state.optionsObjectifs}
                                    />
                                </Grid>
                                <Grid item xs={1}></Grid>
                            </Grid>

                            <label className="label-style">Approche</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                    <Select
                                        placeholder="Approche"
                                        value={selectedOptionApproche}
                                        onChange={this.handleChangeApproche}
                                        options={this.state.optionsApproches}
                                    />
                                </Grid>
                                <Grid item xs={1}></Grid>
                            </Grid>

                            <label className="label-style">Surface pr&eacute;vue (Ha)</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    id="surfacePrevue"
                                    label=""
                                    name="surfacePrevue"
                                    autoComplete="surfacePrevue"
                                    type="number"
                                    inputProps={{style: fontStyle}} 
                                    InputLabelProps={{style: fontStyle}} 
                                    value={this.state.surfacePrevue} onChange={this.onChangeSurfacePrevue}
                                />
                                </Grid>
                                <Grid item xs={1}></Grid>
                            </Grid> 

                            <label className="label-style">Shapefile</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    id="shapefileplan"
                                    label=""
                                    placeholder="Shapefile"
                                    name="shapefileplan"
                                    autoComplete="shapefileplan"
                                    inputProps={{style: fontStyle}} 
                                    InputLabelProps={{style: fontStyle}} 
                                    value={this.state.shapefilePlan} onChange={this.onChangeShapefilePlan}
                                />
                                </Grid>
                                <Grid item xs={1}></Grid>
                            </Grid>  

                            <label className="label-style">Nombre de plants</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    id="nbPlants"
                                    name="nbPlants"
                                    autoComplete="nbPlants"
                                    type="number"
                                    inputProps={{style: fontStyle}} 
                                    InputLabelProps={{style: fontStyle}} 
                                    value={this.state.nbPlants} onChange={this.onChangeNbPlants}
                                />
                                </Grid>
                                <Grid item xs={1}></Grid>
                            </Grid>  

                            <label className="label-style">P&eacute;pini&egrave;re</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                    <Select
                                        placeholder="Pépinière"
                                        value={selectedOptionPepiniere}
                                        onChange={this.handleChangePepiniere}
                                        options={this.state.optionsPepinieres}
                                    />
                                </Grid>
                                <Grid item xs={1}></Grid>
                            </Grid>

                            <label className="label-style">Date de d&eacute;but</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    type="date"
                                    id="dateDebut"
                                    name="dateDebut"
                                    autoComplete="dateDebut"
                                    inputProps={{style: fontStyle}} 
                                    InputLabelProps={{style: fontStyle}} 
                                    value={this.state.dateDebut} onChange={this.onChangeDateDebut}
                                />
                                </Grid>
                                <Grid item xs={1}></Grid>
                            </Grid> 
                            <label className="label-style">Date fin</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    type="date"
                                    id="dateFin"
                                    name="dateFin"
                                    autoComplete="dateFin"
                                    inputProps={{style: fontStyle}} 
                                    InputLabelProps={{style: fontStyle}} 
                                    value={this.state.dateFin} onChange={this.onChangeDateFin}
                                />
                                </Grid>
                                <Grid item xs={1}></Grid>
                            </Grid> 

                            <label className="label-style">District</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                    <Select
                                        placeholder="District"
                                        value={selectedOptionDistrict}
                                        onChange={this.handleChangeDistrict}
                                        options={this.state.optionsDistricts}
                                    />
                                </Grid>
                                <Grid item xs={1}></Grid>
                            </Grid>
                            <label className="label-style">Commune</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                    <Select
                                        placeholder="Commune"
                                        value={selectedOptionCommune}
                                        onChange={this.handleChangeCommune}
                                        options={this.state.optionsCommunes}
                                    />
                                </Grid>
                                <Grid item xs={1}></Grid>
                            </Grid>

                            <label className="label-style">Fokontany</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                    <Select
                                        placeholder="Fokontany"
                                        value={selectedOptionFokontany}
                                        onChange={this.handleChangeFokontany}
                                        options={this.state.optionsFokontany}
                                    />
                                </Grid>
                                <Grid item xs={1}></Grid>
                            </Grid>
                            <label className="label-style">Site</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                    <Select
                                        placeholder="Site"
                                        value={selectedOptionSite}
                                        onChange={this.handleChangeSite}
                                        options={this.state.optionsSites}
                                    />
                                </Grid>
                                <Grid item xs={1}>
                                    <i className="new-icon-style fa  fa-plus-circle fa-fw"></i>
                                </Grid>
                            </Grid>
                            <Grid container spacing={1} style={{marginTop:'40px'}} >
                            <Grid item xs={6} >
                              <Button
                                  type="button"
                                  fullWidth
                                  variant="contained" 
                                  style={fontStyle}                                
                              >
                                  Annuler
                              </Button>
                            </Grid>
                            <Grid item xs={6}>
                              <Button
                                  type="submit"
                                  fullWidth
                                  variant="contained"
                                  color="primary"
                                  className={classesForm.submit}
                                  style={fontStyle}
                                  
                              >
                                  Enregistrer
                              </Button>
                            </Grid>
                        </Grid>
                            </form>
                            <br/>
                                
                            </Paper>  
                            {/* end content of the table */}
                            </Container>
                        </div>
                    </div>
                </div>
                {/* end table */}
              
                {/* <!-- /.content of the page --> */}

            </div>
        )
    }
}
