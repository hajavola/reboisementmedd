import React, { Component } from 'react';
import "./ReboisementValidation.css";
import * as Config from './../Config/Config';
import { Link } from 'react-router-dom';
import axios from 'axios';

//Search
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
//grid for the search
import Grid from '@material-ui/core/Grid';

//Table
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';


//autocomplete
import TextField from '@material-ui/core/TextField';



//Table
const useStyles = makeStyles({
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 600,
    },

  });



// form 
const useStylesForm = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));

// end form


//design
let appBarStyle= {borderRadius:'4px',textAlign:'right',backgroundColor:'#000e2f'};
let buttonBackStyle= {backgroundColor:'#0bca4b', color:'white', marginTop:'5px'};
let fontStyle={fontSize:'14px'};
let styleContainer={backgroundColor:'white',borderRadius:'3px'};
let buttonDetailsStyle={backgroundColor:'#ca4f0b', color:'white', marginTop:'5px'};

export default class ReboisementValidation extends Component {
    constructor(props) {
        super(props);
 
         //Data
        
        /*let currentReboisement={
            id:1,
            idInstitution:1,
            nomInstitution:'MEDD',
            idTypeInstitution:1,
            typeInstitution:'DREDD',
            idSituationJuridique:1,
            nomSituation:'NAP',
            coordonneeX:12.256,
            coordonneeY:12.2589,
            idClient:1,
            nom:'JIRAMA',
            idObjectif:1,
            objectif:'Restauration',
            idApproche:1,
            approche:'Communautaire',
            surfacePrevue:2000.2,
            shapefilePlan:'Néant',
            nbPlants:5000,
            dateDebut:'2020-05-20',
            dateFin:'2020-05-21',
            idPepiniere:1,
            nomPepiniere:'Pepiniere 1',
            superficie:2000,
            shapefile:'néant',
            idCampagne:1,
            nomCampagne:'Campagne 2020',
            idRegion:1,
            nomRegion:'DIANA',
            idDistrict:1,
            nomDistrict:'Antsiranana II',
            idCommune:1,
            nomCommune:'Sakaramy',
            idFokontany:1,
            nomFokontany:'Analamerana',
            idSite:1,
            nomSite:'Irodo'

        };*/

        this.state = {
            userFrom:'',
            userRegion:{},
            loggedUser:{},
            idReboisement:this.props.match.params.id,
            reboisement:{},
            dateReboisement:'',
            
            surface:0,
            shapefile:'',
            nbPlants:0,
         
            beginningURL: Config.beginningURL
        };
    
      
        this.onChangeSurface=this.onChangeSurface.bind(this);
        this.onChangeShapefile=this.onChangeShapefile.bind(this);
        this.onChangeNbPlants=this.onChangeNbPlants.bind(this);
       
        this.onSubmit=this.onSubmit.bind(this);
     
  
      

    }
   
    componentDidMount(){
        //Init title for the page
        if(localStorage.getItem("userRegion")!=null){
            this.setState({
              userRegion:JSON.parse(localStorage.getItem("userRegion"))
            });
        }
        if(localStorage.getItem("title")!=null){
            this.setState({
              userFrom:localStorage.getItem("title")
            });
        }
        if(localStorage.getItem("user")!=null){
            this.setState({
              loggedUser:JSON.parse(localStorage.getItem("user"))
            });
        }

       
        axios.get(this.state.beginningURL+'reboisement/single_read.php?id='+this.props.match.params.id)
        .then(response=>{
            this.setState({
                reboisement:response.data,
                surface:response.data.surfacePrevue,
                shapefile:response.data.shapeFilePlan,
                nbPlants:response.data.nbPlants,
                dateReboisement:response.data.dateDebut.substring(0,10)

               
            })
        })
        .catch(function(error){
            console.log(error);
        });
        
       
        
    }

      //Modals
      


    onChangeSurface(e){
        this.setState({
            surface:e.target.value
        });
    }
    onChangeShapefile(e){
        this.setState({
            shapefile:e.target.value
        });
    }
    onChangeNbPlants(e){
        this.setState({
            nbPlants:e.target.value
        });
    }
 
    

    




    
    
      onSubmit(e){
        e.preventDefault();
     
        if(this.state.nbPlants===0 ||this.state.surface===0 || this.state.shapefile==="" ){
            //console.log(this.state.selectedOptionCampagne);
            this.setState({errorMessage:'Veullez remplir tous les champs.'});
        }
        else{
                
                    const reboisement ={
                        id:this.state.idReboisement,
                        nbPlants:this.state.nbPlants,
                        superficie : this.state.surface,
                        shapeFile : this.state.shapefile,
                        userModule:this.state.loggedUser.module,
                        userType:this.state.loggedUser.type,
                        userProfil:this.state.loggedUser.profil
                    };
                


                    //console.log(camp);
            
                    axios.post(this.state.beginningURL+'reboisement/validate.php',reboisement)
                     .then(res => {
                        console.log(res.data);
                        
                        if(res.data.message==="ok"){
                            alert('Le reboisement a bien été validé.');
                            window.location="/reboisementhistoric";
                        }
                        else{
                            alert('Erreur lors de la validation du reboisement');
                        }
                    });
                }     
    
    }
   
  

    render() {
       
        //Table
        const classes = useStyles;

        //form 
        const classesForm = useStylesForm;

        

        return (
            <div className="container-fluid">
                <div className="row bg-title">
                    <div className="left-title col-lg-4 col-md-4 col-sm-4 col-xs-12" >
                        <h4 className="text-left-title page-title">REBOISEMENTS</h4>
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <h3 className="text-middle-title page-title">{this.state.userFrom}</h3>
                    </div>

                    <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                        {/* <a href="https://wrappixel.com/templates/ampleadmin/" target="_blank"
                            className="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Upgrade
                            to Pro

                        </a> */}
                        <ol className="breadcrumb">
                            
                            <li className="active"><a>Reboisement</a>&nbsp;&nbsp;<i className="linking fa fa-angle-right fa-fw"></i>Validation</li>
                        </ol>
                    </div>
                    {/* <!-- /.col-lg-12 --> */}
                </div>
            
                {/* <!-- content of the page--> */}
                {/* Head */}
                <div className="divAppBar" >
                    <AppBar position="static" style={appBarStyle}>
                        <Toolbar>
                            {/* grid */}
                            
                                <Grid container spacing={3}>
                                    <Grid item xs={3} style={{textAlign:'left'}}>
                                    <Link to='/reboisementhistoric'>
                                        <Button variant="contained" style={buttonBackStyle}>
                                            Voir l'historique
                                        </Button>
                                    </Link>
                                    </Grid>
                                    <Grid item xs={6} style={{textAlign:'center'}}>
                                        <h4 className="smaller-title" >Validation du reboisement n°{this.state.idReboisement}</h4>
                                    </Grid>
                                    <Grid item xs={3}>
                                        <Link to={'/reboisementdetails/'+this.state.idReboisement}>
                                        <Button variant="contained" style={buttonDetailsStyle}>
                                            D&eacute;tails
                                        </Button>
                                    </Link></Grid>
                                </Grid>
                            
                            {/* end grid */}
                        
                        </Toolbar>
                    </AppBar>
                </div>
                {/* end head */}
                {/* Table */}
                <div className="row">
                    <div className="col-sm-12">
                        <div className="white-box formStyleReboisement" >
                        <Container component="main" maxWidth="xs" style={styleContainer}>
                            <Paper className={classes.root}>
                           
                            <Grid container spacing={0}  >
                                <Grid item xs={4} >
                                    <h4 className="labelContent1">Campagne: </h4>
                                </Grid>
                                <Grid item xs={8}>
                                    <h5 className="content-style1"> {this.state.reboisement.nomCampagne}</h5><br/>                      
                                </Grid>
                            </Grid>
                            <Grid container spacing={0}  >
                                <Grid item xs={4} >
                                    <h4 className="labelContent1">Responsable: </h4>
                                </Grid>
                                <Grid item xs={8}>
                                    <h5 className="content-style1"> {this.state.reboisement.nomClient}</h5><br/>
                                </Grid>
                            </Grid>
                            <Grid container spacing={0}  >
                                <Grid item xs={4} >
                                    <h4 className="labelContent1">Date: </h4>
                                </Grid>
                                <Grid item xs={8}>
                                    <h5 className="content-style1"> {this.state.dateReboisement}</h5><br/>
                                </Grid>
                            </Grid>
                            <Grid container spacing={0}  >
                                <Grid item xs={4} >
                                    <h4 className="labelContent1">Emplacement: </h4>
                                </Grid>
                                <Grid item xs={8}>
                                    <h5 className="content-style1">
                                    <b>Site:</b> {this.state.reboisement.nomSite}, <br/><br/>
                                    <b>Fokontany:</b> {this.state.reboisement.nomFokontany},<br/><br/>
                                    <b>Commune:</b> {this.state.reboisement.nomCommune}, <br/><br/>
                                    <b>District:</b> {this.state.reboisement.nomDistrict}, <br/><br/> 
                                    <b>R&eacute;gion:</b> {this.state.reboisement.nomRegion}</h5><br/>
                                </Grid>
                            </Grid>
                            <div className=" ">
                                    <h3 className="labelContent1">Formulaire de validation: </h3>
                            </div> 
                                   
                                     
                            
                            <form className={classesForm.form} onSubmit={this.onSubmit}  noValidate>
                            
                           

                            <label className="label-style">Surface rebois&eacute;e (Ha)</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={12} >
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    id="surface"
                                    label=""
                                    name="surface"
                                    autoComplete="surface"
                                    type="number"
                                    inputProps={{style: fontStyle}} 
                                    InputLabelProps={{style: fontStyle}} 
                                    value={this.state.surface} onChange={this.onChangeSurface}
                                />
                                </Grid>
                                <Grid item xs={0}></Grid>
                            </Grid> 

                            <label className="label-style">Shapefile</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={12} >
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    id="shapefile"
                                    label=""
                                    placeholder="Shapefile"
                                    name="shapefile"
                                    autoComplete="shapefile"
                                    inputProps={{style: fontStyle}} 
                                    InputLabelProps={{style: fontStyle}} 
                                    value={this.state.shapefile} onChange={this.onChangeShapefile}
                                />
                                </Grid>
                                <Grid item xs={0}></Grid>
                            </Grid>  

                            <label className="label-style">Nombre de plants</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={12} >
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    id="nbPlants"
                                    name="nbPlants"
                                    autoComplete="nbPlants"
                                    type="number"
                                    inputProps={{style: fontStyle}} 
                                    InputLabelProps={{style: fontStyle}} 
                                    value={this.state.nbPlants} onChange={this.onChangeNbPlants}
                                />
                                </Grid>
                                <Grid item xs={0}></Grid>
                            </Grid>  

                            
                            <Grid container spacing={1} style={{marginTop:'40px'}} >
                            <Grid item xs={6} >
                              <Button
                                  type="button"
                                  fullWidth
                                  variant="contained" 
                                  style={fontStyle}                                
                              >
                                  Annuler
                              </Button>
                            </Grid>
                            <Grid item xs={6}>
                              <Button
                                  type="submit"
                                  fullWidth
                                  variant="contained"
                                  color="primary"
                                  className={classesForm.submit}
                                  style={fontStyle}
                                  
                              >
                                  Valider
                              </Button>
                            </Grid>
                        </Grid>
                            </form>
                            <br/>
                                
                            </Paper>  
                            {/* end content of the table */}
                            </Container>
                        </div>
                    </div>
                </div>
                {/* end table */}
              
                {/* <!-- /.content of the page --> */}

            </div>
        )
    }
}
