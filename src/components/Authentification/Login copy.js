import React, { Component } from 'react';
import "./Login.css";
import axios from 'axios';
import * as Config from './../Config/Config';

//import {Link} from 'react-router-dom';
//Search
import {  makeStyles } from '@material-ui/core/styles';
//grid for the search
import Grid from '@material-ui/core/Grid';
//Form 
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';

import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';

//Design
import Avatar from '@material-ui/core/Avatar';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';

// form 
const useStyles = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));
// end form


export default class Login extends Component {
    constructor(props){
        super(props);
        this.state={
            errorMessage:'',
            identifiant:'',
            motDePasse:'',
            beginningURL: Config.beginningURL

        }
        this.showErrorMessage=this.showErrorMessage.bind(this);
        this.onChangeIdentifiant=this.onChangeIdentifiant.bind(this);
        this.onChangeMotDePasse=this.onChangeMotDePasse.bind(this);
        this.onSubmit=this.onSubmit.bind(this);
    }

    componentDidMount(){}

   
    onChangeIdentifiant(e){
        this.setState({
            identifiant:e.target.value
        });
    }
    onChangeMotDePasse(e){
        this.setState({
            motDePasse:e.target.value
        });
    }
 

    onSubmit(e){
        e.preventDefault();
        //verify if all the required field are not empty
        if((this.state.identifiant==='')||(this.state.motDePasse==='')){
            this.setState({errorMessage:'Tous les champs doivent être remplis'});
        }
        else{
          
                const data ={
                    identifiant:this.state.identifiant,
                    motDePasse:this.state.motDePasse
                };
                console.log(data);
        
                axios.post(this.state.beginningURL+'utilisateur/login.php',data)
                 .then(res => console.log(res.data));
        
                //window.location="/utilisateurlist";
        }

        

        
    }

    showErrorMessage(){
        if(this.state.errorMessage!==''){
            return(
                <Typography component="h5"  style={{marginTop:'20px',color:'red',backgroundColor:'yellow'}}>
                            {this.state.errorMessage} 
                    </Typography>
            );
        }
        else{
            return
        }
        
    }

    render() {
        //form 
        const classes = useStyles;
        return (

        <Container component="main" maxWidth="xs">
        <CssBaseline />
        {this.showErrorMessage()}
        <div className={classes.paper}>
        <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
            Sign in
        </Typography>
        <form className={classes.form} onSubmit={this.onSubmit} noValidate>
            <TextField
                    autoFocus
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    id="identifiant"
                    label="Identifiant"
                    name="identifiant"
                    autoComplete="identifiant"        
                    type="text" required  value={this.state.identifiant} onChange={this.onChangeIdentifiant}/>

            <TextField
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    id="motDePasse"
                    label="Mot de passe"
                    name="motDePasse"
                    autoComplete="motDePasse"      
                    type="password" value={this.state.motDePasse} onChange={this.onChangeMotDePasse}/>


           
            <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            >
            Se connecter
            </Button>
           
        </form>
        </div>       


        </Container>
        )
    }
}
