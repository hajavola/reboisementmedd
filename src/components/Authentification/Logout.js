import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
// import ListItemText from '@material-ui/core/ListItemText';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import "./Logout.css";

import * as Design from './../Config/Design';



//Menu
const StyledMenu = withStyles({
    paper: {
      border: '1px solid #d3d4d5',
     
    },
  })((props) => (
    <Menu
      elevation={0}
      getContentAnchorEl={null}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'center',
      
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'center',
     
      }}
     
      {...props }
    />
  ));
  
  const StyledMenuItem = withStyles((theme) => ({
    root: {
      '&:focus': {
     
        backgroundColor: '#356edf',
        '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
          color: theme.palette.common.white,
         
          
        },
      },
    },
  }))(MenuItem);


let fontStyle1={fontSize:'14px', color:'#2f323e'};
let menuStyle={marginTop:'15px'};
export default class MenuStock extends Component {
    constructor(props){
        super(props);  
        this.state = {  
            nom:'',
            prenoms:'',
            identifiant:'',
            openLogout:false
          
         }  
        
        this.logout=this.logout.bind(this);
       
    }
    componentDidMount(){
        if(localStorage.getItem("user")!=null){
            const user=JSON.parse(localStorage.getItem("user"));
            this.setState({
              identifiant:user.identifiant,
              nom:user.nom,
              prenoms:user.prenoms
            });
        }
    }
    
    //Menu
    state={
        anchorEl:null
    }


    handleClick = (event) => {
        this.setState({anchorEl:event.currentTarget});
    };
  
    handleClose = () => {
        this.setState({anchorEl:null});
    };
    logout(){     
        localStorage.clear();
        window.location="/";       
    }

    handleClickOpenLogoutDialog  = () => {
      this.setState({openLogout:true});
    };

    handleCloseLogoutDialog  = () => {
      this.setState({openLogout:false});
    };


    //end Menu
    render() {
        
        //menu
        const{anchorEl}=this.state;
        return (
            <div className="li-style">
                
                    <div onClick={this.handleClick} >
                    <a  className="profile-pic profile-style" > <img src="assets/images/logos/logoPerson.png" alt="user-img" width="36" className="img-circle"/><b className="hidden-xs">{this.state.nom+' '+this.state.prenoms}</b></a>
                    </div>
                                        <StyledMenu
                                            id="customized-menu"
                                            anchorEl={anchorEl}
                                            keepMounted
                                            open={Boolean(anchorEl)}
                                            onClose={this.handleClose}
                                            style={menuStyle}
                                        >
                                           
                                            <StyledMenuItem  onClick={this.handleClickOpenLogoutDialog}  style={fontStyle1}>
                                                <ListItemIcon> 
                                                <i className="fa fa-power-off m-r-5 m-l-5"></i> 
                                                </ListItemIcon>
                                                <div>Se d&eacute;connecter</div>
                                                {/* <ListItemText primary="Entrée de stock" /> */}
                                            </StyledMenuItem>
                                            
                                            
                                        </StyledMenu>
                  <Dialog
                      open={this.state.openLogout}
                      onClose={this.handleCloseLogoutDialog}
                      aria-labelledby="alert-dialog-title"
                      aria-describedby="alert-dialog-description"
                    >
                      <DialogTitle  style={Design.fontStyleDialog} id="alert-dialog-title">{"Déconnexion"}</DialogTitle>
                      <DialogContent>
                        <DialogContentText id="alert-dialog-description"  style={Design.fontStyleDialog}>
                          Souhaitez-vous vraiment vous déconnecter?
                        </DialogContentText>
                      </DialogContent>
                      <DialogActions>
                        <Button onClick={this.handleCloseLogoutDialog}  style={Design.fontStyleCancelDialog} color="primary">
                          Annuler
                        </Button>
                        <Button onClick={this.logout}  style={Design.fontStyleDialog} color="primary" autoFocus>
                          <i className="fa fa-power-off m-r-5 m-l-5"></i> Me d&eacute;connecter
                        </Button>
                      </DialogActions>
                    </Dialog>
                          
                                        
          </div>
        )
    }
}
