import React, { Component } from 'react';
import "./Login.css";
import axios from 'axios';
import * as Config from './../Config/Config';
import {Redirect} from 'react-router-dom'
import Signature from '../Template/Signature';

import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

// form 
const useStyles = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      marginBottom:'20'
     
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
      textAlign:'center'
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
     
    },
  }));
  
// end form

//style
let fontStyle={fontSize:'14px'};
let fontStyleSubmit={fontSize:'12px'}
let errorMessageStyle={marginTop:'20px',color:'red', backgroundColor: 'rgb(252, 201, 63)',borderRadius:'3px',fontSize:'14px'};
let containerStyle={marginBottom:'20px'}

export default class Login extends Component {
    constructor(props){
        super(props);
        this.state={
            errorMessage:'',
            identifiant:'',
            motDePasse:'',
            beginningURL: Config.beginningURL

        }
        this.showErrorMessage=this.showErrorMessage.bind(this);
        this.onChangeIdentifiant=this.onChangeIdentifiant.bind(this);
        this.onChangeMotDePasse=this.onChangeMotDePasse.bind(this);
        this.onSubmit=this.onSubmit.bind(this);
    }

    componentDidMount(){}

   
    onChangeIdentifiant(e){
        this.setState({
            identifiant:e.target.value
        });
    }
    onChangeMotDePasse(e){
        this.setState({
            motDePasse:e.target.value
        });
    }
 

    onSubmit(e){
        e.preventDefault();
        //verify if all the required field are not empty
        if((this.state.identifiant==='')||(this.state.motDePasse==='')){
            this.setState({errorMessage:'Tous les champs doivent être remplis'});
        }
        else{
          
                const data ={
                    identifiant:this.state.identifiant,
                    motDePasse:this.state.motDePasse
                };
                console.log(data);

                localStorage.setItem("user",JSON.stringify(data));
                localStorage.setItem("loggedIn","true");
                // axios.post(this.state.beginningURL+'utilisateur/login.php',data)
                //  .then(res => console.log(res.data));
        
                window.location="/dashboard";
        }

        

        
    }

    showErrorMessage(){
        if(this.state.errorMessage!==''){
            return(
                <Typography component="h5"  style={errorMessageStyle}>
                            {this.state.errorMessage} 
                    </Typography>
            );
        }
        else{
            return
        }
        
    }

    render() {
        //form 
        const classes = useStyles;
        return (
            <div className="loginStyle">
            <Container component="main" maxWidth="xs"  style={containerStyle}  className="mainContainer ">
                <CssBaseline />
                <div className={classes.paper} >
                    <br/>
                    <img className="imgLogo" src="assets/images/logos/MEDDLogo.jpg" alt="MEDD" />
                    <br/>
                    <Typography component="h1" variant="h5" style={{marginTop:'20px', fontSize:'20px'}}>
                            Veuillez vous connecter ! 
                            <br/>
                            <LockOutlinedIcon style={{marginTop:'20px'}} color="secondary" />
                       
                    </Typography>
                   {this.showErrorMessage()}
                    
                    
                    
                    <form className={classes.form} onSubmit={this.onSubmit} noValidate>
                    <TextField
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    id="identifiant"
                    label="Identifiant"
                    name="identifiant"
                    autoComplete="identifiant"  
                    inputProps={{style: fontStyle}} 
                    InputLabelProps={{style: fontStyle}}       
                    type="text" required  value={this.state.identifiant} onChange={this.onChangeIdentifiant}/>

                <TextField
                    variant="outlined"
                    margin="normal"
                    fullWidth
                    id="motDePasse"
                    label="Mot de passe"
                    name="motDePasse"
                    autoComplete="motDePasse"  
                    inputProps={{style: fontStyle}} 
                    InputLabelProps={{style: fontStyle}} 
                    required    
                    type="password" value={this.state.motDePasse} onChange={this.onChangeMotDePasse}/>

                
                    
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        style={fontStyleSubmit}
                        
                    >
                        Se connecter 
                    </Button>
                    
                   
                    </form>
                    
                </div>
                <Box mt={8} variant="body2" color="textSecondary" style={{marginTop:'100px'}} align="center">
                    <Signature/>
                </Box> 
            </Container>
           
            </div>
         
        
            
        )
    }
}
