import React, { Component } from 'react';
import "./ClientDetails.css";
import * as Config from './../Config/Config';
import { Link } from 'react-router-dom';
import axios from 'axios';

//Search
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
//grid for the search
import Grid from '@material-ui/core/Grid';

//Table
import { makeStyles } from '@material-ui/core/styles';



//Table
const useStyles = makeStyles({
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 440,
    },

  });



// form 
const useStylesForm = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));

// end form


//design
let appBarStyle= {borderRadius:'4px',textAlign:'right',backgroundColor:'#000e2f'};
let buttonBackStyle= {backgroundColor:'#0bca4b', color:'white', marginTop:'5px'};
let fontStyle={fontSize:'14px'};
//container
let containerStyle={backgroundColor:'white', marginTop:'-20px'}
let styleLink={textAlign:'right', fontSize:'14px', textDecoration:'underline', marginTop:'7px'}

export default class ClientDetails extends Component {
    constructor(props) {
        super(props);
        
        //Data
      
        //let currentClient= {id:1,nom: 'JIRAMA',description:'C\'est une entreprise locale du nom de JIRAMA.'};
    
        this.state = {
            userFrom:'',
            userRegion:{},
            idClient:this.props.match.params.id,
            client:{},
          
           
            beginningURL: Config.beginningURL
        };
      

    }
    componentDidMount(){
         //Init title for the page
         if(localStorage.getItem("userRegion")!=null){
            this.setState({
              userRegion:JSON.parse(localStorage.getItem("userRegion"))
            });
        }
        if(localStorage.getItem("title")!=null){
            this.setState({
              userFrom:localStorage.getItem("title")
            });
        }
        axios.get(this.state.beginningURL+'client/single_read.php/?id='+this.props.match.params.id)
        .then(response=>{
            this.setState({
                client:response.data,
               
            })
        })
        .catch(function(error){
            console.log(error);
        });
        
       
    }
  
      
    
      


    render() {
       
        //form 
        const classesForm = useStylesForm;

        

        return (
            <div className="container-fluid">
                <div className="row bg-title">
                    <div className="left-title col-lg-4 col-md-4 col-sm-4 col-xs-12" >
                        <h4 className="text-left-title page-title">Clients</h4>
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <h3 className="text-middle-title page-title">{this.state.userFrom}</h3>
                    </div>

                    <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                        {/* <a href="https://wrappixel.com/templates/ampleadmin/" target="_blank"
                            className="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Upgrade
                            to Pro

                        </a> */}
                        <ol className="breadcrumb">
                            
                            <li className="active"><a>Clients</a>&nbsp;&nbsp;<i className="linking fa fa-angle-right fa-fw"></i>D&eacute;tails</li>
                        </ol>
                    </div>
                    {/* <!-- /.col-lg-12 --> */}
                </div>
            
                {/* <!-- content of the page--> */}
                {/* Head */}
                <div className="divAppBar" >
                    <AppBar position="static" style={appBarStyle}>
                        <Toolbar>
                            {/* grid */}
                            
                                <Grid container spacing={3}>
                                    <Grid item xs={3} style={{textAlign:'left'}}>
                                    <Link to='/clientlist'>
                                        <Button variant="contained" style={buttonBackStyle}>
                                            Revenir liste
                                        </Button>
                                    </Link>
                                    </Grid>
                                    <Grid item xs={6} style={{textAlign:'center'}}>
                                        <h4 className="smaller-title" >D&eacute;tails sur le client n°{this.state.idClient}</h4>
                                    </Grid>
                                    <Grid item xs={3}>                                    
                                    </Grid>
                                </Grid>
                            
                            {/* end grid */}
                        
                        </Toolbar>
                    </AppBar>
                </div>
                {/* end head */}
                {/* Table */}
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box formStyle1" >
                            <Container component="main" maxWidth="md" style={containerStyle}>
                                <div className={classesForm.paper} >                      
                                    <div className="contentPepiniereDetails" >
                                        <div className=" row">
                                            <h4 className="labelContent">Nom du client: </h4>
                                        </div>  
                                        <div className=" row">
                                                {this.state.client.nomClient}<br/>
                                        </div> 
                                        <div className=" row">
                                            <h4 className="labelContent">Description: </h4> 
                                        </div> 
                                        <div className=" row">
                                                {this.state.client.description}<br/>
                                        </div> 
                                        

                                        <Grid container spacing={2} style={{marginTop:'40px'}} >
                                            <Grid item xs={4} > </Grid>
                                            <Grid item xs={4} style={styleLink}>
                                                <Link to={"/reboisementhistoricclient/"+this.state.idClient}>Voir l'historique de reboisement</Link>
                                            </Grid>
                                            <Grid item xs={4}>
                                            <Link to={"/clientedit/"+this.state.idClient}>
                                                <Button
                                                    type="button"
                                                    fullWidth
                                                    variant="contained"
                                                    color="primary"
                                                    className={classesForm.submit}
                                                    style={fontStyle}
                                                >
                                                Modifier
                                                </Button>
                                            </Link>
                                            </Grid>
                                        </Grid> 
                                    </div>
                                    <br/>
                                </div>
                            </Container>
                        </div>
                    </div>
                </div>
                {/* end table */}
              
                {/* <!-- /.content of the page --> */}

            </div>
        )
    }
}
