import React, { Component } from 'react';
import "./NewClient.css";
import * as Config from './../Config/Config';

import { Link } from 'react-router-dom';
import { MDBInput } from 'mdbreact';
import axios from 'axios';

//Search
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
//grid for the search
import Grid from '@material-ui/core/Grid';

//Table
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

//autocomplete
import TextField from '@material-ui/core/TextField';
//import Autocomplete from '@material-ui/lab/Autocomplete';

import Typography from '@material-ui/core/Typography';


//Table
const useStyles = makeStyles({
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 600,
    },

  });



// form 
const useStylesForm = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));

// end form


//design
let appBarStyle= {borderRadius:'4px',textAlign:'right',backgroundColor:'#000e2f'};
let buttonBackStyle= {backgroundColor:'#0bca4b', color:'white', marginTop:'5px'};
let fontStyle={fontSize:'14px'};
let fontStyle2={fontSize:'16px', fontFamily:'Arial'};
let errorMessageStyle={marginTop:'20px',color:'red', backgroundColor: 'rgb(252, 201, 63)',borderRadius:'3px',fontSize:'14px'};


export default class NewClient extends Component {
    constructor(props) {
        super(props);
 
        
    
        this.state = {
            userFrom:'',
            userRegion:{},
            errorMessage:'',
            nomClient:'',
            description:'',
               
            beginningURL: Config.beginningURL
        };
        this.showErrorMessage=this.showErrorMessage.bind(this);
        this.onChangeNomClient=this.onChangeNomClient.bind(this);
        this.onChangeDescription=this.onChangeDescription.bind(this);
      
        this.onSubmit=this.onSubmit.bind(this);
        this.annuler=this.annuler.bind(this);
      
      
        
      

    }
    componentDidMount(){
          //Init title for the page
          if(localStorage.getItem("userRegion")!=null){
            this.setState({
              userRegion:JSON.parse(localStorage.getItem("userRegion"))
            });
        }
        if(localStorage.getItem("title")!=null){
            this.setState({
              userFrom:localStorage.getItem("title")
            });
        }
        this.setState({
            nomClient:'',
            description:''
        });
        
        
       
      }
      annuler(){
        //on annule tout
        this.componentDidMount();
    }


    

    onChangeNomClient(e){
        this.setState({
            nomClient:e.target.value
        });
    }
    onChangeDescription(e){
        this.setState({
            description:e.target.value
        });
    }
    showErrorMessage(){
        if(this.state.errorMessage!==''){
            return(
                <Typography component="h5"  style={errorMessageStyle}>
                            {this.state.errorMessage} 
                    </Typography>
            );
        }
        else{
            return
        }
        
    }
 

   
      onSubmit(e){
        e.preventDefault();
        if((this.state.nomClient==='' )||(this.state.description==='')){
            this.setState({errorMessage:'Tous les champs doivent être remplis, à part le prénom'});
        }
        else{
                
                    const client ={
                        nomClient:this.state.nomClient,
                        description:this.state.description
                       
                    };
                    //console.log(utilisateur);
            
                    axios.post(this.state.beginningURL+'client/create.php',client)
                     .then(res => {
                        console.log(res.data);
                        if(res.data==="Client could not be created."){
                            alert('Erreur lors de l\'insertion du client.Veuillez réessayer.');
                        }
                        else{
                            alert('Insertion du nouveau client achevée avec succès');
                            window.location="/clientlist";
                        }
                    });
                    
                   

                
          
                
                
        }  
    
    }


    render() {
      
        //Table
        const classes = useStyles;

        //form 
        const classesForm = useStylesForm;

        

        return (
            <div className="container-fluid">
                <div className="row bg-title">
                    <div className="left-title col-lg-4 col-md-4 col-sm-4 col-xs-12" >
                        <h4 className="text-left-title page-title">Clients</h4>
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <h3 className="text-middle-title page-title">{this.state.userFrom}</h3>
                    </div>

                    <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                        {/* <a href="https://wrappixel.com/templates/ampleadmin/" target="_blank"
                            className="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Upgrade
                            to Pro

                        </a> */}
                        <ol className="breadcrumb">
                            
                            <li className="active"><a>Clients</a>&nbsp;&nbsp;<i className="linking fa fa-angle-right fa-fw"></i>Nouveau</li>
                        </ol>
                    </div>
                    {/* <!-- /.col-lg-12 --> */}
                </div>
            
                {/* <!-- content of the page--> */}
                {/* Head */}
                <div className="divAppBar" >
                    <AppBar position="static" style={appBarStyle}>
                        <Toolbar>
                            {/* grid */}
                            
                                <Grid container spacing={3}>
                                    <Grid item xs={3} style={{textAlign:'left'}}>
                                    <Link to='/clientlist'>
                                        <Button variant="contained" style={buttonBackStyle}>
                                            Revenir liste
                                        </Button>
                                    </Link>
                                    </Grid>
                                    <Grid item xs={6} style={{textAlign:'center'}}>
                                        <h4 className="smaller-title" >Nouveau client</h4>
                                    </Grid>
                                    <Grid item xs={3}>                                    
                                    </Grid>
                                </Grid>
                            
                            {/* end grid */}
                        
                        </Toolbar>
                    </AppBar>
                </div>
                {/* end head */}
                {/* Table */}
                <div className="row">
                    <div className="col-sm-12">
                        <div className="white-box formStyleClient" >
                        <Container component="main" maxWidth="xs" style={{backgroundColor:'white',borderRadius:'3px'}}>
                            <Paper className={classes.root}>
                            {this.showErrorMessage()}
                            <form className={classesForm.form} onSubmit={this.onSubmit}  noValidate>
                            <label className="label-style">Nom du client</label>
                            
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    id="nomclient"
                                    label="Nom du client"
                                    name="nomclient"
                                    autoComplete="nomclient"
                                    autoFocus
                                    inputProps={{style: fontStyle}} 
                                    InputLabelProps={{style: fontStyle}} 
                                    value={this.state.nomClient} onChange={this.onChangeNomClient}
                                />
                             <label className="label-style">Description</label>
                             <MDBInput type="textarea"  style={fontStyle2}  placeholder="Description" rows="5" value={this.state.description} onChange={this.onChangeDescription} />

                               
                      
                         

                            <Grid container spacing={1} style={{marginTop:'40px'}} >
                            <Grid item xs={6} >
                              <Button
                                  type="button"
                                  fullWidth
                                  variant="contained" 
                                  style={fontStyle}   
                                  onClick={()=> {this.annuler()}}                              
                              >
                                  Annuler
                              </Button>
                            </Grid>
                            <Grid item xs={6}>
                              <Button
                                  type="submit"
                                  fullWidth
                                  variant="contained"
                                  color="primary"
                                  className={classesForm.submit}
                                  style={fontStyle}
                                  
                              >
                                  Enregistrer
                              </Button>
                            </Grid>
                        </Grid>
                            </form>
                            <br/>
                                
                            </Paper>  
                            {/* end content of the table */}
                            </Container>
                        </div>
                    </div>
                </div>
                {/* end table */}
              
                {/* <!-- /.content of the page --> */}

            </div>
        )
    }
}
