import React, { Component } from 'react';
import "./ClientEdit.css";
import * as Config from './../Config/Config';
import * as Design from './../Config/Design';

import { Link } from 'react-router-dom';
import { MDBInput } from 'mdbreact';
import axios from 'axios';
import Typography from '@material-ui/core/Typography';


//Search
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
//grid for the search
import Grid from '@material-ui/core/Grid';

//Table
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';


//autocomplete
import TextField from '@material-ui/core/TextField';

//Dialog
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';



//Table
const useStyles = makeStyles({
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 600,
    },

  });



// form 
const useStylesForm = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));

// end form


//design
let appBarStyle= {borderRadius:'4px',textAlign:'right',backgroundColor:'#000e2f'};
let buttonBackStyle= {backgroundColor:'#0bca4b', color:'white', marginTop:'5px'};
let fontStyle={fontSize:'14px'};
let fontStyle2={fontSize:'16px', fontFamily:'Arial'};
let buttonDeleteStyle= {backgroundColor:'#e20f00', color:'white', marginTop:'5px'};
let errorMessageStyle={marginTop:'20px',color:'red', backgroundColor: 'rgb(252, 201, 63)',borderRadius:'3px',fontSize:'14px'};


export default class ClientEdit extends Component {
    constructor(props) {
        super(props);
 
        
        //let currentClient= {id:1,nom: 'JIRAMA',description:'C\'est une entreprise locale du nom de JIRAMA.'};
    
        this.state = {
            userFrom:'',
            userRegion:{},
            errorMessage:'',
            idClient:this.props.match.params.id,
            openSupprimer:false,
            client:{},
            nomClient:'',
            description:'',
               
            beginningURL: Config.beginningURL
        };
        this.showErrorMessage=this.showErrorMessage.bind(this);
        this.onChangeNomClient=this.onChangeNomClient.bind(this);
        this.onChangeDescription=this.onChangeDescription.bind(this);
        this.deleteClient=this.deleteClient.bind(this);
        this.showButtonDelete=this.showButtonDelete.bind(this);
        this.onSubmit=this.onSubmit.bind(this);
        this.annuler=this.annuler.bind(this);
 
      
        
      

    }
    componentDidMount(){
        axios.get(this.state.beginningURL+'client/single_read.php/?id='+this.props.match.params.id)
        .then(response=>{
            this.setState({
                client:response.data,
                nomClient:response.data.nomClient,
                description:response.data.description
               
            })
        })
        .catch(function(error){
            console.log(error);
        });
     
     

        //Init title for the page
        if(localStorage.getItem("userRegion")!==null){
            this.setState({
              userRegion:JSON.parse(localStorage.getItem("userRegion"))
            });
        }
        if(localStorage.getItem("title")!=null){
            this.setState({
              userFrom:localStorage.getItem("title")
            });
        }
        
       
    }

    

    onChangeNomClient(e){
        this.setState({
            nomClient:e.target.value
        });
    }
    onChangeDescription(e){
        this.setState({
            description:e.target.value
        });
    }
 

   
      onSubmit(e){
        e.preventDefault();

        //verify if all the required field are not empty
        if((this.state.nomClient==='' )||(this.state.description==='')){
            this.setState({errorMessage:'Tous les champs doivent être remplis, à part le prénom'});
        }
        else{
                
                    const client ={
                        id:this.state.idClient,
                        nomClient:this.state.nomClient,
                        description:this.state.description
                       
                    };
                    //console.log(utilisateur);
            
                    axios.post(this.state.beginningURL+'client/update.php',client)
                     .then(res => {
                        console.log(res.data);
                        if(res.data==="Data could not be updated"){
                            alert('Erreur lors de la modification du client.Veuillez réessayer.');
                        }
                        else{
                            alert('Mis à jour du client achevé');
                            window.location="/clientlist";
                        }
                    });
                    
                   

                
          
                
                
        }  
    }
    
    deleteClient(){
       
        axios.delete(this.state.beginningURL+'client/delete.php',{data:{id:this.props.match.params.id}})
        .then(res => {
             console.log(res.data);
             alert("Le client a été supprimé avec succès.");
             this.setState({openSupprimer:false});
             window.location="/clientlist";
         });
    
    }
    annuler(){
        //on annule tout
        this.componentDidMount();
    }



    showButtonDelete(){
        return(
            <Button variant="contained" onClick={this.handleClickOpenSupprimer} style={buttonDeleteStyle}>
                 <img className="imageSupprimer" alt="supprimer" src="/assets/images/logos/LogoDelete.png"/>&nbsp;
                Supprimer
            </Button>
        )
    }
    showErrorMessage(){
        if(this.state.errorMessage!==''){
            return(
                <Typography component="h5"  style={errorMessageStyle}>
                            {this.state.errorMessage} 
                    </Typography>
            );
        }
        else{
            return
        }
        
    }

    //dialog 
    handleClickOpenSupprimer  = () => {
        this.setState({openSupprimer:true});
      };
  
      handleCloseSupprimer  = () => {
        this.setState({openSupprimer:false});
      };


    render() {
      
        //Table
        const classes = useStyles;

        //form 
        const classesForm = useStylesForm;

        

        return (
            <div className="container-fluid">
                <div className="row bg-title">
                    <div className="left-title col-lg-4 col-md-4 col-sm-4 col-xs-12" >
                        <h4 className="text-left-title page-title">Clients</h4>
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <h3 className="text-middle-title page-title">{this.state.userFrom}</h3>
                    </div>

                    <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                        {/* <a href="https://wrappixel.com/templates/ampleadmin/" target="_blank"
                            className="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Upgrade
                            to Pro

                        </a> */}
                        <ol className="breadcrumb">
                            
                            <li className="active"><a>Clients</a>&nbsp;&nbsp;<i className="linking fa fa-angle-right fa-fw"></i>Modification</li>
                        </ol>
                    </div>
                    {/* <!-- /.col-lg-12 --> */}
                </div>
            
                {/* <!-- content of the page--> */}
                {/* Head */}
                <div className="divAppBar" >
                    <AppBar position="static" style={appBarStyle}>
                        <Toolbar>
                            {/* grid */}
                            
                                <Grid container spacing={3}>
                                    <Grid item xs={3} style={{textAlign:'left'}}>
                                    <Link to='/clientlist'>
                                        <Button variant="contained" style={buttonBackStyle}>
                                            Revenir liste
                                        </Button>
                                    </Link>
                                    </Grid>
                                    <Grid item xs={6} style={{textAlign:'center'}}>
                                        <h4 className="smaller-title" >Modification client n°{this.state.idClient}</h4>
                                    </Grid>
                                    <Grid item xs={3}>     
                                    {this.showButtonDelete()}                                  
                                    </Grid>
                                </Grid>
                            
                            {/* end grid */}
                        
                        </Toolbar>
                    </AppBar>
                </div>
                {/* end head */}
                {/* Dialog delete client */}
                <Dialog
                    open={this.state.openSupprimer}
                    onClose={this.handleCloseSupprimer}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                    >
                    <DialogTitle  style={Design.fontStyleDialog} id="alert-dialog-title">{"Suppression de client"}</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description"  style={Design.fontStyleDialog}>
                        Souhaitez-vous vraiment supprimer le client ID={this.state.idClient}?
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleCloseSupprimer}  style={Design.fontStyleCancelDialog} color="primary">
                        Annuler
                        </Button>
                        <Button onClick={()=> {this.deleteClient()}} style={Design.fontStyleDeleteDialog} color="primary" autoFocus>
                        Supprimer
                        </Button>
                    </DialogActions>
                    </Dialog>
                {/* End Dialog delete client */}
                {/* Table */}
                <div className="row">
                    <div className="col-sm-12">
                        <div className="white-box formStyleClient" >
                        <Container component="main" maxWidth="xs" style={{backgroundColor:'white',borderRadius:'3px'}}>
                            <Paper className={classes.root}>
                            {this.showErrorMessage()}
                            <form className={classesForm.form} onSubmit={this.onSubmit}  noValidate>
                            <label className="label-style">Nom du client</label>
                            
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    id="nomclient"
                                    label="Nom du client"
                                    name="nomclient"
                                    autoComplete="nomclient"
                                    autoFocus
                                    required
                                    inputProps={{style: fontStyle}} 
                                    InputLabelProps={{style: fontStyle}} 
                                    value={this.state.nomClient} onChange={this.onChangeNomClient}
                                />
                             <label className="label-style">Description</label>
                             <MDBInput type="textarea" required style={fontStyle2}  placeholder="Description" rows="5" value={this.state.description} onChange={this.onChangeDescription} />

                               
                      
                         

                            <Grid container spacing={1} style={{marginTop:'40px'}} >
                            <Grid item xs={6} >
                              <Button
                                  type="button"
                                  fullWidth
                                  variant="contained" 
                                  style={fontStyle}   
                                  onClick={()=> {this.annuler()}}                              
                              >
                                  Annuler
                              </Button>
                            </Grid>
                            <Grid item xs={6}>
                              <Button
                                  type="submit"
                                  fullWidth
                                  variant="contained"
                                  color="primary"
                                  className={classesForm.submit}
                                  style={fontStyle}
                                  
                              >
                                  Enregistrer
                              </Button>
                            </Grid>
                        </Grid>
                            </form>
                            <br/>
                                
                            </Paper>  
                            {/* end content of the table */}
                            </Container>
                        </div>
                    </div>
                </div>
                {/* end table */}
              
                {/* <!-- /.content of the page --> */}

            </div>
        )
    }
}
