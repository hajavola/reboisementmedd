import React, { Component } from 'react';
import "./UtilisateurEdit.css";
import * as Config from './../Config/Config';
import * as Design from './../Config/Design';

import { Link } from 'react-router-dom';
//import { MDBInput } from 'mdbreact';
import axios from 'axios';

//Search
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
//grid for the search
import Grid from '@material-ui/core/Grid';

//Table
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';


//select
//import Select from 'react-select';

//autocomplete
import TextField from '@material-ui/core/TextField';
//import Autocomplete from '@material-ui/lab/Autocomplete';

//Dialog 
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';



//Table
const useStyles = makeStyles({
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 600,
    },

  });



// form 
const useStylesForm = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));

// end form


//design
let appBarStyle= {borderRadius:'4px',textAlign:'right',backgroundColor:'#000e2f'};
let buttonBackStyle= {backgroundColor:'#0bca4b', color:'white', marginTop:'5px'};
let fontStyle={fontSize:'14px'};
let errorMessageStyle={marginTop:'20px',color:'red', backgroundColor: 'rgb(252, 201, 63)',borderRadius:'3px',fontSize:'14px'};
let buttonDeleteStyle= {backgroundColor:'#e20f00', color:'white', marginTop:'5px'};


export default class UtilisateurEdit extends Component {
    constructor(props) {
        super(props);
 
        
        //let currentUtilisateur=  {id:3,nom: 'Rakoto', prenoms:'Angel', identifiant:'angel'};
        this.state = {
            userFrom:'',
            userRegion:{},
            loggedUser:{},
            idUtilisateur:this.props.match.params.id,
            openSupprimer:false,
            utilisateur:{},
            errorMessage:'',
            nom:'',
            prenoms:'',
            identifiant:'',
            motDePasse:'',
            motDePasseValidation:'',
            beginningURL: Config.beginningURL
        };
        
        this.showErrorMessage=this.showErrorMessage.bind(this);
        this.onChangeNom=this.onChangeNom.bind(this);
        this.onChangePrenoms=this.onChangePrenoms.bind(this);
        this.onChangeIdentifiant=this.onChangeIdentifiant.bind(this);
        this.onChangeMotDePasse=this.onChangeMotDePasse.bind(this);
        this.onChangeMotDePasseValidation=this.onChangeMotDePasseValidation.bind(this);
        this.deleteUtilisateur=this.deleteUtilisateur.bind(this);
        this.showButtonDelete=this.showButtonDelete.bind(this);
        this.annuler=this.annuler.bind(this);
       

        this.onSubmit=this.onSubmit.bind(this);
 
      
        
      

    }
    componentDidMount(){
         //Init title for the page
         if(localStorage.getItem("userRegion")!=null){
            this.setState({
              userRegion:JSON.parse(localStorage.getItem("userRegion"))
            });
        }
        if(localStorage.getItem("title")!=null){
            this.setState({
              userFrom:localStorage.getItem("title")
            });
        }
        if(localStorage.getItem("user")!=null){
            this.setState({
              loggedUser:JSON.parse(localStorage.getItem("user"))
            });
            if(parseInt(JSON.parse(localStorage.getItem("user")).estAdmin)!==1){
                window.location="/";
            }
        }
        /*this.setState({
            nom:this.state.utilisateur.nom,
            prenoms:this.state.utilisateur.prenoms,
            identifiant:this.state.utilisateur.identifiant
        });*/

        axios.get(this.state.beginningURL+'utilisateur/single_read.php/?id='+this.props.match.params.id)
        .then(response=>{
            this.setState({
                utilisateur:response.data,
                nom:response.data.nom,
                prenoms:response.data.prenoms,
                identifiant:response.data.identifiant
            })
        })
        .catch(function(error){
            console.log(error);
        });
        
       
      }
      annuler(){
        //on annule tout
        this.componentDidMount();
    }


    

    onChangeNom(e){
        this.setState({
            nom:e.target.value
        });
    }
    onChangePrenoms(e){
        this.setState({
            prenoms:e.target.value
        });
    }
    onChangeIdentifiant(e){
        this.setState({
            identifiant:e.target.value
        });
    }
    onChangeMotDePasse(e){
        this.setState({
            motDePasse:e.target.value
        });
    }
    onChangeMotDePasseValidation(e){
        this.setState({
            motDePasseValidation:e.target.value
        });
    }
   
      onSubmit(e){     
        e.preventDefault();
        //verify if all the required field are not empty
        if((this.state.nom==='' )||(this.state.identifiant==='')||(this.state.motDePasse==='')||(this.state.motDePasseValidation==='')){
            this.setState({errorMessage:'Tous les champs doivent être remplis, à part le prénom.'});
        }
        else{
            if(this.state.motDePasse!==this.state.motDePasseValidation){
                this.setState({errorMessage:'Mots de passe non valide, veuillez revalider le mot de passe'});
                
            }
            else{
                const utilisateur ={
                    id:this.state.idUtilisateur,
                    idRegion:this.state.loggedUser.idRegion,
                    nom:this.state.nom,
                    prenoms:this.state.prenoms,
                    identifiant:this.state.identifiant,
                    motDePasse:this.state.motDePasse,
                    module:this.state.loggedUser.module,
                    type:this.state.loggedUser.type,
                    profil:this.state.loggedUser.profil,
                    estAdmin:0
                   
                };
                //console.log(utilisateur);
        
                axios.post(this.state.beginningURL+'utilisateur/update.php',utilisateur)
                 .then(res => {
                    console.log(res.data);
                 
                    window.location="/utilisateurlist";

                    if(res.data==="Data could not be updated"){
                        alert('Erreur lors de la mise à jour de l\'utilisateur. Veuillez réessayer.');
                    }
                    else{
                        alert('Nouveau utilisateur mis à jour avec succès');
                        window.location="/utilisateurlist";
                    }
                
                });
                
              

            }
        }
    }

    showErrorMessage(){
        if(this.state.errorMessage!==''){
            return(
                <Typography component="h5"  style={errorMessageStyle}>
                            {this.state.errorMessage} 
                    </Typography>
            );
        }
        else{
            return
        }
        
    }
    deleteUtilisateur(){
        axios.delete(this.state.beginningURL+'utilisateur/delete.php',{data:{id:this.props.match.params.id}})
       .then(res => {
            console.log(res.data);
            this.setState({openSupprimer:false});
            alert("L'utilisateur a été supprimé avec succès.");
            window.location="/utilisateurlist";
        });
    
    }

    showButtonDelete(){
        return(
            <Button variant="contained" onClick={this.handleClickOpenSupprimer}  style={buttonDeleteStyle}>
                 <img className="imageSupprimer" alt="supprimer" src="/assets/images/logos/LogoDelete.png"/>&nbsp;
                Supprimer
            </Button>
        )
    }
    //Dialog 
     
    handleClickOpenSupprimer  = () => {
        this.setState({openSupprimer:true});
      };
  
    handleCloseSupprimer  = () => {
        this.setState({openSupprimer:false});
      };

    render() {
      
        //Table
        const classes = useStyles;

        //form 
        const classesForm = useStylesForm;

        

        return (
            <div className="container-fluid">
                <div className="row bg-title">
                    <div className="left-title col-lg-4 col-md-4 col-sm-4 col-xs-12" >
                        <h4 className="text-left-title page-title">Utilisateurs</h4>
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <h3 className="text-middle-title page-title">{this.state.userFrom}</h3>
                    </div>

                    <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                        {/* <a href="https://wrappixel.com/templates/ampleadmin/" target="_blank"
                            className="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Upgrade
                            to Pro

                        </a> */}
                        <ol className="breadcrumb">
                            
                            <li className="active"><a>Utilisateurs</a>&nbsp;&nbsp;<i className="linking fa fa-angle-right fa-fw"></i>Modification</li>
                        </ol>
                    </div>
                    {/* <!-- /.col-lg-12 --> */}
                </div>
            
                {/* <!-- content of the page--> */}
                {/* Head */}
                <div className="divAppBar" >
                    <AppBar position="static" style={appBarStyle}>
                        <Toolbar>
                            {/* grid */}
                            
                                <Grid container spacing={3}>
                                    <Grid item xs={3} style={{textAlign:'left'}}>
                                    <Link to='/utilisateurlist'>
                                        <Button variant="contained" style={buttonBackStyle}>
                                            Revenir liste
                                        </Button>
                                    </Link>
                                    </Grid>
                                    <Grid item xs={6} style={{textAlign:'center'}}>
                                        <h4 className="smaller-title" >Modification de l'utilisateur n°{this.state.idUtilisateur}</h4>
                                    </Grid>
                                    <Grid item xs={3}>  
                                        {this.showButtonDelete()}                                   
                                    </Grid>
                                </Grid>
                            
                            {/* end grid */}
                        
                        </Toolbar>
                    </AppBar>
                </div>
                {/* end head */}
                {/* Dialog supprimer  */}
                <Dialog
                    open={this.state.openSupprimer}
                    onClose={this.handleCloseSupprimer}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                    >
                    <DialogTitle  style={Design.fontStyleDialog} id="alert-dialog-title">{"Suppression d'un utilisateur"}</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description"  style={Design.fontStyleDialog}>
                        Souhaitez-vous vraiment supprimer l'utilisateur ID={this.state.idUtilisateur}?
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleCloseSupprimer}  style={Design.fontStyleCancelDialog} color="primary">
                        Annuler
                        </Button>
                        <Button onClick={()=> {this.deleteUtilisateur()}}  style={Design.fontStyleDeleteDialog} color="primary" autoFocus>
                        Supprimer
                        </Button>
                    </DialogActions>
                    </Dialog>
                {/* End Dialog supprimer */}
                {/* Table */}
                <div className="row">
                    <div className="col-sm-12">
                        <div className="white-box formStyleUtilisateur" >
                        <Container component="main" maxWidth="xs" style={{backgroundColor:'white',borderRadius:'3px'}}>
                            
                            <Paper className={classes.root}>
                            {this.showErrorMessage()}
                            <form className={classesForm.form} onSubmit={this.onSubmit} noValidate>                       
                                <label className="label-style">Nom</label>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    id="nom"
                                    label="Nom"
                                    name="nom"
                                    autoComplete="nom"
                                    autoFocus 
                                    required
                                    inputProps={{style: fontStyle}} 
                                    InputLabelProps={{style: fontStyle}}
                                    type="text" value={this.state.nom} onChange={this.onChangeNom}/>
                                <label className="label-style">Pr&eacute;nom(s)</label>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    id="prenoms"
                                    label="Prénom(s)"
                                    name="prenoms"
                                    autoComplete="prenoms"
                                    inputProps={{style: fontStyle}} 
                                    InputLabelProps={{style: fontStyle}}
                                    type="text"   value={this.state.prenoms} onChange={this.onChangePrenoms}/>

                                <label className="label-style">Identifiant</label>
                            
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    id="identifiant"
                                    label="Identifiant"
                                    name="identifiant"
                                    autoComplete="identifiant"
                                    required
                                    inputProps={{style: fontStyle}} 
                                    InputLabelProps={{style: fontStyle}}
                                    type="text"   value={this.state.identifiant} onChange={this.onChangeIdentifiant}/>

                                <label className="label-style">Mot de passe</label>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    required
                                    id="motDePasse"
                                    label="Mot de passe"
                                    name="motDePasse"
                                    autoComplete="motDePasse"
                                    inputProps={{style: fontStyle}} 
                                    InputLabelProps={{style: fontStyle}}
                                    type="password" value={this.state.motDePasse} onChange={this.onChangeMotDePasse}/>
                                
                                <label className="label-style">Valider le mot de passe</label>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    required
                                    id="motDePasseValidation"
                                    label="Mot de passe validation"
                                    name="motDePasseValidation"
                                    autoComplete="motDePasseValidation"
                                    inputProps={{style: fontStyle}} 
                                    InputLabelProps={{style: fontStyle}}
                                    type="password" value={this.state.motDePasseValidation} onChange={this.onChangeMotDePasseValidation}/>
                        
                            
                                <Grid container spacing={1} style={{marginTop:'40px'}} >
                                    <Grid item xs={6} >
                                        <Link to="/utilisateurlist">
                                            <Button
                                            type="button"
                                            fullWidth
                                            variant="contained" 
                                            style={fontStyle}   
                                            onClick={()=> {this.annuler()}}                              
                                            > 
                                                Annuler
                                            </Button>
                                        </Link>
                                    </Grid>
                                    <Grid item xs={6}>
                                    <Button
                                        type="submit"
                                        fullWidth
                                        variant="contained"
                                        color="primary"
                                        className={classesForm.submit}
                                        style={fontStyle}
                                    >
                                        Enregistrer
                                    </Button>
                                    </Grid>
                                </Grid>
                                </form>
                             <br/>
                                
                            </Paper>  
                            {/* end content of the table */}
                            </Container>
                        </div>
                    </div>
                </div>
                {/* end table */}
              
                {/* <!-- /.content of the page --> */}

            </div>
        )
    }
}
