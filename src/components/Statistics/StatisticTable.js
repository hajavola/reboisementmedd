import React, { Component } from 'react';
import "./StatisticTable.css";
import * as Config from './../Config/Config';
import axios from 'axios';

//select
import Select from 'react-select';
//Search
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
//grid for the search
import Grid from '@material-ui/core/Grid';

//Table
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
// import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';


const  StatisticItem= props =>(
     <TableRow hover role="checkbox" tabIndex={-1} key={props.numero}>
        <TableCell style={tableCellStyle}>{props.statistiques.idRegion}</TableCell>
        <TableCell style={tableCellStyle}>{props.statistiques.nomRegion}</TableCell>
        <TableCell style={tableCellStyle}>{props.statistiques.nomCampagne}</TableCell>
        <TableCell style={tableCellStyle}>{props.statistiques.nbPlants}</TableCell>
        <TableCell style={tableCellStyle}>{props.statistiques.surface}</TableCell>
        {/* <TableCell style={tableCellStyle}>{props.showDetailsEspece(props.statistiques.espece)}</TableCell> */}
    </TableRow>
)

//Table
const useStyles = makeStyles({
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 440,
    },

  });
//design
let appBarStyle= {borderRadius:'4px',textAlign:'right',backgroundColor:'#000e2f'};
//grid
let gridSelectStyle={marginBottom:'15px'};
//table
let tableCellTitleStyle={fontSize:'14px',fontFamily:'Segoe UI'};
let tableRowTitleStyle={backgroundColor:'rgb(245, 245, 245)'};
//let tableCellTitleActionColumnStyle={fontSize:'14px',fontFamily:'Segoe UI', textAlign:'center'};
let tableCellStyle={fontSize:'13px', color:'rgb(90, 88, 88)', fontFamily:'Segoe UI' };
//let tableCellActionColumnStyle={fontSize:'13px', color:'rgb(90, 88, 88)', fontFamily:'Segoe UI', textAlign:'center' };
//let buttonDetailsStyle={backgroundColor:'#ca4f0b', color:'white', marginTop:'5px'};

export default class StatisticTable extends Component {
    constructor(props) {
        super(props);
        //data
       
        /*let lcampagnes=[
            {id:0,nomCampagne: 'Toutes Campagnes'},
            {id:1,nomCampagne: 'Campagne 2020'},
            {id:2,nomCampagne: 'Campagne 2019'},
            {id:3,nomCampagne: 'Campagne 2018'}, 
        ];
        let lregions=[
            {id:0,nomRegion: 'Toutes'},
            {id:1,nomRegion: 'Analamanga'},
            {id:2,nomRegion: 'Bongolava'},
            {id:3,nomRegion: 'SAVA'}, 
        ];
        let lStat=[
            {idRegion:6,nomRegion:"Analamanga",nomCampagne:'Campagne 2020',nbPlants:15000, surface:50, espece:[{nomEspece:'Sapin', nombre:10000},{nomEspece:'Cactus', nombre:3000},{nomEspece:'Fruit', nombre:2000}]},
            {idRegion:3,nomRegion:"Bongolava",nomCampagne:'Campagne 2020',nbPlants:18000, surface:100, espece:[{nomEspece:'Sapin', nombre:11000},{nomEspece:'Cactus', nombre:4000},{nomEspece:'Fruit', nombre:3000}]},
            {idRegion:2,nomRegion:"SAVA",nomCampagne:'Campagne 2020',nbPlants:21000, surface:125, espece:[{nomEspece:'Sapin', nombre:12000},{nomEspece:'Cactus', nombre:4000},{nomEspece:'Fruit', nombre:5000}]},
        ];*/
    
        this.state = {
            userFrom:'',
            userRegion:{},
            idRegion:0,
            idCampagne:0,
            statistiques:[],
            campagnes:[],
            regions:[],
            optionsRegions:[],
            optionsCampagnes:[],
            currentRegion:'toutes',
            currentCampagne:'toutes campagnes',
            totalNombre:0,
            totalSurface:0,

            wordToSearch:'',
            nbSearchResult:0,
            isSubmit:false,
            page:0,
            rowsPerPage:10,
            beginningURL: Config.beginningURL
        };
        this.statistiquesList=this.statistiquesList.bind(this);
        this.showDetailsEspece=this.showDetailsEspece.bind(this);
       
     
        this.onChangeSearch= this.onChangeSearch.bind(this);
        this.onSubmitSearch=this.onSubmitSearch.bind(this);
        this.displaySearchResult=this.displaySearchResult.bind(this);
        
        this.refreshTable=this.refreshTable.bind(this);

    }
    state = {
        selectedOptionRegion: null,
        selectedOptionCampagne: null,
      };
    componentDidMount(){
         //Init title for the page
         if(localStorage.getItem("userRegion")!=null){
            this.setState({
              userRegion:JSON.parse(localStorage.getItem("userRegion"))
            });
        }
        if(localStorage.getItem("title")!=null){
            this.setState({
              userFrom:localStorage.getItem("title")
            });
        }
        
        
        //select campagne
        let idLastCampagne=0; //all
        
        axios.get(this.state.beginningURL+'campagne/read.php')
        .then(response =>{
                this.setState({
                    campagnes:response.data.body
                });
                let lcamp=[{value:0, label:'Toutes'}];
                let currentCampagnes=response.data.body;
                for(let i=0;i<currentCampagnes.length;i++){
                    lcamp.push({value:currentCampagnes[i].id, label:currentCampagnes[i].nomCampagne});
                }
                this.setState({optionsCampagnes:lcamp});

                //get lastCampagne
                /*if(currentCampagnes.length>0){
                    idLastCampagne=currentCampagnes[0].id;
                    this.setState({
                        selectedOptionCampagne:{value:currentCampagnes[0].id, label:currentCampagnes[0].nomCampagne},
                        currentCampagne:currentCampagnes[0].nomCampagne
                    });
                }*/

                //Get the regions 
                //Verify first if the logged in user corresponds to a specific region
                let idCurrentRegion=0;
                if(localStorage.getItem("user")!=null){
                    const  loggedUser=JSON.parse(localStorage.getItem("user"));
                    idCurrentRegion=loggedUser.idRegion;
                    this.setState({
                        idRegion:idCurrentRegion
                    });
                    //select for the region
                    axios.get(this.state.beginningURL+'region/read.php')
                    .then(response =>{
                            this.setState({
                                regions:response.data.body

                            });
                            let lreg=[{value:0, label:'Toutes'}];
                            let currentRegions=response.data.body;
                        
                            for(let i=0;i<currentRegions.length;i++){
                                
                                lreg.push({value:currentRegions[i].id, label:currentRegions[i].nomRegion});
                                //verify if the id corresponds to the IDRegion of the loggedIn User, and set it as the default on the select option
                                if(currentRegions[i].id===idCurrentRegion){
                                    this.setState({
                                        selectedOptionRegion:{value:currentRegions[i].id, label:currentRegions[i].nomRegion},
                                        currentRegion:currentRegions[i].nomRegion
                                    });
                                }
                            }
                            this.setState({optionsRegions:lreg});

                            //get Pepiniere depending on Region and Campagne
                            // get all the pepinieres
                            const data ={
                                wordToSearch:this.state.wordToSearch,
                                idRegion:idCurrentRegion,
                                idCampagne:idLastCampagne
                      
                            };
                            //console.log(data);
                            axios.post(this.state.beginningURL+'reboisement/stattablebyidregionandidcampagne.php', data)
                            .then(response =>{
                                    this.setState({
                                        statistiques:response.data.body,
                                        totalNombre:response.data.totalNbPlants,
                                        totalSurface:response.data.totalSurface
                                        
                                    });
                            })
                            .catch(error=>{
                                console.log(error);
                            })


                           
                    })
                    .catch(error=>{
                        console.log(error);
                    })

                 
                }
        })
        .catch(error=>{
            console.log(error);
        })       
    }
    showDetailsEspece(espece){
        return espece.map((currentEspece) => {
            return (<div className="row">- &nbsp;<b>{currentEspece.nomEspece}:</b> &nbsp;{currentEspece.nombre} <br/></div>);
        });
    }
    //select (combobox) 
    
    handleChangeRegion = selectedOptionRegion => {
        this.setState({ selectedOptionRegion , 
            currentRegion:selectedOptionRegion.label,
            idRegion:selectedOptionRegion.value
        });
        this.refreshTable(this.state.wordToSearch, selectedOptionRegion.value, this.state.idCampagne);
      
      };
    handleChangeCampagne = selectedOptionCampagne => {
        this.setState({ selectedOptionCampagne, 
            currentCampagne:selectedOptionCampagne.label,
            idCampagne:selectedOptionCampagne.value
         });
        this.refreshTable(this.state.wordToSearch, this.state.idRegion, selectedOptionCampagne.value);
      
      };

      refreshTable(wordToSearch1,idRegion1,idCampagne1){
        this.setState({
            statistiques:[],
            totalNombre:0,
            totalSurface:0
            
        });
        const constraint ={
            wordToSearch:wordToSearch1,
            idRegion:idRegion1,
            idCampagne:idCampagne1
        };
    //console.log(constraint);
        axios.post(this.state.beginningURL+'reboisement/stattablebyidregionandidcampagne.php', constraint)
        .then(response =>{
                //console.log(response);
                this.setState({
                    statistiques:response.data.body,
                    totalNombre:response.data.totalNbPlants,
                    totalSurface:response.data.totalSurface
                });
        })
        .catch(error=>{
            console.log(error);
            this.setState({
                statistiques:[],
                nbSearchResult:0,
            });
        })

    }
   


    // Search
    onChangeSearch(e){
        this.setState({
            wordToSearch:e.target.value
        });
        //if we erase the word written
        if(e.target.value===""){
            this.setState({
                isSubmit:false,   
            });
            //change to another function depending on the  criteria in select options
            this.componentDidMount();
        }
    }
    onSubmitSearch(e){
        e.preventDefault();
        /*const word ={
            wordToSearch:this.state.wordToSearch,
  
        };*/
        if(this.state.wordToSearch===""){
            this.setState({
                isSubmit:false,   
            });
            this.componentDidMount();
        }
        else{
            this.setState({
                isSubmit:true,   
            });
           
        }    
    }
    displaySearchResult(){
        if(this.state.isSubmit===true){
        return <div className="searchResultDiv"><b className="searchResultStyle">{this.state.nbSearchResult} R&eacute;sultat(s) trouv&eacute;(s) - R&eacute;gion {this.state.currentRegion} - {this.state.currentCampagne}</b></div> 
        } 
        else{
            return 
        }  
    }


    statistiquesList(){

        let i=0;
      
        return this.state.statistiques.slice(this.state.page * this.state.rowsPerPage, this.state.page * this.state.rowsPerPage + this.state.rowsPerPage).map((currentHistoriqueEntree) => {
            i++;
            return <StatisticItem numero={i} showDetailsEspece={this.showDetailsEspece}  statistiques={currentHistoriqueEntree}  key={i}/>;
        });
     
    }
   
    
    

   

    render() {
        //select (combobox)
        const { selectedOptionRegion, selectedOptionCampagne } = this.state; 
   
        //Table
        const classes = useStyles;
        
        const handleChangePage = (event, newPage) => {
            this.setState({page:newPage})
            //setPage(newPage);
        };
        const handleChangeRowsPerPage = (event) => {
            //setRowsPerPage(+event.target.value);
            //setPage(0);
            this.setState({rowsPerPage:+event.target.value});
            this.setState({page:0});
        };
        //end table

        

        return (
            <div className="container-fluid">
                <div className="row bg-title">
                    <div className="left-title col-lg-4 col-md-4 col-sm-4 col-xs-12" >
                        <h4 className="text-left-title page-title">Statistiques</h4>
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <h3 className="text-middle-title page-title">{this.state.userFrom}</h3>
                    </div>

                    <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                        {/* <a href="https://wrappixel.com/templates/ampleadmin/" target="_blank"
                            className="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Upgrade
                            to Pro

                        </a> */}
                        <ol className="breadcrumb">
                            
                            <li className="active"><a>Statistiques</a>&nbsp;&nbsp;<i className="linking fa fa-angle-right fa-fw"></i>Tableau</li>
                        </ol>
                    </div>
                    {/* <!-- /.col-lg-12 --> */}
                </div>
            
                {/* <!-- content of the page--> */}
                {/* Head */}
                <div className="divAppBar" >
                    <AppBar position="static" style={appBarStyle}>
                        <Toolbar>
                            {/* grid */}
                            
                                <Grid container spacing={3}>
                                    <Grid item xs={1} style={{textAlign:'left'}}>
                                    
                                    </Grid>
                                    <Grid item xs={10} style={{textAlign:'center'}}>
                                        <h4 className="smaller-title" >Statistiques de reboisement - R&eacute;gion {this.state.currentRegion} - {this.state.currentCampagne}</h4>
                                    </Grid>
                                    <Grid item xs={1}>
                                   
                                        {/* <div className="div-search" >
                                        <form role="search" id="search" className="app-search hidden-sm hidden-xs m-r-10" onSubmit={this.onSubmitSearch} noValidate>
                                            <input type="text" placeholder="Rechercher..." className="form-control"
                                                value={this.state.wordToSearch}
                                                onChange={this.onChangeSearch}/> 
                                             <a  onClick={this.onSubmitSearch}>
                                                <i className="fa fa-search"></i>
                                            </a> 
                                        </form>

                                      
                                        </div> */}
                                    
                                    </Grid>
                                </Grid>
                            
                            {/* end grid */}
                        
                        </Toolbar>
                    </AppBar>
                </div>
                {/* end head */}
                {/* Table */}
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                                
                        <Grid container spacing={2} style={gridSelectStyle}>
                                <Grid item xs={6} >
                                </Grid>
                                <Grid item xs={3} >
                                <Select
                                        placeholder="Régions"
                                        value={selectedOptionRegion}
                                        onChange={this.handleChangeRegion}
                                        options={this.state.optionsRegions}
                                       
                                        
                                    />
                                
                                </Grid>
                                <Grid item xs={3}>
                                <Select
                                        placeholder="Campagnes"
                                        value={selectedOptionCampagne}
                                        onChange={this.handleChangeCampagne}
                                        options={this.state.optionsCampagnes}
                                       
                                        
                                    />
                                </Grid>
                            </Grid>

                          

                            {/* <h3 class="box-title">Basic Table</h3> */}
                            {/* <p class="text-muted">Add class <code>.table</code></p> */}
                             {/* Content of the table */}
                             {this.displaySearchResult()}
                             <Paper className={classes.root}>
                                
                                <TableContainer className={classes.container}>
                                    <Table  stickyHeader aria-label="sticky table">
                                    <TableBody>
                                            <TableRow style={tableRowTitleStyle}>
                                                <TableCell style={tableCellTitleStyle}>IdRegion</TableCell>
                                                <TableCell style={tableCellTitleStyle}>R&eacute;gion</TableCell>
                                                <TableCell style={tableCellTitleStyle}>Campagne</TableCell>
                                                <TableCell style={tableCellTitleStyle}>Nombre Plants</TableCell>
                                                <TableCell style={tableCellTitleStyle}>Surface (Ha) </TableCell>
                                                {/* <TableCell style={tableCellTitleStyle}>Especes</TableCell> */}
                                            </TableRow>
                                                                    
                                    </TableBody>
                                    <TableBody>
                                        {this.statistiquesList()}       
                                    </TableBody>
                                    <TableBody>
                                        <TableRow style={tableRowTitleStyle}>
                                            <TableCell style={tableCellTitleStyle}></TableCell>
                                            <TableCell style={tableCellTitleStyle}></TableCell>
                                            <TableCell style={tableCellTitleStyle}>Total</TableCell>
                                            <TableCell style={tableCellTitleStyle}>{this.state.totalNombre}</TableCell>
                                            <TableCell style={tableCellTitleStyle}>{this.state.totalSurface}</TableCell>
                                            {/* <TableCell style={tableCellTitleActionColumnStyle}></TableCell> */}
                                        </TableRow> 
                                    </TableBody>
                                        
                                    </Table>
                                </TableContainer>
                                <TablePagination
                                    style={tableCellStyle}
                                    rowsPerPageOptions={[10, 25, 100]}
                                    component="div"
                                    count={this.state.statistiques.length}
                                    rowsPerPage={this.state.rowsPerPage}
                                    page={this.state.page}
                                    onChangePage={handleChangePage}
                                    onChangeRowsPerPage={handleChangeRowsPerPage}
                                />
                            </Paper> 

                            {/* end content of the table */}
                        </div>
                    </div>
                </div>
                {/* end table */}
              
                {/* <!-- /.content of the page --> */}

            </div>
        )
    }
}
