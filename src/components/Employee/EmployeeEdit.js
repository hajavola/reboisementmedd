import React, { Component } from 'react';
import "./EmployeeEdit.css";
import axios from 'axios';
import * as Config from './../Config/Config';

import {Link} from 'react-router-dom';
//Search
import {  makeStyles } from '@material-ui/core/styles';
//grid for the search
import Grid from '@material-ui/core/Grid';
//Form 
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';

import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';


// form 
const useStylesForm = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));
  
// end form


export default class EmployeeEdit extends Component {
    constructor(props){
        super(props);
        this.state={
            userFrom:'',
            userRegion:{},
            errorMessage:'',
            name:'',
            email:'',
            age:'',
            designation:'',
            created:'',
            beginningURL: Config.beginningURL

        }

        this.showErrorMessage=this.showErrorMessage.bind(this);
        this.onChangeName=this.onChangeName.bind(this);
        this.onChangeEmail=this.onChangeEmail.bind(this);
        this.onChangeAge=this.onChangeAge.bind(this);
        this.onChangeDesignation=this.onChangeDesignation.bind(this);
        this.onChangeCreated=this.onChangeCreated.bind(this);
        this.onSubmit=this.onSubmit.bind(this);
    }

    componentDidMount(){
        //Init title for the page
        if(localStorage.getItem("userRegion")!=null){
            this.setState({
              userRegion:JSON.parse(localStorage.getItem("userRegion"))
            });
        }
        if(localStorage.getItem("title")!=null){
            this.setState({
              userFrom:localStorage.getItem("title")
            });
        }
        // get the employee
        axios.get(this.state.beginningURL+'employee/single_read.php/?id='+this.props.match.params.id)
        .then(response=>{
            this.setState({
                name:response.data.name,
                email:response.data.email,
                age:response.data.age,
                designation:response.data.designation,
                created:response.data.created
            })
        })
        .catch(function(error){
            console.log(error);
        });

    }

    onChangeName(e){
        this.setState({
            name:e.target.value
        });
    }
    onChangeEmail(e){
        this.setState({
            email:e.target.value
        });
    }
    onChangeAge(e){
        this.setState({
            age:e.target.value
        });
    }
    onChangeDesignation(e){
        this.setState({
            designation:e.target.value
        });
    }
    onChangeCreated(e){
        
        this.setState({
            created:e.target.value+" 00:00:00"
        });
    }
   
    //if we want to change a date(if we have a date among the states)
    /*onChangeDate(date){
        this.setState({
            date:date
        })
    }*/

    onSubmit(e){
        e.preventDefault();
        //verify if all the required field are not empty
        if((this.state.name==='' )||(this.state.email==='')||(this.state.age==='')||(this.state.designation==='')||(this.state.created==='')){
            this.setState({errorMessage:'Tous les champs doivent être remplis'});
        }
        else{
          
                const employee ={
                    id:this.props.match.params.id,
                    name:this.state.name,
                    email:this.state.email,
                    age:this.state.age,
                    designation:this.state.designation,
                    created:this.state.created
                };
                console.log(employee);
        
                axios.post(this.state.beginningURL+'employee/update.php',employee)
                 .then(res => console.log(res.data));
        
                window.location="/employeelist";
        }

        

        
    }

    showErrorMessage(){
        if(this.state.errorMessage!==''){
            return(
                <Typography component="h5"  style={{marginTop:'20px',color:'red',backgroundColor:'yellow'}}>
                            {this.state.errorMessage} 
                    </Typography>
            );
        }
        else{
            return
        }
        
    }

    render() {
        //form 
        const classesForm = useStylesForm;
        return (
            <div>
                <h1  style={{marginTop:'10px'}}>
                    Modification employ&eacute;
                </h1>
                                  
                <div className="formStyle">
                {/*-- The content of the page-->*/}
                <Container component="main" maxWidth="xs" style={{backgroundColor:'white'}}>
                    <CssBaseline />
                    {this.showErrorMessage()}
                    <div className={classesForm.paper} >
                       
                        
                    <form className={classesForm.form} onSubmit={this.onSubmit} noValidate>                       
                        <TextField
                            variant="outlined"
                            margin="normal"
                            fullWidth
                            id="nom"
                            label="Nom"
                            name="nom"
                            autoComplete="nom"
                            autoFocus 
                            type="text" value={this.state.name} onChange={this.onChangeName}/>

                        <TextField
                            variant="outlined"
                            margin="normal"
                            fullWidth
                            id="email"
                            label="Email"
                            name="email"
                            autoComplete="email"
                             
                            type="email"   value={this.state.email} onChange={this.onChangeEmail}/>
                 
                    
                        <TextField
                            variant="outlined"
                            margin="normal"
                            fullWidth
                            id="age"
                            label="Age"
                            name="age"
                            autoComplete="age"
                             
                            type="number" required  value={this.state.age} onChange={this.onChangeAge}/>

                        <TextField
                            variant="outlined"
                            margin="normal"
                            fullWidth
                            id="designation"
                            label="Désignation"
                            name="designation"
                            autoComplete="designation"
                             
                            type="text" value={this.state.designation} onChange={this.onChangeDesignation}/>
                  
                        <label>Date cr&eacute;ation</label>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            fullWidth
                            id="created"
                            label=""
                            name="created"
                            autoComplete="created"
                             
                            type="date" required value={this.state.created.substring(0,10)} onChange={this.onChangeCreated}/>
                    
                        <Grid container style={{marginTop:'40px'}} >
                            <Grid item xs={6} >
                                <Link to="/employeelist">
                                    <Button
                                    type="button"
                                    fullWidth
                                    variant="contained" 
                                    style={{marginRight:'8px'}}                                 
                                    > 
                                        Annuler
                                    </Button>
                                </Link>
                            </Grid>
                            <Grid item xs={6}>
                              <Button
                                  type="submit"
                                  fullWidth
                                  variant="contained"
                                  color="primary"
                                  className={classesForm.submit}
                                  style={{marginLeft:'8px'}} 
                              >
                                  Enregistrer
                              </Button>
                            </Grid>
                        </Grid>
                        </form>
                        <br/>
                    </div>
                    
                </Container>
                
    
                {/*-- End content of the page-->*/}
                </div>
            
                
            </div>
        )
    }
}
