import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import "./EmployeeList.css";
import * as Config from './../Config/Config';
import axios from 'axios';

//Table
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
//button
import Button from '@material-ui/core/Button';



const  Employee= props =>(
    <TableRow hover role="checkbox" tabIndex={-1} key={props.employee.code}>
        <TableCell>{props.employee.id}</TableCell>
        <TableCell>{props.employee.name}</TableCell>
        <TableCell>{props.employee.email}</TableCell>
        <TableCell>{props.employee.age}</TableCell>
        <TableCell>{props.employee.designation}</TableCell>
        <TableCell>{props.employee.created}</TableCell>
        <TableCell style={{textAlign:'center'}}><img className="imageSupprimer"  alt="supprimer" src="assets/images/logos/LogoDelete.png" onClick={()=> {props.deleteEmployee(props.employee.id)}}/> | <Link to={"/employeeedit/"+props.employee.id}><img className="imageModifier" alt="edit" src="assets/images/logos/editButtonLogo.png"/></Link></TableCell>
    </TableRow>
)

//Table
const useStyles = makeStyles({
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 440,
    },
  });
  

export default class EmployeeList extends Component {
    constructor(props){
        super(props)
        this.state={
            userFrom:'',
            userRegion:{},
            employees:[],
            page:0,
            rowsPerPage:10,
            beginningURL: Config.beginningURL

        } 
        this.deleteEmployee=this.deleteEmployee.bind(this);
        this.employeeList=this.employeeList.bind(this);
        
    }

    componentDidMount(){
        //Init title for the page
        if(localStorage.getItem("userRegion")!=null){
            this.setState({
              userRegion:JSON.parse(localStorage.getItem("userRegion"))
            });
        }
        if(localStorage.getItem("title")!=null){
            this.setState({
              userFrom:localStorage.getItem("title")
            });
        }
        //get the list
        axios.get(this.state.beginningURL+'employee/read.php')
            .then(response =>{
                //if(response.data.length>0){
                    this.setState({
                        employees:response.data.body

                    });
                   
                //}
            })
            .catch(error=>{
                console.log(error);
            })
    }

    

    deleteEmployee(id){
        axios.delete(this.state.beginningURL+'employee/delete.php',{data:{id:id}})
        .then(res => console.log(res.data));
        this.setState({
            employees:this.state.employees.filter(el=>el.id !==id)
        });    
        
    }

    employeeList(){

        return this.state.employees.slice(this.state.page * this.state.rowsPerPage, this.state.page * this.state.rowsPerPage + this.state.rowsPerPage).map((currentEmployee) => {
            return <Employee  employee={currentEmployee} deleteEmployee={this.deleteEmployee} key={currentEmployee.id}/>;
        });
     
    }
    
    render() {
        //Table
        const classes = useStyles;
        const handleChangePage = (event, newPage) => {
            this.setState({page:newPage})
            //setPage(newPage);
        };
        const handleChangeRowsPerPage = (event) => {
            //setRowsPerPage(+event.target.value);
            //setPage(0);
            this.setState({rowsPerPage:+event.target.value});
            this.setState({page:0});
        };
        //end table

        return (
            <div>
                <h1>Liste de tous les employ&eacute;s</h1><br/>
                 {/*-- ============================================================== -->*/}
                {/*-- Search bar -->*/}
                {/*-- ============================================================== -->*/}
                <Link to='/employeecreate'>
                    <Button variant="contained" color="secondary">
                        Nouveau
                    </Button>
                </Link>
                          
                {/*-- The content of the page-->*/}
                <Paper className={classes.root}>
                    <TableContainer className={classes.container}>
                        <Table stickyHeader aria-label="sticky table">
                        <TableHead>
                            <TableRow>
                                <TableCell>Id</TableCell>
                                <TableCell>Nom</TableCell>
                                <TableCell>Email</TableCell>
                                <TableCell>Age</TableCell>
                                <TableCell>D&eacute;signation</TableCell>
                                <TableCell>Cr&eacute;ation</TableCell>
                                <TableCell style={{textAlign:'center'}}>Actions</TableCell>
                            </TableRow>
                            
                        </TableHead>
                        <TableBody>
                            {this.employeeList()}       
                        </TableBody>
                        </Table>
                    </TableContainer>
                    <TablePagination
                        rowsPerPageOptions={[10, 25, 100]}
                        component="div"
                        count={this.state.employees.length}
                        rowsPerPage={this.state.rowsPerPage}
                        page={this.state.page}
                        onChangePage={handleChangePage}
                        onChangeRowsPerPage={handleChangeRowsPerPage}
                    />
                    </Paper>  
                {/*-- End content of the page-->*/}
            </div>
        )
    }
}
