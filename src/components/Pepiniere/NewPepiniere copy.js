import React, { Component } from 'react';
import "./NewPepiniere.css";
import * as Config from './../Config/Config';
import NewLocalisationModal from './Modal/NewLocalisationModal';
import { Link } from 'react-router-dom';
import axios from 'axios';

//Search
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
//grid for the search
import Grid from '@material-ui/core/Grid';

//Table
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';


//select
import Select from 'react-select';

//autocomplete
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';


//Table
const useStyles = makeStyles({
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 600,
    },

  });



// form 
const useStylesForm = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));

// end form


//design
let appBarStyle= {borderRadius:'4px',textAlign:'right',backgroundColor:'#000e2f'};
let buttonBackStyle= {backgroundColor:'#0bca4b', color:'white', marginTop:'5px'};
let fontStyle={fontSize:'14px'};
export default class NewPepiniere extends Component {
    constructor(props) {
        super(props);
        //data
        let lpepinieres=[
            {id:1, idLocalisationPepiniere:1,idTypePepiniere:1, idPepinieriste:1, idModeDeProduction:1, nomPepiniere:'La pépinière 1',nbJeunesPlants:2000},
            {id:2, idLocalisationPepiniere:1,idTypePepiniere:1, idPepinieriste:1, idModeDeProduction:1, nomPepiniere:'La pépinière 2',nbJeunesPlants:2000},
            {id:3, idLocalisationPepiniere:1,idTypePepiniere:1, idPepinieriste:1, idModeDeProduction:1, nomPepiniere:'La pépinière 3',nbJeunesPlants:2000},
            {id:4, idLocalisationPepiniere:1,idTypePepiniere:1, idPepinieriste:1, idModeDeProduction:1, nomPepiniere:'La pépinière 4',nbJeunesPlants:2000},
            {id:5, idLocalisationPepiniere:1,idTypePepiniere:1, idPepinieriste:1, idModeDeProduction:1, nomPepiniere:'La pépinière 5',nbJeunesPlants:2000},
            {id:6, idLocalisationPepiniere:1,idTypePepiniere:1, idPepinieriste:1, idModeDeProduction:1, nomPepiniere:'La pépinière 6',nbJeunesPlants:2000},
            {id:7, idLocalisationPepiniere:1,idTypePepiniere:1, idPepinieriste:1, idModeDeProduction:1, nomPepiniere:'La pépinière 7',nbJeunesPlants:2000},
            {id:8, idLocalisationPepiniere:1,idTypePepiniere:1, idPepinieriste:1, idModeDeProduction:1, nomPepiniere:'La pépinière 8',nbJeunesPlants:2000},
            {id:9, idLocalisationPepiniere:1,idTypePepiniere:1, idPepinieriste:1, idModeDeProduction:1, nomPepiniere:'La pépinière 9',nbJeunesPlants:2000},
            {id:10, idLocalisationPepiniere:1,idTypePepiniere:1, idPepinieriste:1, idModeDeProduction:1, nomPepiniere:'La pépinière 10',nbJeunesPlants:2000},
            {id:11, idLocalisationPepiniere:1,idTypePepiniere:1, idPepinieriste:1, idModeDeProduction:1, nomPepiniere:'La pépinière 11',nbJeunesPlants:2000},
        
        ];
        //Data
      
        let lDistricts=[
            {id:1,nomDistrict: 'Ambanja'},
            {id:2,nomDistrict: 'Nosy Be'},
            {id:3,nomDistrict: 'SAVA'}, 
            {id:4,nomDistrict: 'Ambilobe'}
        ];
        let lLocalisations=[
            {id:1,idRegion:1,idDistrict:1, nomLocalisation:'ENCEINTE RTV,PK4 MORAFENO'},
            {id:2,idRegion:1,idDistrict:1, nomLocalisation:'STADE MUNICIPAL'},
            {id:3,idRegion:1,idDistrict:1, nomLocalisation:'BANA'},
            {id:4,idRegion:1,idDistrict:1, nomLocalisation:'ANDRANOMANITRA'},
        ];
        let lPepinieristes=[
            {id:1,idTypePepinieriste:1,nomPepinieriste:'GRAINE DE VIE'},
            {id:1,idTypePepinieriste:1,nomPepinieriste:'CLUB 201'},
            {id:1,idTypePepinieriste:1,nomPepinieriste:'RM7'},
            {id:1,idTypePepinieriste:1,nomPepinieriste:'AZIMUT'}
        ];
        let lTypePepinieres=[
            {id:1, nomType:'Educatif'},
            {id:2, nomType:'Potentielle'}
        ];
        let lModeDeProduction=[
            {id:1, modeDeProduction:'SUR PLATE BANDE'}
        ];
    
        this.state = {
            userFrom:'DREDD REGION SAVA',
            nomPepiniere:'',
            district:'',
            localisation:'',
            pepinieriste:'',
            typePepiniere:'',
            modeDeProduction:'',
            districts: lDistricts,
            localisations:lLocalisations,
            pepinieristes:lPepinieristes,
            typePepinieres:lTypePepinieres,
            modeDeProductions:lModeDeProduction,
            optionsDistricts: [],
            optionsLocalisations:[],
            optionsPepinieristes:[],
            optionsTypePepinieres:[],
            optionsModeDeProductions:[],          
            beginningURL: Config.beginningURL
        };
        this.onChangeNomPepiniere=this.onChangeNomPepiniere.bind(this);
        this.onChangeDistrict=this.onChangeDistrict.bind(this);
        this.onChangeLocalisation=this.onChangeLocalisation.bind(this);
        this.onChangePepinieriste=this.onChangePepinieriste.bind(this);
        this.onChangeTypePepiniere=this.onChangeTypePepiniere.bind(this);
        this.onChangeModeDeProduction=this.onChangeModeDeProduction.bind(this);
        this.onSubmit=this.onSubmit.bind(this);
        //Modals
        this.showLocalisationModal=this.showLocalisationModal.bind(this);
      
        
      

    }
    componentDidMount(){
        let i=0;
       
        //select districts
        let lDist=[];
        let currentDistricts=this.state.districts;
        for(i=0;i<currentDistricts.length;i++){
            lDist.push({value:currentDistricts[i].id, label:currentDistricts[i].nomDistrict});
        }
        this.setState({optionsDistricts:lDist});

        //select Localisations
        let lLoc=[];
        let currentLocalisation=this.state.localisations;
        for(i=0;i<currentLocalisation.length;i++){
            lLoc.push({value:currentLocalisation[i].id, label:currentLocalisation[i].nomLocalisation});
        }
        this.setState({optionsLocalisations:lLoc});

        // optionsPepinieristes:[],
        //select Pepinieristes
        let lPep=[];
        let currentPepinieriste=this.state.pepinieristes;
        for(i=0;i<currentPepinieriste.length;i++){
            lPep.push({value:currentPepinieriste[i].id, label:currentPepinieriste[i].nomPepinieriste});
        }
        this.setState({optionsPepinieristes:lPep});

        // optionsTypePepinieres:[],
        let lTypePep=[];
        let currentTypePepinieres=this.state.typePepinieres;
        for(i=0;i<currentTypePepinieres.length;i++){
            lTypePep.push({value:currentTypePepinieres[i].id, label:currentTypePepinieres[i].nomType});
        }
        this.setState({optionsTypePepinieres:lTypePep});

        // optionsModeDeProductions:[], 
        let lMode=[];
        let currentModeDeProductions=this.state.modeDeProductions;
        for(i=0;i<currentModeDeProductions.length;i++){
            lMode.push({value:currentModeDeProductions[i].id, label:currentModeDeProductions[i].modeDeProduction});
        }
        this.setState({optionsModeDeProductions:lMode});
        
       
      }

      //Modals
      state = {
        showLocalisation: false
      };
      showLocalisationModal = e => {
        this.setState({
            showLocalisation: !this.state.showLocalisation
        });
      };

      onChangeNomPepiniere(e){
        this.setState({
            nomPepiniere:e.target.value
        });
      }
      onChangeDistrict(e){
        this.setState({
            district:e.target.value
        });
      }
      onChangeLocalisation(e){
        this.setState({
            localisation:e.target.value
        });
      }
      onChangePepinieriste(e){
        this.setState({
            pepinieriste:e.target.value
        });
      }
      onChangeTypePepiniere(e){
        this.setState({
            typePepiniere:e.target.value
        });
      }
      onChangeModeDeProduction(e){
        this.setState({
            modeDeProduction:e.target.value
        });
      }
    
      onSubmit(e){
        e.preventDefault();
     
        //verify if all the required field are not empty
        // if((this.state.diseaseName==='' )||(this.state.description==='')||(this.state.idDoctorSpeciality==='')||(this.state.symptomsChecked.length===0)){
        //     this.setState({errorMessage:'Tous les champs doivent être remplis'});
        // }
        // else{
     
        //         const disease ={
        //             name:this.state.diseaseName,
        //             description:this.state.description,
        //             idDoctorSpeciality:this.state.idDoctorSpeciality,
        //             symptoms:this.state.symptomsChecked
                
        //         };
        //         console.log(disease);
        
        //         axios.post(this.state.beginningURL+'disease/addwithsymptoms',disease)
        //          .then(res => console.log(res.data));
        
        //         window.location="/diagnosticlist";
           
  
        //   }   
    }


    render() {
        //select (combobox)
        const { selectedOptionRegion, selectedOptionCampagne } = this.state; 
        //Table
        const classes = useStyles;
        
        const handleChangePage = (event, newPage) => {
            this.setState({page:newPage})
            //setPage(newPage);
        };
        const handleChangeRowsPerPage = (event) => {
            //setRowsPerPage(+event.target.value);
            //setPage(0);
            this.setState({rowsPerPage:+event.target.value});
            this.setState({page:0});
        };
        //end table

        //form 
        const classesForm = useStylesForm;

        

        return (
            <div className="container-fluid">
                <div className="row bg-title">
                    <div className="left-title col-lg-4 col-md-4 col-sm-4 col-xs-12" >
                        <h4 className="text-left-title page-title">P&eacute;pini&egrave;res</h4>
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <h3 className="text-middle-title page-title">{this.state.userFrom}</h3>
                    </div>

                    <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                        {/* <a href="https://wrappixel.com/templates/ampleadmin/" target="_blank"
                            className="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Upgrade
                            to Pro

                        </a> */}
                        <ol className="breadcrumb">
                            
                            <li className="active"><a href="#">P&eacute;pini&egrave;res</a>&nbsp;&nbsp;<i className="linking fa fa-angle-right fa-fw"></i>Nouvelle</li>
                        </ol>
                    </div>
                    {/* <!-- /.col-lg-12 --> */}
                </div>
            
                {/* <!-- content of the page--> */}
                {/* Head */}
                <div className="divAppBar" >
                    <AppBar position="static" style={appBarStyle}>
                        <Toolbar>
                            {/* grid */}
                            
                                <Grid container spacing={3}>
                                    <Grid item xs={3} style={{textAlign:'left'}}>
                                    <Link to='/pepinierelist'>
                                        <Button variant="contained" style={buttonBackStyle}>
                                            Revenir liste
                                        </Button>
                                    </Link>
                                    </Grid>
                                    <Grid item xs={6} style={{textAlign:'center'}}>
                                        <h4 className="smaller-title" >Nouvelle P&eacute;pini&egrave;re</h4>
                                    </Grid>
                                    <Grid item xs={3}>                                    
                                    </Grid>
                                </Grid>
                            
                            {/* end grid */}
                        
                        </Toolbar>
                    </AppBar>
                </div>
                {/* end head */}
                {/* Table */}
                <div className="row">
                    <div className="col-sm-12">
                        <div className="white-box formStyle" >
                        <Container component="main" maxWidth="sm" style={{backgroundColor:'white'}}>



                       
                            <Paper className={classes.root}>
                            <form className={classesForm.form} onSubmit={this.onSubmit}  noValidate>
                            <label className="label-style">Nom de la p&eacute;pini&egrave;re</label>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                fullWidth
                                id="nommaladie"
                                label="Nom de la pépinière"
                                name="nompepiniere"
                                autoComplete="nompepiniere"
                                autoFocus
                                inputProps={{style: fontStyle}} 
                                InputLabelProps={{style: fontStyle}} 
                                value={this.state.nomPepiniere} onChange={this.onChangeNomPepiniere}
                            />

                            <label className="label-style">District</label>
                            <Autocomplete
                                id="combo-box-district"
                                options={this.state.optionsDistricts}
                                getOptionLabel={(option) => option.label}
                                onChange={this.onChangeDistrict}
                                onSelect={this.onChangeDistrict}
                                inputprops={{style: fontStyle}} 
                                style={{ fontSize: 'inherit'}}
                                renderInput={(params) => <TextField {...params}    label="District"  variant="outlined" value={this.state.district} onChange={this.onChangeDistrict} />}
                                />
                            
                            <label className="label-style">Localisation</label>


                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                    <Autocomplete
                                        id="combo-box-localisation"
                                        options={this.state.optionsLocalisations}
                                        getOptionLabel={(option) => option.label}
                                        onChange={this.onChangeLocalisation}
                                        onSelect={this.onChangeLocalisation}

                                        renderInput={(params) => <TextField {...params} label="Localisation" variant="outlined" value={this.state.localisation} onChange={this.onChangeLocalisation} />}
                                        />
                                </Grid>
                                <Grid item xs={1}>
                                    <i className="new-icon-style fa  fa-plus-circle fa-fw"></i>
                                </Grid>
                            </Grid>
                            {/* <NewLocalisationModal onClose={this.showLocalisationModal} show={this.state.showLocalisation} show={this.state.showLocalisation} >Nouvelle localisation</NewLocalisationModal> */}
                            
                            {/* <NewLocalisationModal onClose={this.showLocalisationModal} show={this.state.showLocalisation} show={this.state.showLocalisation} >Nouvelle localisation</NewLocalisationModal> */}
                            
                            <label className="label-style">P&eacute;pinieriste</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                <Autocomplete
                                    id="combo-box-pepinieriste"
                                    options={this.state.optionsPepinieristes}
                                    getOptionLabel={(option) => option.label}
                                    onChange={this.onChangePepinieriste}
                                    onSelect={this.onChangePepinieriste}
                                    renderInput={(params) => <TextField {...params} label="Pépinieriste" variant="outlined" value={this.state.pepinieriste} onChange={this.onChangePepinieriste} />}
                                    />
                                </Grid>
                                <Grid item xs={1}>
                                    <i className="new-icon-style fa  fa-plus-circle fa-fw"></i>
                                </Grid>
                            </Grid>

                           
                            <label className="label-style">Type de p&eacute;pini&egrave;re</label>
                            <Autocomplete
                                id="combo-box-type-pepiniere"
                                options={this.state.optionsTypePepinieres}
                                getOptionLabel={(option) => option.label}
                                onChange={this.onChangeTypePepiniere}
                                onSelect={this.onChangeTypePepiniere}
                                renderInput={(params) => <TextField {...params} label="Type pépinière" variant="outlined" value={this.state.typePepiniere} onChange={this.onChangeTypePepiniere} />}
                                />
                            <label className="label-style">Mode de production</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                <Autocomplete
                                    id="combo-box-mode-de-production"
                                    options={this.state.optionsModeDeProductions}
                                    getOptionLabel={(option) => option.label}
                                    onChange={this.onChangeModeDeProduction}
                                    onSelect={this.onChangeModeDeProduction}
                                    renderInput={(params) => <TextField {...params} label="Mode de production" variant="outlined" value={this.state.modeDeProduction} onChange={this.onChangeModeDeProduction} />}
                                    />
                                </Grid>
                                <Grid item xs={1}>
                                    <i className="new-icon-style fa  fa-plus-circle fa-fw"></i>
                                </Grid>
                            </Grid>
                            


                            <Grid container spacing={1} style={{marginTop:'40px'}} >
                            <Grid item xs={6} >
                              <Button
                                  type="button"
                                  fullWidth
                                  variant="contained" 
                                  style={fontStyle}                                
                              >
                                  Annuler
                              </Button>
                            </Grid>
                            <Grid item xs={6}>
                              <Button
                                  type="submit"
                                  fullWidth
                                  variant="contained"
                                  color="primary"
                                  className={classesForm.submit}
                                  style={fontStyle}
                                  
                              >
                                  Enregistrer
                              </Button>
                            </Grid>
                        </Grid>
                            </form>
                            <br/>
                                
                            </Paper>  
                            {/* end content of the table */}
                            </Container>
                        </div>
                    </div>
                </div>
                {/* end table */}
              
                {/* <!-- /.content of the page --> */}

            </div>
        )
    }
}
