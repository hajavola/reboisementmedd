import React, { Component } from 'react';
import "./CommandeReboisement.css";
import * as Config from './../Config/Config';
import { Link } from 'react-router-dom';
//import axios from 'axios';

//Search
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
//grid for the search
import Grid from '@material-ui/core/Grid';

//Table
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
// import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';


const  Commande= props =>(
        <TableRow hover role="checkbox" tabIndex={-1} key={props.commande.id}>
        <TableCell style={tableCellStyle}>{props.commande.id}</TableCell>
        <TableCell style={tableCellStyle}>{props.commande.campagne}</TableCell>
        <TableCell style={tableCellStyle}>{props.commande.dateCommande}</TableCell>
        <TableCell style={tableCellStyle}>{props.commande.pepiniere}</TableCell>
        <TableCell style={tableCellStyle}>{props.commande.idReboisement}</TableCell>
        <TableCell style={tableCellStyle}>{props.commande.nbPlants}</TableCell>
        <TableCell style={tableCellActionColumnStyle}><Link to={"/commandedetails/"+props.commande.id}><img className="imageModifier" alt="details" src="assets/images/logos/logoAdd.png"/></Link></TableCell>
    </TableRow>
)

//Table
const useStyles = makeStyles({
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 440,
    },

  });
//design
let appBarStyle= {borderRadius:'4px',textAlign:'right',backgroundColor:'#000e2f'};
//grid
//let gridSelectStyle={marginBottom:'15px'};
//table
let tableCellTitleStyle={fontSize:'14px',fontFamily:'Segoe UI'};
let tableRowTitleStyle={backgroundColor:'rgb(245, 245, 245)'};
let tableCellTitleActionColumnStyle={fontSize:'14px',fontFamily:'Segoe UI', textAlign:'center'};
let tableCellStyle={fontSize:'13px', color:'rgb(90, 88, 88)', fontFamily:'Segoe UI' };
let tableCellActionColumnStyle={fontSize:'13px', color:'rgb(90, 88, 88)', fontFamily:'Segoe UI', textAlign:'center' };

export default class CommandeReboisement extends Component {
    constructor(props) {
        super(props);
        //data
        let lcommandes=[
            {id:5, campagne:'Campagne 2020',dateCommande:'2020-12-25',pepiniere:'Pepiniere 1', idReboisement:22, nbPlants:2000},
            {id:4, campagne:'Campagne 2020',dateCommande:'2020-12-25',pepiniere:'Pepiniere 1', idReboisement:22, nbPlants:5000},
            {id:3, campagne:'Campagne 2020',dateCommande:'2020-12-25',pepiniere:'Pepiniere 1', idReboisement:22, nbPlants:4000},
            {id:2, campagne:'Campagne 2020',dateCommande:'2020-12-25',pepiniere:'Pepiniere 1', idReboisement:22, nbPlants:3000},
            {id:1, campagne:'Campagne 2020',dateCommande:'2020-12-25',pepiniere:'Pepiniere 1', idReboisement:22, nbPlants:2000},
         
        ];

        this.state = {
            userFrom:'',
            userRegion:{},
            commandes:lcommandes,
            wordToSearch:'',
            nbSearchResult:0,
            isSubmit:false,
            page:0,
            rowsPerPage:10,
            beginningURL: Config.beginningURL
        };
        this.commandeList=this.commandeList.bind(this);
        this.onChangeSearch= this.onChangeSearch.bind(this);
        this.onSubmitSearch=this.onSubmitSearch.bind(this);
        this.displaySearchResult=this.displaySearchResult.bind(this);
      

    }
    componentDidMount(){
          //Init title for the page
          if(localStorage.getItem("userRegion")!=null){
            this.setState({
              userRegion:JSON.parse(localStorage.getItem("userRegion"))
            });
        }
        if(localStorage.getItem("title")!=null){
            this.setState({
              userFrom:localStorage.getItem("title")
            });
        }
       
    }

   

    // Search
    onChangeSearch(e){
        this.setState({
            wordToSearch:e.target.value
        });
        //if we erase the word written
        if(e.target.value===""){
            this.setState({
                isSubmit:false,   
            });
            //change to another function depending on the  criteria in select options
            this.componentDidMount();
        }
    }
    onSubmitSearch(e){
        e.preventDefault();
        /*const word ={
            wordToSearch:this.state.wordToSearch,
  
        };*/
        if(this.state.wordToSearch===""){
            this.setState({
                isSubmit:false,   
            });
            this.componentDidMount();
        }
        else{
            this.setState({
                isSubmit:true,   
            });
            // axios.post(this.state.beginningURL+'doctor/okdoctorssearch',word)
            // .then(res => {
            //     this.setState({
            //         doctors:res.data,
            //         nbSearchResult:res.data.length,
            //         isSubmit:true
            //     });
            // });
        }    
    }
    displaySearchResult(){
        if(this.state.isSubmit===true){
        return <div className="searchResultDiv"><b className="searchResultStyle">{this.state.nbSearchResult} R&eacute;sultat(s) trouv&eacute;(s)</b></div> 
        } 
        else{
            return 
        }  
    }


    commandeList(){

        return this.state.commandes.slice(this.state.page * this.state.rowsPerPage, this.state.page * this.state.rowsPerPage + this.state.rowsPerPage).map((currentCommande) => {
            return <Commande  commande={currentCommande}  key={currentCommande.id}/>;
        });
     
    }

    render() {
        
        //Table
        const classes = useStyles;
        
        const handleChangePage = (event, newPage) => {
            this.setState({page:newPage})
            //setPage(newPage);
        };
        const handleChangeRowsPerPage = (event) => {
            //setRowsPerPage(+event.target.value);
            //setPage(0);
            this.setState({rowsPerPage:+event.target.value});
            this.setState({page:0});
        };
        //end table

        

        return (
            <div className="container-fluid">
                <div className="row bg-title">
                    <div className="left-title col-lg-4 col-md-4 col-sm-4 col-xs-12" >
                        <h4 className="text-left-title page-title">P&eacute;pini&egrave;res</h4>
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <h3 className="text-middle-title page-title">{this.state.userFrom}</h3>
                    </div>

                    <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                        {/* <a href="https://wrappixel.com/templates/ampleadmin/" target="_blank"
                            className="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Upgrade
                            to Pro

                        </a> */}
                        <ol className="breadcrumb">
                            
                            <li className="active"><a>P&eacute;pini&egrave;res</a>&nbsp;&nbsp;<i className="linking fa fa-angle-right fa-fw"></i>Commande</li>
                        </ol>
                    </div>
                    {/* <!-- /.col-lg-12 --> */}
                </div>
            
                {/* <!-- content of the page--> */}
                {/* Head */}
                <div className="divAppBar" >
                    <AppBar position="static" style={appBarStyle}>
                        <Toolbar>
                            {/* grid */}
                            
                                <Grid container spacing={3}>
                                    <Grid item xs={3} style={{textAlign:'left'}}>
                                   
                                    </Grid>
                                    <Grid item xs={6} style={{textAlign:'center'}}>
                                        <h4 className="smaller-title" >Historique des commandes pour reboisement</h4>
                                    </Grid>
                                    <Grid item xs={3}>
                                   
                                        <div className="div-search" >
                                        <form role="search" id="search" className="app-search hidden-sm hidden-xs m-r-10" onSubmit={this.onSubmitSearch} noValidate>
                                            <input type="text" placeholder="Rechercher..." className="form-control"
                                                value={this.state.wordToSearch}
                                                onChange={this.onChangeSearch}/> 
                                             <a  onClick={this.onSubmitSearch}>
                                                <i className="fa fa-search"></i>
                                            </a> 
                                        </form>

                                      
                                        </div>
                                    
                                    </Grid>
                                </Grid>
                            
                            {/* end grid */}
                        
                        </Toolbar>
                    </AppBar>
                </div>
                {/* end head */}
                {/* Table */}
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                         
                            {/* <h3 class="box-title">Basic Table</h3> */}
                            {/* <p class="text-muted">Add class <code>.table</code></p> */}
                             {/* Content of the table */}
                             {this.displaySearchResult()}
                            <Paper className={classes.root}>
                                
                                <TableContainer className={classes.container}>
                                    <Table  stickyHeader aria-label="sticky table">
                                    <TableBody>
                                        <TableRow style={tableRowTitleStyle}>
                                            <TableCell style={tableCellTitleStyle}>Id</TableCell>
                                            <TableCell style={tableCellTitleStyle}>Campagne</TableCell>
                                            <TableCell style={tableCellTitleStyle}>Date commande</TableCell>
                                            <TableCell style={tableCellTitleStyle}>Pépinière</TableCell>
                                            <TableCell style={tableCellTitleStyle}>Id Reboisement</TableCell>
                                            <TableCell style={tableCellTitleStyle}>Nombre Jeunes Plants</TableCell>                                           
                                            <TableCell style={tableCellTitleActionColumnStyle}>D&eacute;tails</TableCell>
                                        </TableRow>
                                        
                                    </TableBody>
                                    <TableBody>
                                        {this.commandeList()}       
                                    </TableBody>
                                    </Table>
                                </TableContainer>
                                <TablePagination
                                    style={tableCellStyle}
                                    rowsPerPageOptions={[10, 25, 100]}
                                    component="div"
                                    count={this.state.commandes.length}
                                    rowsPerPage={this.state.rowsPerPage}
                                    page={this.state.page}
                                    onChangePage={handleChangePage}
                                    onChangeRowsPerPage={handleChangeRowsPerPage}
                                />
                            </Paper>  
                    

                            {/* end content of the table */}
                        </div>
                    </div>
                </div>
                {/* end table */}
              
                {/* <!-- /.content of the page --> */}

            </div>
        )
    }
}
