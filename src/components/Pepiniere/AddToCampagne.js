import React, { Component } from 'react';
import "./PepiniereEdit.css";
import * as Config from './../Config/Config';
import * as Design from './../Config/Design';
//import NewLocalisationModal from './Modal/NewLocalisationModal';
import { Link } from 'react-router-dom';
import axios from 'axios';
import Typography from '@material-ui/core/Typography';


//Search
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
//grid for the search
import Grid from '@material-ui/core/Grid';

//Table
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';


//select
import Select from 'react-select';


//Table
const useStyles = makeStyles({
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 600,
    },

  });



// form 
const useStylesForm = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));

// end form


//design
let appBarStyle= {borderRadius:'4px',textAlign:'right',backgroundColor:'#000e2f'};
let buttonBackStyle= {backgroundColor:'#0bca4b', color:'white', marginTop:'5px'};
//let buttonDeleteStyle= {backgroundColor:'#e20f00', color:'white', marginTop:'5px'};
let fontStyle={fontSize:'14px'};

export default class AddToCampagne extends Component {
    constructor(props) {
        super(props);
       
    
        this.state = {
            userFrom:'',
            userRegion:{},
            loggedUser:{},
            errorMessage:'',
            idPepiniere:this.props.match.params.id,
            pepiniere:{},
            nomPepiniere:'',
            idCampagne:0,
            campagnes: [],
            optionsCampagnes: [],    
            beginningURL: Config.beginningURL
        };
       
        this.onSubmit=this.onSubmit.bind(this);
     
        this.showErrorMessage=this.showErrorMessage.bind(this);
        this.annuler=this.annuler.bind(this);     

    }
     //select (combobox) 
     state = {
        selectedOptionCampagne: null,
      };

    componentDidMount(){
         //Init title for the page
         if(localStorage.getItem("userRegion")!=null){
            this.setState({
              userRegion:JSON.parse(localStorage.getItem("userRegion"))
            });
        }
        if(localStorage.getItem("title")!=null){
            this.setState({
              userFrom:localStorage.getItem("title")
            });
        }
        if(localStorage.getItem("user")!=null){
            this.setState({
              loggedUser:JSON.parse(localStorage.getItem("user"))
            });
        }

        //get the pepiniere from database 
        axios.get(this.state.beginningURL+'pepiniere/single_read.php/?id='+this.props.match.params.id)
        .then(res=>{
            this.setState({
                pepiniere:res.data,
                nomPepiniere:res.data.nomPepiniere
            });
        });
        // optionsCampagnes:[],
        //select campagnes
        axios.get(this.state.beginningURL+'campagne/read.php')
        .then(response=>{
            this.setState({
                pepinieristes:response.data.body
               
            })
          
            let currentCampagnes=response.data.body;
            let lcamp=[{value:0, label:''}];
             
                for(let i=0;i<currentCampagnes.length;i++){
                    lcamp.push({value:currentCampagnes[i].id, label:currentCampagnes[i].nomCampagne});
                }
                this.setState({optionsCampagnes:lcamp});
        })
        .catch(function(error){
            console.log(error);
        });

       
      }

      //Modals
     

   
    handleChangeCampagne= selectedOptionCampagne => {
        this.setState({ 
            selectedOptionCampagne , 
            idCampagne:selectedOptionCampagne.value,
         
        });
    };
   
    
      onSubmit(e){     
        e.preventDefault();
    
        if(this.state.idCampagne===0 || this.state.selectedOptionCampagne===undefined){
            //console.log(this.state.selectedOptionCampagne);
            this.setState({errorMessage:'Veullez définir la campagne.'});
        }
        else{
                
                    const camp ={
                        idPepiniere:this.state.idPepiniere,
                        idCampagne : this.state.idCampagne,
                        userModule:this.state.loggedUser.module,
                        userType:this.state.loggedUser.type,
                        userProfil:this.state.loggedUser.profil
                    };
                    //console.log(camp);
            
                    axios.post(this.state.beginningURL+'pepinierecampagne/create.php',camp)
                     .then(res => {
                        console.log(res.data);
                        
                        if(res.data==="ok"){
                            alert('Ajout à la campagne "'+this.state.selectedOptionCampagne.label+'" achevée avec succès');
                            window.location="/pepinierelist";
                        }
                        else{
                            alert('Erreur lors de l\'ajout à la campagne.');
                        }
                    });
                }     
    }

    annuler(){
        //on annule tout
        this.componentDidMount();
    }


    
    showErrorMessage(){
        if(this.state.errorMessage!==''){
            return(
                <Typography component="h5"  style={Design.errorMessageStyle}>
                            {this.state.errorMessage} 
                </Typography>
            );
        }
        else{
            return
        }
        
    }


    render() {
        //select (combobox)
        const { selectedOptionCampagne} = this.state; 
        //Table
        const classes = useStyles;

        //form 
        const classesForm = useStylesForm;

        

        return (
            <div className="container-fluid">
                <div className="row bg-title">
                    <div className="left-title col-lg-4 col-md-4 col-sm-4 col-xs-12" >
                        <h4 className="text-left-title page-title">P&eacute;pini&egrave;res</h4>
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <h3 className="text-middle-title page-title">{this.state.userFrom}</h3>
                    </div>

                    <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                        {/* <a href="https://wrappixel.com/templates/ampleadmin/" target="_blank"
                            className="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Upgrade
                            to Pro

                        </a> */}
                        <ol className="breadcrumb">
                            
                            <li className="active"><a >P&eacute;pini&egrave;res</a>&nbsp;&nbsp;<i className="linking fa fa-angle-right fa-fw"></i>Ajout Campagne</li>
                        </ol>
                    </div>
                    {/* <!-- /.col-lg-12 --> */}
                </div>
            
                {/* <!-- content of the page--> */}
                {/* Head */}
                <div className="divAppBar" >
                    <AppBar position="static" style={appBarStyle}>
                        <Toolbar>
                            {/* grid */}
                            
                                <Grid container spacing={3}>
                                    <Grid item xs={2} style={{textAlign:'left'}}>
                                    <Link to='/pepinierelist'>
                                        <Button variant="contained" style={buttonBackStyle}>
                                            Revenir liste
                                        </Button>
                                    </Link>
                                    </Grid>
                                    <Grid item xs={8} style={{textAlign:'center'}}>
                                        <h4 className="smaller-title" >Ajout de la p&eacute;pini&egrave;re "{this.state.pepiniere.nomPepiniere}" &agrave; une campagne</h4>
                                    </Grid>
                                    <Grid item xs={2}>  
                                                                 
                                    </Grid>
                                </Grid>
                            
                            {/* end grid */}
                        
                        </Toolbar>
                    </AppBar>
                </div>
                {/* end head */}
                {/* Table */}
                <div className="row">
                    <div className="col-sm-12">
                        <div className="white-box formStylePepiniere" >
                        <Container component="main" maxWidth="xs" style={{backgroundColor:'white',borderRadius:'3px'}}>
                            <Paper className={classes.root}>
                            {this.showErrorMessage()}
                            <form className={classesForm.form} onSubmit={this.onSubmit}  noValidate>
                                              
                            

                            <label className="label-style">Campagne</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                    <Select
                                        placeholder="Campagne"
                                        value={selectedOptionCampagne}
                                        onChange={this.handleChangeCampagne}
                                        options={this.state.optionsCampagnes}
                                    />
                                </Grid>
                                <Grid item xs={1}></Grid>
                            </Grid>
                            
                            
                           

                            <Grid container spacing={1} style={{marginTop:'40px'}} >
                            <Grid item xs={6} >
                              <Button
                                  type="button"
                                  fullWidth
                                  variant="contained" 
                                  style={fontStyle} 
                                  onClick={()=> {this.annuler()}}                               
                              >
                                  Annuler
                              </Button>
                            </Grid>
                            <Grid item xs={6}>
                              <Button
                                  type="submit"
                                  fullWidth
                                  variant="contained"
                                  color="primary"
                                  className={classesForm.submit}
                                  style={fontStyle}
                                  
                              >
                                  Ajouter
                              </Button>
                            </Grid>
                        </Grid>
                            </form>
                            <br/>
                                
                            </Paper>  
                            {/* end content of the table */}
                            </Container>
                        </div>
                    </div>
                </div>
                {/* end table */}
              
                {/* <!-- /.content of the page --> */}

            </div>
        )
    }
}
