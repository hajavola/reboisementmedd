import React, { Component } from 'react';
import "./PepiniereEdit.css";
import * as Config from './../Config/Config';
import * as Design from './../Config/Design';
//import NewLocalisationModal from './Modal/NewLocalisationModal';
import { Link } from 'react-router-dom';
import axios from 'axios';
import Typography from '@material-ui/core/Typography';


//Search
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
//grid for the search
import Grid from '@material-ui/core/Grid';

//Table
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';


//select
import Select from 'react-select';

//autocomplete
import TextField from '@material-ui/core/TextField';

//Dialog
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

//Table
const useStyles = makeStyles({
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 600,
    },

  });



// form 
const useStylesForm = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));

// end form


//design
let appBarStyle= {borderRadius:'4px',textAlign:'right',backgroundColor:'#000e2f'};
let buttonBackStyle= {backgroundColor:'#0bca4b', color:'white', marginTop:'5px'};
let buttonDeleteStyle= {backgroundColor:'#e20f00', color:'white', marginTop:'5px'};
let fontStyle={fontSize:'14px'};
export default class PepiniereEdit extends Component {
    constructor(props) {
        super(props);
        /*//data
        let currentPepiniere={id:1, idDistrict:2, idLocalisation:2,idTypePepiniere:2, idPepinieriste:1, idModeDeProduction:1, nomPepiniere:'La pépinière 1',nbJeunesPlants:2000};

        //Data
      
        let lDistricts=[
            {id:1,nomDistrict: 'Ambanja'},
            {id:2,nomDistrict: 'Nosy Be'},
            {id:3,nomDistrict: 'SAVA'}, 
            {id:4,nomDistrict: 'Ambilobe'}
        ];
        let lLocalisations=[
            {id:1,idRegion:1,idDistrict:1, nomLocalisation:'ENCEINTE RTV,PK4 MORAFENO'},
            {id:2,idRegion:1,idDistrict:1, nomLocalisation:'STADE MUNICIPAL'},
            {id:3,idRegion:1,idDistrict:1, nomLocalisation:'BANA'},
            {id:4,idRegion:1,idDistrict:1, nomLocalisation:'ANDRANOMANITRA'},
        ];
        let lPepinieristes=[
            {id:1,idTypePepinieriste:1,nomPepinieriste:'GRAINE DE VIE'},
            {id:1,idTypePepinieriste:1,nomPepinieriste:'CLUB 201'},
            {id:1,idTypePepinieriste:1,nomPepinieriste:'RM7'},
            {id:1,idTypePepinieriste:1,nomPepinieriste:'AZIMUT'}
        ];
        let lTypePepinieres=[
            {id:1, nomType:'Educatif'},
            {id:2, nomType:'Potentielle'}
        ];
        let lModeDeProduction=[
            {id:1, modeDeProduction:'SUR PLATE BANDE'}
        ];*/
    
        this.state = {
            userFrom:'',
            userRegion:{},
            errorMessage:'',
            openLocalisation:false,
            newLocalisation:'',
            typepepinieriste:0,
            openPepinieriste:false,
            newPepinieriste:'',
            openModeDeProduction:false,
            newModeDeProduction:'',
            idPepiniere:this.props.match.params.id,
            openSupprimer:false,
            pepiniere:{},
            nomPepiniere:'',
            district:0,
            localisation:'',
            pepinieriste:'',
            typePepiniere:'',
            modeDeProduction:'',
            districts: [],
            localisations:[],
            pepinieristes:[],
            typePepinieres:[],
            modeDeProductions:[],
            optionsDistricts: [],
            optionsLocalisations:[],
            optionsPepinieristes:[],
            optionsTypePepinieres:[],
            optionsModeDeProductions:[], 
            optionsTypePepinieristes:[],  
            errorMessageLocalisation:'',
            errorMessagePepinieriste:'',  
            errorMessageModeDeProduction:'',             
            beginningURL: Config.beginningURL
        };
        this.onChangeNomPepiniere=this.onChangeNomPepiniere.bind(this);
        this.onSubmit=this.onSubmit.bind(this);
        this.deletePepiniere=this.deletePepiniere.bind(this);
        this.showButtonDelete=this.showButtonDelete.bind(this);
        this.showErrorMessage=this.showErrorMessage.bind(this);
        this.annuler=this.annuler.bind(this);
        //Modals
        this.showLocalisationModal=this.showLocalisationModal.bind(this);
      
        //Dialogs
        this.onChangeNewLocalisation=this.onChangeNewLocalisation.bind(this);
        this.onChangeNewPepinieriste=this.onChangeNewPepinieriste.bind(this);
        this.onChangeNewModeDeProduction=this.onChangeNewModeDeProduction.bind(this);
      
        this.onSubmitLocalisation=this.onSubmitLocalisation.bind(this);
        this.onSubmitPepinieriste=this.onSubmitPepinieriste.bind(this);
        this.onSubmitModeDeProduction=this.onSubmitModeDeProduction.bind(this);
      
      

    }
     //select (combobox) 
     state = {
        selectedOptionDistrict: null,
        selectedOptionLocalisation: null,
        selectedOptionPepinieriste: null,
        selectedOptionTypePepiniere: null,
        selectedOptionModeDeProduction: null,
        selectedOptionTypePepinieriste:null
      };

    componentDidMount(){
         //Init title for the page
         if(localStorage.getItem("userRegion")!=null){
            this.setState({
              userRegion:JSON.parse(localStorage.getItem("userRegion"))
            });
        }
        if(localStorage.getItem("title")!=null){
            this.setState({
              userFrom:localStorage.getItem("title")
            });
        }

        //get the pepiniere from database 
        axios.get(this.state.beginningURL+'pepiniere/single_read.php/?id='+this.props.match.params.id)
        .then(res=>{
            this.setState({
                pepiniere:res.data,
                nomPepiniere:res.data.nomPepiniere
            });


        //select districts
        //find by idRegion
        axios.get(this.state.beginningURL+'district/readbyidregion.php/?id='+res.data.idRegion)
        .then(response=>{
            this.setState({
                districts:response.data.body
            });
            // let lDist=[{value:0,label:""}];
            let lDist=[];
            let currentDistricts=response.data.body;
            for(let i=0;i<currentDistricts.length;i++){
                lDist.push({value:currentDistricts[i].id, label:currentDistricts[i].nomDistrict});
                if(currentDistricts[i].id===res.data.idDistrict){
                    this.setState({ selectedOptionDistrict:{value:currentDistricts[i].id, label:currentDistricts[i].nomDistrict}, district:currentDistricts[i].id });
                    
                }
            }
            this.setState({optionsDistricts:lDist});
            

        })
        .catch(function(error){
            console.log(error);
        });
        // get the correspondant idLocalisationPepiniere
        axios.get(this.state.beginningURL+'localisationpepiniere/readbyiddistrict.php/?id='+res.data.idDistrict)
        .then(response=>{
            this.setState({
                localisations:response.data.body
            });
            //initialize idLocalisationPepiniere
            let lLoc=[{value:0,label:""}];
             //select Localisations
           let currentLocalisation=response.data.body;
            for(let i=0;i<currentLocalisation.length;i++){
                lLoc.push({value:currentLocalisation[i].id, label:currentLocalisation[i].nomLocalisation});
                if(currentLocalisation[i].id===res.data.idLocalisationPepiniere){
                    this.setState({ selectedOptionLocalisation:{value:currentLocalisation[i].id, label:currentLocalisation[i].nomLocalisation} , localisation:currentLocalisation[i].id});
                  
                }
            }
            this.setState({optionsLocalisations:lLoc});

        })
        .catch(function(error){
            console.log(error);
        });

      
        

       

        // optionsPepinieristes:[],
        //select Pepinieristes
        axios.get(this.state.beginningURL+'pepinieriste/read.php')
        .then(response=>{
            this.setState({
                pepinieristes:response.data.body
               
            })
            let lPep=[{value:0,label:""}];
            let currentPepinieriste=response.data.body;
            for(let i=0;i<currentPepinieriste.length;i++){
                lPep.push({value:currentPepinieriste[i].id, label:currentPepinieriste[i].nomPepinieriste});
                if(currentPepinieriste[i].id===res.data.idPepinieriste){
                    this.setState({ selectedOptionPepinieriste:{value:currentPepinieriste[i].id, label:currentPepinieriste[i].nomPepinieriste}, pepinieriste:currentPepinieriste[i].id });
                   
                }
            }
            this.setState({optionsPepinieristes:lPep});
        })
        .catch(function(error){
            console.log(error);
        });
       

        // optionsTypePepinieres:[],
        axios.get(this.state.beginningURL+'typepepiniere/read.php')
        .then(response=>{
            this.setState({
                typePepinieres:response.data.body
               
            })
            let lTypePep=[{value:0,label:""}];
            let currentTypePepinieres=response.data.body;
            for(let i=0;i<currentTypePepinieres.length;i++){
                lTypePep.push({value:currentTypePepinieres[i].id, label:currentTypePepinieres[i].nomType});
                if(currentTypePepinieres[i].id===res.data.idTypePepiniere){
                    this.setState({ selectedOptionTypePepiniere:{value:currentTypePepinieres[i].id, label:currentTypePepinieres[i].nomType}, typePepiniere:currentTypePepinieres[i].id });
                
                }
            }
            this.setState({optionsTypePepinieres:lTypePep});
    
        })
        .catch(function(error){
            console.log(error);
        });
        
        // optionsModeDeProductions:[], 
        axios.get(this.state.beginningURL+'modedeproduction/read.php')
        .then(response=>{
            this.setState({
                modeDeProductions:response.data.body
               
            })
            let lMode=[{value:0,label:""}];
            let currentModeDeProductions=response.data.body;
            for(let i=0;i<currentModeDeProductions.length;i++){
                lMode.push({value:currentModeDeProductions[i].id, label:currentModeDeProductions[i].modeDeProduction});
                //init the combobox
             if(currentModeDeProductions[i].id===res.data.idModeDeProduction){
                this.setState({ selectedOptionModeDeProduction:{value:currentModeDeProductions[i].id, label:currentModeDeProductions[i].modeDeProduction} ,modeDeProduction:currentModeDeProductions[i].id});
              
            }
            }
            this.setState({optionsModeDeProductions:lMode});
        
    
        })
        .catch(function(error){
            console.log(error);
        });
        })
        .catch(function(error){
            console.log(error);
        });

          //type pepinieriste
        axios.get(this.state.beginningURL+'typepepinieriste/read.php')
          .then(response=>{
             
              let ltp=[{value:0,label:""}];
              let currentTypePepinieriste=response.data.body;
              for(let i=0;i<currentTypePepinieriste.length;i++){
                  ltp.push({value:currentTypePepinieriste[i].id, label:currentTypePepinieriste[i].nomType});
              }
              this.setState({optionsTypePepinieristes:ltp});
          
      
        })
        .catch(function(error){
            console.log(error);
        });
        
      

        
        
       
      }

        //Dialog
        handleClickOpenLocalisation  = () => {
            this.setState({openLocalisation:true});
          };
      
        handleCloseLocalisation  = () => {
            this.setState({openLocalisation:false,errorMessageLocalisation:''});
          };
    
        handleClickOpenPepinieriste  = () => {
            this.setState({openPepinieriste:true});
          };
      
        handleClosePepinieriste  = () => {
            this.setState({openPepinieriste:false,errorMessagePepinieriste:''});
          };
        
        handleClickOpenModeDeProduction  = () => {
            this.setState({openModeDeProduction:true});
          };
      
        handleCloseModeDeProduction  = () => {
            this.setState({openModeDeProduction:false,errorMessageModeDeProduction:''});
          };

    //Dialog forms 
    onChangeNewLocalisation(e){
        this.setState({
            newLocalisation:e.target.value
        });
      }
    handleChangeTypePepinieriste = selectedOptionTypePepinieriste => {
        this.setState({ selectedOptionTypePepinieriste , typepepinieriste:selectedOptionTypePepinieriste.value});
      };
    onChangeNewPepinieriste(e){
        this.setState({
            newPepinieriste:e.target.value
        });
      }
    onChangeNewModeDeProduction(e){
        this.setState({
            newModeDeProduction:e.target.value
        });
      }
    onSubmitLocalisation(e){
        e.preventDefault();
        console.log("here");
        if(this.state.newLocalisation==='' ){
            this.setState({errorMessageLocalisation:'Champ vide, veuillez remplir le champ'});
            
        }
        else if(this.state.district===0){
            this.setState({errorMessageLocalisation:'Veuillez préciser le nom du district dans le formulaire précédent, avant de remplir le champ'});

        }
        else{
                
                    const localisation ={
                        idDistrict : this.state.district,
                        idRegion : this.state.userRegion.id,
                        nomLocalisation : this.state.newLocalisation,
                      
                    };
                    console.log(localisation);
            
                    axios.post(this.state.beginningURL+'localisationpepiniere/create.php',localisation)
                     .then(res => {
                        console.log(res.data);
                        
                        if(res.data==="ok"){
                           
                            //select Localisation
                            axios.get(this.state.beginningURL+'localisationpepiniere/readbyiddistrict.php/?id='+this.state.district)
                                .then(response=>{
                                    this.setState({
                                        localisations:response.data.body
                                    });
                                    //initialize idLocalisationPepiniere
                                    let lLoc=[{value:0,label:""}];
                                    //select Localisations
                                let currentLocalisation=response.data.body;
                                    for(let i=0;i<currentLocalisation.length;i++){
                                        lLoc.push({value:currentLocalisation[i].id, label:currentLocalisation[i].nomLocalisation});
                                    }
                                    this.setState({optionsLocalisations:lLoc});
                                    

                                })
                                .catch(function(error){
                                    console.log(error);
                                });
                                alert('Insertion de la nouvelle localisation achevée avec succès');
                                this.setState({errorMessageLocalisation:'',newLocalisation:'', openLocalisation:false});
       
                        
                        }
                        else{
                            this.setState({errorMessageLocalisation:'Erreur lors de l\'insertion de la nouvelle localisation.Veuillez réessayer.'});
                            
                        }
                    });
                }        
                   
    }
    onSubmitPepinieriste(e){
        e.preventDefault();
      
        if(this.state.newPepinieriste==='' ){
            this.setState({errorMessagePepinieriste:'Champ vide, veuillez remplir le champ'});
           
        }
        else if(this.state.typepepinieriste===0){
            this.setState({errorMessagePepinieriste:'Veuiller choisir le type de pépinière'});
         

        }
        else{
                
                    const pepinieriste ={
                        idTypePepinieriste : this.state.typepepinieriste,
                        nomPepinieriste : this.state.newPepinieriste                      
                    };
                    //console.log(pepinieriste);
            
                    axios.post(this.state.beginningURL+'pepinieriste/create.php',pepinieriste)
                     .then(res => {
                        console.log(res.data);
                        
                        if(res.data==="ok"){
                           
                            //select Pepinieriste
                            axios.get(this.state.beginningURL+'pepinieriste/read.php')
                            .then(response=>{
                                this.setState({
                                    pepinieristes:response.data.body
                                   
                                })
                                let lPep=[{value:0,label:""}];
                                let currentPepinieriste=response.data.body;
                                for(let i=0;i<currentPepinieriste.length;i++){
                                    lPep.push({value:currentPepinieriste[i].id, label:currentPepinieriste[i].nomPepinieriste});
                                }
                                this.setState({optionsPepinieristes:lPep});
                            })
                            .catch(function(error){
                                console.log(error);
                            });
                            alert('Insertion du nouveau pépiniériste achevée avec succès');
                            this.setState({errorMessagePepinieriste:'',newPepinieriste:'', openPepinieriste:false});
         
       
                        
                        }
                        else{
                            this.setState({errorMessagePepinieriste:'Erreur lors de l\'insertion du nouveau pépinieriste.Veuillez réessayer.'});
         
                        }
                    });
                }        
                   
    }
    onSubmitModeDeProduction(e){
        e.preventDefault();
      
        if(this.state.newModeDeProduction==='' ){
            this.setState({errorMessageModeDeProduction:'Champ vide, veuillez remplir le champ'});
        }
        else{
                
                    const modeDeProduction ={
                        modeDeProduction : this.state.newModeDeProduction                  
                    };
                    //console.log(modeDeProduction);
            
                    axios.post(this.state.beginningURL+'modedeproduction/create.php',modeDeProduction)
                     .then(res => {
                        console.log(res.data);
                        
                        if(res.data==="ok"){
                           
                            //select ModeDeProduction
                            axios.get(this.state.beginningURL+'modedeproduction/read.php')
                            .then(response=>{
                                this.setState({
                                    modeDeProductions:response.data.body
                                   
                                })
                                let lMode=[{value:0,label:""}];
                                let currentModeDeProductions=response.data.body;
                                for(let i=0;i<currentModeDeProductions.length;i++){
                                    lMode.push({value:currentModeDeProductions[i].id, label:currentModeDeProductions[i].modeDeProduction});
                                }
                                this.setState({optionsModeDeProductions:lMode});
                            
                        
                            })
                            .catch(function(error){
                                console.log(error);
                            });
                            alert('Insertion de la nouvelle mode de production achevée avec succès');
                            this.setState({errorMessageModeDeProduction:'',newModeDeProduction:'', openModeDeProduction:false});
                        }
                        else{
                            this.setState({errorMessageModeDeProduction:'Erreur lors de l\'insertion de la nouvelle mode de production.Veuillez réessayer.'});
                           
                        }
                    });
                }        
                   
    }
        

      //Modals
      state = {
        showLocalisation: false
      };
      showLocalisationModal = e => {
        this.setState({
            showLocalisation: !this.state.showLocalisation
        });
      };


      onChangeNomPepiniere(e){
        this.setState({
            nomPepiniere:e.target.value
        });
      }

   
    handleChangeDistrict = selectedOptionDistrict => {
        this.setState({ 
            selectedOptionDistrict , 
            district:selectedOptionDistrict.value,
            selectedOptionLocalisation: null,
            localisation:0
        });
         // get thee correspondant idLocalisationPepiniere
         axios.get(this.state.beginningURL+'localisationpepiniere/readbyiddistrict.php/?id='+selectedOptionDistrict.value)
         .then(response=>{
             this.setState({
                 localisations:response.data.body
             });
             //initialize idLocalisationPepiniere
             let lLoc=[{value:0,label:""}];
              //select Localisations
            let currentLocalisation=response.data.body;
             for(let i=0;i<currentLocalisation.length;i++){
                 lLoc.push({value:currentLocalisation[i].id, label:currentLocalisation[i].nomLocalisation});
             }
             this.setState({optionsLocalisations:lLoc});
 
         })
         .catch(function(error){
             console.log(error);
         });
    };
    handleChangeLocalisation = selectedOptionLocalisation => {
        this.setState({ selectedOptionLocalisation , localisation:selectedOptionLocalisation.value});
      };
    handleChangePepinieriste = selectedOptionPepinieriste => {
        this.setState({ selectedOptionPepinieriste , pepinieriste:selectedOptionPepinieriste.value});
      };
    handleChangeTypePepiniere = selectedOptionTypePepiniere => {
        this.setState({ selectedOptionTypePepiniere , typePepiniere:selectedOptionTypePepiniere.value});
      };
    handleChangeModeDeProduction = selectedOptionModeDeProduction => {
        this.setState({ selectedOptionModeDeProduction , modeDeProduction:selectedOptionModeDeProduction.value});
      };
    
      onSubmit(e){     
        e.preventDefault();
       // console.log("nomPepiniere="+this.state.nomPepiniere+", region="+this.state.region+", pepinieriste="+this.state.pepinieriste+", typePepiniere="+this.state.typePepiniere+", mode de production="+this.state.modeDeProduction)
        if((this.state.nomPepiniere==='' )||(this.state.region===0)||(this.state.pepinieriste===0)||(this.state.typePepiniere===0)||(this.state.modeDeProduction===0)){
            this.setState({errorMessage:'Tous les champs doivent être remplis, à part la localisation exacte'});
        }
        else{
                
                    const pepiniere ={
                        id:this.state.idPepiniere,
                        idLocalisationPepiniere : this.state.localisation,
                        idTypePepiniere : this.state.typePepiniere,
                        idPepinieriste : this.state.pepinieriste,
                        idModeDeProduction : this.state.modeDeProduction,
                        nomPepiniere : this.state.nomPepiniere
                    };
                    //console.log(pepiniere);
            
                    axios.post(this.state.beginningURL+'pepiniere/update.php',pepiniere)
                     .then(res => {
                        console.log(res.data);
                        
                        if(res.data==="Pepiniere data updated."){
                            alert('Mise à jour de la pépinière achevée avec succès');
                            window.location="/pepinierelist";
                        }
                        else{
                            alert('Erreur lors de la mise à jour de la pépinière.Veuillez réessayer.');
                        }
                    });
                }     
    }

    deletePepiniere(){
        
        axios.delete(this.state.beginningURL+'pepiniere/delete.php',{data:{id:this.props.match.params.id}})
        .then(res => {
             console.log(res.data);
             this.setState({openSupprimer:false});
             alert("La pépinière a été supprimée avec succès.");
             window.location="/pepinierelist";
         });
    
    }
    annuler(){
        //on annule tout
        this.componentDidMount();
    }


    showButtonDelete(){
        return(
            <Button variant="contained" onClick={this.handleClickOpenSupprimer }  style={buttonDeleteStyle}>
                 <img className="imageSupprimer" alt="supprimer" src="/assets/images/logos/LogoDelete.png"/>&nbsp;
                Supprimer
            </Button>
        )
    }
    showErrorMessage(){
        if(this.state.errorMessage!==''){
            return(
                <Typography component="h5"  style={Design.errorMessageStyle}>
                            {this.state.errorMessage} 
                </Typography>
            );
        }
        else{
            return
        }
        
    }

    //Dialog 
    handleClickOpenSupprimer  = () => {
        this.setState({openSupprimer:true});
      };
  
      handleCloseSupprimer  = () => {
        this.setState({openSupprimer:false});
      };
  


    render() {
        //select (combobox)
        const { selectedOptionDistrict, selectedOptionLocalisation, selectedOptionPepinieriste, selectedOptionTypePepiniere, selectedOptionModeDeProduction,selectedOptionTypePepinieriste} = this.state; 
        //Table
        const classes = useStyles;

        //form 
        const classesForm = useStylesForm;

        

        return (
            <div className="container-fluid">
                <div className="row bg-title">
                    <div className="left-title col-lg-4 col-md-4 col-sm-4 col-xs-12" >
                        <h4 className="text-left-title page-title">P&eacute;pini&egrave;res</h4>
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <h3 className="text-middle-title page-title">{this.state.userFrom}</h3>
                    </div>

                    <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                        {/* <a href="https://wrappixel.com/templates/ampleadmin/" target="_blank"
                            className="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Upgrade
                            to Pro

                        </a> */}
                        <ol className="breadcrumb">
                            
                            <li className="active"><a >P&eacute;pini&egrave;res</a>&nbsp;&nbsp;<i className="linking fa fa-angle-right fa-fw"></i>Modification</li>
                        </ol>
                    </div>
                    {/* <!-- /.col-lg-12 --> */}
                </div>
            
                {/* <!-- content of the page--> */}
                {/* Head */}
                <div className="divAppBar" >
                    <AppBar position="static" style={appBarStyle}>
                        <Toolbar>
                            {/* grid */}
                            
                                <Grid container spacing={3}>
                                    <Grid item xs={3} style={{textAlign:'left'}}>
                                    <Link to='/pepinierelist'>
                                        <Button variant="contained" style={buttonBackStyle}>
                                            Revenir liste
                                        </Button>
                                    </Link>
                                    </Grid>
                                    <Grid item xs={6} style={{textAlign:'center'}}>
                                        <h4 className="smaller-title" >Modification de la p&eacute;pini&egrave;re "{this.state.pepiniere.nomPepiniere}"</h4>
                                    </Grid>
                                    <Grid item xs={3}>  
                                        {this.showButtonDelete()}                                  
                                    </Grid>

                                </Grid>
                              
                            
                            {/* end grid */}
                        
                        </Toolbar>
                    </AppBar>
                </div>
                {/* end head */}
                {/* Dialog supprimer */}
                <Dialog
                                    open={this.state.openSupprimer}
                                    onClose={this.handleCloseSupprimer}
                                    aria-labelledby="alert-dialog-title"
                                    aria-describedby="alert-dialog-description"
                                    >
                                    <DialogTitle  style={Design.fontStyleDialog} id="alert-dialog-title">{"Suppression de pépinière"}</DialogTitle>
                                    <DialogContent>
                                        <DialogContentText id="alert-dialog-description"  style={Design.fontStyleDialog}>
                                        Souhaitez-vous vraiment supprimer la pépinière ID={this.state.idPepiniere} ?
                                        </DialogContentText>
                                    </DialogContent>
                                    <DialogActions>
                                        <Button onClick={this.handleCloseSupprimer}  style={Design.fontStyleCancelDialog} color="primary">
                                        Annuler
                                        </Button>
                                        <Button  onClick={()=> {this.deletePepiniere()}} style={Design.fontStyleDeleteDialog} color="primary" autoFocus>
                                        Supprimer
                                        </Button>
                                    </DialogActions>
                                    </Dialog>
                {/* End Dialog supprimer */}
                {/* Table */}
                <div className="row">
                    <div className="col-sm-12">
                        <div className="white-box formStylePepiniere" >
                        <Container component="main" maxWidth="xs" style={{backgroundColor:'white',borderRadius:'3px'}}>
                            <Paper className={classes.root}>
                            {this.showErrorMessage()}
                            <form className={classesForm.form} onSubmit={this.onSubmit}  noValidate>
                            <label className="label-style">Nom de la p&eacute;pini&egrave;re</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    id="nommaladie"
                                    label="Nom de la pépinière"
                                    name="nompepiniere"
                                    autoComplete="nompepiniere"
                                    autoFocus
                                    inputProps={{style: fontStyle}} 
                                    InputLabelProps={{style: fontStyle}} 
                                    value={this.state.nomPepiniere} onChange={this.onChangeNomPepiniere}
                                />
                                </Grid>
                                <Grid item xs={1}></Grid>
                            </Grid>
                            

                            <label className="label-style">District</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                    <Select
                                        placeholder="District"
                                        value={selectedOptionDistrict}
                                        onChange={this.handleChangeDistrict}
                                        options={this.state.optionsDistricts}
                                    />
                                </Grid>
                                <Grid item xs={1}></Grid>
                            </Grid>
                            
                            
                            <label className="label-style">Localisation</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                    <Select
                                        placeholder="Localisation"
                                        value={selectedOptionLocalisation}
                                        onChange={this.handleChangeLocalisation}
                                        options={this.state.optionsLocalisations}
                                    />
                                </Grid>
                                <Grid item xs={1}>
                                    <i onClick={this.handleClickOpenLocalisation} className="new-icon-style fa  fa-plus-circle fa-fw"></i>
                                </Grid>
                            </Grid>
                            {/* <NewLocalisationModal onClose={this.showLocalisationModal} show={this.state.showLocalisation} show={this.state.showLocalisation} >Nouvelle localisation</NewLocalisationModal> */}
                            
                            {/* <NewLocalisationModal onClose={this.showLocalisationModal} show={this.state.showLocalisation} show={this.state.showLocalisation} >Nouvelle localisation</NewLocalisationModal> */}
                            
                            <label className="label-style">P&eacute;pinieriste</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                    <Select
                                        placeholder="Pépinieriste"
                                        value={selectedOptionPepinieriste}
                                        onChange={this.handleChangePepinieriste}
                                        options={this.state.optionsPepinieristes}
                                    />
                                </Grid>
                                <Grid item xs={1}>
                                    <i onClick={this.handleClickOpenPepinieriste}  className="new-icon-style fa  fa-plus-circle fa-fw"></i>
                                </Grid>
                            </Grid>

                           
                            <label className="label-style">Type de p&eacute;pini&egrave;re</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                    <Select
                                        placeholder="Type de pépinière"
                                        value={selectedOptionTypePepiniere}
                                        onChange={this.handleChangeTypePepiniere}
                                        options={this.state.optionsTypePepinieres}
                                    />
                                </Grid>
                                <Grid item xs={1}></Grid>
                            </Grid>
                            
                            <label className="label-style">Mode de production</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                    <Select
                                        placeholder="Mode de production"
                                        value={selectedOptionModeDeProduction}
                                        onChange={this.handleChangeModeDeProduction}
                                        options={this.state.optionsModeDeProductions}
                                    />
                                </Grid>
                                <Grid item xs={1}>
                                    <i onClick={this.handleClickOpenModeDeProduction} className="new-icon-style fa  fa-plus-circle fa-fw"></i>
                                </Grid>
                            </Grid>
                            


                            <Grid container spacing={1} style={{marginTop:'40px'}} >
                            <Grid item xs={6} >
                              <Button
                                  type="button"
                                  fullWidth
                                  variant="contained" 
                                  style={fontStyle} 
                                  onClick={()=> {this.annuler()}}                               
                              >
                                  Annuler
                              </Button>
                            </Grid>
                            <Grid item xs={6}>
                              <Button
                                  type="submit"
                                  fullWidth
                                  variant="contained"
                                  color="primary"
                                  className={classesForm.submit}
                                  style={fontStyle}
                                  
                              >
                                  Enregistrer
                              </Button>
                            </Grid>
                        </Grid>
                            </form>
                            <br/>
                                
                            </Paper>  
                            {/* end content of the table */}
                            </Container>
                        </div>
                    </div>
                </div>
                {/* end table */}
              
                {/* <!-- /.content of the page --> */}
                 {/* Dialogs */}
                {/* Localisation */}
                <Dialog
                    open={this.state.openLocalisation}
                    onClose={this.handleCloseLocalisation}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                    >
                    <DialogTitle  style={Design.fontStyleDialog} id="alert-dialog-title">{"Enregistrer une nouvelle localisation"}</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description"  style={Design.fontStyleDialog}>
                            (PS. Veuillez bien valider un district avant de remplir le formulaire.)<br/>
                            <Typography component="h5"  style={Design.errorMessageStyle}>
                                    {this.state.errorMessageLocalisation} 
                            </Typography>
                            <label className="label-style"><strong>Nouvelle localisation</strong></label>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    id="newlocalisation"
                                    label="Localisation"
                                    name="newlocalisation"
                                    autoComplete="newlocalisation"
                
                                    inputProps={{style: fontStyle}} 
                                    InputLabelProps={{style: fontStyle}} 
                                    value={this.state.newLocalisation} onChange={this.onChangeNewLocalisation}
                                />  
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                    <form className={classesForm.form} onSubmit={this.onSubmitLocalisation}  noValidate>
                        <Button onClick={this.handleCloseLocalisation}  style={Design.fontStyleCancelDialog} color="primary">
                            Annuler
                        </Button>
                        <Button
                                  type="submit"
                                
                                  variant="contained"
                                  color="primary"
                                  className={classesForm.submit}
                                  style={fontStyle}
                                  autoFocus
                              >
                                  Enregistrer
                        </Button>
                        </form> 
                    </DialogActions>
                   
                </Dialog>
                 {/* Pépiniériste */}
                 <Dialog
                    open={this.state.openPepinieriste}
                    onClose={this.handleClosePepinieriste}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                    >
                    <DialogTitle  style={Design.fontStyleDialog} id="alert-dialog-title">{"Enregistrer un nouveau pépiniériste"}</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description"  style={Design.fontStyleDialog}>
                          
                            <Typography component="h5"  style={Design.errorMessageStyle}>
                                    {this.state.errorMessagePepinieriste} 
                            </Typography>
                            <label className="label-style"><strong>Type de p&eacute;pini&eacute;riste</strong></label>
                            
                                    <Select
                                        placeholder="Type de pépiniériste"
                                        value={selectedOptionTypePepinieriste}
                                        onChange={this.handleChangeTypePepinieriste}
                                        options={this.state.optionsTypePepinieristes}
                                    />
                            
                            <label className="label-style"><strong>Nouveau p&eacute;pini&eacute;riste</strong></label>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    id="newpepinieriste"
                                    placeholder="Pépiniériste"
                                    name="newpepinieriste"
                                    autoComplete="newpepinieriste"
                
                                    inputProps={{style: fontStyle}} 
                                    InputLabelProps={{style: fontStyle}} 
                                    value={this.state.newPepinieriste} onChange={this.onChangeNewPepinieriste}
                                />  
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                    <form className={classesForm.form} onSubmit={this.onSubmitPepinieriste}  noValidate>
                        <Button onClick={this.handleClosePepinieriste}  style={Design.fontStyleCancelDialog} color="primary">
                            Annuler
                        </Button>
                        <Button
                                  type="submit"
                                
                                  variant="contained"
                                  color="primary"
                                  className={classesForm.submit}
                                  style={fontStyle}
                                  autoFocus
                              >
                                  Enregistrer
                        </Button>
                        </form> 
                    </DialogActions>
                   
                </Dialog>
                {/* Mode de production */}
                <Dialog
                    open={this.state.openModeDeProduction}
                    onClose={this.handleCloseModeDeProduction}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                    >
                    <DialogTitle  style={Design.fontStyleDialog} id="alert-dialog-title">{"Enregistrer une nouvelle mode de production"}</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description"  style={Design.fontStyleDialog}>

                            <Typography component="h5"  style={Design.errorMessageStyle}>
                                    {this.state.errorMessageModeDeProduction} 
                            </Typography>
                            <label className="label-style"><strong>Nouvelle Mode de production</strong></label>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    id="newmodedeproduction"
                                    label="Mode de production"
                                    name="newmodedeproduction"
                                    autoComplete="newmodedeproduction"
                
                                    inputProps={{style: fontStyle}} 
                                    InputLabelProps={{style: fontStyle}} 
                                    value={this.state.newModeDeProduction} onChange={this.onChangeNewModeDeProduction}
                                />  
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                    <form className={classesForm.form} onSubmit={this.onSubmitModeDeProduction}  noValidate>
                        <Button onClick={this.handleCloseModeDeProduction}  style={Design.fontStyleCancelDialog} color="primary">
                            Annuler
                        </Button>
                        <Button
                                  type="submit"
                                
                                  variant="contained"
                                  color="primary"
                                  className={classesForm.submit}
                                  style={fontStyle}
                                  autoFocus
                              >
                                  Enregistrer
                        </Button>
                        </form> 
                    </DialogActions>
                   
                </Dialog>

                {/* End Dialogs */}

            </div>
        )
    }
}
