import React, { Component } from 'react';
import "./PepiniereDetails.css";
import * as Config from './../Config/Config';
import { Link } from 'react-router-dom';
import axios from 'axios';

//Search
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
//grid for the search
import Grid from '@material-ui/core/Grid';

//Table
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
//Table
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
// import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';





const  Inventaire= props =>(
    <TableRow hover role="checkbox" tabIndex={-1} key={props.numero}>
        <TableCell style={tableCellStyle}>{props.numero}</TableCell>
        <TableCell style={tableCellStyle}>{props.inventaire.dateEtat}</TableCell>
        <TableCell style={tableCellStyle}>{props.inventaire.nomCampagne}</TableCell>
        <TableCell style={tableCellStyle}>{props.inventaire.nomEspece}</TableCell>
        <TableCell style={tableCellActionColumnStyle}>{props.inventaire.nombre}</TableCell>
    </TableRow>
)

//Table
const useStyles = makeStyles({
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 440,
    },

  });



// form 
const useStylesForm = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));

// end form


//design
let appBarStyle= {borderRadius:'4px',textAlign:'right',backgroundColor:'#000e2f'};
let buttonBackStyle= {backgroundColor:'#0bca4b', color:'white', marginTop:'5px'};
let fontStyle={fontSize:'14px'};
//container
let containerStyle={backgroundColor:'white', marginTop:'-20px'}
//table
let tableCellTitleStyle={fontSize:'14px',fontFamily:'Segoe UI'};
let tableRowTitleStyle={backgroundColor:'rgb(245, 245, 245)'};
let tableCellTitleActionColumnStyle={fontSize:'14px',fontFamily:'Segoe UI', textAlign:'center'};
let tableCellStyle={fontSize:'13px', color:'rgb(90, 88, 88)', fontFamily:'Segoe UI' };
let tableCellActionColumnStyle={fontSize:'13px', color:'rgb(90, 88, 88)', fontFamily:'Segoe UI', textAlign:'center' };


export default class PepiniereDetails extends Component {
    constructor(props) {
        super(props);
        //data
        /*let lInventaires=[
            {dateInventaire:"2020-08-25",campagne:'Campagne 2020', especePlant:'Sapin', nombre:12},
            {dateInventaire:"2020-08-25",campagne:'Campagne 2020', especePlant:'Sapin', nombre:26},
            {dateInventaire:"2020-08-25",campagne:'Campagne 2020', especePlant:'Sapin', nombre:40},
           
        ];*/
        //Data
      
      
    
        this.state = {
            userFrom:'',
            userRegion:{},
            loggedUser:{},
            inventaires:[],
            idPepiniere:this.props.match.params.id,
            pepiniere:{},
            nomPepiniere:'',
            region:'',
            district:'',
            localisation:'',
            pepinieriste:'',
            typePepiniere:'',
            modeDeProduction:'',
            page:0,
            rowsPerPage:10,
            total:0,
            showInventaire:false,         
            beginningURL: Config.beginningURL
        };
        
        this.inventaireList=this.inventaireList.bind(this);
        this.renderInventaire=this.renderInventaire.bind(this);
        this.generateInventaire=this.generateInventaire.bind(this);

    }
    componentDidMount(){
         //Init title for the page
         if(localStorage.getItem("userRegion")!=null){
            this.setState({
              userRegion:JSON.parse(localStorage.getItem("userRegion"))
            });
        }
        if(localStorage.getItem("title")!=null){
            this.setState({
              userFrom:localStorage.getItem("title")
            });
        }
        if(localStorage.getItem("user")!=null){
            this.setState({
              loggedUser:JSON.parse(localStorage.getItem("user"))
            });
        }
        axios.get(this.state.beginningURL+'pepiniere/single_read.php?id='+this.props.match.params.id)
        .then(response=>{
            this.setState({
                pepiniere:response.data,
                nomPepiniere:response.data.nomPepiniere,
                region:response.data.nomRegion,
                district:response.data.nomDistrict,
                localisation:response.data.nomLocalisation,
                pepinieriste:response.data.nomPepinieriste,
                typePepiniere:response.data.nomType,
                modeDeProduction:response.data.modeDeProduction
               
            })
        })
        .catch(function(error){
            console.log(error);
        });
        
       
    }
    inventaireList(){
        let i=0;
      
        return this.state.inventaires.slice(this.state.page * this.state.rowsPerPage, this.state.page * this.state.rowsPerPage + this.state.rowsPerPage).map((currentInventaire) => {
            i++;
            return <Inventaire numero={i}  inventaire={currentInventaire}  key={i}/>;
        });
     
    }

    generateInventaire(){
        const data ={
            idPepiniere:+this.props.match.params.id,
            idRegion : this.state.userRegion.id,
            wordToSearch:"",
            userModule:this.state.loggedUser.module,
            userType:this.state.loggedUser.type,
            userProfil:this.state.loggedUser.profil
        };

      
        //console.log(data);
        axios.post(this.state.beginningURL+'etatdestock/generate.php', data)
        .then(response =>{
            //console.log(response.data);
               
                this.setState({ 
                    showInventaire:true, 
                    inventaires:response.data.body,
                    total:response.data.total, 
                    page:0,
                    rowsPerPage:10
                });
        })
        .catch(error=>{
            console.log(error);
        })
    }

    renderInventaire(classes,classesForm,handleChangePage,handleChangeRowsPerPage){
        if(this.state.showInventaire){
            return(
                 
                <Paper className={classes.root}>
                                
                <TableContainer className={classes.container}>
                    <Table  stickyHeader aria-label="sticky table">
                    <TableBody>
                        <TableRow style={tableRowTitleStyle}>
                            <TableCell style={tableCellTitleStyle}>Num&eacute;ro</TableCell>
                            <TableCell style={tableCellTitleStyle}>Date</TableCell>
                            <TableCell style={tableCellTitleStyle}>Campagne</TableCell>
                            <TableCell style={tableCellTitleStyle}>Esp&egrave;ce plants</TableCell>
                            <TableCell style={tableCellTitleActionColumnStyle}>Nombres</TableCell>
                        </TableRow>
                        
                    </TableBody>
                    <TableBody>
                        {this.inventaireList()}       
                    </TableBody>
                    <TableBody>
                        <TableRow style={tableRowTitleStyle}>
                            <TableCell style={tableCellTitleStyle}></TableCell>
                            <TableCell style={tableCellTitleStyle}></TableCell>
                            <TableCell style={tableCellTitleStyle}></TableCell>
                            <TableCell style={tableCellTitleStyle}>Total</TableCell>
                            <TableCell style={tableCellTitleActionColumnStyle}>{this.state.total}</TableCell>
                        </TableRow> 
                    </TableBody>

                    </Table>
                </TableContainer>
                <TablePagination
                    style={tableCellStyle}
                    rowsPerPageOptions={[10, 25, 100]}
                    component="div"
                    count={this.state.inventaires.length}
                    rowsPerPage={this.state.rowsPerPage}
                    page={this.state.page}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                />
            </Paper>  


            );
        }
        else{
            return (
                <div>
                    <Button
                        type="button"
                        
                        variant="contained"
                    
                        className={classesForm.submit}
                        style={fontStyle}
                        onClick={()=> {this.generateInventaire()}}
                    >
                    INVENTAIRE DE PLANTS
                    </Button>
                </div>
            );
        }

    }

      
    
      


    render() {
       //Table
        const classes = useStyles;
        
        const handleChangePage = (event, newPage) => {
            this.setState({page:newPage})
            //setPage(newPage);
        };
        const handleChangeRowsPerPage = (event) => {
            //setRowsPerPage(+event.target.value);
            //setPage(0);
            this.setState({rowsPerPage:+event.target.value});
            this.setState({page:0});
        };
        //end table

        //form 
        const classesForm = useStylesForm;

        

        return (
            <div className="container-fluid">
                <div className="row bg-title">
                    <div className="left-title col-lg-4 col-md-4 col-sm-4 col-xs-12" >
                        <h4 className="text-left-title page-title">P&eacute;pini&egrave;res</h4>
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <h3 className="text-middle-title page-title">{this.state.userFrom}</h3>
                    </div>

                    <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                        {/* <a href="https://wrappixel.com/templates/ampleadmin/" target="_blank"
                            className="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Upgrade
                            to Pro

                        </a> */}
                        <ol className="breadcrumb">
                            
                            <li className="active"><a>P&eacute;pini&egrave;res</a>&nbsp;&nbsp;<i className="linking fa fa-angle-right fa-fw"></i>D&eacute;tails</li>
                        </ol>
                    </div>
                    {/* <!-- /.col-lg-12 --> */}
                </div>
            
                {/* <!-- content of the page--> */}
                {/* Head */}
                <div className="divAppBar" >
                    <AppBar position="static" style={appBarStyle}>
                        <Toolbar>
                            {/* grid */}
                            
                                <Grid container spacing={3}>
                                    <Grid item xs={3} style={{textAlign:'left'}}>
                                    <Link to='/pepinierelist'>
                                        <Button variant="contained" style={buttonBackStyle}>
                                            Revenir liste
                                        </Button>
                                    </Link>
                                    </Grid>
                                    <Grid item xs={6} style={{textAlign:'center'}}>
                                        <h4 className="smaller-title" >D&eacute;tails sur la P&eacute;pini&egrave;re "{this.state.nomPepiniere}" + inventaire</h4>
                                    </Grid>
                                    <Grid item xs={3}>                                    
                                    </Grid>
                                </Grid>
                            
                            {/* end grid */}
                        
                        </Toolbar>
                    </AppBar>
                </div>
                {/* end head */}
                {/* Table */}
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box formStyle1" >
                            <Container component="main" maxWidth="md" style={containerStyle}>
                                <div className={classesForm.paper} >                      
                                    <div className="contentPepiniereDetails" >
                                        <div className=" row">
                                            <h4 className="labelContent">Nom de la p&eacute;pini&egrave;re: </h4>
                                        </div>  
                                        <div className=" row">
                                                {this.state.nomPepiniere}<br/>
                                        </div> 
                                        <div className=" row">
                                            <h4 className="labelContent">R&eacute;gion: </h4> 
                                        </div> 
                                        <div className=" row">
                                                {this.state.region}<br/>
                                        </div> 
                                        <div className=" row">
                                            <h4 className="labelContent">District: </h4> 
                                        </div> 
                                        <div className=" row">
                                            {this.state.district}<br/>
                                        </div> 
                                        <div className=" row">
                                            <h4 className="labelContent">Localisation: </h4>
                                        </div>
                                        <div className=" row">
                                                {this.state.localisation}<br/>
                                        </div>
                                        <div className=" row">
                                            <h4 className="labelContent">P&eacute;pini&egrave;riste: </h4> 
                                            
                                        </div>
                                        <div className="row">
                                                {this.state.pepinieriste}<br/>
                                        </div>
                                        <div className=" row">
                                            <h4 className="labelContent">Type de p&eacute;pin&egrave;re: </h4> 
                                        </div>
                                        <div className=" row">
                                            {this.state.typePepiniere}<br/>
                                        </div>
                                        <div className=" row">
                                            <h4 className="labelContent">Mode de production: </h4> 
                                        </div>
                                        <div className=" row">
                                                {this.state.modeDeProduction} <br/>
                                        </div>

                                        <Grid container spacing={1} style={{marginTop:'40px'}} >
                                            <Grid item xs={8} > </Grid>
                                            <Grid item xs={4}>
                                            <Link to={"/pepiniereedit/"+this.state.idPepiniere}>
                                                <Button
                                                    type="button"
                                                    fullWidth
                                                    variant="contained"
                                                    color="primary"
                                                    className={classesForm.submit}
                                                    style={fontStyle}
                                                >
                                                Modifier
                                                </Button>
                                            </Link>
                                            </Grid>
                                        </Grid> 
                                    </div>
                                    <br/>
                                    <div className="" >
                                        <div className=" ">
                                                <h2 className="labelInventaire">Inventaire: </h2><br/>
                                                
                                        </div>
                                        {this.renderInventaire(classes,classesForm,handleChangePage,handleChangeRowsPerPage)}
                                       
                                    </div>
                                    
                                </div>
                            </Container>
                        </div>
                    </div>
                </div>
                {/* end table */}
              
                {/* <!-- /.content of the page --> */}

            </div>
        )
    }
}
