import React, { Component } from 'react';
import "./CommandeDetails.css";
import * as Config from './../Config/Config';
import { Link } from 'react-router-dom';
//import axios from 'axios';

//Search
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
//grid for the search
import Grid from '@material-ui/core/Grid';

//Table
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
//Table
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';

import TableRow from '@material-ui/core/TableRow';





const  Plant= props =>(
    <TableRow hover role="checkbox" tabIndex={-1} key={props.plant.espece}>
        <TableCell style={tableCellStyle}>{props.plant.espece}</TableCell>
        <TableCell style={tableCellStyle}>{props.plant.nombre}</TableCell>
    </TableRow>
)

//Table
const useStyles = makeStyles({
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 440,
    },

  });



// form 
const useStylesForm = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));

// end form


//design
let appBarStyle= {borderRadius:'4px',textAlign:'right',backgroundColor:'#000e2f'};
let buttonBackStyle= {backgroundColor:'#0bca4b', color:'white', marginTop:'5px'};
//let fontStyle={fontSize:'14px'};
//container
let containerStyle={backgroundColor:'white', marginTop:'-20px'}
//table
let tableCellTitleStyle={fontSize:'14px',fontFamily:'Segoe UI'};
let tableRowTitleStyle={backgroundColor:'rgb(245, 245, 245)'};
let tableCellStyle={fontSize:'13px', color:'rgb(90, 88, 88)', fontFamily:'Segoe UI' };


export default class CommandeDetails extends Component {
    constructor(props) {
        super(props);
        //data
        let lJeunesPlants=[
            {espece:'cactus', nombre:1500},
            {espece:'plante 1', nombre:500},
            {espece:'plante 2', nombre:500},
           
        ];
        //Data
      
      
    
        this.state = {
            userFrom:'',
            userRegion:{},
         
            idCommande:this.props.match.params.id,
            dateCommande: '2020-12-25',
            campagne:'Campagne 2020',
            pepiniere: 'Pepiniere 1',
            idReboisement: 25,
            nbTotalJeunesPlants:2500,
            jeunesPlants:lJeunesPlants,
            beginningURL: Config.beginningURL
        };
        
        this.showJeunesPlants=this.showJeunesPlants.bind(this);

    }
    componentDidMount(){
          //Init title for the page
          if(localStorage.getItem("userRegion")!=null){
            this.setState({
              userRegion:JSON.parse(localStorage.getItem("userRegion"))
            });
        }
        if(localStorage.getItem("title")!=null){
            this.setState({
              userFrom:localStorage.getItem("title")
            });
        }
        
       
    }
 
    showJeunesPlants(){
        return this.state.jeunesPlants.map((currentPlant) => {
           
            return <Plant   plant={currentPlant}  key={currentPlant.espece}/>;
        });
    }

      
    
      


    render() {
     
        //Table
        const classes = useStyles;
        
     
        //end table

        //form 
        const classesForm = useStylesForm;

        

        return (
            <div className="container-fluid">
                <div className="row bg-title">
                    <div className="left-title col-lg-4 col-md-4 col-sm-4 col-xs-12" >
                        <h4 className="text-left-title page-title">P&eacute;pini&egrave;res</h4>
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <h3 className="text-middle-title page-title">{this.state.userFrom}</h3>
                    </div>

                    <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                        {/* <a href="https://wrappixel.com/templates/ampleadmin/" target="_blank"
                            className="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Upgrade
                            to Pro

                        </a> */}
                        <ol className="breadcrumb">
                            
                            <li className="active"><a>P&eacute;pini&egrave;res</a>&nbsp;&nbsp;<i className="linking fa fa-angle-right fa-fw"></i>D&eacute;tails Commande</li>
                        </ol>
                    </div>
                    {/* <!-- /.col-lg-12 --> */}
                </div>
            
                {/* <!-- content of the page--> */}
                {/* Head */}
                <div className="divAppBar" >
                    <AppBar position="static" style={appBarStyle}>
                        <Toolbar>
                            {/* grid */}
                            
                                <Grid container spacing={3}>
                                    <Grid item xs={3} style={{textAlign:'left'}}>
                                    <Link to='/commandereboisement'>
                                        <Button variant="contained" style={buttonBackStyle}>
                                            Revenir liste
                                        </Button>
                                    </Link>
                                    </Grid>
                                    <Grid item xs={6} style={{textAlign:'center'}}>
                                        <h4 className="smaller-title" >D&eacute;tails sur la commande n° {this.state.idCommande}</h4>
                                    </Grid>
                                    <Grid item xs={3}>                                    
                                    </Grid>
                                </Grid>
                            
                            {/* end grid */}
                        
                        </Toolbar>
                    </AppBar>
                </div>
                {/* end head */}
                {/* Table */}
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box formStyle1" >
                            <Container component="main" maxWidth="md" style={containerStyle}>
                                <div className={classesForm.paper} >                      
                                    <div className="contentPepiniereDetails" >
                                        <div className=" row">
                                            <h4 className="labelContent">Date de la commande: </h4>
                                        </div>  
                                        <div className=" row">
                                                {this.state.dateCommande}<br/>
                                        </div> 
                                        <div className=" row">
                                            <h4 className="labelContent">Campagne: </h4>
                                        </div>  
                                        <div className=" row">
                                                {this.state.campagne}<br/>
                                        </div> 
                                        <div className=" row">
                                            <h4 className="labelContent">Pepiniere: </h4>
                                        </div>  
                                        <div className=" row">
                                                {this.state.pepiniere}<br/>
                                        </div> 
                                        <div className=" row">
                                            <h4 className="labelContent">Id Reboisement: </h4> 
                                        </div> 
                                        <div className=" row">
                                                {this.state.idReboisement}<br/>
                                        </div> 
                                        <div className=" row">
                                            <h4 className="labelContent">Nombre total de jeunes plants: </h4> 
                                        </div> 
                                        <div className=" row">
                                            {this.state.nbTotalJeunesPlants}<br/>
                                        </div> 
                                        <div className=" row">
                                            <h4><strong>D&eacute;tails: </strong></h4> <br/>
                                        </div> 
                                        <div className=" row">
                                        <Paper className={classes.root}>
                                
                                            <TableContainer className={classes.container}>
                                                <Table  stickyHeader aria-label="sticky table">
                                                <TableBody>
                                                    <TableRow style={tableRowTitleStyle}>
                                                        <TableCell style={tableCellTitleStyle}>Esp&egrave;ce</TableCell>
                                                        <TableCell style={tableCellTitleStyle}>Nombre</TableCell>
                                                    </TableRow>
                                                    
                                                </TableBody>
                                                <TableBody>
                                                    {this.showJeunesPlants()}       
                                                </TableBody>
                                                

                                                </Table>
                                            </TableContainer>
                                           
                                        </Paper>  

                                          
                                        </div> 
                                        

                                    </div>
                                    </div>
                            </Container>
                        </div>
                    </div>
                </div>
                {/* end table */}
              
                {/* <!-- /.content of the page --> */}

            </div>
        )
    }
}
