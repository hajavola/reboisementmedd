import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import * as Design from './../../Config/Design';



export default class NewLocalisationModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
          open:false
        };
       
    }
    
    handleClickOpen  = () => {
      this.setState({open:true});
    };

    handleClose  = () => {
      this.setState({open:false});
    };

   
    
    render() {     

      
     


        return (
          <div>
            <Button style={Design.fontStyleDialog} variant="outlined" color="primary" onClick={this.handleClickOpen}>
              Open alert dialog
            </Button>
            <Dialog
              open={this.state.open}
              onClose={this.handleClose}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
            >
              <DialogTitle  style={Design.fontStyleDialog} id="alert-dialog-title">{"Use Google's location service?"}</DialogTitle>
              <DialogContent>
                <DialogContentText id="alert-dialog-description"  style={Design.fontStyleDialog}>
                  Let Google help apps determine location. This means sending anonymous location data to
                  Google, even when no apps are running.
                </DialogContentText>
              </DialogContent>
              <DialogActions>
                <Button onClick={this.handleClose}  style={Design.fontStyleCancelDialog} color="primary">
                  Disagree
                </Button>
                <Button onClick={this.handleClose}  style={Design.fontStyleDialog} color="primary" autoFocus>
                  Agree
                </Button>
              </DialogActions>
            </Dialog>
          </div>
        )
    }
}
