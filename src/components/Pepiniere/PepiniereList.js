import React, { Component } from 'react';
import "./PepiniereList.css";
import * as Config from './../Config/Config';
import { Link } from 'react-router-dom';
import axios from 'axios';

//Search
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
//grid for the search
import Grid from '@material-ui/core/Grid';

//Table
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
// import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';

//select
import Select from 'react-select';

const  Pepiniere= props =>(
    <TableRow hover role="checkbox" tabIndex={-1} key={props.pepiniere.code}>
        <TableCell style={tableCellStyle}>{props.pepiniere.id}</TableCell>
        <TableCell style={tableCellStyle}>{props.pepiniere.nomPepiniere}</TableCell>
        <TableCell style={tableCellActionColumnStyle}>{props.showAddToCampagne(props.idRegion, props.pepiniere.id)}</TableCell>
        <TableCell style={tableCellActionColumnStyle}><Link to={"/pepinieredetails/"+props.pepiniere.id}><img className="imageModifier" alt="details" src="assets/images/logos/logoAdd.png"/></Link></TableCell>
    </TableRow>
)

//Table
const useStyles = makeStyles({
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 440,
    },

  });
//design
let appBarStyle= {borderRadius:'4px',textAlign:'right',backgroundColor:'#000e2f'};
//grid
let gridSelectStyle={marginBottom:'15px'};
//table
let tableCellTitleStyle={fontSize:'14px',fontFamily:'Segoe UI'};
let tableRowTitleStyle={backgroundColor:'rgb(245, 245, 245)'};
let tableCellTitleActionColumnStyle={fontSize:'14px',fontFamily:'Segoe UI', textAlign:'center'};
let tableCellStyle={fontSize:'13px', color:'rgb(90, 88, 88)', fontFamily:'Segoe UI' };
let tableCellActionColumnStyle={fontSize:'13px', color:'rgb(90, 88, 88)', fontFamily:'Segoe UI', textAlign:'center' };
let buttonDetailsStyle={backgroundColor:'#ca4f0b', color:'white', marginTop:'5px'};


export default class PepiniereList extends Component {
    constructor(props) {
        super(props);
        //data
        /*let lpepinieres=[
            {id:1, idLocalisation:1,idTypePepiniere:1, idPepinieriste:1, idModeDeProduction:1, nomPepiniere:'La pépinière 1',nbJeunesPlants:2000},
            {id:2, idLocalisation:1,idTypePepiniere:1, idPepinieriste:1, idModeDeProduction:1, nomPepiniere:'La pépinière 2',nbJeunesPlants:2000},
            {id:3, idLocalisation:1,idTypePepiniere:1, idPepinieriste:1, idModeDeProduction:1, nomPepiniere:'La pépinière 3',nbJeunesPlants:2000},
            {id:4, idLocalisation:1,idTypePepiniere:1, idPepinieriste:1, idModeDeProduction:1, nomPepiniere:'La pépinière 4',nbJeunesPlants:2000},
            {id:5, idLocalisation:1,idTypePepiniere:1, idPepinieriste:1, idModeDeProduction:1, nomPepiniere:'La pépinière 5',nbJeunesPlants:2000},
            {id:6, idLocalisation:1,idTypePepiniere:1, idPepinieriste:1, idModeDeProduction:1, nomPepiniere:'La pépinière 6',nbJeunesPlants:2000},
            {id:7, idLocalisation:1,idTypePepiniere:1, idPepinieriste:1, idModeDeProduction:1, nomPepiniere:'La pépinière 7',nbJeunesPlants:2000},
            {id:8, idLocalisation:1,idTypePepiniere:1, idPepinieriste:1, idModeDeProduction:1, nomPepiniere:'La pépinière 8',nbJeunesPlants:2000},
            {id:9, idLocalisation:1,idTypePepiniere:1, idPepinieriste:1, idModeDeProduction:1, nomPepiniere:'La pépinière 9',nbJeunesPlants:2000},
            {id:10, idLocalisation:1,idTypePepiniere:1, idPepinieriste:1, idModeDeProduction:1, nomPepiniere:'La pépinière 10',nbJeunesPlants:2000},
            {id:11, idLocalisation:1,idTypePepiniere:1, idPepinieriste:1, idModeDeProduction:1, nomPepiniere:'La pépinière 11',nbJeunesPlants:2000},
        
        ];*/
        /*let lcampagnes=[
            {id:0,nomCampagne: 'Toutes Campagnes'},
            {id:1,nomCampagne: 'Campagne 2020'},
            {id:2,nomCampagne: 'Campagne 2019'},
            {id:3,nomCampagne: 'Campagne 2018'}, 
        ];
        let lregions=[
            {id:0,nomRegion: 'Toutes'},
            {id:1,nomRegion: 'Analamanga'},
            {id:2,nomRegion: 'Bongolava'},
            {id:3,nomRegion: 'SAVA'}, 
        ];*/
    
        this.state = {
            userFrom:'',
            userRegion:{},
            pepinieres:[],
            campagnes:[],
            regions:[],
            optionsRegions:[],
            optionsCampagnes:[],
            currentRegion:'toutes',
            currentCampagne:'toutes campagnes',
            wordToSearch:'',
            nbSearchResult:0,
            isSubmit:false,
            page:0,
            rowsPerPage:10,
            beginningURL: Config.beginningURL
        };
        this.pepiniereList=this.pepiniereList.bind(this);
        this.onChangeSearch= this.onChangeSearch.bind(this);
        this.onSubmitSearch=this.onSubmitSearch.bind(this);
        this.displaySearchResult=this.displaySearchResult.bind(this);
        this.onInitSearch=this.onInitSearch.bind(this);
        this.refreshTable=this.refreshTable.bind(this);

        this.showAddToCampagne=this.showAddToCampagne.bind(this);

    }
    //select (combobox) 
    state = {
        selectedOptionRegion: null,
        selectedOptionCampagne: null,
    };

    componentDidMount(){
          //Init title for the page
          if(localStorage.getItem("userRegion")!=null){
            this.setState({
              userRegion:JSON.parse(localStorage.getItem("userRegion"))
            });
        }
        if(localStorage.getItem("title")!=null){
            this.setState({
              userFrom:localStorage.getItem("title")
            });
        }
        
        
        //select campagne
        let idLastCampagne=0; //all
        
        axios.get(this.state.beginningURL+'campagne/read.php')
        .then(response =>{
                this.setState({
                    campagnes:response.data.body
                });
                let lcamp=[{value:0, label:'Toutes'}];
                let currentCampagnes=response.data.body;
                for(let i=0;i<currentCampagnes.length;i++){
                    lcamp.push({value:currentCampagnes[i].id, label:currentCampagnes[i].nomCampagne});
                }
                this.setState({optionsCampagnes:lcamp});

                //get lastCampagne
                /*if(currentCampagnes.length>0){
                    idLastCampagne=currentCampagnes[0].id;
                    this.setState({
                        selectedOptionCampagne:{value:currentCampagnes[0].id, label:currentCampagnes[0].nomCampagne},
                        currentCampagne:currentCampagnes[0].nomCampagne
                    });
                }*/

                //Get the regions 
                //Verify first if the logged in user corresponds to a specific region
                let idCurrentRegion=0;
                if(localStorage.getItem("user")!=null){
                    const  loggedUser=JSON.parse(localStorage.getItem("user"));
                    idCurrentRegion=loggedUser.idRegion;
                    //select for the region
                    axios.get(this.state.beginningURL+'region/read.php')
                    .then(response =>{
                            this.setState({
                                regions:response.data.body

                            });
                            let lreg=[{value:0, label:'Toutes'}];
                            let currentRegions=response.data.body;
                        
                            for(let i=0;i<currentRegions.length;i++){
                                
                                lreg.push({value:currentRegions[i].id, label:currentRegions[i].nomRegion});
                                //verify if the id corresponds to the IDRegion of the loggedIn User, and set it as the default on the select option
                                if(currentRegions[i].id===idCurrentRegion){
                                    this.setState({
                                        selectedOptionRegion:{value:currentRegions[i].id, label:currentRegions[i].nomRegion},
                                        currentRegion:currentRegions[i].nomRegion
                                    });
                                }
                            }
                            this.setState({optionsRegions:lreg});

                            //get Pepiniere depending on Region and Campagne
                            // get all the pepinieres
                            const data ={
                                wordToSearch:this.state.wordToSearch,
                                idRegion:idCurrentRegion,
                                idCampagne:idLastCampagne
                      
                            };
                            //console.log(data);
                            //axios.post(this.state.beginningURL+'pepiniere/readperregionandcampagneid.php', data)
                            //console.log(this.state.beginningURL+'pepiniere/readperregionandcampagneidget.php?wordToSearch='+this.state.wordToSearch+'&idRegion='+idCurrentRegion+'&idCampagne='+idLastCampagne);
                            axios.get(this.state.beginningURL+'pepiniere/readperregionandcampagneidget.php?wordToSearch='+this.state.wordToSearch+'&idRegion='+idCurrentRegion+'&idCampagne='+idLastCampagne)
                            .then(response =>{
                                    this.setState({
                                        pepinieres:response.data.body

                                    });
                            })
                            .catch(error=>{
                                console.log(error);
                            })


                           
                    })
                    .catch(error=>{
                        console.log(error);
                    })

                 
                }
        })
        .catch(error=>{
            console.log(error);
        })       
      }

  
    handleChangeRegion = selectedOptionRegion => {
        this.setState({ selectedOptionRegion , currentRegion:selectedOptionRegion.label});
        let valueCampagne=0;
        if(this.state.selectedOptionCampagne!=null){
            valueCampagne=this.state.selectedOptionCampagne.value;
        }

        this.refreshTable(this.state.wordToSearch, selectedOptionRegion.value,valueCampagne );
       
      };
    handleChangeCampagne = selectedOptionCampagne => {
        this.setState({ selectedOptionCampagne, currentCampagne:selectedOptionCampagne.label });
        let valueRegion=0;
        if(this.state.selectedOptionRegion!=null){
            valueRegion=this.state.selectedOptionRegion.value;
        }
        this.refreshTable(this.state.wordToSearch, valueRegion, selectedOptionCampagne.value);
      
      };


    // Search
    onChangeSearch(e){
        this.setState({
            wordToSearch:e.target.value
        });
        //if we erase the word written
        if(e.target.value===""){
            this.setState({
                isSubmit:false,   
            });
            this.onInitSearch();
        }
    }
    onSubmitSearch(e){
        e.preventDefault();
       
        if(this.state.wordToSearch===""){
            this.setState({
                isSubmit:false,   
            });
            this.onInitSearch();
        }
        else{
            this.setState({
                isSubmit:true,   
            });
            const constraintPepiniere ={
                wordToSearch:this.state.wordToSearch,
                idRegion:this.state.selectedOptionRegion.value,
                idCampagne:this.state.selectedOptionCampagne.value
      
            };
            //axios.post(this.state.beginningURL+'pepiniere/readperregionandcampagneid.php', constraintPepiniere)
            //console.log(this.state.beginningURL+'pepiniere/readperregionandcampagneidget.php?wordToSearch='+this.state.wordToSearch+'&idRegion='+this.state.selectedOptionRegion.value+"&idCampagne="+this.state.selectedOptionCampagne.value);
            axios.get(this.state.beginningURL+'pepiniere/readperregionandcampagneidget.php?wordToSearch='+this.state.wordToSearch+'&idRegion='+this.state.selectedOptionRegion.value+"&idCampagne="+this.state.selectedOptionCampagne.value)
            .then(response =>{
                    this.setState({
                        pepinieres:response.data.body,
                        nbSearchResult:response.data.itemCount,
                        isSubmit:true
                    });
            })
            .catch(error=>{
                console.log(error);
                this.setState({
                    pepinieres:[],
                    nbSearchResult:0,
                    isSubmit:true
                });
            })
         
        }    
    }
    onInitSearch(){
        this.refreshTable(this.state.wordToSearch,this.state.selectedOptionRegion.value,this.state.selectedOptionCampagne.value);

    }

    refreshTable(wordToSearch1,idRegion1,idCampagne1){
        const constraintPepiniere ={
            wordToSearch:wordToSearch1,
            idRegion:idRegion1,
            idCampagne:idCampagne1
        };
        //console.log(constraintPepiniere);
        //axios.post(this.state.beginningURL+'pepiniere/readperregionandcampagneid.php', constraintPepiniere)
        //console.log(this.state.beginningURL+'pepiniere/readperregionandcampagneidget.php?wordToSearch='+wordToSearch1+'&idRegion='+idRegion1+'&idCampagne='+idCampagne1);
        axios.get(this.state.beginningURL+'pepiniere/readperregionandcampagneidget.php?wordToSearch='+wordToSearch1+'&idRegion='+idRegion1+'&idCampagne='+idCampagne1)
        .then(response =>{
                //console.log(response);
                this.setState({
                    pepinieres:response.data.body

                });
        })
        .catch(error=>{
            console.log(error);
            this.setState({
                pepinieres:[],
                nbSearchResult:0,
            });
        })

    }

    displaySearchResult(){
        if(this.state.isSubmit===true){
        return <div className="searchResultDiv"><b className="searchResultStyle">{this.state.nbSearchResult} R&eacute;sultat(s) trouv&eacute;(s) - R&eacute;gion {this.state.currentRegion} - {this.state.currentCampagne}</b></div> 
        } 
        else{
            return 
        }  
    }

    showAddToCampagne(idRegion,idPepiniere){
        if(idRegion===JSON.parse(localStorage.getItem("userRegion")).id){
            return(<Link to={"/addtocampagne/"+idPepiniere}>Ajouter à une campagne</Link>);
        }
        else{
            return
        }
    }


    pepiniereList(){

        return this.state.pepinieres.slice(this.state.page * this.state.rowsPerPage, this.state.page * this.state.rowsPerPage + this.state.rowsPerPage).map((currentPepiniere) => {
            return <Pepiniere idRegion={this.state.selectedOptionRegion.value} showAddToCampagne={this.showAddToCampagne} pepiniere={currentPepiniere}  key={currentPepiniere.id}/>;
        });
     
    }

    render() {
        //select (combobox)
        const { selectedOptionRegion, selectedOptionCampagne } = this.state; 
        //Table
        const classes = useStyles;
        
        const handleChangePage = (event, newPage) => {
            this.setState({page:newPage})
            //setPage(newPage);
        };
        const handleChangeRowsPerPage = (event) => {
            //setRowsPerPage(+event.target.value);
            //setPage(0);
            this.setState({rowsPerPage:+event.target.value});
            this.setState({page:0});
        };
        //end table

        

        return (
            <div className="container-fluid">
                <div className="row bg-title">
                    <div className="left-title col-lg-4 col-md-4 col-sm-4 col-xs-12" >
                        <h4 className="text-left-title page-title">P&eacute;pini&egrave;res</h4>
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <h3 className="text-middle-title page-title">{this.state.userFrom}</h3>
                    </div>

                    <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                        {/* <a href="https://wrappixel.com/templates/ampleadmin/" target="_blank"
                            className="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Upgrade
                            to Pro

                        </a> */}
                        <ol className="breadcrumb">
                            
                            <li className="active"><a>P&eacute;pini&egrave;res</a>&nbsp;&nbsp;<i className="linking fa fa-angle-right fa-fw"></i>Liste</li>
                        </ol>
                    </div>
                    {/* <!-- /.col-lg-12 --> */}
                </div>
            
                {/* <!-- content of the page--> */}
                {/* Head */}
                <div className="divAppBar" >
                    <AppBar position="static" style={appBarStyle}>
                        <Toolbar>
                            {/* grid */}
                            
                                <Grid container spacing={3}>
                                    <Grid item xs={3} style={{textAlign:'left'}}>
                                    <Link to='/newpepiniere'>
                                        <Button variant="contained" style={buttonDetailsStyle}>
                                            Nouvelle
                                        </Button>
                                    </Link>
                                    </Grid>
                                    <Grid item xs={6} style={{textAlign:'center'}}>
                                        <h4 className="smaller-title" >Liste des p&eacute;pini&egrave;res - R&eacute;gion {this.state.currentRegion} - {this.state.currentCampagne}</h4>
                                    </Grid>
                                    <Grid item xs={3}>
                                   
                                        <div className="div-search" >
                                        <form role="search" id="search" className="app-search hidden-sm hidden-xs m-r-10" onSubmit={this.onSubmitSearch} noValidate>
                                            <input type="text" placeholder="Rechercher..." className="form-control"
                                                value={this.state.wordToSearch}
                                                onChange={this.onChangeSearch}/> 
                                             <a  onClick={this.onSubmitSearch}>
                                                <i className="fa fa-search"></i>
                                            </a> 
                                        </form>

                                      
                                        </div>
                                    
                                    </Grid>
                                </Grid>
                            
                            {/* end grid */}
                        
                        </Toolbar>
                    </AppBar>
                </div>
                {/* end head */}
                {/* Table */}
                {/* <div class="row"> */}
                <div>
                    {/* <div class="col-sm-12"> */}
                    <div>
                        {/* <div class="white-box"> */}
                        <div>
                            
                            <Grid container spacing={2} style={gridSelectStyle}>
                                <Grid item xs={6} >
                                </Grid>
                                <Grid item xs={3} >
                                <Select
                                        placeholder="Régions"
                                        value={selectedOptionRegion}
                                        onChange={this.handleChangeRegion}
                                        options={this.state.optionsRegions}
                                       
                                        
                                    />
                                
                                </Grid>
                                <Grid item xs={3}>
                                <Select
                                        placeholder="Campagnes"
                                        value={selectedOptionCampagne}
                                        onChange={this.handleChangeCampagne}
                                        options={this.state.optionsCampagnes}                                        
                                    />
                                </Grid>
                            </Grid>

                            {/* <h3 class="box-title">Basic Table</h3> */}
                            {/* <p class="text-muted">Add class <code>.table</code></p> */}
                             {/* Content of the table */}
                             {this.displaySearchResult()}
                            <Paper className={classes.root}>
                                
                                <TableContainer className={classes.container}>
                                    <Table  stickyHeader aria-label="sticky table">
                                    <TableBody>
                                        <TableRow style={tableRowTitleStyle}>
                                            <TableCell style={tableCellTitleStyle}>Id</TableCell>
                                            <TableCell style={tableCellTitleStyle}>Nom de la p&eacute;pini&egrave;re</TableCell>
                                            
                                            <TableCell style={tableCellTitleActionColumnStyle}>Ajouter à une campagne</TableCell>
                                            <TableCell style={tableCellTitleActionColumnStyle}>D&eacute;tails</TableCell>
                                        </TableRow>
                                        
                                    </TableBody>
                                    <TableBody>
                                        {this.pepiniereList()}       
                                    </TableBody>
                                    </Table>
                                </TableContainer>
                                <TablePagination
                                    style={tableCellStyle}
                                    rowsPerPageOptions={[10, 25, 100]}
                                    component="div"
                                    count={this.state.pepinieres.length}
                                    rowsPerPage={this.state.rowsPerPage}
                                    page={this.state.page}
                                    onChangePage={handleChangePage}
                                    onChangeRowsPerPage={handleChangeRowsPerPage}
                                />
                            </Paper>  
                    

                            {/* end content of the table */}
                        </div>
                    </div>
                </div>
                {/* end table */}
              
                {/* <!-- /.content of the page --> */}

            </div>
        )
    }
}
