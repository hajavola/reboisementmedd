import React, { Component } from 'react';
import * as Config from './../Config/Config';
import { Link } from 'react-router-dom';
import {GoogleMap,Marker,InfoWindow} from 'react-google-maps';
import "./Map.css";



export default class Map extends Component {
    constructor(props){
        super(props);
        //data
        // Antananarivo:defaultCenter={{lat: -18.91368,lng:47.53613}}
        let lreboisements=[
            {id:5, campagne:'Campagne 2020', institution: 'MEDD', typeInstitution:'DREDD', dateDebut:'2020-12-31', responsable:'JIRAMA',superficie:'500',nbPlants:5000,coordonneeX:-12.272658,coordonneeY:49.373466, estAcheve:0},
            {id:4, campagne:'Campagne 2020',institution: 'MNP', typeInstitution:'DREDD', dateDebut:'2020-10-26', responsable:'GENDARME', superficie:'1000',nbPlants:2000,coordonneeX:-12.275104,coordonneeY:49.389503, estAcheve:0},
            {id:3, campagne:'Campagne 2020',institution: 'MEDD', typeInstitution:'CIREDD', dateDebut:'2020-02-15', responsable:'SANTE', superficie:'1500',nbPlants:3000,coordonneeX:-12.274772,coordonneeY:49.388652, estAcheve:1},
            {id:2, campagne:'Campagne 2020',institution: 'MNP', typeInstitution:'DREDD', dateDebut:'2020-09-30', responsable:'FINANCE', superficie:'150',nbPlants:4000,coordonneeX:-12.273345,coordonneeY:49.387504, estAcheve:1},
            {id:1, campagne:'Campagne 2020',institution: 'MEDD', typeInstitution:'CIREDD', dateDebut:'2020-11-15', responsable:'ENVIRONNEMENT', superficie:'20',nbPlants:5000,coordonneeX:-12.27656,coordonneeY:49.378879, estAcheve:1}
        ];
        this.state = {
            userFrom:'',
            userRegion:{},
           reboisements:lreboisements, 
           selectedReboisement:null,       
            beginningURL: Config.beginningURL
        };
        this.showInfoWindow=this.showInfoWindow.bind(this);
        this.showMarker=this.showMarker.bind(this);

    }
    componentDidMount(){
        //Init title for the page
        if(localStorage.getItem("userRegion")!=null){
            this.setState({
              userRegion:JSON.parse(localStorage.getItem("userRegion"))
            });
        }
        if(localStorage.getItem("title")!=null){
            this.setState({
              userFrom:localStorage.getItem("title")
            });
        }  
    }

    showMarker(reboisement){
        if(reboisement.estAcheve===1){
            return(
                <Marker 
                key={reboisement.id} 
                position={{lat: reboisement.coordonneeX,lng:reboisement.coordonneeY}}
                onClick={()=>{
                    this.setState({selectedReboisement:reboisement})
                }}
                icon={{
                    url: `/assets/images/logos/greenIconLocalisation.png`,
                    scaledSize: new window.google.maps.Size(40, 40)
                  }} 
                />

            );
        }
        else{
            return(
                <Marker 
                key={reboisement.id} 
                position={{lat: reboisement.coordonneeX,lng:reboisement.coordonneeY}}
                onClick={()=>{
                    this.setState({selectedReboisement:reboisement})
                }} 
                icon={{
                    url: `/assets/images/logos/greenIconLocalisationNonValid.png`,
                    scaledSize: new window.google.maps.Size(40, 40)
                  }} 
                />

            );
        }

    }
    showInfoWindow(){
        if(this.state.selectedReboisement!==null){
            if(this.state.selectedReboisement.estAcheve===0){
                return(
                <InfoWindow
                       
                        position={{lat: this.state.selectedReboisement.coordonneeX,lng:this.state.selectedReboisement.coordonneeY}}
                        onCloseClick={() => {
                            this.setState({selectedReboisement:null});
                        }}
                    >
                        <div className="non-valid">
                            <h4 className="title-infowindow "><b>Responsable: {this.state.selectedReboisement.responsable}</b></h4>
                            <p><b>Campagne:</b> {this.state.selectedReboisement.campagne}, <br/> <b>Surface:</b> {this.state.selectedReboisement.superficie} Ha,<br/> <b>Nombre plants:</b> {this.state.selectedReboisement.nbPlants}</p>
                            <p className="p-non-valid">En attente de validation ...</p>
                            <p className="link-style"><Link to={"/reboisementdetails/"+this.state.selectedReboisement.id}>D&eacute;tails du reboisement</Link></p>
                        </div>
                    </InfoWindow>);
            }
            if(this.state.selectedReboisement.estAcheve===1){
                return(
                    <InfoWindow
                        
                        position={{lat: this.state.selectedReboisement.coordonneeX,lng:this.state.selectedReboisement.coordonneeY}}
                        onCloseClick={() => {
                            this.setState({selectedReboisement:null});
                        }}
                    >
                        <div  className="valid">
                            <h4 className="title-infowindow "><b>Responsable: {this.state.selectedReboisement.responsable}</b></h4>
                            <p><b>Campagne:</b> {this.state.selectedReboisement.campagne},<br/> <b>Surface:</b> {this.state.selectedReboisement.superficie} Ha,<br/> <b>Nombre plants:</b> {this.state.selectedReboisement.nbPlants}</p>
                            <p className="link-style"><Link to={"/reboisementdetails/"+this.state.selectedReboisement.id}>D&eacute;tails du reboisement</Link></p>
                        </div>
                    </InfoWindow>

                );
            }
        }
    }
    render() {
        return (
           <GoogleMap 
                defaultZoom={10}
                defaultCenter={{lat: -12.2787,lng:49.29171}}
                >
                {this.state.reboisements.map((reboisement)=>(
                   this.showMarker(reboisement)
                ))}
                

                {this.showInfoWindow()}
            </GoogleMap>
        )
    }
}
