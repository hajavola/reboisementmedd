import React, { Component } from 'react';
import "./ReboisementMap.css";
import * as Config from './../Config/Config';
import Map from './Map';

import {withScriptjs, withGoogleMap} from 'react-google-maps';


const WrappedMap= withScriptjs(withGoogleMap(Map));

export default class ReboisementMap extends Component {
    constructor(props) {
        super(props);
      
        this.state = {
            userFrom:'',
            userRegion:{},
        
            beginningURL: Config.beginningURL
        };
      

    }
    componentDidMount(){
        //Init title for the page
        if(localStorage.getItem("userRegion")!=null){
            this.setState({
              userRegion:JSON.parse(localStorage.getItem("userRegion"))
            });
        }
        if(localStorage.getItem("title")!=null){
            this.setState({
              userFrom:localStorage.getItem("title")
            });
        }
         
        
    }
    render() {
        return (
            <div className="container-fluid">
                <div className="row bg-title">
                    <div className="left-title col-lg-4 col-md-4 col-sm-4 col-xs-12" >
                        <h4 className="text-left-title page-title">CARTE DE REBOISEMENT</h4>
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <h3 className="text-middle-title page-title">{this.state.userFrom}</h3>
                    </div>

                    <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                        {/* <a href="https://wrappixel.com/templates/ampleadmin/" target="_blank"
                            className="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Upgrade
                            to Pro

                        </a> */}
                        
                       
                        <ol className="breadcrumb">
                            <li className="active"><i className="linking fa fa-angle-right fa-fw"></i>Carte repr&eacute;sentant les reboisements achev&eacute;s</li>
                        </ol>
                    </div>
                    {/* <!-- /.col-lg-12 --> */}
                </div>
            
                {/* <!-- content of the page--> */}
               
                    
                <div  style={{ width: "100vw", height: "100vh" }}>
                        {/* Map */}`
                        
                        <WrappedMap googleMapURL={`https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=${
                            process.env.REACT_APP_GOOGLE_KEY
                            }`}
                            loadingElement={<div style={{ height: `100%` }} />}
                            containerElement={<div style={{ height: `100%` }} />}
                            mapElement={<div style={{ height: `100%` }} />}
                        />
                        {/* End Map */}
                    

               </div>
                
                {/* <!-- /.content of the page --> */}

            </div>
        )
    }
}
