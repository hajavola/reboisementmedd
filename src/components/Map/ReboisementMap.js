import React, { Component } from 'react';
import "./ReboisementMap.css";
import * as Config from './../Config/Config';
import axios from 'axios';
import { Link } from 'react-router-dom';
import L from 'leaflet';
import { Map, TileLayer, Marker, Popup } from 'react-leaflet';
import nonValidIconImage from './logos/greenIconLocalisationNonValid.png';
import validIconImage from './logos/greenIconLocalisation.png';

let mapStyle={  height: "100vh" };
var  validIcon = L.icon({
    iconUrl: validIconImage,
    iconSize:     [45, 45], // size of the icon
    iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
    popupAnchor:  [-3, -76]
  });
var  nonValidIcon = L.icon({
    iconUrl: nonValidIconImage,
    iconSize:     [45, 45], // size of the icon
    iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
    popupAnchor:  [-3, -76]
  });

export default class ReboisementMap extends Component {
    constructor(props) {
        super(props);
        /*let lreboisements=[
            {id:5, campagne:'Campagne 2020', institution: 'MEDD', typeInstitution:'DREDD', dateDebut:'2020-12-31', responsable:'JIRAMA',superficie:'500',nbPlants:5000,coordonneeX:-12.272658,coordonneeY:49.373466, estAcheve:0},
            {id:4, campagne:'Campagne 2020',institution: 'MNP', typeInstitution:'DREDD', dateDebut:'2020-10-26', responsable:'GENDARME', superficie:'1000',nbPlants:2000,coordonneeX:-12.275104,coordonneeY:49.389503, estAcheve:0},
            {id:3, campagne:'Campagne 2020',institution: 'MEDD', typeInstitution:'CIREDD', dateDebut:'2020-02-15', responsable:'SANTE', superficie:'1500',nbPlants:3000,coordonneeX:-12.274772,coordonneeY:49.388652, estAcheve:1},
            {id:2, campagne:'Campagne 2020',institution: 'MNP', typeInstitution:'DREDD', dateDebut:'2020-09-30', responsable:'FINANCE', superficie:'150',nbPlants:4000,coordonneeX:-12.273345,coordonneeY:49.387504, estAcheve:1},
            {id:1, campagne:'Campagne 2020',institution: 'MEDD', typeInstitution:'CIREDD', dateDebut:'2020-11-15', responsable:'ENVIRONNEMENT', superficie:'20',nbPlants:5000,coordonneeX:-12.27656,coordonneeY:49.378879, estAcheve:1}
        ];*/
      
        this.state = {
            userFrom:'',
            userRegion:{},
            reboisements:[], 
            position: {
                lat: -12.2787,
                lng:49.29171
              },
            zoom: 12,
        
            beginningURL: Config.beginningURL
        };
        this.showMarkerAndPopup=this.showMarkerAndPopup.bind(this);
       
    }

    componentDidMount(){
        //Init title for the page
        if(localStorage.getItem("userRegion")!=null){
            this.setState({
              userRegion:JSON.parse(localStorage.getItem("userRegion"))
            });
        }
        if(localStorage.getItem("title")!=null){
            this.setState({
              userFrom:localStorage.getItem("title")
            });
        }
        //set Latitude and longitude of Antananarivo 
        this.setState({
            position: {
                lat: -18.91368,
                lng:47.53613
              }
        });

        //get all the reboisement 
       
      
        axios.get(this.state.beginningURL+'reboisement/read.php')
        .then(response =>{
                this.setState({
                    reboisements:response.data.body

                });
        })
        .catch(error=>{
            console.log(error);
        })
       
        
      
         
         
        
    }

    showMarkerAndPopup(reboisement){
        if(parseInt(reboisement.estAcheve)===1){
            return(
                <Marker position={{lat: reboisement.coordonneeX,lng:reboisement.coordonneeY}} icon={validIcon} >
                        <Popup>
                        <div  className="valid">
                            <h4 className="title-infowindow "><strong>Responsable: {reboisement.nomClient}</strong></h4>
                            <p><strong>Campagne:</strong> {reboisement.nomCampagne},<br/> <strong>Surface:</strong> {reboisement.superficie} Ha,<br/> <strong>Nombre plants:</strong> {reboisement.nbPlants}</p>
                            <p className="link-style"><Link to={"/reboisementdetails/"+reboisement.id}>D&eacute;tails du reboisement</Link></p>
                        </div>                  
                        </Popup>
                </Marker>

            );
        }
        else{
            return(
                <Marker position={{lat: reboisement.coordonneeX,lng:reboisement.coordonneeY}} icon={nonValidIcon} >
                        <Popup>
                        <div className="non-valid">
                            <h4 className="title-infowindow "><strong>Responsable: {reboisement.nomClient}</strong></h4>
                            <p><strong>Campagne:</strong> {reboisement.nomCampagne}, <br/> <strong>Surface:</strong> {reboisement.superficie} Ha,<br/> <strong>Nombre plants:</strong> {reboisement.nbPlants}</p>
                            <p className="p-non-valid">En attente de validation ...</p>
                            <p className="link-style"><Link to={"/reboisementdetails/"+reboisement.id}>D&eacute;tails du reboisement</Link></p>
                        </div>                  
                        </Popup>
                </Marker>

            );
        }
    }

  
    render() {
        return (
            <div className="container-fluid">
                <div className="row bg-title">
                    <div className="left-title col-lg-4 col-md-4 col-sm-4 col-xs-12" >
                        <h4 className="text-left-title page-title">CARTE DE REBOISEMENT</h4>
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <h3 className="text-middle-title page-title">{this.state.userFrom}</h3>
                    </div>

                    <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                        {/* <a href="https://wrappixel.com/templates/ampleadmin/" target="_blank"
                            className="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Upgrade
                            to Pro

                        </a> */}
                        
                       
                        <ol className="breadcrumb">
                            <li className="active"><i className="linking fa fa-angle-right fa-fw"></i>Carte repr&eacute;sentant les reboisements achev&eacute;s</li>
                        </ol>
                    </div>
                    {/* <!-- /.col-lg-12 --> */}
                </div>
            
                {/* <!-- content of the page--> */}
               
                    
                {/* Map */}
                <Map style={mapStyle} center={this.state.position} zoom={this.state.zoom}>
                    <TileLayer
                    attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                    url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    />
                   
                    {this.state.reboisements.map((reboisement)=>(
                        this.showMarkerAndPopup(reboisement)
                    ))}
                    
                </Map>
                {/* end Map */}
                
                {/* <!-- /.content of the page --> */}

            </div>
        )
    }
}
