import React, { Component } from 'react';
import "./Dashboard.css";
import * as Config from './../Config/Config';
import axios from 'axios';

import Grid from '@material-ui/core/Grid';
import {Pie} from 'react-chartjs-2';

//Table
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Container from '@material-ui/core/Container';


const useStyles = makeStyles({
    root: {
      width: '100%',
     
    },
    container: {
      maxHeight: 440,
     
    },

  })

export default class Dashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userFrom:'',
            userRegion:{},
            loggedUser:{},
            nbTotalPlants:0,
            surfacePlanted:0,
            nbNurseries:0,
            labelsPie:[],
            dataPie:[],
            beginningURL: Config.beginningURL
        };
        this.setPieData=this.setPieData.bind(this);
    }
    componentDidMount(){
        //Init title for the page
        if(localStorage.getItem("userRegion")!=null){
            this.setState({
              userRegion:JSON.parse(localStorage.getItem("userRegion"))
            });
        }
        if(localStorage.getItem("title")!=null){
            this.setState({
              userFrom:localStorage.getItem("title")
            });
        }
        if(localStorage.getItem("user")!=null){
            this.setState({
              loggedUser:JSON.parse(localStorage.getItem("user"))
            });
        }

        //get number of plants and the surface
        axios.get(this.state.beginningURL+'reboisement/totalplantsandsurfaceplanted.php')
        .then(res=>{
            this.setState({
                nbTotalPlants:res.data[0].totalNombre,
                surfacePlanted:res.data[0].totalSurface
            });
        });

        //pépinière total en collaboration
       
        axios.get(this.state.beginningURL+'pepiniere/totalpepinierecollaboration.php')
        .then(res=>{
            this.setState({
                nbNurseries:res.data[0].totalPepiniere
               
            });
        });
        //pie
        axios.get(this.state.beginningURL+'reboisement/statnbplantsperregion.php')
        .then(response=>{
            
            this.setPieData(response.data.nomRegions,response.data.nbPlants);
          })
          .catch(error=>{
            console.log(error)
          });
        
    }
    setPieData(allLabels,arrayData){
        let allColors= [];
        let colorIsPresent=true;
        let randomColor="";
        
        let i=0;
        for (i = 0; i < allLabels.length; i++) {
           //take a random color
           //make sure all color inside the array are not the same 
            colorIsPresent=true;
            while (colorIsPresent) {
                randomColor="#" +Math.floor(Math.random()*16777215).toString(16);
                colorIsPresent=allColors.some(el => el === randomColor);               
            }
            allColors.push(randomColor);
            
            
        }
        this.setState({
            labelsPie:allLabels,
            dataPie:arrayData,
            pieColorArray:allColors
        });
        
      }
 
    render() {
        const classes = useStyles;
        //for the pie
        //Pie 
        const alldataPie = {
            labels:this.state.labelsPie,
            datasets: [{
                data: this.state.dataPie,
                backgroundColor: this.state.pieColorArray,
                hoverBackgroundColor: this.state.pieColorArray
            }]
        };
        return (
            <div className="container-fluid">
                <div className="row bg-title">
                    <div className="left-title col-lg-4 col-md-4 col-sm-4 col-xs-12" >
                        <h4 className="text-left-title page-title">Tableau de bord</h4>
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <h3 className="text-middle-title page-title">{this.state.userFrom}</h3>
                    </div>

                    <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                        {/* <a href="https://wrappixel.com/templates/ampleadmin/" target="_blank"
                            className="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Upgrade
                            to Pro

                        </a> */}
                        
                       
                        <ol className="breadcrumb">
                            <li className="active"><i className="linking fa fa-angle-right fa-fw"></i>Tableau de bord</li>
                        </ol>
                    </div>
                    {/* <!-- /.col-lg-12 --> */}
                </div>
            
                {/* <!-- content of the page--> */}
               
                    
                <div className="row">
                    
                    <div className="col-lg-4 col-sm-6 col-xs-12 card ">
                        <div className="white-box backgroung-card-1  analytics-info">    
                            <h3 className="box-title">Arbres plant&eacute;s</h3>
                            <ul className="list-inline two-part">
                                <li >
                                    <div className="col-sm-6 col-md-4 col-lg-3 icon-dashboard"><i className="fa  fa-bar-chart-o"></i></div>
                                </li>
                                <li className="text-right"><i className="ti-arrow-up text-success"></i> <span className="counter text-span">{this.state.nbTotalPlants}</span></li>
                            </ul>
                        </div>
                    </div>
                    <div className="col-lg-4 col-sm-6 col-xs-12 card ">
                        <div className="white-box backgroung-card-2  analytics-info">    
                            <h3 className="box-title">Surface totale rebois&eacute;e</h3>
                            <ul className="list-inline two-part">
                                <li>
                                    <div className="col-sm-6 col-md-4 col-lg-3 icon-dashboard"><i className="fa  fa-bar-chart-o"></i></div>
                                </li>
                                <li className="text-right"><i className="ti-arrow-up text-success"></i> <span className="counter text-span">{this.state.surfacePlanted} Ha</span></li>
                            </ul>
                        </div>
                    </div>
                    <div className="col-lg-4 col-sm-6 col-xs-12 card ">
                        <div className="white-box backgroung-card-3  analytics-info">    
                            <h3 className="box-title">P&eacute;pini&egrave;res en collaboration</h3>
                            <ul className="list-inline two-part"> 
                                <li>        
                                    <div className="col-sm-6 col-md-4 col-lg-3 icon-dashboard"><i className="fa  fa-bar-chart-o"></i></div> 
                                </li>  
                                <li className="text-right"><i className="ti-arrow-up text-success"></i> <span className="counter text-span">{this.state.nbNurseries}</span></li>
                            </ul>
                        </div>
                    </div>


               </div>
               <div className="row">
               <Container component="main"  style={{backgroundColor:'transparent',borderRadius:'3px'}}>
                       
                       <Paper className={classes.root}>
                           <Grid container spacing={3}>
                                  
                                  
                                  
                                  <Grid item xs={8} style={{textAlign:'center'}}>
                                      
                                       {/* Pie  */}
                                        <Paper style={{marginBottom:'20px'}}>
                                            <h3>Nombre plants mis en terre par région</h3>
                                            <Pie data={alldataPie} />
                                        </Paper>
                                        {/* End Pie  */}
                                  </Grid>
                                  <Grid item xs={4} style={{textAlign:'center'}}>
                                   
                                            <h3 className="description-title-dashboard">Description</h3>
                                            <h6>Le tableau de bord suivant représente un résumé de la situation globale  des reboisements effectués dans tout Madagascar.</h6>
                                            <h3  className="description-title-dashboard">Description du site</h3>
                                            <h6> Gestion de reboisement dans tout Madagascar.<br/>
                                            Ce site a été conçu pour permettre un suivi de reboisements dans toute la territoire malagasy, ce,
                                             à partir de la gestion de stocks des jeunes plants provenant des différentes pépinières en collaboration, jusqu'à un suivi géolocalisé des reboisements.
                                              En accompagnement avec le site, une application mobile est aussi disponible, une application qui garantit les suivis des reboisements sur terrain.   </h6>
                                    
                                    
                                    
                                      

                                  </Grid>
                                
                              </Grid>
                            {/* <div  className="">
                            <Paper >
                                <h3>Surfaces reboisées</h3>
                                
                            </Paper>
                            </div> */}

                        </Paper> 
                        </Container> 
               </div>
                
                {/* <!-- /.content of the page --> */}

            </div>
        )
    }
}
