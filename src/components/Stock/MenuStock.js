import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
// import ListItemText from '@material-ui/core/ListItemText';
import "./MenuStock.css";

import { Link } from 'react-router-dom';

//Menu
const StyledMenu = withStyles({
    paper: {
      border: '1px solid #d3d4d5',
     
    },
  })((props) => (
    <Menu
      elevation={0}
      getContentAnchorEl={null}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'center',
      
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'center',
     
      }}
     
      {...props }
    />
  ));
  
  const StyledMenuItem = withStyles((theme) => ({
    root: {
      '&:focus': {
     
        backgroundColor: theme.palette.primary.main,
        '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
          color: theme.palette.common.white,
          fontSize:'15px',
          
        },
      },
    },
  }))(MenuItem);


let fontStyle1={fontSize:'14px', color:'black'};
let buttonBackStyle= {backgroundColor:'#0bca4b', color:'white', marginTop:'5px', fontSize:'13px'};

export default class MenuStock extends Component {

    
    //Menu
    state={
        anchorEl:null
    }


    handleClick = (event) => {
        this.setState({anchorEl:event.currentTarget});
    };
  
    handleClose = () => {
        this.setState({anchorEl:null});
    };

    //end Menu
    render() {
        
        //menu
        const{anchorEl}=this.state;
        return (
            <div>
                 <Button
                                            aria-controls="customized-menu"
                                            aria-haspopup="true"
                                            variant="contained" 
                                            style={buttonBackStyle}
                                            onClick={this.handleClick}
                                        >
                                            <i  className="fa  fa-bars fa-fw"></i>
                                           
                                            
                                            Menu
                                        </Button>
                                        <StyledMenu
                                            id="customized-menu"
                                            anchorEl={anchorEl}
                                            keepMounted
                                            open={Boolean(anchorEl)}
                                            onClose={this.handleClose}
                                            style={fontStyle1}
                                        >
                                            <Link to='/entreestock'>
                                                 <StyledMenuItem  style={fontStyle1}>
                                                <ListItemIcon> 
                                                    <i  className="fa  fa-arrow-circle-o-right fa-fw icon-style"></i> 
                                                </ListItemIcon>
                                                <div>Entr&eacute;e stock</div>
                                                {/* <ListItemText primary="Entrée de stock" /> */}
                                            </StyledMenuItem>
                                            </Link>
                                            <Link to='/etatdestock'>
                                            <StyledMenuItem  style={fontStyle1}>
                                                <ListItemIcon>
                                                <i  className="fa  fa-arrow-circle-o-right fa-fw icon-style"></i>
                                                </ListItemIcon>
                                                <div>Etat de stock</div>
                                                {/* <ListItemText  primary="Etat de stock" /> */}
                                            </StyledMenuItem>
                                            </Link>
                                            <Link to='/entreehistoric'>
                                            <StyledMenuItem  style={fontStyle1}>
                                                <ListItemIcon>
                                                <i  className="fa  fa-arrow-circle-o-right fa-fw icon-style"></i>
                                                </ListItemIcon>
                                                <div>Historique Entrée</div>
                                                {/* <ListItemText primary="Historique Entrée" /> */}
                                            </StyledMenuItem>
                                            </Link>
                                            <Link to='/sortiehistoric'>
                                            <StyledMenuItem style={fontStyle1} >
                                                <ListItemIcon>
                                                <i  className="fa  fa-arrow-circle-o-right fa-fw icon-style"></i>
                                                </ListItemIcon>
                                                <div>Historique Sortie</div>
                                                {/* <ListItemText primary="Historique Sortie" /> */}
                                            </StyledMenuItem>
                                            </Link>
                                        </StyledMenu>
                                       
                                        
            </div>
        )
    }
}
