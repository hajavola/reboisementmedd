import React, { Component } from 'react';
import "./EntreeStock.css";
import * as Config from './../Config/Config';
import * as Design from './../Config/Design';
import axios from 'axios';

//Search
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
//grid for the search
import Grid from '@material-ui/core/Grid';

//Table
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';


//select
import Select from 'react-select';

//autocomplete
import TextField from '@material-ui/core/TextField';




import MenuStock from './MenuStock';



//Table
const useStyles = makeStyles({
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 600,
    },

  });



// form 
const useStylesForm = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));

// end form


//design
let appBarStyle= {borderRadius:'4px',textAlign:'right',backgroundColor:'#000e2f'};
//let buttonBackStyle= {backgroundColor:'#0bca4b', color:'white', marginTop:'5px', fontSize:'13px'};
let fontStyle={fontSize:'14px'};


export default class EntreeStock extends Component {
    constructor(props) {
        super(props);
 
        //Data
        // let lpepinieres=[
        //     {id:1, nomPepiniere:'Pepiniere 1'},
        //     {id:2, nomPepiniere:'Pepiniere 2'},
        //     {id:3, nomPepiniere:'Pepiniere 3'}
        // ];
        // let lespecesplants=[
        //     {id:1, nomEspece: 'SAPIN'},
        //     {id:2, nomEspece: 'FRUIT'},
        //     {id:3, nomEspece: 'GRAND ARBRE'}
        // ];
        // let lcampagnes=[
        //     {id:3, nomCampagne:'Campagne 2020'},
        //     {id:2, nomCampagne:'Campagne 2019'},
        //     {id:1, nomCampagne:'Campagne 2018'}
        // ];

        // --------------
      
    
        this.state = {
            userFrom:'',
            userRegion:{},
            loggedUser:{},
            errorMessage:'',
            pepiniere:0,
            especeplant:0,
            nombre:0,
            campagne:0,
           
            pepinieres: [],
            especeplants:[],
            campagnes:[],
            optionsPepinieres: [],
            optionsEspecePlants:[],
            optionsCampagnes:[],
                
            beginningURL: Config.beginningURL
        };
        this.onChangeNombre=this.onChangeNombre.bind(this);
        this.onSubmit=this.onSubmit.bind(this);
        this.showErrorMessage=this.showErrorMessage.bind(this);
        this.annuler=this.annuler.bind(this);    
      
    }
    componentDidMount(){
        //Init title for the page
        if(localStorage.getItem("userRegion")!=null){
            this.setState({
            userRegion:JSON.parse(localStorage.getItem("userRegion"))
            });
        }
        if(localStorage.getItem("title")!=null){
            this.setState({
            userFrom:localStorage.getItem("title")
            });
        }
        if(localStorage.getItem("user")!=null){
            this.setState({
              loggedUser:JSON.parse(localStorage.getItem("user"))
            });
        }

        
       

     
       
        //select pepinieres
        const data ={
            wordToSearch:"",
            idRegion:JSON.parse(localStorage.getItem("userRegion")).id,
            idCampagne:0
  
        };
        
        //console.log(data);
        // axios.post(this.state.beginningURL+'pepiniere/readperregionandcampagneid.php', data)
        axios.get(this.state.beginningURL+'pepiniere/readperregionandcampagneidget.php?wordToSearch='+data.wordToSearch+'&idRegion='+data.idRegion+'&idCampagne='+data.idCampagne)
        .then(response =>{
                this.setState({
                    pepinieres:response.data.body

                });
              
                let lPep=[{value:0,label:""}];
                let currentPepinieres=response.data.body;
                for(let i=0;i<currentPepinieres.length;i++){
                    lPep.push({value:currentPepinieres[i].id, label:currentPepinieres[i].nomPepiniere});
                }
                this.setState({optionsPepinieres:lPep});
        })
        .catch(error=>{
            console.log(error);
        })
        

        //select EspecePlants
        axios.get(this.state.beginningURL+'especeplants/read.php')
        .then(response=>{
            this.setState({
                especeplants:response.data.body
               
            })
          
            let lEsp=[{value:0,label:""}];
            let currentEspecePlant=response.data.body;
            for(let i=0;i<currentEspecePlant.length;i++){
                lEsp.push({value:currentEspecePlant[i].id, label:currentEspecePlant[i].nomEspece});
            }
            this.setState({optionsEspecePlants:lEsp});

        })
        .catch(function(error){
            console.log(error);
        });
       

    
        //select Campagnes
         // optionsCampagnes:[],
        //select campagnes
        axios.get(this.state.beginningURL+'campagne/read.php')
        .then(response=>{
            this.setState({
                pepinieristes:response.data.body
               
            })
         
            let lCam=[{value:0,label:""}];
            let currentCampagnes=response.data.body;
            for(let i=0;i<currentCampagnes.length;i++){
                lCam.push({value:currentCampagnes[i].id, label:currentCampagnes[i].nomCampagne});
            }
            this.setState({optionsCampagnes:lCam});

        })
        .catch(function(error){
            console.log(error);
        });
        
      
       
      }

      


      onChangeNombre(e){
        this.setState({
            nombre:e.target.value
        });
      }

    //select (combobox) 
    state = {
        selectedOptionPepiniere: null,
        selectedOptionEspecePlants: null,
        selectedOptionCampagne: null
      };
    handleChangePepiniere = selectedOptionPepiniere => {
        this.setState({ selectedOptionPepiniere , pepiniere:selectedOptionPepiniere.value});
      };
    handleChangeEspecePlant = selectedOptionEspecePlants=> {
        this.setState({ selectedOptionEspecePlants , especeplant:selectedOptionEspecePlants.value});
      
      };
    handleChangeCampagne = selectedOptionCampagne => {
        this.setState({ selectedOptionCampagne , campagne:selectedOptionCampagne.value});
      };
  
    
      onSubmit(e){
        e.preventDefault();
       
     
        if(this.state.campagne===0 || this.state.pepiniere===0|| this.state.especeplant===0|| parseInt(this.state.nombre)<=0){
           
            this.setState({errorMessage:'Veullez bien remplir tous les champs.'});
        }
        else{                
                    const stock ={
                        idPepiniere:this.state.pepiniere,
                        idEspecePlants:this.state.especeplant,
                        idCampagne:this.state.campagne,
                        nombre : this.state.nombre,
                        userModule:this.state.loggedUser.module,
                        userType:this.state.loggedUser.type,
                        userProfil:this.state.loggedUser.profil
                    };
                    console.log(stock);
            
                    axios.post(this.state.beginningURL+'stock/create.php',stock)
                     .then(res => {
                        console.log(res.data);
                        
                        if(res.data==="ok"){
                            alert('Enregistrement en stock achevé avec succès');
                            window.location="/entreehistoric";
                        }
                        else{
                            alert('Erreur lors de l\'entrée de stock.');
                        }
                    });
                }    
    }

    annuler(){
        this.setState({
            pepiniere:0,
            especeplant:0,
            nombre:0,
            campagne:0,
            selectedOptionPepiniere: null,
            selectedOptionEspecePlants: null,
            selectedOptionCampagne: null
        });

        //on annule tout
        this.componentDidMount();
    }


    
    showErrorMessage(){
        if(this.state.errorMessage!==''){
            return(
                <Typography component="h5"  style={Design.errorMessageStyle}>
                            {this.state.errorMessage} 
                </Typography>
            );
        }
        else{
            return
        }
        
    }


    render() {
        //select (combobox)
        const { selectedOptionPepiniere, selectedOptionEspecePlants, selectedOptionCampagne} = this.state; 
        //Table
        const classes = useStyles;

        //form 
        const classesForm = useStylesForm;


        

        return (
            <div className="container-fluid">
                <div className="row bg-title">
                    <div className="left-title col-lg-4 col-md-4 col-sm-4 col-xs-12" >
                        <h4 className="text-left-title page-title">P&eacute;pini&egrave;res</h4>
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <h3 className="text-middle-title page-title">{this.state.userFrom}</h3>
                    </div>

                    <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                        {/* <a href="https://wrappixel.com/templates/ampleadmin/" target="_blank"
                            className="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Upgrade
                            to Pro

                        </a> */}
                        <ol className="breadcrumb">
                            
                            <li className="active"><a>Stock</a>&nbsp;&nbsp;<i className="linking fa fa-angle-right fa-fw"></i>Entr&eacute;e</li>
                        </ol>
                    </div>
                    {/* <!-- /.col-lg-12 --> */}
                </div>
            
                {/* <!-- content of the page--> */}
                {/* Head */}
                <div className="divAppBar" >
                    <AppBar position="static" style={appBarStyle}>
                        <Toolbar>
                            {/* grid */}
                            
                                <Grid container spacing={3}>
                                    <Grid item xs={3} style={{textAlign:'left'}}>
                                        {/* --------------Menu------------------------------- */}
                                       <MenuStock/>
                                       {/* ---------------End Menu ------------------------- */}

                                    </Grid>
                                   
                                    <Grid item xs={6} style={{textAlign:'center'}}>
                                        <h4 className="smaller-title" >Entr&eacute;e stock</h4>
                                    </Grid>
                                    <Grid item xs={3}>                                    
                                    </Grid>
                                   
                                </Grid>
                            
                            {/* end grid */}
                        
                        </Toolbar>
                    </AppBar>
                </div>
                {/* end head */}
                {/* Table */}
                <div className="row">
                    <div className="col-sm-12">
                        <div className="white-box formStyleStock" >
                        <Container component="main" maxWidth="xs" style={{backgroundColor:'white',borderRadius:'3px'}}>
                            <Paper className={classes.root}>
                            {this.showErrorMessage()}
                            <form className={classesForm.form} onSubmit={this.onSubmit}  noValidate>
                            <label className="label-style">P&eacute;pini&egrave;re</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                    <Select
                                        placeholder="Pépinière"
                                        value={selectedOptionPepiniere}
                                        onChange={this.handleChangePepiniere}
                                        options={this.state.optionsPepinieres}
                                    />
                                </Grid>
                                <Grid item xs={1}></Grid>
                            </Grid>
                            
                            
                            <label className="label-style">Espece de plants</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                    <Select
                                        placeholder="Espèce"
                                        value={selectedOptionEspecePlants}
                                        onChange={this.handleChangeEspecePlant}
                                        options={this.state.optionsEspecePlants}
                                    />
                                </Grid>
                                <Grid item xs={1}>
                                   
                                </Grid>
                            </Grid>

                            <label className="label-style">Nombre</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    id="nombre"
                                    placeholder="Nombre"
                                    name="nombre"
                                    autoComplete="nombre"
                                    type='number'
                                   
                                    inputProps={{style: fontStyle}} 
                                    InputLabelProps={{style: fontStyle}} 
                                    value={this.state.nombre} onChange={this.onChangeNombre}
                                />
                                </Grid>
                                <Grid item xs={1}></Grid>
                            </Grid>
                            

                           
                           
                            <label className="label-style">Campagne</label>
                            <Grid container spacing={0}  >
                                <Grid item xs={11} >
                                    <Select
                                        placeholder="Campagne"
                                        value={selectedOptionCampagne}
                                        onChange={this.handleChangeCampagne}
                                        options={this.state.optionsCampagnes}
                                    />
                                </Grid>
                                <Grid item xs={1}>
                                    
                                </Grid>
                            </Grid>

                           
                           
                            <Grid container spacing={1} style={{marginTop:'40px'}} >
                            <Grid item xs={6} >
                              <Button
                                  type="button"
                                  fullWidth
                                  variant="contained" 
                                  style={fontStyle} 
                                  onClick={()=> {this.annuler()}}                                  
                              >
                                  Annuler
                              </Button>
                            </Grid>
                            <Grid item xs={6}>
                              <Button
                                  type="submit"
                                  fullWidth
                                  variant="contained"
                                  color="primary"
                                  className={classesForm.submit}
                                  style={fontStyle}
                                  
                              >
                                  Enregistrer
                              </Button>
                            </Grid>
                        </Grid>
                            </form>
                            <br/>
                                
                            </Paper>  
                            {/* end content of the table */}
                            </Container>
                        </div>
                    </div>
                </div>
                {/* end table */}
              
                {/* <!-- /.content of the page --> */}

            </div>
        )
    }
}
