import React, { Component } from 'react';
import "./EtatDeStock.css";
import * as Config from './../Config/Config';
import axios from 'axios';
import MenuStock from './MenuStock';
//select
import Select from 'react-select';
//Search
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
//grid for the search
import Grid from '@material-ui/core/Grid';

//Table
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
// import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';


const  EtatDeStockItem= props =>(
     <TableRow hover role="checkbox" tabIndex={-1} key={props.numero}>
        <TableCell style={tableCellStyle}>{props.numero}</TableCell>
        <TableCell style={tableCellStyle}>{props.etatDeStock.dateEtat}</TableCell>
        <TableCell style={tableCellStyle}>{props.etatDeStock.nomCampagne}</TableCell>
        <TableCell style={tableCellStyle}>{props.etatDeStock.nomEspece}</TableCell>
        <TableCell style={tableCellActionColumnStyle}>{props.etatDeStock.nombre}</TableCell>
    </TableRow>
)

//Table
const useStyles = makeStyles({
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 440,
    },

  });
//design
let appBarStyle= {borderRadius:'4px',textAlign:'right',backgroundColor:'#000e2f'};
//grid
let gridSelectStyle={marginBottom:'15px'};
//table
let tableCellTitleStyle={fontSize:'14px',fontFamily:'Segoe UI'};
let tableRowTitleStyle={backgroundColor:'rgb(245, 245, 245)'};
let tableCellTitleActionColumnStyle={fontSize:'14px',fontFamily:'Segoe UI', textAlign:'center'};
let tableCellStyle={fontSize:'13px', color:'rgb(90, 88, 88)', fontFamily:'Segoe UI' };
let tableCellActionColumnStyle={fontSize:'13px', color:'rgb(90, 88, 88)', fontFamily:'Segoe UI', textAlign:'center' };
let buttonDetailsStyle={backgroundColor:'#ca4f0b', color:'white', marginTop:'5px'};

export default class EtatDeStock extends Component {
    constructor(props) {
        super(props);
        //data
       
        /*let lPepiniere=[
            {id:0,nomPepiniere: ''},
            {id:1,nomPepiniere: 'Pépinière 1'},
            {id:2,nomPepiniere: 'Pépiniére 2'},
            {id:3,nomPepiniere: 'Pépinière 3'},
          
        ];*/
    
        this.state = {
            userFrom:'',
            userRegion:{},
            loggedUser:{},
            showTable:false,
            etatDeStocks:[],
            pepinieres:[],
            pepiniere:0,
            nomPepiniere:'',
            optionsPepinieres:[],
            total:0,
            wordToSearch:'',
            nbSearchResult:0,
            isSubmit:false,
            page:0,
            rowsPerPage:10,
            beginningURL: Config.beginningURL
        };
        this.etatDeStockList=this.etatDeStockList.bind(this);
        this.generateEtatDeStock=this.generateEtatDeStock.bind(this);
        this.showTable=this.showTable.bind(this);
        this.onChangeSearch= this.onChangeSearch.bind(this);
        this.onSubmitSearch=this.onSubmitSearch.bind(this);
        this.displaySearchResult=this.displaySearchResult.bind(this);
      

    }
    componentDidMount(){
        //Init title for the page
        if(localStorage.getItem("userRegion")!=null){
            this.setState({
              userRegion:JSON.parse(localStorage.getItem("userRegion"))
            });
        }
        if(localStorage.getItem("title")!=null){
            this.setState({
              userFrom:localStorage.getItem("title")
            });
        }
        if(localStorage.getItem("user")!=null){
            this.setState({
              loggedUser:JSON.parse(localStorage.getItem("user"))
            });
        }

        let i=0;
       
        //select pepinieres
         //select pepinieres
         const dataPepiniere ={
            wordToSearch:"",
            idRegion:JSON.parse(localStorage.getItem("userRegion")).id,
            idCampagne:0
  
        };
        
        //console.log(dataPepiniere);
        
        //axios.post(this.state.beginningURL+'pepiniere/readperregionandcampagneid.php', dataPepiniere)
        axios.get(this.state.beginningURL+'pepiniere/readperregionandcampagneidget.php?wordToSearch='+dataPepiniere.wordToSearch+'&idRegion='+dataPepiniere.idRegion+'&idCampagne='+dataPepiniere.idCampagne)
        .then(response =>{
                this.setState({
                    pepinieres:response.data.body

                });
              
                let lPep=[{value:0,label:""}];
                let currentPepinieres=response.data.body;
                for(i=0;i<currentPepinieres.length;i++){
                    lPep.push({value:currentPepinieres[i].id, label:currentPepinieres[i].nomPepiniere});
                }
                this.setState({optionsPepinieres:lPep}); 
        })
        .catch(error=>{
            console.log(error);
        })
       
    }
    state = {
        selectedOptionPepiniere: null
     
      };
    handleChangePepiniere = selectedOptionPepiniere => {
        this.setState({ selectedOptionPepiniere , 
            pepiniere:selectedOptionPepiniere.value});
      };

   


    // Search
    onChangeSearch(e){
        this.setState({
            wordToSearch:e.target.value
        });
        //if we erase the word written
        if(e.target.value===""){
            this.setState({
                isSubmit:false,   
            });
            //change to another function depending on the  criteria in select options
            this.componentDidMount();
        }
    }
    onSubmitSearch(e){
        e.preventDefault();
        /*const word ={
            wordToSearch:this.state.wordToSearch,
  
        };*/
        if(this.state.wordToSearch===""){
            this.setState({
                isSubmit:false,   
            });
            this.componentDidMount();
        }
        else{
            this.setState({
                isSubmit:true,   
            });
        }    
    }
    displaySearchResult(){
        if(this.state.isSubmit===true){
        return <div className="searchResultDiv"><b className="searchResultStyle">{this.state.nbSearchResult} R&eacute;sultat(s) trouv&eacute;(s)</b></div> 
        } 
        else{
            return 
        }  
    }


    etatDeStockList(){

        let i=0;
      
        return this.state.etatDeStocks.slice(this.state.page * this.state.rowsPerPage, this.state.page * this.state.rowsPerPage + this.state.rowsPerPage).map((currentEtatDeStock) => {
            i++;
            return <EtatDeStockItem numero={i}  etatDeStock={currentEtatDeStock}  key={i}/>;
        });
     
    }
    generateEtatDeStock(){
        if(this.state.selectedOptionPepiniere!=null){
            if(this.state.selectedOptionPepiniere.value===0){
                this.setState({ nomPepiniere:this.state.selectedOptionPepiniere.label, showTable:false});
            }
            else{
                this.setState({ nomPepiniere:this.state.selectedOptionPepiniere.label});
                const data ={
                    idPepiniere:this.state.selectedOptionPepiniere.value,
                    idRegion : this.state.userRegion.id,
                    wordToSearch:this.state.wordToSearch,
                    userModule:this.state.loggedUser.module,
                    userType:this.state.loggedUser.type,
                    userProfil:this.state.loggedUser.profil
                };
        
              
                console.log(data);
                axios.post(this.state.beginningURL+'etatdestock/generate.php', data)
                .then(response =>{
                    console.log(response.data);
                     
                        this.setState({ 
                            showTable:true, 
                            etatDeStocks:response.data.body,
                            total:response.data.total, 
                            page:0,
                            rowsPerPage:10
                        });
                })
                .catch(error=>{
                    console.log(error);
                })
                /*let lEtatDeStock=[
                    {dateInventaire:"2020-08-25",campagne:'Campagne 2020', especePlant:'Sapin', nombre:12},
                    {dateInventaire:"2020-08-25",campagne:'Campagne 2020', especePlant:'Fruit', nombre:26},
                    {dateInventaire:"2020-08-25",campagne:'Campagne 2020', especePlant:'Palmier', nombre:40},
                
                ];
                this.setState({ showTable:true, etatDeStocks:lEtatDeStock,total:78, page:0,rowsPerPage:10});
                   */
            }
        }
    
    }

    showTable(classes,handleChangePage,handleChangeRowsPerPage){
        if(this.state.showTable){
            return(
                <Paper className={classes.root}>
                                
                <TableContainer className={classes.container}>
                    <Table  stickyHeader aria-label="sticky table">
                    <TableBody>
                            <TableRow style={tableRowTitleStyle}>
                                <TableCell style={tableCellTitleStyle}>Num&eacute;ro</TableCell>
                                <TableCell style={tableCellTitleStyle}>Date</TableCell>
                                <TableCell style={tableCellTitleStyle}>Campagne</TableCell>
                                <TableCell style={tableCellTitleStyle}>Esp&egrave;ce plants</TableCell>
                                <TableCell style={tableCellTitleActionColumnStyle}>Nombres</TableCell>
                                </TableRow>
                                                    
                            </TableBody>
                    <TableBody>
                        {this.etatDeStockList()}       
                    </TableBody>
                    <TableBody>
                        <TableRow style={tableRowTitleStyle}>
                            <TableCell style={tableCellTitleStyle}></TableCell>
                            <TableCell style={tableCellTitleStyle}></TableCell>
                            <TableCell style={tableCellTitleStyle}></TableCell>
                            <TableCell style={tableCellTitleStyle}>Total</TableCell>
                            <TableCell style={tableCellTitleActionColumnStyle}>{this.state.total}</TableCell>
                        </TableRow> 
                     </TableBody>
                    </Table>
                </TableContainer>
                <TablePagination
                    style={tableCellStyle}
                    rowsPerPageOptions={[10, 25, 100]}
                    component="div"
                    count={this.state.etatDeStocks.length}
                    rowsPerPage={this.state.rowsPerPage}
                    page={this.state.page}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                />
            </Paper>  
    
            );

        }else{
            return
        }
    }

    render() {
        const { selectedOptionPepiniere } = this.state; 
   
        //Table
        const classes = useStyles;
        
        const handleChangePage = (event, newPage) => {
            this.setState({page:newPage})
            //setPage(newPage);
        };
        const handleChangeRowsPerPage = (event) => {
            //setRowsPerPage(+event.target.value);
            //setPage(0);
            this.setState({rowsPerPage:+event.target.value});
            this.setState({page:0});
        };
        //end table

        

        return (
            <div className="container-fluid">
                <div className="row bg-title">
                    <div className="left-title col-lg-4 col-md-4 col-sm-4 col-xs-12" >
                        <h4 className="text-left-title page-title">P&eacute;pini&egrave;res</h4>
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <h3 className="text-middle-title page-title">{this.state.userFrom}</h3>
                    </div>

                    <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                        {/* <a href="https://wrappixel.com/templates/ampleadmin/" target="_blank"
                            className="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Upgrade
                            to Pro

                        </a> */}
                        <ol className="breadcrumb">
                            
                            <li className="active"><a>Stock</a>&nbsp;&nbsp;<i className="linking fa fa-angle-right fa-fw"></i>Etat de stock</li>
                        </ol>
                    </div>
                    {/* <!-- /.col-lg-12 --> */}
                </div>
            
                {/* <!-- content of the page--> */}
                {/* Head */}
                <div className="divAppBar" >
                    <AppBar position="static" style={appBarStyle}>
                        <Toolbar>
                            {/* grid */}
                            
                                <Grid container spacing={3}>
                                    <Grid item xs={3} style={{textAlign:'left'}}>
                                      {/* --------------Menu------------------------------- */}
                                      <MenuStock/>
                                       {/* ---------------End Menu ------------------------- */}
                                    </Grid>
                                    <Grid item xs={6} style={{textAlign:'center'}}>
                                        <h4 className="smaller-title" >Etat de stock {this.state.nomPepiniere}</h4>
                                    </Grid>
                                    <Grid item xs={3}>
                                   
                                        {/* <div className="div-search" >
                                        <form role="search" id="search" className="app-search hidden-sm hidden-xs m-r-10" onSubmit={this.onSubmitSearch} noValidate>
                                            <input type="text" placeholder="Rechercher..." className="form-control"
                                                value={this.state.wordToSearch}
                                                onChange={this.onChangeSearch}/> 
                                             <a  onClick={this.onSubmitSearch}>
                                                <i className="fa fa-search"></i>
                                            </a> 
                                        </form>

                                      
                                        </div> */}
                                    
                                    </Grid>
                                </Grid>
                            
                            {/* end grid */}
                        
                        </Toolbar>
                    </AppBar>
                </div>
                {/* end head */}
                {/* Table */}
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                              
                        <Grid container spacing={0} style={gridSelectStyle}>
                                <Grid item xs={8} >
                                </Grid>
                                <Grid item xs={3} >
                                <Select
                                        placeholder="Pépinières"
                                        value={selectedOptionPepiniere}
                                        onChange={this.handleChangePepiniere}
                                        options={this.state.optionsPepinieres}
                                       
                                        
                                    />
                                
                                </Grid>
                                <Grid item xs={1}>
                                     <Button onClick={()=> {this.generateEtatDeStock()}}  variant="contained" style={buttonDetailsStyle}>
                                            Générer
                                    </Button>
                                </Grid>
                            </Grid>

                            

                            {/* <h3 class="box-title">Basic Table</h3> */}
                            {/* <p class="text-muted">Add class <code>.table</code></p> */}
                             {/* Content of the table */}
                             {this.displaySearchResult()}
                            {this.showTable(classes,handleChangePage,handleChangeRowsPerPage)}

                            {/* end content of the table */}
                        </div>
                    </div>
                </div>
                {/* end table */}
              
                {/* <!-- /.content of the page --> */}

            </div>
        )
    }
}
