import React, { Component } from 'react';
import "./SortieHistoric.css";
import * as Config from './../Config/Config';
import { Link } from 'react-router-dom';
import axios from 'axios';
import MenuStock from './MenuStock';
//select
import Select from 'react-select';
//Search
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
//grid for the search
import Grid from '@material-ui/core/Grid';

//Table
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
// import TableHead from '@material-ui/core/TableHead';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';


const  HistoriqueSortieItem= props =>(
     <TableRow hover role="checkbox" tabIndex={-1} key={props.numero}>
        <TableCell style={tableCellStyle}>{props.historiqueSortie.id}</TableCell>
        <TableCell style={tableCellStyle}>{props.historiqueSortie.dateSortie}</TableCell>
        <TableCell style={tableCellStyle}>{props.historiqueSortie.nomCampagne}</TableCell>
        <TableCell style={tableCellStyle}>{props.historiqueSortie.idReboisement}</TableCell>
        <TableCell style={tableCellStyle}>{props.historiqueSortie.nomPepiniere}</TableCell>
        <TableCell style={tableCellStyle}>{props.historiqueSortie.nomEspece}</TableCell>
        <TableCell style={tableCellActionColumnStyle}>{props.historiqueSortie.nombre}</TableCell>
        <TableCell style={tableCellActionColumnStyle}><Link to={"/reboisementdetails/"+props.historiqueSortie.idReboisement}><img className="imageModifier" alt="details" src="assets/images/logos/logoAdd.png"/></Link></TableCell>
    </TableRow>
)

//Table
const useStyles = makeStyles({
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 440,
    },

  });
//design
let appBarStyle= {borderRadius:'4px',textAlign:'right',backgroundColor:'#000e2f'};
//grid
let gridSelectStyle={marginBottom:'15px'};
//table
let tableCellTitleStyle={fontSize:'14px',fontFamily:'Segoe UI'};
let tableRowTitleStyle={backgroundColor:'rgb(245, 245, 245)'};
let tableCellTitleActionColumnStyle={fontSize:'14px',fontFamily:'Segoe UI', textAlign:'center'};
let tableCellStyle={fontSize:'13px', color:'rgb(90, 88, 88)', fontFamily:'Segoe UI' };
let tableCellActionColumnStyle={fontSize:'13px', color:'rgb(90, 88, 88)', fontFamily:'Segoe UI', textAlign:'center' };
//let buttonDetailsStyle={backgroundColor:'#ca4f0b', color:'white', marginTop:'5px'};

export default class SortieHistoric extends Component {
    constructor(props) {
        super(props);
        //data
       
        /*let lPepiniere=[
            {id:0,nomPepiniere: ''},
            {id:1,nomPepiniere: 'Pépinière 1'},
            {id:2,nomPepiniere: 'Pépinière 2'},
            {id:3,nomPepiniere: 'Pépinière 3'},
          
        ];
        let lHistoriqueSortie=[
            {id:6,dateEntree:"2020-08-25",nomCampagne:'Campagne 2020',nomPepiniere:'Pepiniere 1', nomEspece:'Sapin', nombre:2578},
            {id:5,dateEntree:"2020-08-25",nomCampagne:'Campagne 2020',nomPepiniere:'Pepiniere 1', nomEspece:'Fruit', nombre:4484},
            {id:4,dateEntree:"2020-08-25",nomCampagne:'Campagne 2020',nomPepiniere:'Pepiniere 1', nomEspece:'Grand arbre', nombre:5428},
            {id:3,dateEntree:"2020-08-25",nomCampagne:'Campagne 2020',nomPepiniere:'Pepiniere 2', nomEspece:'Sapin', nombre:2036},
            {id:2,dateEntree:"2020-08-25",nomCampagne:'Campagne 2020',nomPepiniere:'Pepiniere 2', nomEspece:'Fruit', nombre:7525},
            {id:1,dateEntree:"2020-08-25",nomCampagne:'Campagne 2020',nomPepiniere:'Pepiniere 2', nomEspece:'Grand arbre', nombre:4589},
        ];*/
    
        this.state = {
            userFrom:'',
            userRegion:{},
          
            historiqueSorties:[],
            pepinieres:[],
            pepiniere:0,
            total:0,
            nomPepiniere:'',
            optionsPepinieres:[],
      
            wordToSearch:'',
            nbSearchResult:0,
            isSubmit:false,
            page:0,
            rowsPerPage:10,
            beginningURL: Config.beginningURL
        };
        this.historiqueSortieList=this.historiqueSortieList.bind(this);
       
     
        this.onChangeSearch= this.onChangeSearch.bind(this);
        this.onSubmitSearch=this.onSubmitSearch.bind(this);
        this.displaySearchResult=this.displaySearchResult.bind(this);
      

    }
    componentDidMount(){
        //Init title for the page
        let currentIdRegion=0;
        if(localStorage.getItem("userRegion")!=null){
            currentIdRegion=JSON.parse(localStorage.getItem("userRegion")).id;
            this.setState({
              userRegion:JSON.parse(localStorage.getItem("userRegion"))
            });
        }
        if(localStorage.getItem("title")!=null){
            this.setState({
              userFrom:localStorage.getItem("title")
            });
        }
        let loggedUser={};
        if(localStorage.getItem("user")!=null){
            loggedUser=JSON.parse(localStorage.getItem("user"));
            this.setState({
              loggedUser:JSON.parse(localStorage.getItem("user"))
            });
        }

        let i=0;
       
        //select pepinieres
         //select pepinieres
         const dataPepiniere ={
            wordToSearch:"",
            idRegion:JSON.parse(localStorage.getItem("userRegion")).id,
            idCampagne:0
  
        };
        
        //console.log(data);
        //axios.post(this.state.beginningURL+'pepiniere/readperregionandcampagneid.php', dataPepiniere)
        axios.get(this.state.beginningURL+'pepiniere/readperregionandcampagneidget.php?wordToSearch='+dataPepiniere.wordToSearch+'&idRegion='+dataPepiniere.idRegion+'&idCampagne='+dataPepiniere.idCampagne)
        .then(response =>{
         
                this.setState({
                    pepinieres:response.data.body

                });
              
                let lPep=[{value:0,label:""}];
                let currentPepinieres=response.data.body;
                for(i=0;i<currentPepinieres.length;i++){
                    lPep.push({value:currentPepinieres[i].id, label:currentPepinieres[i].nomPepiniere});
                }
                this.setState({optionsPepinieres:lPep}); 
        })
        .catch(error=>{
            console.log(error);
        })
       
        const data ={
            idPepiniere:0,
            idRegion : currentIdRegion,
            wordToSearch:this.state.wordToSearch,
            userModule:loggedUser.module,
            userType:loggedUser.type,
            userProfil:loggedUser.profil
        };

      
        console.log(data);
        axios.post(this.state.beginningURL+'sortie/readbyidpepiniereandidregion.php', data)
        .then(response =>{
            //console.log(response);
            //console.log(response.data);
                this.setState({
                    historiqueSorties:response.data.body,
                    total:response.data.total
                });
        })
        .catch(error=>{
            console.log(error);
        })

       
    }
    state = {
        selectedOptionPepiniere: null
     
      };
    handleChangePepiniere = selectedOptionPepiniere => {
        this.setState({ selectedOptionPepiniere , 
            pepiniere:selectedOptionPepiniere.value,
            nomPepiniere:selectedOptionPepiniere.label
        });
        const data ={
            idPepiniere:selectedOptionPepiniere.value,
            idRegion : this.state.userRegion.id,
            wordToSearch:this.state.wordToSearch,
            userModule:this.state.loggedUser.module,
            userType:this.state.loggedUser.type,
            userProfil:this.state.loggedUser.profil
        };

      
        //console.log(data);
        axios.post(this.state.beginningURL+'sortie/readbyidpepiniereandidregion.php', data)
        .then(response =>{
            //console.log(response.data);
                this.setState({
                    historiqueSorties:response.data.body,
                    total:response.data.total
                });
        })
        .catch(error=>{
            console.log(error);
        })

      };
    

   


    // Search
    onChangeSearch(e){
        this.setState({
            wordToSearch:e.target.value
        });
        //if we erase the word written
        if(e.target.value===""){
            this.setState({
                isSubmit:false,   
            });
            //change to another function depending on the  criteria in select options
            this.componentDidMount();
        }
    }
    onSubmitSearch(e){
        e.preventDefault();
        /*const word ={
            wordToSearch:this.state.wordToSearch,
  
        };*/
        if(this.state.wordToSearch===""){
            this.setState({
                isSubmit:false,
                nbSearchResult:0   
            });
            this.componentDidMount();
        }
        else{
            this.setState({
                isSubmit:true,
                nbSearchResult:0   
            });
          
            const data ={
                idPepiniere:this.state.pepiniere,
                idRegion : this.state.userRegion.id,
                wordToSearch:this.state.wordToSearch,
                userModule:this.state.loggedUser.module,
                userType:this.state.loggedUser.type,
                userProfil:this.state.loggedUser.profil
            };
    
          
            //console.log(data);
            axios.post(this.state.beginningURL+'sortie/readbyidpepiniereandidregion.php', data)
            .then(response =>{
                //console.log(response.data);
                    this.setState({
                        historiqueSorties:response.data.body,
                        total:response.data.total,
                        nbSearchResult:response.data.body.length,
                        isSubmit:true
                    });
            })
            .catch(error=>{
                console.log(error);
               /* this.setState({
                    historiqueSorties:[],
                    total:0,
                    nbSearchResult:0,
                    isSubmit:true
                });*/
            })
        }      
    }
    displaySearchResult(){
        if(this.state.isSubmit===true){
        return <div className="searchResultDiv"><b className="searchResultStyle">{this.state.nbSearchResult} R&eacute;sultat(s) trouv&eacute;(s)</b></div> 
        } 
        else{
            return 
        }  
    }


    historiqueSortieList(){

        let i=0;
      
        return this.state.historiqueSorties.slice(this.state.page * this.state.rowsPerPage, this.state.page * this.state.rowsPerPage + this.state.rowsPerPage).map((currentHistoriqueSortie) => {
            i++;
            return <HistoriqueSortieItem numero={i}  historiqueSortie={currentHistoriqueSortie}  key={i}/>;
        });
     
    }
   
    
    

   

    render() {
        const { selectedOptionPepiniere } = this.state; 
   
        //Table
        const classes = useStyles;
        
        const handleChangePage = (event, newPage) => {
            this.setState({page:newPage})
            //setPage(newPage);
        };
        const handleChangeRowsPerPage = (event) => {
            //setRowsPerPage(+event.target.value);
            //setPage(0);
            this.setState({rowsPerPage:+event.target.value});
            this.setState({page:0});
        };
        //end table

        

        return (
            <div className="container-fluid">
                <div className="row bg-title">
                    <div className="left-title col-lg-4 col-md-4 col-sm-4 col-xs-12" >
                        <h4 className="text-left-title page-title">P&eacute;pini&egrave;res</h4>
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <h3 className="text-middle-title page-title">{this.state.userFrom}</h3>
                    </div>

                    <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                        {/* <a href="https://wrappixel.com/templates/ampleadmin/" target="_blank"
                            className="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Upgrade
                            to Pro

                        </a> */}
                        <ol className="breadcrumb">
                            
                            <li className="active"><a>Stock</a>&nbsp;&nbsp;<i className="linking fa fa-angle-right fa-fw"></i>Historique Sortie</li>
                        </ol>
                    </div>
                    {/* <!-- /.col-lg-12 --> */}
                </div>
            
                {/* <!-- content of the page--> */}
                {/* Head */}
                <div className="divAppBar" >
                    <AppBar position="static" style={appBarStyle}>
                        <Toolbar>
                            {/* grid */}
                            
                                <Grid container spacing={3}>
                                    <Grid item xs={3} style={{textAlign:'left'}}>
                                      {/* --------------Menu------------------------------- */}
                                      <MenuStock/>
                                       {/* ---------------End Menu ------------------------- */}
                                    </Grid>
                                    <Grid item xs={6} style={{textAlign:'center'}}>
                                        <h4 className="smaller-title" >Historique Sortie {this.state.nomPepiniere}</h4>
                                    </Grid>
                                    <Grid item xs={3}>
                                   
                                        <div className="div-search" >
                                        <form role="search" id="search" className="app-search hidden-sm hidden-xs m-r-10" onSubmit={this.onSubmitSearch} noValidate>
                                            <input type="text" placeholder="Rechercher..." className="form-control"
                                                value={this.state.wordToSearch}
                                                onChange={this.onChangeSearch}/> 
                                             <a  onClick={this.onSubmitSearch}>
                                                <i className="fa fa-search"></i>
                                            </a> 
                                        </form>

                                      
                                        </div>
                                    
                                    </Grid>
                                </Grid>
                            
                            {/* end grid */}
                        
                        </Toolbar>
                    </AppBar>
                </div>
                {/* end head */}
                {/* Table */}
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                              
                        <Grid container spacing={0} style={gridSelectStyle}>
                                <Grid item xs={9} >
                                </Grid>
                                <Grid item xs={3} >
                                <Select
                                        placeholder="Pépinières"
                                        value={selectedOptionPepiniere}
                                        onChange={this.handleChangePepiniere}
                                        options={this.state.optionsPepinieres}
                                       
                                        
                                    />
                                
                                </Grid>
                                <Grid item xs={0}>
                                     
                                </Grid>
                            </Grid>

                            

                            {/* <h3 class="box-title">Basic Table</h3> */}
                            {/* <p class="text-muted">Add class <code>.table</code></p> */}
                             {/* Content of the table */}
                             {this.displaySearchResult()}
                             <Paper className={classes.root}>
                                
                                <TableContainer className={classes.container}>
                                    <Table  stickyHeader aria-label="sticky table">
                                    <TableBody>
                                            <TableRow style={tableRowTitleStyle}>
                                                <TableCell style={tableCellTitleStyle}>Id</TableCell>
                                                <TableCell style={tableCellTitleStyle}>Date</TableCell>
                                                <TableCell style={tableCellTitleStyle}>Campagne</TableCell>
                                                <TableCell style={tableCellTitleStyle}>Id Reboisement</TableCell>
                                                <TableCell style={tableCellTitleStyle}>P&eacute;pini&egrave;re</TableCell>
                                                <TableCell style={tableCellTitleStyle}>Esp&egrave;ce plants</TableCell>
                                                <TableCell style={tableCellTitleActionColumnStyle}>Nombre</TableCell>
                                                <TableCell style={tableCellTitleActionColumnStyle}>D&eacute;tails</TableCell>
                                                </TableRow>
                                                                    
                                            </TableBody>
                                    <TableBody>
                                        {this.historiqueSortieList()}       
                                    </TableBody>
                                    <TableBody>
                                        <TableRow style={tableRowTitleStyle}>
                                            <TableCell style={tableCellTitleStyle}></TableCell>
                                            <TableCell style={tableCellTitleStyle}></TableCell>
                                            <TableCell style={tableCellTitleStyle}></TableCell>
                                            <TableCell style={tableCellTitleStyle}></TableCell>
                                            <TableCell style={tableCellTitleStyle}></TableCell>
                                            <TableCell style={tableCellTitleStyle}>Total</TableCell>
                                            <TableCell style={tableCellTitleActionColumnStyle}>{this.state.total}</TableCell>
                                            <TableCell style={tableCellTitleActionColumnStyle}></TableCell>
                                        </TableRow> 
                                    </TableBody>
                                        
                                    </Table>
                                </TableContainer>
                                <TablePagination
                                    style={tableCellStyle}
                                    rowsPerPageOptions={[10, 25, 100]}
                                    component="div"
                                    count={this.state.historiqueSorties.length}
                                    rowsPerPage={this.state.rowsPerPage}
                                    page={this.state.page}
                                    onChangePage={handleChangePage}
                                    onChangeRowsPerPage={handleChangeRowsPerPage}
                                />
                            </Paper> 

                            {/* end content of the table */}
                        </div>
                    </div>
                </div>
                {/* end table */}
              
                {/* <!-- /.content of the page --> */}

            </div>
        )
    }
}
