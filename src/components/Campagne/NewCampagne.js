import React, { Component } from 'react';
import "./NewCampagne.css";
import * as Config from './../Config/Config';

import { Link } from 'react-router-dom';
import axios from 'axios';

//Search
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
//grid for the search
import Grid from '@material-ui/core/Grid';

//Table
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

//autocomplete
import TextField from '@material-ui/core/TextField';

import Typography from '@material-ui/core/Typography';



//Table
const useStyles = makeStyles({
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 600,
    },

  });



// form 
const useStylesForm = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));

// end form


//design
let appBarStyle= {borderRadius:'4px',textAlign:'right',backgroundColor:'#000e2f'};
let buttonBackStyle= {backgroundColor:'#0bca4b', color:'white', marginTop:'5px'};
let fontStyle={fontSize:'14px'};
let errorMessageStyle={marginTop:'20px',color:'red', backgroundColor: 'rgb(252, 201, 63)',borderRadius:'3px',fontSize:'14px'};


export default class NewCampagne extends Component {
    constructor(props) {
        super(props);
 
        
    
        this.state = {
            userFrom:'',
            userRegion:{},
            errorMessage:'',
            nomCampagne:'',
            dateDebut:'',
            dateFin:'',      
            beginningURL: Config.beginningURL
        };
        this.showErrorMessage=this.showErrorMessage.bind(this);
        this.onChangeNomCampagne=this.onChangeNomCampagne.bind(this);
        this.onChangeDateDebut=this.onChangeDateDebut.bind(this);
        this.onChangeDateFin=this.onChangeDateFin.bind(this);
        this.onSubmit=this.onSubmit.bind(this);
        this.annuler=this.annuler.bind(this);
      
 
      
        
      

    }
    componentDidMount(){
        //Init title for the page
        if(localStorage.getItem("userRegion")!=null){
            this.setState({
              userRegion:JSON.parse(localStorage.getItem("userRegion"))
            });
        }
        if(localStorage.getItem("title")!=null){
            this.setState({
              userFrom:localStorage.getItem("title")
            });
        }
       
        
       
      }

    

    onChangeNomCampagne(e){
        this.setState({
            nomCampagne:e.target.value
        });
    }
    onChangeDateDebut(e){
        this.setState({
            dateDebut:e.target.value
        });
    }
    onChangeDateFin(e){
        this.setState({
            dateFin:e.target.value
        });
    }
    annuler(){
        //on annule tout
        this.componentDidMount();
    }

   
      onSubmit(e){
        e.preventDefault();
     
        if((this.state.nomCampagne==='' )||(this.state.dateDebut==='')||(this.state.dateFin==='')){
            this.setState({errorMessage:'Tous les champs doivent être remplis'});
        }
        else{
                
                    const campagne ={
                        nomCampagne:this.state.nomCampagne,
                        dateDebut:this.state.dateDebut,
                        dateFin:this.state.dateFin
                       
                    };
                  
            
                    //axios.post(this.state.beginningURL+'campagne/create.php',campagne)
                    axios.get(this.state.beginningURL+'campagne/createget.php?nomCampagne='+campagne.nomCampagne+'&dateDebut='+campagne.dateDebut+'&dateFin='+campagne.dateFin)
                     .then(res => {
                      
                       
                        if(res.data.msg==="Campagne could not be created."){
                            //alert('Erreur lors de l\'insertion de la campagne.Veuillez réessayer.');
                            this.setState({errorMessage:'Erreur lors de l\'insertion de la campagne.Veuillez réessayer.'});
                        }
                        else if(res.data.msg==="errorDate"){
                            //alert('La date de début doit être avant la date de fin.');
                            this.setState({errorMessage:'La date de début doit être avant la date de fin.'});
                        }
                        else{
                            alert('Insertion de la nouvelle campagne achevée avec succès');
                            window.location="/campagnelist";
                        }
                    });    
                
        }
    }
    showErrorMessage(){
        if(this.state.errorMessage!==''){
            return(
                <Typography component="h5"  style={errorMessageStyle}>
                            {this.state.errorMessage} 
                    </Typography>
            );
        }
        else{
            return
        }
        
    }


    render() {
      
        //Table
        const classes = useStyles;

        //form 
        const classesForm = useStylesForm;

        

        return (
            <div className="container-fluid">
                <div className="row bg-title">
                    <div className="left-title col-lg-4 col-md-4 col-sm-4 col-xs-12" >
                        <h4 className="text-left-title page-title">REBOISEMENTS</h4>
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <h3 className="text-middle-title page-title">{this.state.userFrom}</h3>
                    </div>

                    <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                        {/* <a href="https://wrappixel.com/templates/ampleadmin/" target="_blank"
                            className="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Upgrade
                            to Pro

                        </a> */}
                        <ol className="breadcrumb">
                            
                            <li className="active"><a>Campagnes</a>&nbsp;&nbsp;<i className="linking fa fa-angle-right fa-fw"></i>Nouvelle</li>
                        </ol>
                    </div>
                    {/* <!-- /.col-lg-12 --> */}
                </div>
            
                {/* <!-- content of the page--> */}
                {/* Head */}
                <div className="divAppBar" >
                    <AppBar position="static" style={appBarStyle}>
                        <Toolbar>
                            {/* grid */}
                            
                                <Grid container spacing={3}>
                                    <Grid item xs={3} style={{textAlign:'left'}}>
                                    <Link to='/campagnelist'>
                                        <Button variant="contained" style={buttonBackStyle}>
                                            Revenir liste
                                        </Button>
                                    </Link>
                                    </Grid>
                                    <Grid item xs={6} style={{textAlign:'center'}}>
                                        <h4 className="smaller-title" >Nouvelle Campagne</h4>
                                    </Grid>
                                    <Grid item xs={3}>                                    
                                    </Grid>
                                </Grid>
                            
                            {/* end grid */}
                        
                        </Toolbar>
                    </AppBar>
                </div>
                {/* end head */}
                {/* Table */}
                <div className="row">
                    <div className="col-sm-12">
                        <div className="white-box formStyleCampagne" >
                        <Container component="main" maxWidth="xs" style={{backgroundColor:'white',borderRadius:'3px'}}>
                            <Paper className={classes.root}>
                            {this.showErrorMessage()}
                            <form className={classesForm.form} onSubmit={this.onSubmit}  noValidate>
                            <label className="label-style">Nom de la campagne</label>
                            
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    id="nommaladie"
                                    label="Nom de la campagne"
                                    name="nomcampagne"
                                    autoComplete="nomcampagne"
                                    autoFocus
                                    inputProps={{style: fontStyle}} 
                                    InputLabelProps={{style: fontStyle}} 
                                    value={this.state.nomCampagne} onChange={this.onChangeNomCampagne}
                                />
                             <label className="label-style">Date de d&eacute;but</label>
                           
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    type="date"
                                    id="dateDebut"
                                    name="dateDebut"
                                    autoComplete="dateDebut"
                                    inputProps={{style: fontStyle}} 
                                    InputLabelProps={{style: fontStyle}} 
                                    value={this.state.dateDebut} onChange={this.onChangeDateDebut}
                                />
                               
                            <label className="label-style">Date fin</label>
                         
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    type="date"
                                    id="dateFin"
                                    name="dateFin"
                                    autoComplete="dateFin"
                                    inputProps={{style: fontStyle}} 
                                    InputLabelProps={{style: fontStyle}} 
                                    value={this.state.dateFin} onChange={this.onChangeDateFin}
                                />
                         

                            <Grid container spacing={1} style={{marginTop:'40px'}} >
                            <Grid item xs={6} >
                              <Button
                                  type="button"
                                  fullWidth
                                  variant="contained" 
                                  style={fontStyle}  
                                  onClick={()=> {this.annuler()}}                               
                              >
                                  Annuler
                              </Button>
                            </Grid>
                            <Grid item xs={6}>
                              <Button
                                  type="submit"
                                  fullWidth
                                  variant="contained"
                                  color="primary"
                                  className={classesForm.submit}
                                  style={fontStyle}
                                  
                              >
                                  Enregistrer
                              </Button>
                            </Grid>
                        </Grid>
                            </form>
                            <br/>
                                
                            </Paper>  
                            {/* end content of the table */}
                            </Container>
                        </div>
                    </div>
                </div>
                {/* end table */}
              
                {/* <!-- /.content of the page --> */}

            </div>
        )
    }
}
