import React, { Component } from 'react';
import "./CampagneEdit.css";
import * as Config from './../Config/Config';
import * as Design from './../Config/Design';

import { Link } from 'react-router-dom';
import axios from 'axios';
import Typography from '@material-ui/core/Typography';


//Search
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
//grid for the search
import Grid from '@material-ui/core/Grid';

//Table
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

//autocomplete
import TextField from '@material-ui/core/TextField';

//Dialog
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

//Table
const useStyles = makeStyles({
    root: {
      width: '100%',
    },
    container: {
      maxHeight: 600,
    },

  });



// form 
const useStylesForm = makeStyles((theme) => ({
    paper: {
      marginTop: theme.spacing(8),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));

// end form


//design
let appBarStyle= {borderRadius:'4px',textAlign:'right',backgroundColor:'#000e2f'};
let buttonBackStyle= {backgroundColor:'#0bca4b', color:'white', marginTop:'5px'};
let fontStyle={fontSize:'14px'};
let buttonDeleteStyle= {backgroundColor:'#e20f00', color:'white', marginTop:'5px'};
let errorMessageStyle={marginTop:'20px',color:'red', backgroundColor: 'rgb(252, 201, 63)',borderRadius:'3px',fontSize:'14px'};


export default class CampagneEdit extends Component {
    constructor(props) {
        super(props);
 
        
        //let currentCampagne= {id:3,nomCampagne: 'Campagne 2020', dateDebut:'2020-02-05', dateFin:'2021-02-04'};
        this.state = {
            userFrom:'',
            userRegion:{},
            errorMessage:'',
            idCampagne:this.props.match.params.id,
            openSupprimer:false,
            campagne:{},
            nomCampagne:'',
            dateDebut:'',
            dateFin:'',
            beginningURL: Config.beginningURL
        };
        this.onChangeNomCampagne=this.onChangeNomCampagne.bind(this);
        this.onChangeDateDebut=this.onChangeDateDebut.bind(this);
        this.onChangeDateFin=this.onChangeDateFin.bind(this);
        this.onSubmit=this.onSubmit.bind(this);
        this.deleteCampagne=this.deleteCampagne.bind(this);
        this.showButtonDelete=this.showButtonDelete.bind(this);
        this.showErrorMessage=this.showErrorMessage.bind(this);
        this.annuler=this.annuler.bind(this);
 
      
        
      

    }
    componentDidMount(){
       this.setState({
           nomCampagne:this.state.campagne.nomCampagne,
           dateDebut:this.state.campagne.dateDebut,
           dateFin:this.state.campagne.dateFin
       });

       //Init title for the page
       if(localStorage.getItem("userRegion")!=null){
            this.setState({
            userRegion:JSON.parse(localStorage.getItem("userRegion"))
            });
        }
        if(localStorage.getItem("title")!=null){
            this.setState({
            userFrom:localStorage.getItem("title")
            });
        }

        axios.get(this.state.beginningURL+'campagne/single_read.php/?id='+this.props.match.params.id)
        .then(response=>{
            this.setState({
                campagne:response.data,
                nomCampagne:response.data.nomCampagne,
                dateDebut:response.data.dateDebut.substring(0,10),
                dateFin:response.data.dateFin.substring(0,10)
               
               
            })
        })
        .catch(function(error){
            console.log(error);
        });
        
       
    }

    //Dialog
    handleClickOpenSupprimer  = () => {
        this.setState({openSupprimer:true});
      };
  
    handleCloseSupprimer  = () => {
        this.setState({openSupprimer:false});
      };

    //End dialog

    onChangeNomCampagne(e){
        this.setState({
            nomCampagne:e.target.value
        });
    }
    onChangeDateDebut(e){
        this.setState({
            dateDebut:e.target.value
        });
    }
    onChangeDateFin(e){
        this.setState({
            dateFin:e.target.value
        });
    }

   
      onSubmit(e){
        e.preventDefault();
     
        if((this.state.nomCampagne==='' )||(this.state.dateDebut==='')||(this.state.dateFin==='')){
            this.setState({errorMessage:'Tous les champs doivent être remplis'});
        }
        else{
                
                    const campagne ={
                        id:this.state.idCampagne,
                        nomCampagne:this.state.nomCampagne,
                        dateDebut:this.state.dateDebut,
                        dateFin:this.state.dateFin
                       
                    };
                  
            
                    axios.post(this.state.beginningURL+'campagne/update.php',campagne)
                     .then(res => {
                      
                       
                        if(res.data.msg==="Data could not be updated"){
                            this.setState({errorMessage:'Erreur lors de la modification de la campagne.Veuillez réessayer.'});
                        }
                        else if(res.data.msg==="errorDate"){
                            this.setState({errorMessage:'La date de début doit être avant la date de fin.'});
                        }
                        else{
                            alert('Modification de la campagne achevée avec succès');
                            window.location="/campagnelist";
                        }
                    });    
                
        }
    }
    deleteCampagne(){
        axios.delete(this.state.beginningURL+'campagne/delete.php',{data:{id:this.props.match.params.id}})
        .then(res => {
             console.log(res.data);
             alert("La campagne a été supprimée avec succès.");
             window.location="/campagnelist";
         });
    
    }
    annuler(){
        //on annule tout
        this.componentDidMount();
    }
    showButtonDelete(){
        return(
            <Button variant="contained" onClick={this.handleClickOpenSupprimer }  style={buttonDeleteStyle}>
                 <img className="imageSupprimer" alt="supprimer" src="/assets/images/logos/LogoDelete.png"/>&nbsp;
                Supprimer
            </Button>
        )
    }
    showErrorMessage(){
        if(this.state.errorMessage!==''){
            return(
                <Typography component="h5"  style={errorMessageStyle}>
                            {this.state.errorMessage} 
                    </Typography>
            );
        }
        else{
            return
        }
        
    }


    render() {
      
        //Table
        const classes = useStyles;

        //form 
        const classesForm = useStylesForm;

        

        return (
            <div className="container-fluid">
                <div className="row bg-title">
                    <div className="left-title col-lg-4 col-md-4 col-sm-4 col-xs-12" >
                        <h4 className="text-left-title page-title">REBOISEMENTS</h4>
                    </div>

                    <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <h3 className="text-middle-title page-title">{this.state.userFrom}</h3>
                    </div>

                    <div className="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                        {/* <a href="https://wrappixel.com/templates/ampleadmin/" target="_blank"
                            className="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Upgrade
                            to Pro

                        </a> */}
                        <ol className="breadcrumb">
                            
                            <li className="active"><a>Campagnes</a>&nbsp;&nbsp;<i className="linking fa fa-angle-right fa-fw"></i>Nouvelle</li>
                        </ol>
                    </div>
                    {/* <!-- /.col-lg-12 --> */}
                </div>
            
                {/* <!-- content of the page--> */}
                {/* Head */}
                <div className="divAppBar" >
                    <AppBar position="static" style={appBarStyle}>
                        <Toolbar>
                            {/* grid */}
                            
                                <Grid container spacing={3}>
                                    <Grid item xs={3} style={{textAlign:'left'}}>
                                    <Link to='/campagnelist'>
                                        <Button variant="contained" style={buttonBackStyle}>
                                            Revenir liste
                                        </Button>
                                    </Link>
                                    </Grid>
                                    <Grid item xs={6} style={{textAlign:'center'}}>
                                        <h4 className="smaller-title" >Modification Campagne N°{this.state.idCampagne}</h4>
                                    </Grid>
                                    <Grid item xs={3}> 
                                    {this.showButtonDelete()}                                   
                                    </Grid>
                                </Grid>
                            
                            {/* end grid */}
                        
                        </Toolbar>
                    </AppBar>
                </div>
                {/* end head */}
                {/* Dialog supprimer */}
                <Dialog
                                    open={this.state.openSupprimer}
                                    onClose={this.handleCloseSupprimer}
                                    aria-labelledby="alert-dialog-title"
                                    aria-describedby="alert-dialog-description"
                                    >
                                    <DialogTitle  style={Design.fontStyleDialog} id="alert-dialog-title">{"Suppression de campagne"}</DialogTitle>
                                    <DialogContent>
                                        <DialogContentText id="alert-dialog-description"  style={Design.fontStyleDialog}>
                                        Souhaitez-vous vraiment supprimer la campagne ID={this.state.idCampagne} ?
                                        </DialogContentText>
                                    </DialogContent>
                                    <DialogActions>
                                        <Button onClick={this.handleCloseSupprimer}  style={Design.fontStyleCancelDialog} color="primary">
                                        Annuler
                                        </Button>
                                        <Button  onClick={()=> {this.deleteCampagne()}} style={Design.fontStyleDeleteDialog} color="primary" autoFocus>
                                        Supprimer
                                        </Button>
                                    </DialogActions>
                                    </Dialog>
                {/* End Dialog supprimer */}

                {/* Table */}
                <div className="row">
                    <div className="col-sm-12">
                        <div className="white-box formStyleCampagne" >
                        <Container component="main" maxWidth="xs" style={{backgroundColor:'white',borderRadius:'3px'}}>
                            <Paper className={classes.root}>
                            {this.showErrorMessage()}
                            <form className={classesForm.form} onSubmit={this.onSubmit}  noValidate>
                            <label className="label-style">Nom de la campagne</label>
                            
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    id="nommaladie"
                                    label="Nom de la campagne"
                                    name="nomcampagne"
                                    autoComplete="nomcampagne"
                                    autoFocus
                                    inputProps={{style: fontStyle}} 
                                    InputLabelProps={{style: fontStyle}} 
                                    value={this.state.nomCampagne} onChange={this.onChangeNomCampagne}
                                />
                             <label className="label-style">Date de d&eacute;but</label>
                           
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    type="date"
                                    id="dateDebut"
                                    name="dateDebut"
                                    autoComplete="dateDebut"
                                    inputProps={{style: fontStyle}} 
                                    InputLabelProps={{style: fontStyle}} 
                                    value={this.state.dateDebut} onChange={this.onChangeDateDebut}
                                />
                               
                            <label className="label-style">Date fin</label>
                         
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    fullWidth
                                    type="date"
                                    id="dateFin"
                                    name="dateFin"
                                    autoComplete="dateFin"
                                    inputProps={{style: fontStyle}} 
                                    InputLabelProps={{style: fontStyle}} 
                                    value={this.state.dateFin} onChange={this.onChangeDateFin}
                                />
                         

                            <Grid container spacing={1} style={{marginTop:'40px'}} >
                            <Grid item xs={6} >
                              <Button
                                  type="button"
                                  fullWidth
                                  variant="contained" 
                                  style={fontStyle} 
                                  onClick={()=> {this.annuler()}}                                                             
                              >
                                  Annuler
                              </Button>
                            </Grid>
                            <Grid item xs={6}>
                              <Button
                                  type="submit"
                                  fullWidth
                                  variant="contained"
                                  color="primary"
                                  className={classesForm.submit}
                                  style={fontStyle}
                                  
                              >
                                  Enregistrer
                              </Button>
                            </Grid>
                        </Grid>
                            </form>
                            <br/>
                                
                            </Paper>  
                            {/* end content of the table */}
                            </Container>
                        </div>
                    </div>
                </div>
                {/* end table */}
              
                {/* <!-- /.content of the page --> */}

            </div>
        )
    }
}
