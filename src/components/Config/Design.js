//font
export let fontStyle={fontSize:'14px'};
export let fontStyle2={fontSize:'16px', fontFamily:'Arial'};

export let fontStyleDialog={fontSize:'14px'};
export let fontStyleDialogTitle={fontSize:'50px'};
export let fontStyleCancelDialog={fontSize:'14px', color:'#a1b7b5'};
export let fontStyleDeleteDialog={fontSize:'14px', color:'#ff1f0f'};
//button
export let buttonDeleteStyle= {backgroundColor:'#e20f00', color:'white', marginTop:'5px'};

//select 
export  let selectStyle={fontSize:'20px'};
export let errorMessageStyle={marginTop:'20px',color:'red', backgroundColor: 'rgb(252, 201, 63)',borderRadius:'3px',fontSize:'14px'};
//table
export let tableCellTitleStyle={fontSize:'14px',fontFamily:'Segoe UI'};
export let tableRowTitleStyle={backgroundColor:'rgb(245, 245, 245)'};
export let tableCellTitleActionColumnStyle={fontSize:'14px',fontFamily:'Segoe UI', textAlign:'center'};
export let tableCellStyle={fontSize:'13px', color:'rgb(90, 88, 88)', fontFamily:'Segoe UI' };
export let tableCellActionColumnStyle={fontSize:'13px', color:'rgb(90, 88, 88)', fontFamily:'Segoe UI', textAlign:'center' };
export let styleAddEspece={backgroundColor:'#e5e3e1'}
