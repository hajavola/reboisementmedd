
import React, { Component } from 'react';
import {  Switch, Route } from 'react-router-dom';
import Login from '../Authentification/Login';
import Template from '../Template/Template';
// import NewLocalisationModal from '../Pepiniere/Modal/NewLocalisationModal';






export default class Routes extends Component {
    render() {
   
        return (
           
            <Switch>
                    <Route path='/' component={Login} exact/>
                    <Route path='/dashboard' component={Template} exact/>
                    {/* Pépiniere */}
                    <Route path='/pepinierelist' component={Template} exact/>
                    <Route path="/pepinieredetails/:id" component={Template} exact/>
                    <Route path="/pepiniereedit/:id" component={Template} exact/>
                    <Route path='/addtocampagne/:id' component={Template} exact/>
                    <Route path='/newpepiniere' component={Template} exact/>
                    <Route path='/commandereboisement' component={Template} exact/>
                    <Route path="/commandedetails/:id" component={Template} exact/>
                    {/* Gestion de stock */}
                    <Route path='/entreestock' component={Template} exact/>
                    <Route path='/entreehistoric' component={Template} exact/>
                    <Route path='/etatdestock' component={Template} exact/>
                    <Route path='/sortiehistoric' component={Template} exact/>
                    {/* Reboisement */}
                    <Route path='/newreboisement' component={Template} exact/>
                    <Route path='/reboisementhistoric' component={Template} exact/>
                    <Route path='/reboisementtovalidate' component={Template} exact/>
                    <Route path="/reboisementdetails/:id" component={Template} exact/>
                    <Route path="/reboisementvalidation/:id" component={Template} exact/>
                    <Route path="/reboisementedit/:id" component={Template} exact/>
                    {/* Campagne */}
                    <Route path="/campagnelist" component={Template} exact/>
                    <Route path="/newcampagne" component={Template} exact/>
                    <Route path="/campagneedit/:id" component={Template} exact/>   
                    {/* Map */}
                    <Route path="/reboisementmap" component={Template} exact/>
                    {/* Statistics */}
                    <Route path='/statistictable' component={Template} exact/>
                    <Route path='/statisticchart' component={Template} exact/>          
                    {/* Client */}
                    <Route path="/clientlist" component={Template} exact/>
                    <Route path="/newClient" component={Template} exact/>
                    <Route path="/clientedit/:id" component={Template} exact/>
                    <Route path="/clientdetails/:id" component={Template} exact/>
                    <Route path="/reboisementhistoricclient/:id" component={Template} exact/>
                    {/* Employee */}
                    <Route path="/employeelist" component={Template} exact/>
                    <Route path="/employeeedit/:id" component={Template} exact/>
                    <Route path="/employeecreate" component={Template} exact/> 
                    {/* Utilisateur */}
                    <Route path="/utilisateurlist" component={Template} exact/>
                    <Route path="/utilisateuredit/:id" component={Template} exact/>
                    <Route path="/utilisateurcreate" component={Template} exact/> 
                    
            </Switch>
            
           
        
        )
    }
}
