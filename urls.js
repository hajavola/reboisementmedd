module.exports = {
    local: 'http://localhost:3000/',
    prod: 'https://reboisementmedd.herokuapp.com',
    qa:'https://reboisementmedd-qa.herokuapp.com'
}